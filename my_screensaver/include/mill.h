/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** mill
*/

#ifndef MILL_H_
	#define MILL_H_

typedef struct windmiler
{
	sfVector2f A;
	sfVector2f B;
	sfVector2f C;
	sfVector2f D;
	sfVector2f E;
} house_t;

typedef struct propellers
{
	sfVector2f F;
	sfVector2f G;
	sfVector2f H;
	sfVector2f I;
	sfVector2f J;
	sfVector2f K;
	sfVector2f L;
	sfVector2f M;
	sfVector2f N;
	double angle1;
	double angle2;
	double angle3;
	double angle4;
	double angle5;
	double angle6;
	double angle7;
	double angle8;
} prop_t;

#endif
