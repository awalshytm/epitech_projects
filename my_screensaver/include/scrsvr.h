/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** header
*/

#ifndef SCRSVR_H_
	#define SCRSVR_H_

#include <SFML/Graphics.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct windmiler house_t;
typedef struct propellers prop_t;

typedef struct framebuffer
{
	unsigned int width;
	unsigned int height;
	sfUint8 *pixels;
} framebuffer_t;

typedef struct graph
{
	sfTexture *texture;
	sfSprite *sprite;
	sfRenderWindow *window;
	framebuffer_t *buffer;
	sfClock *clock;
	sfTime time;
	double sec;
} graph_t;

/*fb tools*/
framebuffer_t *framebuffer_create(unsigned int, unsigned int);
void framebuffer_trail(framebuffer_t *buffer);
void framebuffer_clear(framebuffer_t *buffer);

/*tools*/
graph_t *init(void);
void destroy(graph_t *graph);
void update(graph_t *graph);
sfColor rand_color(void);

/*draw*/
void my_put_pixel(framebuffer_t *buffer, int x, int y, sfColor color);
void draw_circle_ext(framebuffer_t *buffer, sfVector2i C, sfVector2i P, sfColor color, int r);
void draw_circle(framebuffer_t *buffer, sfVector2i C, int r, sfColor color);
void draw_square(framebuffer_t *buffer, sfVector2i C, int r, sfColor color);
void draw_line(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);
void draw_line(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);

/*line_tools*/
void case1(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);
void case2(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);
void case3(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);
void case4(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);
void case5(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color);

/*animation*/
void rand_crcl_dp(graph_t *, int *);
int change_animation(graph_t *graph, int *nbr_anim, sfEvent event);
int check_event(graph_t *graph, int *nbr_anim);
void rand_line(graph_t *graph, int *nbr_anim);
void change_dir(int *a, int *b, sfVector2i C);
void circleTour(graph_t *graph, int *nbr_anim);
void rand_rect(graph_t *, int *);
void mill(graph_t *graph, int *nbr_anim);
void rcos(graph_t *graph, int *nbr_anim);
void r_pt(graph_t *graph, int *nbr_anim);

/*rand_rect*/
void fade_to_blue_ext(framebuffer_t *buffer, int y, int x);
void fade_to_blue(framebuffer_t *buffer);

/*manage*/
void manage_scrsvr(graph_t *graph, int *nbr_anim, void (**ptr)(graph_t *, int *));
int man(char **argv);

/*mill*/
void recalculate_angle(prop_t *prop, double i);
void recalculate_pt(prop_t *prop);
house_t init_house(void);
void draw_prop(framebuffer_t *buffer, prop_t prop, sfColor color);
void draw_house(framebuffer_t *buffer, house_t house, sfColor color);

/*my*/
void my_putstr(char *str);
int my_getnbr(char *str);

#endif
