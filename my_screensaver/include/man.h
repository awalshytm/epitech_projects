/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** man
*/

#ifndef MAN_H_
	#define MAN_H_

#define MAN1 "animation rendering in a CSFML window.\n\nUSAGE\n ./my_screensave"
#define MAN2 "r [OPTIONS] animation_id\n  animation_id    ID of the animation t"
#define MAN3 "o process (between 1 and 20).\n\nOPTIONS\n -d\t\t  print the desc"
#define MAN4 "ription of all the animations and quit.\n -h\t\t  print the usage"
#define MAN5 " and quit.\n\nUSER INTERACTIONS\n LEFT_ARROW\t  switch to the pre"
#define MAN6 "vious animation.\n RIGHT_ARROW\t  switch to the next animation.\n"
#define MAN7 " UP_ARROW\t  accelerate the speed of the animations.\n DOWN_ARROW"
#define MAN8 "\t  slow down the animations.\n q\t\t  quit.\n c\t\t  clear.\n"

#define D1 "0:  Random circles all over the screen\n"
#define D2 "1:  Random rays\n2:  Wind mill\n3:  Bouncing ball\n"
#define D3 "4:  Opening rectangle5:  Random plain circle\n6:  couloured dots\n"

#endif
