/*
** EPITECH PROJECT, 2017
** printf
** File description:
** tools
*/

void my_putchar(char c);
void my_putstr(char *str);
void my_putnbr_in_base(long nbr, char *base);

void ft_adress(char *ft)
{
        if (ft[1] == 'p')
                my_putstr("0x");
}

void print_zero(char c)
{
	if (c - 48 < 16)
		my_putchar('0');
	if (c - 48 < 8)
		my_putchar('0');
}

void printf_nptb(char *str)
{
        int i = 0;

	if (str == '\0') {
		write(1, "(null)", 6);
		return;
	}
	while (str[i] != '\0') {
                if (str[i] < 127 && str[i] > 32)
                        my_putchar(str[i]);
                else {
                      	my_putchar(92);
			print_zero(str[i]);
			my_putnbr_in_base(str[i], "01234567");
                }
                i++;
        }
}
