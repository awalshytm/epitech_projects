/*
** EPITECH PROJECT, 2017
** my_printf
** File description:
** main ft
*/

#include <stdarg.h>

void my_putchar(char c);
void my_put_nbr(int nb);
void my_putstr(char *str);
void my_putnbr_in_base(long nbr, char *base);
void ft_adress(char *ft);
void printf_per(void);
void printf_nptb(char *str);

int double_percent(char *str, int *i)
{
	if (my_strncmp("%%", str + *i, 2) == 0) {
		write(1, "%", 1);
		if (str[*i + 2] == '%')
			i[0]++;
		return (1);
	}
	return (0);
}

int base_ft(va_list arg, char *str, int *i)
{
	char lettre[] = "ouxXbp";
	char ft[3] = "%a\0";
	int check = 0;
	char *base[] = {"01234567", "0123456789", "0123456789abcdef",
			"0123456789ABCDEF", "01", "0123456789abcdef"};

	for (int j = 0; lettre[j]; j++) {
		ft[1] = lettre[j];
		if (my_strncmp(ft, str + *i, 2) == 0) {
			ft_adress(ft);
			my_putnbr_in_base(va_arg(arg, long), base[j]);
			i[0]++;
			check++;
		}
	}
	if (double_percent(str, i) == 1) {
		check++;
		i[0]++;
	}
	return (check == 0 ? 0 : 1);
}

int basic_ft(va_list arg, char *str, int *i)
{
	int check = 0;
	char *lettre = "sdicS";
	void (*ftptr[5])(void *) = {&my_putstr, &my_put_nbr, &my_put_nbr,
				    &my_putchar, &printf_nptb};
	char ft[3] = "%a\0";

	for (int j = 0; lettre[j] != '\0'; j++) {
		ft[1] = lettre[j];
		if (my_strncmp(ft, str + *i, 2) == 0) {
			ftptr[j](va_arg(arg, void *));
			i[0]++;
			check++;
		}
		if (base_ft(arg, str, i) != 0)
			check++;
	}
	return (check == 0 ? 0 : 1);
}

void my_printf(char const *str, ...)
{
	va_list arg;
	int i = 0;

	va_start(arg, str);
	while (str[i]) {
		if (basic_ft(arg, str, &i) != 1)
			my_putchar(str[i]);
		i++;
	}
	va_end(arg);
}
