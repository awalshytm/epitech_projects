/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** output string
*/

void my_putchar(char c);

void my_putstr(char *str)
{
	int a = 0;

	if (str == '\0') {
		write(1, "(null)", 6);
		return;
	}	
	while (str[a] != '\0') {
		my_putchar(str[a]);
		a++;
	}
}
