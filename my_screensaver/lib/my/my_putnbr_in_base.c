/*
** EPITECH PROJECT, 2017
** printf
** File description:
** putnbr_in_base long
*/

void my_putnbr_in_base(long nbr, char *base)
{
 	int divisor = ft_strlen(base);

	if (nbr > 0) {
		my_putnbr_in_base(nbr / divisor, base);
		my_putchar(base[nbr % divisor]);
	}
}
