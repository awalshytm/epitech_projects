/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** animation change
*/

#include "scrsvr.h"

int change_animation(graph_t *graph, int *nbr_anim, sfEvent event)
{
	if (event.key.code == sfKeyLeft) {
		*nbr_anim -= 1;
		if (*nbr_anim == -1)
			*nbr_anim = 6;
		return (1);
	}
	else if (event.key.code == sfKeyRight) {
		*nbr_anim += 1;
		if (*nbr_anim == 7)
			*nbr_anim = 0;
		return (1);
	}
	else if (event.key.code == sfKeyC)
		framebuffer_clear(graph->buffer);
	else if (event.key.code == sfKeyUp)
		graph->sec -= 0.01;
	else if (event.key.code == sfKeyDown)
		graph->sec += 0.01;
	return (0);
}

int check_event(graph_t *graph, int *nbr_anim)
{
	sfEvent event;

	while (sfRenderWindow_pollEvent(graph->window, &event)) {
		if (event.type == sfEvtClosed || (event.type == sfEvtKeyPressed
						&& event.key.code == sfKeyQ)) {
			sfRenderWindow_close(graph->window);
			return (1);
		}
		if (event.type == sfEvtKeyPressed) {
			return (change_animation(graph, nbr_anim, event));
		}
	}
	return (0);
}
