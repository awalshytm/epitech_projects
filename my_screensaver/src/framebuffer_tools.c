/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** framebuffer_tools
*/

#include "scrsvr.h"

framebuffer_t *framebuffer_create(unsigned int width, unsigned int height)
{
	framebuffer_t *buffer = malloc(sizeof(*buffer));

	buffer->width = width;
	buffer->height = height;
	buffer->pixels = malloc(sizeof(sfUint8) * 4 * width * height * 32);
}

void fb_trail_ext(framebuffer_t *buffer, int y, int x)
{
	if (buffer->pixels[((buffer->width * y + x) * 4 + 3)] > 2) {
		buffer->pixels[((buffer->width * y + x) * 4 + 3)] -= 2;
	}
}

void framebuffer_trail(framebuffer_t *buffer)
{
	int i = 0;
	int j = 0;

	while (i <= buffer->height) {
		j = 0;
		while (j <= buffer->width) {
			fb_trail_ext(buffer, i, j);
			j++;
		}
		i++;
	}
}

void framebuffer_clear(framebuffer_t *buffer)
{
	int i = 0;
	int j = 0;

	while (i < buffer->height) {
		j = 0;
		while (j < buffer->width) {
			my_put_pixel(buffer, j, i, sfBlack);
			j++;
		}
		i++;
	}
}
