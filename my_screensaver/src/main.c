/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** main
*/

#include "scrsvr.h"

void manage_scrsvr(graph_t *graph, int *nbr_anim,
		void (**ptr)(graph_t *, int *))
{
	if (check_event(graph, nbr_anim) == 1)
		return;
	if (*nbr_anim < 7)
		(*ptr[*nbr_anim])(graph, nbr_anim);
}

int anim_check(int nb)
{
	if (nb < 0 || nb > 6)
		return (0);
	return (1);
}

int main(int argc, char **argv)
{
	graph_t *graph;
	int nbr_anim = 0;
	void (*ptr[7])(graph_t *, int *) = {&rand_crcl_dp, &rand_line, &mill,
					&circleTour, &rand_rect, &rcos, &r_pt};

	if (argc == 2) {
		graph = init();
		nbr_anim = my_getnbr(argv[1]);
		if (man(argv) == 1)
			return (0);
		if (anim_check(nbr_anim) == 0)
			return (84);
		while (sfRenderWindow_isOpen(graph->window) &&
		       nbr_anim >= 0) {
			manage_scrsvr(graph, &nbr_anim, ptr);
		}
		destroy(graph);
		return (0);
	}
	return (84);
}
