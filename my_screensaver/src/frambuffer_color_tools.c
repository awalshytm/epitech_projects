/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** frambuffer_color_tools
*/

#include "scrsvr.h"

void fade_to_blue_ext(framebuffer_t *buffer, int y, int x)
{
	if (buffer->pixels[((buffer->width) * y + x) * 4] > 2)
		buffer->pixels[((buffer->width) * y + x) * 4] -= 2;
	if (buffer->pixels[((buffer->width) * y + x) * 4 + 1] > 2)
		buffer->pixels[((buffer->width) * y + x) * 4 + 1] -= 2;
}

void fade_to_blue(framebuffer_t *buffer)
{
	int i = 0;
	int j = 0;

	while (i <= buffer->height) {
		j = 0;
		while (j <= buffer->width) {
			fade_to_blue_ext(buffer, i, j);
			j++;
		}
		i++;
	}
}
