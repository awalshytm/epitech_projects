/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** draw
*/

#include "scrsvr.h"

void my_put_pixel(framebuffer_t *buffer, int x, int y, sfColor color)
{
	buffer->pixels[(buffer->width * y + x) * 4] = color.r;
	buffer->pixels[(buffer->width * y + x) * 4 + 1] = color.g;
	buffer->pixels[(buffer->width * y + x) * 4 + 2] = color.b;
	buffer->pixels[(buffer->width * y + x) * 4 + 3] = color.a;
}

void draw_square(framebuffer_t *buffer, sfVector2i A, int r, sfColor color)
{
	int i = A.y;
	int j = A.x;

	while (i < A.y + r) {
		j = A.x;
		while (j < A.x + r) {
			my_put_pixel(buffer, j, i, color);
			j++;
		}
		i++;
	}
}

void draw_circle_ext(framebuffer_t *buffer, sfVector2i C, sfVector2i P,
		sfColor color, int r)
{
	if ((P.x - C.x) * (P.x - C.x) + (P.y - C.y) * (P.y - C.y) <= (r * r))
		my_put_pixel(buffer, P.x, P.y, color);
}

void draw_circle(framebuffer_t *buffer, sfVector2i C, int r, sfColor color)
{
	sfVector2i P = {0, C.y - r};

	while (P.y < C.y + r) {
		P.x = C.x - r;
		while (P.x < C.x + r) {
			draw_circle_ext(buffer, C, P, color, r);
			P.x++;
		}
		P.y++;
	}
}

void draw_line(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	sfVector2f P;
	sfVector2f save = A;
	float a;

	if (A.x > B.x) {
		A = B;
		B = save;
	}
	P = A;
	a = ((A.y - B.y) / (A.x - B.x));
	if (P.y == B.y)
		case1(buffer, A, B, color);
	else if (P.x == B.x)
		case5(buffer, A, B, color);
	else if (a < 1 && a > -1)
		case2(buffer, A, B, color);
	else if (a == 1 || a == -1)
		case3(buffer, A, B, color);
	else
		case4(buffer, A, B, color);
}
