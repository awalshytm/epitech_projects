/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** moulin
*/

#include "scrsvr.h"
#include "mill.h"

void draw_house(framebuffer_t *buffer, house_t house, sfColor color)
{
	sfVector2i circle = {400, 360};

	draw_line(buffer, house.A, house.B, color);
	draw_line(buffer, house.B, house.C, color);
	draw_line(buffer, house.C, house.D, color);
	draw_line(buffer, house.D, house.E, color);
	draw_line(buffer, house.E, house.A, color);
	draw_circle(buffer, circle, 10, color);
}

void draw_prop(framebuffer_t *buffer, prop_t prop, sfColor color)
{
	prop.F.x = 400;
	prop.F.y = 360;
	draw_line(buffer, prop.F, prop.G, color);
	draw_line(buffer, prop.G, prop.H, color);
	draw_line(buffer, prop.H, prop.F, color);
	draw_line(buffer, prop.F, prop.I, color);
	draw_line(buffer, prop.I, prop.J, color);
	draw_line(buffer, prop.J, prop.F, color);
	draw_line(buffer, prop.F, prop.K, color);
	draw_line(buffer, prop.K, prop.L, color);
	draw_line(buffer, prop.L, prop.F, color);
	draw_line(buffer, prop.F, prop.M, color);
	draw_line(buffer, prop.M, prop.N, color);
	draw_line(buffer, prop.N, prop.F, color);
}

house_t init_house(void)
{
	house_t house;

	house.A.x = 400;
	house.A.y = 290;
	house.B.x = 500;
	house.B.y = 440;
	house.C.x = 500;
	house.C.y = 590;
	house.D.x = 300;
	house.D.y = 590;
	house.E.x = 300;
	house.E.y = 440;
	return (house);
}

void recalculate_pt(prop_t *prop)
{
	sfVector2i circle = {400, 360};

	prop->G.x = round(circle.x + 210 * cos(prop->angle1));
	prop->G.y = round(circle.y + 210 * sin(prop->angle1));
	prop->H.x = round(circle.x + 190 * cos(prop->angle2));
	prop->H.y = round(circle.y + 190 * sin(prop->angle2));
	prop->I.x = round(circle.x + 210 * cos(prop->angle3));
	prop->I.y = round(circle.y + 210 * sin(prop->angle3));
	prop->J.x = round(circle.x + 190 * cos(prop->angle4));
	prop->J.y = round(circle.y + 190 * sin(prop->angle4));
	prop->K.x = round(circle.x + 210 * cos(prop->angle5));
	prop->K.y = round(circle.y + 210 * sin(prop->angle5));
	prop->L.x = round(circle.x + 190 * cos(prop->angle6));
	prop->L.y = round(circle.y + 190 * sin(prop->angle6));
	prop->M.x = round(circle.x + 210 * cos(prop->angle7));
	prop->M.y = round(circle.y + 210 * sin(prop->angle7));
	prop->N.x = round(circle.x + 190 * cos(prop->angle8));
	prop->N.y = round(circle.y + 190 * sin(prop->angle8));
}

void recalculate_angle(prop_t *prop, double i)
{
	double pi = 3.1415926535897932384;

	prop->angle1 = i * pi / 180;
	prop->angle2 = (i + 10) * pi / 180;
	prop->angle3 = (i + 90) * pi / 180;
	prop->angle4 = (i + 100) * pi / 180;
	prop->angle5 = (i + 180) * pi / 180;
	prop->angle6 = (i + 190) * pi / 180;
	prop->angle7 = (i + 270) * pi / 180;
	prop->angle8 = (i + 280) * pi / 180;
}
