/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** animations_bis
*/

#include "scrsvr.h"

void rcos_draw_circle(framebuffer_t *buffer, sfVector2f C, sfColor color)
{
	double pi = 3.1415926535897932384;
	sfVector2f D;
	double angle;
	
	framebuffer_trail(buffer);
	for (double i = 0.0; i < 360.0; i += 0.1) {
		angle = i * pi / 180;
		D.x = round(C.x + 100 * cos(angle));
		D.y = round(C.y + 100 * sin(angle));
		my_put_pixel(buffer, D.x, D.y, color);
	}
}

void rcos(graph_t *graph, int *nbr_anim)
{
	sfColor color = rand_color();
	sfVector2f C;

	while (sfRenderWindow_isOpen(graph->window)) {
		C.x = rand() % 600 + 100;
		C.y = rand() % 400 + 100;
		if (check_event(graph, nbr_anim) == 1)
			return;
		graph->time = sfClock_getElapsedTime(graph->clock);
		if (graph->time.microseconds > graph->sec * 1000000) {
			rcos_draw_circle(graph->buffer, C, color);
			sfClock_restart(graph->clock);
		}
		update(graph);
	}
}

void r_pt(graph_t *graph, int *nbr_anim)
{
	sfVector2i A;
	sfVector2i B;

	srand(time(NULL));
	framebuffer_clear(graph->buffer);
	while (sfRenderWindow_isOpen(graph->window)) {
		if (check_event(graph, nbr_anim) == 1)
			return;
		graph->time = sfClock_getElapsedTime(graph->clock);
		if (graph->time.microseconds > graph->sec * 1000000) {
			A.x = rand() % 790 + 5;
			A.y = rand() % 590 + 5;
			B.x = rand() % 790 + 5;
			B.y = rand() % 590 + 5;
			my_put_pixel(graph->buffer, A.x, A.y, rand_color());
			my_put_pixel(graph->buffer, B.x, B.y, rand_color());
			update(graph);
		}
		update(graph);
	}
}
