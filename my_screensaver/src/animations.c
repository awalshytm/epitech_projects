/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** animations
*/

#include "scrsvr.h"
#include "mill.h"

void rand_crcl_dp(graph_t *graph, int *nbr_anim)
{
	sfVector2i centre;
	sfColor color = rand_color();
	int size = 0;

	srand(time(NULL));
	while (sfRenderWindow_isOpen(graph->window)) {
		if (check_event(graph, nbr_anim) == 1)
			return;
		graph->time = sfClock_getElapsedTime(graph->clock);
		if (graph->time.microseconds > graph->sec * 1000000) {
			centre.x = rand() % 700 + 50;
			centre.y = rand() % 500 + 50;
			size = rand() % 30;
			framebuffer_trail(graph->buffer);
			draw_circle(graph->buffer, centre, size, color);
			update(graph);
			sfClock_restart(graph->clock);
		}
	}
}

void rand_line(graph_t *graph, int *nbr_anim)
{
	sfVector2f A = {400, 300};
	sfVector2f B;
	sfVector2i C = {400, 300};
	sfColor color = rand_color();

	srand(time(NULL));
	framebuffer_clear(graph->buffer);
	while (sfRenderWindow_isOpen(graph->window)) {
		if (check_event(graph, nbr_anim) == 1)
			return;
		graph->time = sfClock_getElapsedTime(graph->clock);
		if (graph->time.microseconds > graph->sec * 1000000) {
			B.x = rand() % 700 + 50;
			B.y = rand() % 500 + 50;
			framebuffer_trail(graph->buffer);
			draw_line(graph->buffer, A, B, color);
			update(graph);
			sfClock_restart(graph->clock);
		}
	}
}

void change_dir(int *a, int *b, sfVector2i C)
{
	if (C.x <= 15)
		*a *= -1;
	if (C.x >= 790)
		*a *= -1;
	if (C.y <= 15)
		*b *= -1;
	if (C.y >= 590)
		*b *= -1;
}

void circleTour(graph_t *graph, int *nbr_anim)
{
	sfVector2i C = {40, 40};
	int a = 3;
        int b = 3;

	while (sfRenderWindow_isOpen(graph->window)) {
		if (check_event(graph, nbr_anim) == 1)
			return;
		framebuffer_trail(graph->buffer);
		fade_to_blue(graph->buffer);
		C.x += a;
		C.y += b;
		change_dir(&a, &b, C);
		draw_circle(graph->buffer, C, 10, sfWhite);
		update(graph);
	}
}

void mill(graph_t *graph, int *nbr_anim)
{
	sfVector2i circle = {400, 360};
	house_t house = init_house();
	prop_t prop;
	sfColor color = rand_color();

	for (double i = 0.0; i < 360; i += 0.1) {
		if (check_event(graph, nbr_anim) == 1)
			return;
		recalculate_angle(&prop, i);
		recalculate_pt(&prop);
		framebuffer_trail(graph->buffer);
		draw_house(graph->buffer, house, color);
		draw_prop(graph->buffer, prop, color);
		update(graph);
	}
}
