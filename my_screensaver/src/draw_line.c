/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** line
*/

#include "scrsvr.h"

void case1(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	double a = ((A.y - B.y) / (A.x - B.x));
	sfVector2f P = A;

	while (P.x <= B.x) {
		my_put_pixel(buffer, P.x, P.y, color);
		P.x++;
	}
}

void case2(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	double a = ((A.y - B.y) / (A.x - B.x));
	double b = A.y - (a * A.x);
	int i = 1;
	sfVector2f P = A;

	while (P.x < B.x) {
		P.x++;
		P.y = a * P.x + b;
		my_put_pixel(buffer, P.x, P.y, color);
	}
}

void case3(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	double a = ((A.y - B.y) / (A.x - B.x));
	sfVector2f P = A;
	int i = 1;

	if (a < 0)
		i *= -1;
	while (P.x < B.x) {
		my_put_pixel(buffer, P.x, P.y, color);
		P.x++;
		P.y += i;
	}
}

void case4(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	double a = ((A.y - B.y) / (A.x - B.x));
	double b = A.y - (a * A.x);
	sfVector2f P = A;
	int i = 1;

	if (a < 0)
		i *= -1;
	while (P.y != B.y) {
		my_put_pixel(buffer, P.x, P.y, color);
		P.y += i;
		P.x = (P.y - b) / a;
	}
}

void case5(framebuffer_t *buffer, sfVector2f A, sfVector2f B, sfColor color)
{
	sfVector2f P = A;

	while (P.y > B.y) {
		P.y--;
		my_put_pixel(buffer, P.x, P.y, color);
	}
	while (P.y < B.y) {
		P.y++;
		my_put_pixel(buffer, P.x, P.y, color);
	}
}
