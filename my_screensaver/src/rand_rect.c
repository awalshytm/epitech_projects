/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** rand_rect
*/

#include "scrsvr.h"

void zoom_in(sfVector2f *A, sfVector2f *B, sfVector2f *C, sfVector2f *D)
{
	A->x += 2;
	A->y += 2;
	B->x -= 2;
	B->y += 2;
	C->x -= 2;
	C->y -= 2;
	D->x += 2;
	D->y -= 2;
}

void zoom_out(sfVector2f *A, sfVector2f *B, sfVector2f *C, sfVector2f *D)
{
	A->x -= 1;
	A->y -= 1;
	B->x += 1;
	B->y -= 1;
	C->x += 1;
	C->y += 1;
	D->x -= 1;
	D->y += 1;
}

void rand_rect(graph_t *graph, int *nbr_anim)
{
	sfVector2f A = {10, 10};
	sfVector2f B = {790, 10};
	sfVector2f C = {790, 550};
	sfVector2f D = {10, 550};
	sfColor color = rand_color();

	while (A.y < D.y) {
		framebuffer_trail(graph->buffer);
		zoom_in(&A, &B, &C, &D);
		draw_line(graph->buffer, A, B, color);
		draw_line(graph->buffer, B, C, color);
		draw_line(graph->buffer, C, D, color);
		draw_line(graph->buffer, D, A, color);
		update(graph);
	}
	while (A.y > 10) {
		framebuffer_trail(graph->buffer);
		zoom_out(&A, &B, &C, &D);
		draw_line(graph->buffer, A, B, color);
		draw_line(graph->buffer, B, C, color);
		draw_line(graph->buffer, C, D, color);
		draw_line(graph->buffer, D, A, color);
		update(graph);
	}
}
