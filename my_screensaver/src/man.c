/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** manual
*/

#include "scrsvr.h"
#include "man.h"

int man(char **argv)
{
	if (argv[1][0] == '-' && argv[1][1] == 'h') {
		my_putstr(MAN1);
		my_putstr(MAN2);
		my_putstr(MAN3);
		my_putstr(MAN4);
		my_putstr(MAN5);
		my_putstr(MAN6);
		my_putstr(MAN7);
		my_putstr(MAN8);
		return (1);
	}
	else if (argv[1][0] == '-' && argv[1][1] == 'd') {
		my_putstr(D1);
		my_putstr(D2);
		my_putstr(D3);
		return (1);
	}
	return (0);
}
