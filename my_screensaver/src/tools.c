/*
** EPITECH PROJECT, 2017
** my_screensaver
** File description:
** tools.c
*/

#include "scrsvr.h"

graph_t *init(void)
{
	graph_t *graph = malloc(sizeof(*graph));
	sfVideoMode mode = {800, 600, 32};

	graph->texture = sfTexture_create(800, 600);
	graph->sprite = sfSprite_create();
	sfSprite_setTexture(graph->sprite, graph->texture, sfTrue);
	graph->window = sfRenderWindow_create(mode, "My_screensaver",
					sfResize | sfClose, NULL);
	graph->buffer = framebuffer_create(800, 600);
	graph->clock = sfClock_create();
	graph->sec = 0;
	return (graph);
}

void destroy(graph_t *graph)
{
	sfSprite_destroy(graph->sprite);
	sfTexture_destroy(graph->texture);
	sfRenderWindow_destroy(graph->window);
	free(graph->buffer->pixels);
	free(graph->buffer);
	free(graph);
}

void update(graph_t *graph)
{
	sfRenderWindow_clear(graph->window, sfBlack);
	sfTexture_updateFromPixels(graph->texture, graph->buffer->pixels,
				800, 600, 0, 0);
	sfRenderWindow_drawSprite(graph->window, graph->sprite, NULL);
	sfRenderWindow_display(graph->window);
}

sfColor rand_color(void)
{
	int r = rand() % 255;
	int g = rand() % 255;
	int b = rand() % 255;
	sfColor color = {r, g, b, 255};

	return (color);
}
