/*
** EPITECH PROJECT, 2018
** anthill
** File description:
** header
*/

#ifndef ANTHILL_H_
#define ANTHILL_H_

#define COMMENT_CHAR '#'
#define COMMAND_STR "##"

typedef enum room_type {
	NORMAL,
	ENTRY,
	EXIT
} room_type_t;

typedef struct room {
	char *name;
	int pos[2];
	int ants;
	room_type_t type;
	int nb_next;
	struct room **next;
} room_t;

typedef struct anthill {
	int ants;
	room_t **rooms;
	room_t *entry;
	room_t *exit;
	char ***moves;
} anthill_t;

char ***add_to_network(char ***, char **);
room_t **copy_rooms_and_add_new_room(room_t **, room_t *);
void connect_rooms(room_t *, room_t *);

int create_anthill(anthill_t *);

int check_and_add_room(anthill_t *, char **, int, char *);
int check_and_add_connection(anthill_t *, char **, char *);

int nb_rooms(room_t **);
int is_command(char const *);
int check_special_room(room_type_t *, int);
room_t *create_room(char **, int, int);
char *get_next_info(void);

#endif
