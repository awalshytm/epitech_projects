/*
** EPITECH PROJECT, 2018
** lem_in
** File description:
** ants
*/

#ifndef ANTS_H_
#define ANTS_H_

#include "algorithm.h"
#include "graph.h"

typedef struct ant {
	path_t *path;
	int id;
} ant_t;

int move(ant_t **, int);
int create_ant(ant_t **, int, path_t *);
void display_move(ant_t **, int);
void destroy_ant(ant_t **, int);
int move_ants(anthill_t *);

#endif
