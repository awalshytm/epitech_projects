/*
** EPITECH PROJECT, 2018
** display
** File description:
** header
*/

#ifndef DISPLAY_H_
#define DISPLAY_H_

void display_moves(anthill_t *anthill);
void display_ants(int nb);

#endif
