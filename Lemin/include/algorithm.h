/*
** EPITECH PROJECT, 2018
** algorithm
** File description:
** header
*/

#ifndef ALGORITHM_H_
#define ALGORITHM_H_

#include <stdlib.h>
#include "graph.h"

typedef struct element {
	room_t *room;
	int cost;
	int score;
	room_t *prev;
	struct element *next;
} element_t;

typedef struct list {
	element_t *first;
	element_t *last;
	int size;
} list_t;

typedef struct path {
	room_t *room;
	struct path *next;
} path_t;

int remove_first(list_t *list);
void add_with_priority(list_t *list, element_t *element);
int add_to_list(list_t *list, element_t *element, int priority);
int add_to_path(path_t **path, element_t *element);

int init_open_list(list_t *list, anthill_t *anthill);
path_t *make_path(list_t *closed_list, list_t *open_list, element_t *final);
path_t *get_shortest_path(anthill_t *anthill);
path_t *make_move(anthill_t *anthill);

element_t *element_dup(element_t *element);
int is_in_list(list_t *list, element_t *current);
int compute_square_distance(room_t *room, room_t *exit);

void destroy_list(list_t *list);
void destroy_path(path_t *path);

#endif
