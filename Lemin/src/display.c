/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** final display
*/

#include <stdlib.h>
#include "my_strings.h"
#include "graph.h"

void display_ants(int nb)
{
	my_putstr("#number_of_ants\n");
	my_printf("%d\n", nb);
	my_putstr("#rooms\n");
}
