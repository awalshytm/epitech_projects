/*
** EPITECH PROJECT, 2018
** lem-in
** File description:
** main file
*/

#include <stdlib.h>
#include "my_strings.h"
#include "graph.h"
#include "algorithm.h"
#include "destroy.h"
#include "ant.h"

static int usage(char *prog_name)
{
	my_putstr("USAGE\n\t");
	my_putstr(prog_name);
	my_putstr(" file\n\n");
	my_putstr("DESCRIPTION\n\tfile\tfile with anthill informations\n");
	return (0);
}

int main(int ac, char **av)
{
	anthill_t anthill = {0, NULL, NULL, NULL, NULL};
	int ret = 0;

	if (ac == 2 && my_strcmp(av[1], "-h") == 0)
		return (usage(av[0]));
	if (create_anthill(&anthill) == 1 ||
	anthill.entry == NULL || anthill.exit == NULL)
		return (84);
	ret = move_ants(&anthill);
	destroy_anthill(&anthill);
	return (ret);
}
