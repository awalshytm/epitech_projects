/*
** EPITECH PROJECT, 2018
** lem-in
** File description:
** main function to compute moves
*/

#include "algorithm.h"
#include "my_strings.h"

int init_open_list(list_t *list, anthill_t *anthill)
{
	element_t *first;

	if (anthill->rooms == NULL || anthill->entry == NULL)
		return (1);
	first = malloc(sizeof(*first));
	if (first == NULL)
		return (1);
	first->room = anthill->entry;
	first->next = NULL;
	first->prev = NULL;
	first->cost = 0;
	first->score = 0;
	list->first = first;
	list->last = first;
	list->size = 1;
	return (0);
}

path_t *make_path(list_t *closed_list, list_t *open_list, element_t *final)
{
	path_t *path = malloc(sizeof(*path));
	element_t *cursor = closed_list->last;
	element_t *tmp = NULL;

	if (path == NULL)
		return (NULL);
	path->room = final->room;
	path->next = NULL;
	add_to_path(&path, cursor);
	while (cursor->room->type != ENTRY) {
		tmp = closed_list->first;
		while (tmp->room != cursor->prev)
			tmp = tmp->next;
		cursor = tmp;
		add_to_path(&path, cursor);
	}
	free(final);
	destroy_list(open_list);
	destroy_list(closed_list);
	return (path);
}

static void check_in_lists(element_t *current, list_t *open_list,
			list_t *closed_list, anthill_t *anthill)
{
	static element_t neighbor = {NULL, 0, 0, NULL, NULL};

	neighbor.prev = current->room;
	for (int i = 0 ; i < current->room->nb_next ; i++) {
		neighbor.room = current->room->next[i];
		if (is_in_list(closed_list, &neighbor) ||
		is_in_list(open_list, &neighbor))
			continue;
		else {
			neighbor.cost = current->cost + 1;
			neighbor.score = neighbor.cost +
				compute_square_distance(neighbor.room,
							anthill->exit);
			add_to_list(open_list, &neighbor, 1);
		}
	}
}

path_t *get_shortest_path(anthill_t *anthill)
{
	list_t open_list;
	list_t closed_list = {NULL, NULL, 0};
	element_t *current = NULL;

	if (init_open_list(&open_list, anthill) == 1)
		return (NULL);
	while (open_list.size != 0) {
		current = open_list.first;
		remove_first(&open_list);
		if (current->room->type == EXIT)
			return (make_path(&closed_list, &open_list, current));
		check_in_lists(current, &open_list, &closed_list, anthill);
		add_to_list(&closed_list, current, 0);
		free(current);
	}
	return (NULL);
}

path_t * make_move(anthill_t *anthill)
{
	path_t *path = get_shortest_path(anthill);

	if (path == NULL)
		return (NULL);
	return (path);
}
