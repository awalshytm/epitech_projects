/*
** EPITECH PROJECT, 2018
** lem-in
** File description:
** destroy
*/

#include "algorithm.h"

void destroy_list(list_t *list)
{
	element_t *tmp;

	if (list == NULL)
		return;
	tmp = list->first;
	while (tmp) {
		tmp = tmp->next;
		free(list->first);
		list->first = tmp;
	}
}

void destroy_path(path_t *path)
{
	path_t *tmp;

	while (path) {
		tmp = path->next;
		free(path);
		path = tmp;
	}
}
