/*
** EPITECH PROJECT, 2018
** lem-in
** File description:
** tools for a star algorithm
*/

#include "algorithm.h"
#include "my.h"

element_t *element_dup(element_t *element)
{
	element_t *new = malloc(sizeof(*new));

	if (new == NULL)
		return (NULL);
	new->room = element->room;
	new->cost = element->cost;
	new->score = element->score;
	new->prev = element->prev;
	new->next = element->next;
	return (new);
}

int is_in_list(list_t *list, element_t *current)
{
	element_t *tmp = list->first;

	while (tmp != NULL) {
		if (tmp->room == current->room)
			return (tmp->cost < current->cost ? 1 : 0);
		tmp = tmp->next;
	}
	return (0);
}

int compute_square_distance(room_t *room, room_t *exit)
{
	int a = my_pow_rec(room->pos[0] - exit->pos[0], 2);
	int b = my_pow_rec(room->pos[1] - exit->pos[1], 2);

	return (a + b);
}
