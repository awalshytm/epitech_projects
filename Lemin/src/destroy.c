/*
** EPITECH PROJECT, 2018
** anthill
** File description:
** destroy anthill
*/

#include <stdlib.h>
#include "my_strings.h"
#include "graph.h"

void destroy_anthill(anthill_t *anthill)
{
	if (anthill == NULL || anthill->rooms == NULL)
		return;
	for (int i = 0 ; anthill->rooms[i] ; i++) {
		if (anthill->rooms[i]->name != NULL)
			free(anthill->rooms[i]->name);
		if (anthill->rooms[i]->next != NULL)
			free(anthill->rooms[i]->next);
		if (anthill->rooms[i] != NULL)
			free(anthill->rooms[i]);
	}
	free(anthill->rooms);
}
