/*
** EPITECH PROJECT, 2018
** lem_in
** File description:
** lem_in
*/

#include "ant.h"
#include "my_strings.h"

int move(ant_t **ants, int nb)
{
	if (ants == NULL)
		return (1);
	for (int i = 0 ; i < nb ; i++) {
		if (ants[i] && ants[i]->path != NULL)
			ants[i]->path = ants[i]->path->next;
	}
	return (0);
}

int create_ant(ant_t **ants, int id, path_t *path)
{
	ants[id] = malloc(sizeof(ant_t));
	if (ants[id] == NULL)
		return (1);
	ants[id]->id = id + 1;
	ants[id]->path = path;
	return (0);
}

void display_move(ant_t **ants, int actual)
{
	for (int i = 0 ; i < actual ; i++) {
		if (ants[i]->path != NULL) {
			my_printf("P%d-%s%c", ants[i]->id,
				ants[i]->path->room->name,
				(i + 1 < actual ? ' ' : '\n'));
		}
	}
}

void destroy_ant(ant_t **a, int nb)
{
	for (int i = 0 ; i < nb ; i++)
		free(a[i]);
	free(a);
}

int move_ants(anthill_t *anthill)
{
	ant_t **ants = malloc(sizeof(ant_t *) * anthill->ants);
	path_t *shortest_path = make_move(anthill);

	if (ants == NULL || create_ant(ants, 0, shortest_path) == 1)
		return (84);
	my_putstr("#moves\n");
	for (int i = 1 ; i < anthill->ants ; i++) {
		if (create_ant(ants, i, shortest_path) == 1)
			return (84);
		move(ants, i);
		display_move(ants, i);
	}
	while (ants[anthill->ants - 1]->path != NULL) {
		move(ants, anthill->ants);
		display_move(ants, anthill->ants);
	}
	destroy_ant(ants, anthill->ants);
	destroy_path(shortest_path);
	return (0);
}
