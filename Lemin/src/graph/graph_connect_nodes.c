/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** connect nodes in graph
*/

#include <stdlib.h>
#include "graph.h"

room_t **copy_rooms_and_add_new_room(room_t **rooms, room_t *new)
{
	int size = nb_rooms(rooms);
	room_t **copy = malloc(sizeof(room_t *) * (size + 2));

	if (copy == NULL)
		return (NULL);
	for (int i = 0 ; i < size ; i++)
		copy[i] = rooms[i];
	copy[size] = new;
	copy[size + 1] = NULL;
	if (rooms)
		free(rooms);
	return (copy);
}

void connect_rooms(room_t *room1, room_t *room2)
{
	room_t **tmp;

	if (room1 == NULL || room2 == NULL)
		return;
	tmp = copy_rooms_and_add_new_room(room1->next, room2);
	room1->next = tmp;
	tmp = copy_rooms_and_add_new_room(room2->next, room1);
	room2->next = tmp;
	room1->nb_next++;
	room2->nb_next++;
}
