/*
** EPITECH PROJECT, 2018
** lem-in
** File description:
** auxiliary file for creating graph
*/

#include <stdlib.h>
#include "my_strings.h"
#include "graph.h"

static int is_in_name(char const *str, char c)
{
	for (int i = 0 ; str[i] ; i++)
		if (str[i] == c)
			return (1);
	return (0);
}

static int add_new_connection(anthill_t *anthill, char **parser)
{
	int indexes[2];
	int k = 0;

	if (anthill == NULL || anthill->rooms == NULL)
		return (1);
	for (int i = 0 ; anthill->rooms[i] != NULL && k < 2 ; i++)
		if (my_strcmp(parser[k], anthill->rooms[i]->name) == 0) {
			indexes[k++] = i;
			i = -1;
		}
	if (k < 2)
		return (2);
	connect_rooms(anthill->rooms[indexes[0]], anthill->rooms[indexes[1]]);
	return (0);
}

static int add_new_room(anthill_t *anthill, char **parser, int type)
{
	room_t *tmp = create_room(parser, anthill->ants, type);

	if (tmp == NULL)
		return (1);
	anthill->rooms = copy_rooms_and_add_new_room(anthill->rooms, tmp);
	if (anthill->rooms == NULL)
		return (1);
	if (type == ENTRY)
		anthill->entry = tmp;
	else if (type == EXIT)
		anthill->exit = tmp;
	return (0);
}

int check_and_add_room(anthill_t *anthill, char **parser, int room, char *infos)
{
	int len = my_tablen(parser);
	int ret = 0;

	if (len == 3) {
		if (is_in_name(parser[0], '-')) {
			my_puterror("Confusing name\n");
			return (2);
		}
		ret = add_new_room(anthill, parser, room);
		if (ret == 0)
			my_printf("%s\n", infos);
	} else
		ret = 2;
	return (ret);
}

int check_and_add_connection(anthill_t *anthill, char **parser, char *infos)
{
	int len = my_tablen(parser);
	int ret = 0;

	if (len == 2) {
		ret = add_new_connection(anthill, parser);
		if (ret == 0)
			my_printf("%s\n", infos);
	} else
		ret = 2;
	return (ret);
}
