/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** creates the anthill graph
*/

#include <stdlib.h>
#include "my_strings.h"
#include "graph.h"
#include "display.h"

static int add_info(anthill_t *anthill, char *infos, int room_type)
{
	static int pipes = 0;
	char **parser = my_str_to_word_array(infos, (pipes == 0 ? " " : "-"));
	int ret = 0;

	if (parser == NULL)
		return (1);
	if (pipes == 0 && my_tablen(parser) == 1) {
		pipes = 1;
		destroy_tab(parser);
		parser = my_str_to_word_array(infos, "-");
		if (my_tablen(parser) == 2)
			my_putstr("#tunnels\n");
	}
	if (pipes == 0)
		ret = check_and_add_room(anthill, parser, room_type, infos);
	else
		ret = check_and_add_connection(anthill, parser, infos);
	destroy_tab(parser);
	return (ret);
}

static int add(anthill_t *anthill, char *str, room_type_t *type)
{
	int ret = 0;

	if (str && is_command(str)) {
		if (my_strcmp(&str[2], "start") == 0)
			return (check_special_room(type, 0));
		else if (my_strcmp(&str[2], "end") == 0)
			return (check_special_room(type, 1));
		else
			return (0);
	} else if (str) {
		ret = add_info(anthill, str, *type);
		*type = NORMAL;
	}
	return (ret);
}

int create_anthill(anthill_t *anthill)
{
	int ret = 0;
	char *str = get_next_info();
	room_type_t type = NORMAL;

	if (my_get_check_nbr(str, &(anthill->ants)) == -1 || anthill->ants <= 0)
		return (1);
	display_ants(anthill->ants);
	while (str) {
		free(str);
		str = get_next_info();
		ret = add(anthill, str, &type);
		if (ret == 1)
			return (1);
		else if (ret == 2) {
			free(str);
			return (0);
		}
	}
	return (0);
}
