/*
** EPITECH PROJECT, 2018
** lemin
** File description:
** graph tools
*/

#include <stdlib.h>
#include "my_files.h"
#include "my_strings.h"
#include "graph.h"

int nb_rooms(room_t **rooms)
{
	int n = 0;

	if (rooms)
		while (rooms[n])
			n++;
	return (n);
}

int is_command(char const *str)
{
	if (str == NULL)
		return (0);
	return (my_strncmp(str, COMMAND_STR, 2) ? 0 : 1);
}

int check_special_room(room_type_t *type, int mode)
{
	static int check[2] = {0, 0};

	if (check[mode] == 0 && *type == NORMAL) {
		check[mode]++;
		*type = (mode == 0 ? ENTRY : EXIT);
		my_putstr(mode == 0 ? "##start\n" : "##end\n");
	} else
		return (2);
	return (0);
}

room_t *create_room(char **infos, int ants, int type)
{
	room_t *new = malloc(sizeof(*new));

	if (new == NULL)
		return (NULL);
	new->name = my_strdup(infos[0]);
	if (my_get_check_nbr(infos[1], &(new->pos[0])) == -1 ||
	my_get_check_nbr(infos[2], &(new->pos[1])) == -1)
		return (NULL);
	new->nb_next = 0;
	new->next = NULL;
	new->type = type;
	new->ants = (type == ENTRY ? ants : 0);
	return (new);
}

char *get_next_info(void)
{
	char *str = get_next_line(0);

	while (str && str[0] == COMMENT_CHAR && is_command(str) == 0) {
		free(str);
		str = get_next_line(0);
	}
	return (str);
}
