/*
** EPITECH PROJECT, 2018
** my_files
** File description:
** header
*/

#ifndef MY_FILES_H_
#define MY_FILES_H_

#ifndef READ_SIZE
#define READ_SIZE (1)
#endif

char *get_next_line(int fd);

#endif
