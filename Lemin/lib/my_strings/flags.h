/*
** EPITECH PROJECT, 2017
** flags
** File description:
** flags module header
*/

#ifndef FLAGS_H_
#define FLAGS_H_

#include "my_printf.h"

int flag_char(va_list ap, list_t *element);
int non_print_str(char *str);
int flag_str(va_list ap, list_t *element);
int flag_int(va_list ap, list_t *element);
int flag_basenum(va_list ap, list_t *element);

#endif
