/*
** EPITECH PROJECT, 2017
** my_printf
** File description:
** header
*/

#ifndef MY_PRINTF_H_
#define MY_PRINTF_H_

#include <stdarg.h>

typedef struct list {
	char flags[5];
	int field;
	int precision;
	int length_mod;
	char conv;
	int nb_bytes;
} list_t;

int check_if_regular(char c);
char check_conversion(char const *str, int i);
int display_element(va_list ap, list_t *element);
int do_modulo(va_list ap, char const *str, int *i);
int my_printf(char const *str, ...);

#endif
