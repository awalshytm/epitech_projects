/*
** EPITECH PROJECT, 2017
** my_str_to_word_array
** File description:
** splits a string into words and store them in a tab
*/

#include <stdlib.h>

static int is_in_str(char c, char const *str)
{
	for (int i = 0 ; str[i] ; i++)
		if (c == str[i])
			return (1);
	return (0);
}

static int my_count_words(char const *str, char const *d)
{
	int i = 0;
	int cnt = 0;

	while (str[i]) {
		cnt++;
		while (str[i] && is_in_str(str[i], d) == 0)
			i++;
		while (str[i] && is_in_str(str[i], d) != 0)
			i++;
	}
	return (cnt);
}

static int word_len(char const *str, char const *d)
{
	int n = 0;

	while (str[n] && is_in_str(str[n], d) == 0)
		n++;
	return (n);
}

char **my_str_to_word_array(char const *str, char const *d)
{
	char **tab;
	int cnt = 0;
	int i = 0;
	int n = 0;

	if (!str || !(tab = malloc(sizeof(char *) *
				(my_count_words(str, d) + 1))))
		return (NULL);
	while (str[i]) {
		n = 0;
		if ((tab[cnt] = malloc(word_len(&str[i], d) + 1)) == NULL)
			return (NULL);
		while (str[i] && is_in_str(str[i], d) == 0)
			tab[cnt][n++] = str[i++];
		tab[cnt++][n] = 0;
		while (str[i] && is_in_str(str[i], d) == 1)
			i++;
	}
	tab[cnt] = NULL;
	return (tab);
}
