/*
** EPITECH PROJECT, 2018
** printf_modulo
** File description:
** header
*/

#ifndef PRINTF_MODULO_H_
#define PRINTF_MODULO_H_

list_t *auxiliary_modulo(list_t *elmt, char const *str, int *i);

#endif
