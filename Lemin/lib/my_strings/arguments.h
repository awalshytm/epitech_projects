/*
** EPITECH PROJECT, 2017
** arguments
** File description:
** header
*/

#ifndef ARGUMENTS_H_
#define ARGUMENTS_H_

#include "my_printf.h"

int base_min(long long nb, char const *base, list_t *elmt);
int length_modifier(va_list ap, list_t *elmt, char *str);
int unsigned_argument(va_list ap, list_t *element, char *str);
int signed_argument(va_list ap, list_t *elmt);

#endif
