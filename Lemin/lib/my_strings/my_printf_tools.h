/*
** EPITECH PROJECT, 2017
** my_printf_tools
** File description:
** header
*/

#ifndef MY_PRINTF_TOOLS_H_
#define MY_PRINTF_TOOLS_H_

#include "my_printf.h"

list_t *init_list(char conv);
int is_digit(char c);
int num_len(long long nb, char const *base);
int put_zeros(char c);
int check_flag(list_t *elmt, char c);

#endif
