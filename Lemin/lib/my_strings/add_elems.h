/*
** EPITECH PROJECT, 2017
** add_elems
** File description:
** module for my_printf module
*/

#ifndef ADD_ELEMS_H_
#define ADD_ELEMS_H_

#include "my_printf.h"

void add_flag_to_elem(list_t *element, char c);
void add_flags(list_t *element, char const *str, int *i);
void add_field_width(list_t *element, char const *str, int *i);
void add_precision(list_t *element, char const *str, int *i);
void add_length(list_t *element, char const *str, int *i);

#endif
