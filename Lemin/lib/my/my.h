/*
** EPITECH PROJECT, 2017
** my.h
** File description:
** contains the header of all libmy.a functions
*/

#ifndef MY_H_
#define MY_H_

int my_pow_rec(int nb, int p);
void my_sort_int_array(int *tab, int size);
int my_sqrt(int nb);
void my_swap(int *a, int *b);

#endif
