/*
** EPITECH PROJECT, 2017
** my_str_to_word_array
** File description:
** str to word
*/

#include <stdlib.h>

int nbr_words(char *str)
{
	int i = 0;
	int j = 0;
	int nbr = 0;

	while (str[i] != '\0') {
		while (str[i] == ' ' || str[i] == '\n' || str[i] == '\t' ||
		str[i] == '\v' || str[i] == '\f' || str[i] == '\r')
			i++;
		j = 0;
		while (str[i] >= 33 && str[i] <= 126) {
			if (j == 0)
				nbr++;
			i++;
			j++;
		}
	}
	return (nbr);
}

int nbr_char(char *str, int aw)
{
	int i = 0;
	int j;
	int nbr = 0;

	while (str[i] != '\0')
	{
		while (str[i] == ' ' || str[i] == '\n' || str[i] == '\t' ||
		str[i] == '\v' || str[i] == '\f' || str[i] == '\r')
			i++;
		j = 0;
		while (str[i] >= 33 && str[i] <= 126)
		{
			if (j == 0)
				nbr++;
			i++;
			j++;
		}
		if (nbr == (aw + 1))
			return (j);
	}
	return (0);
}

char	**my_str_to_word_array(char *str)
{
	char **src;
	int i = 0;
	int j;
	int k = 0;

	src = (char**)malloc(sizeof(char*) * (nbr_words(str) + 1));
	while (i < (nbr_words(str)))
	{
		j = 0;
		src[i] = (char*)malloc(sizeof(char) * (nbr_char(str, i)));
		while (j < (nbr_char(str, i)))
		{
			while (str[k] == ' ' || str[k] == '\n' || str[k] == '\t')
				k++;
			src[i][j] = str[k];
			j++;
			k++;
		}
		src[i][j] = '\0';
		i++;
	}
	src[i] = 0;
	return (src);
}
