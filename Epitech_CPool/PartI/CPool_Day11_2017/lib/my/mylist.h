/*
** EPITECH PROJECT, 2017
** Day11 header
** File description:
** header for Day 11
*/

#ifndef MYLIST_H_
# define MYLIST_H_

typedef struct linked_list
{
	void *data;
	struct linked_list *next;
} linked_list_t;

#endif
