/*
** EPITECH PROJECT, 2017
** my_swap
** File description:
** swap using pointers
*/

void my_swap(int *a, int *b)
{
	int c = 0;

	c = *a;
	*a = *b;
	*b = c;
}
