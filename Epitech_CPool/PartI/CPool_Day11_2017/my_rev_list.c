/*
** EPITECH PROJECT, 2017
** my_rev_list
** File description:
** reverse linked list
*/

#include "./include/mylist.h"

int my_list_size(linked_list_t *beguin)
{
        linked_list_t *tmp;
        int i = 0;

	tmp = beguin;
        while (tmp != 0)
        {
                i++;
                tmp = tmp->next;
        }
        return (i);
}

void my_rev_list(linked_list_t **begin)
{
	linked_list_t *tmp;
	linked_list_t *tmp2;
	int i = 0;
	int j = my_list_size((*begin));

	tmp = (*begin)->next;
	tmp2 = tmp->next;
	(*begin)->next = 0;
	while (i < (j - 2)) {
		tmp->next = (*begin);
		(*begin) = tmp;
		tmp = tmp2;
		tmp2 = tmp2->next;
		i++;
	}
	tmp->next = (*begin);
	(*begin) = tmp;
}
