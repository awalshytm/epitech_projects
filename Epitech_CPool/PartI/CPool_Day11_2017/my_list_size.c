/*
** EPITECH PROJECT, 2017
** my_list_size
** File description:
** calculate the list size
*/

#include "./include/mylist.h"

int my_list_size(linked_list_t const *begin)
{
	linked_list_t *tmp;
	int i = 0;

	tmp = begin;
	while (tmp != 0) {
		i++;
		tmp = tmp->next;
	}
	return (i);
}
