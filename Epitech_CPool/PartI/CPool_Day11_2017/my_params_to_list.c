/*
** EPITECH PROJECT, 2017
** params to list
** File description:
** params to list
*/

#include "./include/mylist.h"

int my_put_in_list(linked_list_t **list, char *data)
{
	linked_list_t *element;

	element = malloc(sizeof(*element));
	element->data = data;
	element->next = *list;
	*list = element;
	return (0);
}

linked_list_t *my_params_to_list(int ac, char const * const *av)
{
	int i = 0;
	linked_list_t *list;
	
	list = 0;
	while (i < ac) {
		my_put_in_list(&list, av[i]);
		i++;
	}
	return (list);
}
