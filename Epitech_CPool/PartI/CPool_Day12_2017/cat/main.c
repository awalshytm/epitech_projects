/*
** EPITECH PROJECT, 2017
** cat
** File description:
** main + errors
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "../include/my.h"

void read_file(int fd)
{
	char buffer[30720];
	int size = 1;

	if (fd != -1) {
		while (size != 0) {
			size = read(fd, buffer, 30719);
			write(1, buffer, size);
		}
	}
}

void my_putstr_error(char *str)
{
	int i = 0;

	write(2, "cat: ", 5);
	while (str[i] != '\0') {
		write(2, &str[i], 1);
		i++;
	}
	write(2, ": No such file or directory\n", 28);
}

int open_file(int argc, char **argv)
{
	int fd;
	int i = 1;
	int arg_size = 0;
	int error = 0;

       	while (i < argc) {
		fd = open(argv[i], O_RDONLY);
		if (fd == -1) {
			my_putstr_error(argv[i]);
			error = 1;
		}
		read_file(fd);
		close(fd);
		i++;
	}
	return (error == 1 ? 84 : 0);
}

void loop()
{
	char buffer[30720];
	int size = 0;
	
	while (1) {
		size = read(0, buffer, 30719);
		write(1, buffer, size);
	}
}

int main(int argc, char **argv)
{
	if (argc > 1) {
		open_file(argc, argv);
	} else if (argc == 1) {
		loop();
	} else
		return (84);
	return (0);
}
