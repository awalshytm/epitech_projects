/*
** EPITECH PROJECT, 2017
** my_shozstr
** File description:
** show str with nonprintables
*/

void print_in_hex(char c)
{
	char hex_base[] = "0123456789abcdef";
	int ascii_value = c;

	my_putchar((ascii_value / 16) + '0');
	my_putchar(hex_base[ascii_value % 16]);
}

int my_showstr(char const *str)
{
	int i = 0;

	while (str[i] != '\0') {
		if (str[i] <= 123 && str[i] >= 32)
			my_putchar(str[i]);
		else {
			my_putchar(92);
			print_in_hex(str[i]);
		}
		i++;
	}
	return (0);
}
