/*
** EPITECH PROJECT, 2017
** my_strstr
** File description:
** strstr function
*/

char *my_strstr(char const *str, char const *to_find)
{
	int i = 0;
	int j = 0;

	while (str[i] != '\0') {
		while (to_find[j] == str[i + j] && to_find[j]) {
			j++;
		}
		if (to_find[j] == '\0')
			return (&str[i]);
		i++;
	}
	return (0);
}
