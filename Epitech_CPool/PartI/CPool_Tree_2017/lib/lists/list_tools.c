/*
** EPITECH PROJECT, 2018
** list
** File description:
** list
*/

#include "lists.h"

int get_list_size(elem_t *list)
{
	elem_t *tmp = list;
	int i = 0;

	if (tmp == NULL)
		return (-1);
	while (tmp != NULL) {
		i++;
		tmp = tmp->next;
	}
	free(tmp);
	return (i);
}

void print_list(elem_t *list)
{
	elem_t *tmp = list;

	if (tmp == NULL)
		return;
	while (tmp != NULL) {
		my_putstr(tmp->data);
		tmp = tmp->next;
	}
}
