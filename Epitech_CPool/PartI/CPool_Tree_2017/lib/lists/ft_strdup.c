/*
** EPITECH PROJECT, 2018
** lists
** File description:
** ft_strdup
*/

#include "lists.h"

char *ft_strdup(char *str)
{
	char *new = malloc(sizeof(char) * my_strlen(str) + 1);
	int i = 0;

	if (new == NULL)
		return (NULL);
	while (str[i])
		new[i] = str[i++];
	new[i] = '\0';
	return (new);
}
