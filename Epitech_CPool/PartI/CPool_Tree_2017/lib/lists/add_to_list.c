/*
** EPITECH PROJECT, 2018
** list
** File description:
** list
*/

#include "lists.h"

elem_t *add_to_list(elem_t *list, char *data)
{
	elem_t *end;
	elem_t *e;

	e = malloc(sizeof(elem_t));
	if (e == NULL)
		return (NULL);
	e->data = ft_strdup(data);
	if (list != NULL) {
		end = last(list);
		e->prev = end;
		end->next = e;
		e->next = NULL;
		free(end);
		return (list);
	}
	e->prev = NULL;
	e->next = NULL;
	return (e);
}
