/*
** EPITECH PROJECT, 2018
** list
** File description:
** list
*/

#ifndef LISTS_H_
#define LISTS_H_

#include <unistd.h>
#include <stdlib.h>

typedef struct element elem_t;

struct element
{
	char *data;
	elem_t *next;
	elem_t *prev;
};

/*go_to*/
elem_t *first(elem_t *);
elem_t *last(elem_t *);

/*add_to*/
elem_t *add_to_list(elem_t *, char *);

/*tools*/
int get_list_size(elem_t *);
void print_list(elem_t *);

/*my_libc*/
char *ft_strdup(char *);
void my_putstr(char *);
int my_strlen(char *);

#endif
