/*
** EPITECH PROJECT, 2018
** list
** File description:
** list
*/

#include "lists.h"

elem_t *first(elem_t *e)
{
	if (e == NULL)
		return (e);
	else if (e->prev == NULL)
		return (e);
	else
		return (first(e->prev));
}

elem_t *last(elem_t *e)
{
	if (e == NULL)
		return (e);
	else if (e->next == NULL)
		return (e);
	else
		return (last(e->next));
}
