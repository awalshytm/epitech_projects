/*
** EPITECH PROJECT, 2018
** libc
** File description:
** my_sqrt
*/

int my_sqrt(int nb, int *res)
{
	int i = 1;
	int j = 0;

	if (nb <= 0) {
		*res = 0;
		return (0);
	}
	while (j < nb) {
		j = i * i;
		if (j == nb) {
			*res = i;
			return (0);
		}
		if (j > nb)
			return (-1);
		i++;	
	}
	return (-1);
}
