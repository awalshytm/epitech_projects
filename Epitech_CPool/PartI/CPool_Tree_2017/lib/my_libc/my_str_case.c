/*
** EPITECH PROJECT, 2018
** libc
** File description:
** str case
*/

#include "my_libc.h"

char *my_strlowcase(char *str)
{
	if (str == NULL)
		return (NULL);
	for (int i = 0; str[i]; i++)
		if (str[i] >= 65 && str[i] <= 90)
			str[i] += 32;
	return (str);
}

char *my_struppercase(char *str)
{
	if (str == NULL)
		return (NULL);
	for (int i = 0; str[i]; i++)
		if (str[i] >= 97 && str[i] <= 122)
			str[i] -= 32;
	return (str);
}
