/*
** EPITECH PROJECT, 2017
** my_putnbr_base
** File description:
** put nbr base
*/

#include "my_libc.h"

void my_putnbr_base(int nbr, char *base)
{
	int divisor = my_strlen(base);

	if (nbr < 0)
		nbr *= -1;
	if (nbr > 0) {
		my_putnbr_base(nbr / divisor, base);
		my_putchar(base[nbr % divisor]);
	}
}
