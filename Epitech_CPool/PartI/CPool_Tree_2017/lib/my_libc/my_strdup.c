/*
** EPITECH PROJECT, 2018
** libc
** File description:
** strdup
*/

#include "my_libc.h"

char *my_strdupf(char *str)
{
	char *new = malloc(sizeof(char) * my_strlen(str) + 1);
	int i = 0;

	if (new == NULL)
		return (NULL);
	while (str[i]) {
		new[i] = str[i];
		i++;
	}
	free(str);
	new[i] = '\0';
	return (new);
}

char *my_strdup(char *str)
{
	char *new = malloc(sizeof(char) * my_strlen(str) + 1);
	int i = 0;

	if (new == NULL)
		return (NULL);
	while (str[i]) {
		new[i] = str[i];
		i++;
	}
	new[i] = '\0';
	return (new);
}
