/*
** EPITECH PROJECT, 2018
** libc
** File description:
** putstr
*/

#include "my_libc.h"

void my_putstr(char *s)
{
	int size = my_strlen(s);

	if (s == NULL || size == -1)
		return;
	write(STDOUT_FILENO, s, size);
}
