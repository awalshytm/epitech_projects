/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse string
*/

#include "my_libc.h"

char *my_revstr(char *str)
{
	int i = 0;
	int j = 0;
	char c;

	i = my_strlen(str) - 1;
	while (j < i) {
		c = str[i];
		str[i] = str[j];
		str[j] = c;
		i--;
		j++;
	}
	return (str);
}
