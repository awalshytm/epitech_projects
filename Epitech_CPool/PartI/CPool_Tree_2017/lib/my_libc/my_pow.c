/*
** EPITECH PROJECT, 2018
** libc
** File description:
** pow
*/

int my_pow(int nb, int p)
{
	if (p == 0)
		return (1);
	if (p < 0)
		return (0);
	if (p > 1)
		nb = nb * my_pow(nb, p - 1);
	return (nb);
}
