/*
** EPITECH PROJECT, 2018
** libc
** File description:
** atoi
*/

int my_atoi(char *str)
{
	int i = 0;
	int neg = 0;
	int nb = 0;
	int j = 0;

	while (str[i] == '\n' || str[i] == ' ' || str[i] == '\t')
		i++;
	while (str[i] == '-' || str[i] == '+')
		i++;
	if (i > 0 && str[i - 1] == '-')
		neg = 1;
	while (str[i] >= '0' && str[i] <= '9') {
		nb = nb * 10 + str[i] - 48;
		i++;
	}
	return (neg == 1 ? nb *= -1 : nb);
}
