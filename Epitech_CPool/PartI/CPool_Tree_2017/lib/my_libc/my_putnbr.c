/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** output number
*/

#include "my_libc.h"

void my_putnbr(int nb)
{
	long int i = nb;

	if (i < 0) {
		my_putchar('-');
		i = i * -1;
	}
	if (i >= 10)
		my_putnbr(i / 10);
	my_putchar(i % 10 + 48);
}
