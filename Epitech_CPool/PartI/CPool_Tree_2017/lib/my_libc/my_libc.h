/*
** EPITECH PROJECT, 2018
** lic
** File description:
** header
*/

#ifndef MY_LIB_C_H_
#define MY_LIB_C_H_

#include <unistd.h>
#include <stdlib.h>

/*int*/
int my_atoi(char *);
int my_pow(int, int);
int my_sqrt(int);
int my_strcmp(char *, char *);
int my_strncmp(char *, char *, int);
int my_strlen(char *);
int my_swap(int *, int *);

/*void printing*/
void my_putstr(char *);
void my_putchar(char);
void my_puterr(char *);
void my_putnbr_base(int, char *);
void my_putnbr(int);
void my_sort_int_array(int *, int, int);

/*char star*/
char *my_revstr(char *);
char *my_strlowcase(char *);
char *my_struppercase(char *);
char *mystrcat(char *, char *);
char *my_strcpy(char *, char *);
char *my_strdupf(char *);
char *my_strdup(char *);
char *my_strncpy(char *, char *, int);

#endif
