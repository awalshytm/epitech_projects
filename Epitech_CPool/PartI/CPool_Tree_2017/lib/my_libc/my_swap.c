/*
** EPITECH PROJECT, 2018
** libc
** File description:
** swap
*/

void my_swap(int *a, int *b)
{
	int c = 0;

	c = *a;
	*a = *b;
	*b = c;
}
