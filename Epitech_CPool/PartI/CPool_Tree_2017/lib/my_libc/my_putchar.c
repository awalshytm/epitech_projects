/*
** EPITECH PROJECT, 2018
** libc
** File description:
** putchar
*/

#include "my_libc.h"

void my_putchar(char c)
{
	write(1, &c, 1);
}
