/*
** EPITECH PROJECT, 2018
** libc
** File description:
** puterr
*/

#include "my_libc.h"

void my_puterr(char *s)
{
	int size = my_strlen(s);

	if (s == NULL || size == -1)
		return;
	write(2, s, my_strlen(s));
}
