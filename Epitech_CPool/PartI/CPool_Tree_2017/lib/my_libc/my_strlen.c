/*
** EPITECH PROJECT, 2018
** libc
** File description:
** strlen
*/

#include "my_libc.h"

int my_strlen(char *s)
{
	int i = 0;

	if (s == NULL)
		return (-1);
	while (s[i++]);
	return (i);
}
