/*
** EPITECH PROJECT, 2017
** fir tree
** File description:
** tree
*/

void	print_core(int *nb, char c)
{
	int a = 0;
	
	while (a < *nb) {
		my_putchar(c);
		a++;
	}
}


void	print_trunk(int nb, char c)
{
	int i = 0;

	while (i < nb) {
		my_putchar(c);
		i++;
	}
	if (c == '|') {
		my_putchar('\n');
	}
}

void	print_floor(int *nb_spaces, int *nb_stars, int nb_lign)
{
	int a = 0;

	while (a < nb_lign) {
		print_core(nb_spaces, ' ');
		print_core(nb_stars, '*');
		my_putchar('\n');
		*nb_stars += 2;
		*nb_spaces -= 1;
		a++;
	}
}

void	trunk(int size, int nb_spaces)
{
	int a = 0;
	int width = 0;
	
	if (size % 2 == 0) {
		while (a < size) {
			width = size + 1;
			print_trunk((nb_spaces - (width / 2)), ' ');
			print_trunk(width, '|');
			a++;
		}
	} else {
		width = size;
		while (a < size) {
			print_trunk((nb_spaces - (width / 2)), ' ');
			print_trunk(width, '|');
			a++;
		}
	}
}

void	tree(int nb)
{
	int nb_spaces = ((((((nb * (nb - 1)) / 2) + (4 * nb)) * 2) - 1 - (((nb - 1) * 2) * 2)) / 2);
	int spaces_save = nb_spaces;
	int nb_stars = 1;
	int nb_lign = 4;
	int nb_take_off = 2;
	int i = 0;
	int *a;
	int *b;

	a = &nb_spaces;
	b = &nb_stars;
	while (i < nb) {
		print_floor(a, b, nb_lign);
		nb_lign++;
		if (i % 2 != 0)
			nb_take_off++;
		nb_spaces += nb_take_off;
		nb_stars -= nb_take_off * 2;
		i++;
	}
	trunk(nb, spaces_save);
}

