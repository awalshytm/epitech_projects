/*
** EPITECH PROJECT, 2017
** test_putstr
** File description:
** test putstr
*/

#include <unistd.h>

void my_putchar(char c)
{
	write(1, &c, 1);
}

int main()
{
        char const str[] = "bonjour";

        my_putstr(str);
        return (0);
}

