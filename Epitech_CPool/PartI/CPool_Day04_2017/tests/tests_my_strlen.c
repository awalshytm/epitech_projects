/*
** EPITECH PROJECT, 2017
** test_my_evil_str
** File description:
** testing of the evil_str function
*/

#include <criterion/criterion.h>

const char *str = "Bonjour";
const char *str2 = "De"
const int len = 7;
const int len2 = 2;

Test(utils, is_str_len_equal_to_len_v1)
{
	cr_assert(strlen(str) == len);
}

Test(utils, id_str_len_equal_to_len_v2)
{
	cr_assert_eq(strlen(str2), len2);
}
