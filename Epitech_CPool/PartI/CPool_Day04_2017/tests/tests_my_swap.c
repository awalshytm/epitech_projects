/*
** EPITECH PROJECT, 2017
** test_swap
** File description:
** testing of swap function
*/

#include <stdio.h>

int main()
{
        int first = 6;
        int second = 3;
        int *a = &first;
        int *b = &second;
        printf("first: %d, second: %d\n", first, second);
        my_swap(a, b);
        printf("first: %d, second: %d\n", first, second);
}
