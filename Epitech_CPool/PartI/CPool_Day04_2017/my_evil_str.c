/*
** EPITECH PROJECT, 2017
** my_evil_str
** File description:
** big swap
*/

int my_strlen2(char const *str)
{
	int a;

	while (str[a] != '\0') {
		a++;
	}
	return (a);
}

char *my_evil_str(char *str)
{
	int a;
	int b = 0;
	char c;

	a = my_strlen2(str);
	a--;
	while (a != b && b <= a) {
		c = str[a];
		str[a] = str[b];
		str[b] = c;
		a--;
		b++;
	}
	return (str);
}
