/*
** EPITECH PROJECT, 2017
** my_sort_int_array
** File description:
** sort ints in array
*/

int swaping(int *tab, int size, int a)
{
	int swap = 0;
	int swapping = 0;
	
	while (a + 1 < size) {
		if (tab[a] > tab[a + 1]) {
			swapping = tab[a];
			tab[a] = tab[a + 1];
			tab[a + 1] = swapping;
			swap++;
		}
		a++;
	}
	return (swap);
}

void my_sort_int_tab(int *array, int size)
{
	int nb_swap = 1;
	int a = 0;

	while (nb_swap > 0)
	{
		nb_swap = 0;
		a = 0;
		nb_swap = swaping(array, size, a);
	}
}
