/*
** EPITECH PROJECT, 2017
** my_print_comb
** File description:
** print combinaison abc type
*/

void	print_comb(char a, char b, char c)
{
	if (a == '7' && b == '8' && c == '9') {
		my_putchar(a);
		my_putchar(b);
		my_putchar(c);
	} else {
		my_putchar(a);
		my_putchar(b);
		my_putchar(c);
		my_putchar(',');
		my_putchar(' ');
	}
}

int	print_comb_complement(int a, int b)
{
	char	c = '2';
	while (b <= '8') {
		c = b + 1;
		while (c <= '9') {
			print_comb(a, b, c);
			c++;
		}
		b++;
	}
	return 1;
}

int	my_print_comb()
{
	char	a = '0';
	char	b = '1';
	int	c = 0;

	while (a <= '7') {
		b = a + 1;
		c = print_comb_complement(a, b);
		if (c == 1)
			a++;
		c = 0;
	}
	return 0;
}
