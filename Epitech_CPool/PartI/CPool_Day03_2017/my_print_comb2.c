/*
** EPITECH PROJECT, 2017
** my_print_comb2
** File description:
** print combinaison type ab dc
*/

void	print_comb2(char a, char b, char c, char d)
{
	if (a < c || (a == c && b < d)) {
		if (a == '9' && b == '8' && c == '9' && d == '9') {
			my_putchar(a);
			my_putchar(b);
			my_putchar(' ');
			my_putchar(c);
			my_putchar(d);
		} else {
			my_putchar(a);
			my_putchar(b);
			my_putchar(' ');
			my_putchar(c);
			my_putchar(d);
			my_putchar(',');
			my_putchar(' ');
		}
	}
}

void	count_var(char a, char b, char c, char d)
{
	while (a <= '9') {
		d++;
		if (d == 58)
			c++;
		if (d == 58)
			d = '0';
		if (c == 58)
			b++;
		if (c == 58)
			c = '0';
		if (b == 58)
			a++;
		if (b == 58)
			b = '0';
		print_comb2(a, b, c, d);
	}
}

int	my_print_comb2()
{
	char	a = '0';
	char	b = '0';
	char	c = '0';
	char	d = '0';

	count_var(a, b, c, d);
	return 0;
}
