/*
** EPITECH PROJECT, 2017
** my_print_aplha.c
** File description:
** print_alpha
*/

int my_print_alpha()
{
	char lettre = 'a';

	while (lettre <= 'z') {
		my_putchar(lettre);
		lettre += 1;
	}
	return 0;
}
