/*
** EPITECH PROJECT, 2017
** my_print_digits
** File description:
** print_digits
*/

int	my_print_digits()
{
	char nbr = '0';

	while (nbr <= '9') {
		my_putchar(nbr);
		nbr += 1;
	}
	return 0;
}
