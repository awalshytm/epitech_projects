/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** test for my_put_nbr
*/

int my_put_nbr(int nb);

int main()
{
	my_put_nbr(-2147483648);
	my_putchar('\n');
	my_put_nbr(12);
	my_putchar('\n');
	my_put_nbr(2147483647);
}
