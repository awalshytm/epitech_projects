/*
** EPITECH PROJECT, 2017
** my_print_revalpha.c
** File description:
** print_reverse_alpha
*/

int	my_print_revalpha()
{
	char lettre = 'z';

	while (lettre >= 'a') {
		my_putchar(lettre);
		lettre -= 1;
	}
	return 0;
}
