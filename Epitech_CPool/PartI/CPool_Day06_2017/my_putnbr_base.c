/*
** EPITECH PROJECT, 2017
** my_putnbr_base
** File description:
** put nbr base
*/

void my_putchar(char c)
{
	write(1, &c, 1);
}

int ft_strlen(char const *str)
{
	int i = 0;

	while (str[i] != '\0') {
		i++;
	}
	return (i);
}

int my_putnbr_base(int nbr, char const *base)
{
	int divisor = ft_strlen(base);

	if (nbr > 0) {
		my_putnbr_base(nbr / divisor, base);
		my_putchar(base[nbr % divisor]);
	}
	return (0);
}


int main(int argc, char **argv)
{
	if (argc == 3)
	{
		my_putnbr_base(atoi(argv[1]), argv[2]);
	}
}
