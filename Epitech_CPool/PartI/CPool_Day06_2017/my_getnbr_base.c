/*
** EPITECH PROJECT, 2017
** my_getnbr_base
** File description:
** get nbr base
*/

int my_getnbr_base(char const *str, char const *base)
{
	int nb = 0;
	int i = my_strlen(str) - 1;
	int size = my_strlen(base);
	int j = 0;
	int k = 0;

	while (i >= 0) {
		k = 0;
		while (str[i] != base[k])
			k++;
		nb += k * my_compute_power_rec(size, j);
		j++;
		i--;
	}
	return (nb);
}

int main(int argc, char const *argv[]) {
	if (argc == 3)
	{
		printf("%d\n", my_getnbr_base(argv[1], argv[2]));
	}
	return 0;
}
