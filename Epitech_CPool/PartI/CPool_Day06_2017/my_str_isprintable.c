/*
** EPITECH PROJECT, 2017
** my_is_printable
** File description:
** is printable case
*/

int my_str_isprintable(char const *str)
{
	int i = 0;

	if (str[0] == '\0')
		return (1);
	while (str[i] >= 32 && str[i] <= 126)
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}
