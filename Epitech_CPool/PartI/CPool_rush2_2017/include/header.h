/*
** EPITECH PROJECT, 2017
** What Language is this
** File description:
** step1 header
*/

#ifndef HEADER_H_
	#define HEADER_H_

void print_percentage(char *str, int result);
void my_multiple_task(int ac, char **str);
void my_print(int nb, char c, char *str);
void my_putchar(char c);
void my_put_nbr(int nb);
int maj_or_min(char c);
int my_count_lettre(int min_maj, char *str, char c);
int my_check(char *str, char c);
int my_length(char *str);

#define ABS(value) (value < 0) ? (value * (-1)) : value

#endif
