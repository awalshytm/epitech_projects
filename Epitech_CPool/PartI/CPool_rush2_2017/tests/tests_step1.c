/*
** EPITECH PROJECT, 2017
** What Language is This
** File description:
** test step1
*/

#include <criterion/criterion.h>

Test(utils, my_check)
{
	char tab[] = "Bonjour";
	char str[] = "";

	cr_assert(my_check(tab, 'a') == 0, "Check my_check 'TVB'");
	cr_assert(my_check(str, 'a') == 84, "Check, my_check 'Problem'");
	cr_assert(my_check(tab, '1') == 84, "Check my_check 'Problem' char c");
}

Test(utils, maj_or_min)
{
	char c1 = 'c';
	char c2 = 'C';
	char c3 = '1';
	cr_assert(maj_or_min(c1) == 0, "Check maj_or_min");
	cr_assert(maj_or_min(c2) == 1, "Check maj_or_min");
	cr_assert(maj_or_min(c3) == 84, "Check maj_or_min");
}

Test(utils, my_count_lettre)
{
	char str1[] = "Bonjour Arthur comment ca va";
	char str2[] = "Echappatoire est le raisonnement";
	cr_assert(my_count_lettre(0, str1, 'a') == 3);
	cr_assert(my_count_lettre(1, str2, 'E') == 6);
}
