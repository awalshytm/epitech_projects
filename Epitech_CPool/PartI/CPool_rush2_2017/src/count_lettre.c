/*                                                                                                    
** EPITECH PROJECT, 2017                                                                              
**  What Language is This                                                                             
** File description:                                                                                  
** count lettre and length                                                                            
*/

#include "../include/header.h"

int maj_or_min(char c)
{
        if (c >= 65 && c <= 90)
                return (1);
        if (c >= 97 && c <= 122)
                return (0);
        return (84);
}

int my_count_lettre(int min_maj, char *str, char c)
{
        int i = 0;
        int nb_lettre = 0;

        while (str[i] != '\0') {
                if (str[i] == c)
                        nb_lettre++;
                if (min_maj == 1 && (str[i] == (c + 32)))
                        nb_lettre++;
                if (min_maj == 0 && (str[i] == (c - 32)))
                        nb_lettre++;
                i++;
        }
        return (nb_lettre);
}

int my_length(char *str)
{
        int i = 0;
        int j = 0;

        while (str[i] != '\0') {
                if (str[i] >= 123 || str[i] < 65 || (str[i] > 90 && str[i] < 97))
                        j++;
                i++;
        }
        return (i - j);
}
