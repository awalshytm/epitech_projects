/*                                                                                                    
** EPITECH PROJECT, 2017                                                                              
** What Language is This                                                                              
** File description:                                                                                  
** multiple print with precentage                                                                     
*/

#include "../include/header.h"
#include <unistd.h>

void print_percentage(char *str, int result)
{
        int before = 0;
        int i = 0;
        int after = 0;

        i = (result * 10000 / my_length(str));
        write(1, " (", 2);
        before = i / 100;
        my_put_nbr(before);
        my_putchar(',');
        after = (i % 100);
        my_put_nbr(after);
        write(1, "%)", 3);
}

void my_print(int nb, char c, char *str)
{
        my_putchar(c);
        my_putchar(':');
        my_put_nbr(nb);
        print_percentage(str, nb);
        my_putchar('\n');
}

void my_multiple_task(int ac, char **str)
{
        int i = 2;
        int result;
        int min_maj;

        while (i < ac) {
                result = 0;
                min_maj = 0;
                min_maj = maj_or_min(str[i][0]);
                result = my_count_lettre(min_maj, str[1], str[i][0]);
                my_print(result, str[i][0], str[1]);
                i++;
        }
}
