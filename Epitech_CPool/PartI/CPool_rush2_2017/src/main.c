/*                                                                                                    
** EPITECH PROJECT, 2017                                                                              
** What Language is This                                                                              
** File description:                                                                                  
** main and errors                                                                                    
*/

#include "../include/header.h"

int my_check(char *str, char c)
{
        int i = 0;

        while (str[i] != '\0')
                i++;
        if (((c <= 122 && c >= 97) || (c <= 90 && c >= 65))
            && i != 0)
                return (0);
        return (84);
}

int main(int argc, char **argv)
{
        int result = 0;
        int i = 0;

        if (argc > 2) {
                if (my_check(argv[1], argv[2][0]) == 84)
                        return (84);
                if(my_check(argv[1], argv[2][0]) == 0) {
                        my_multiple_task(argc, argv);
                }
        } else
                return(84);
}
