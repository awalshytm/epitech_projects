/*
** EPITECH PROJECT, 2017
** my_compute_power_it
** File description:
** iterative power
*/

int my_compute_power_it(int nb, int p)
{
	int nb_final = nb;

	if (p == 0)
		return (1);
	if (p < 0)
		return (0);
	while (p > 1) {
		nb_final = nb_final * nb;
		p--;
	}
	return (nb_final);
}
