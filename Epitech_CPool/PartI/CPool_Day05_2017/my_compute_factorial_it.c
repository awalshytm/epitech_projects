/*
** EPITECH PROJECT, 2017
** my_compute_factorial_it
** File description:
** factorial iterative
*/

int my_compute_factorial_it(int nb)
{
	int nb_final = 1;

	if (nb == 0)
		return (1);
	if (nb < 0)
		return (0);
	if (nb >= 13)
		return (0);
	while (nb > 0) {
		nb_final = nb_final * nb;
		nb--;
	}
	return (nb_final);
}
