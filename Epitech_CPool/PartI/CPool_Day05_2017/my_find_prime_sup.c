/*
** EPITECH PROJECT, 2017
** my_find_prime_sup
** File description:
** find next prime number
*/

int ft_is_prime(int nb)
{
	int a = 1;
	int b = 2;
	int prime = 1;

	if (nb <= 1)
		return (0);
	if (nb == 2)
		return (1);
	while (a <= nb) {
		if (nb % b == 0)
			prime = 0;
		b++;
		a = b * b;
	}
	if (prime == 1)
		return (1);
	return (0);
}

int my_find_prime_sup(int nb)
{
	if (ft_is_prime(nb) == 1)
		return (nb);
	while (ft_is_prime(nb) == 0) {
		nb++;
	}
	return (nb);
}
