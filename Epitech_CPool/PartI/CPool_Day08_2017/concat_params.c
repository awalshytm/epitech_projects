/*
** EPITECH PROJECT, 2017
** concats_params
** File description:
** concat parameters
*/

int count_size(int ac, char **av)
{
	int i = 0;
	int j;
	int count = 0;

	while (i < ac) {
		j = 0;
		while (av[i][j] != '\0') {
			j++;
			count++;
		}
		i++;
	}
	return (count);
}

char *concat_params(int argc, char **argv)
{
	char *str;
	int i = 0;
	int j = 0;
	int k = 0;

	str = (char *)malloc(sizeof(str) * (count_size(argc, argv) + argc));
	if (str == 0)
		return (0);
	while (i < argc) {
		j = 0;
		while (argv[i][j] != '\0') {
			str[k] = argv[i][j];
			k++;
			j++;
		}
		if (j > 0)
			str[k] = '\n';
		k++;
		i++;
	}
	return (str);
}
