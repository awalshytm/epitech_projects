/*
** EPITECH PROJECT, 2017
** advanced_do-op
** File description:
** op functions
*/

void my_add(int nbr1, int nbr2)
{
	return (nbr1 + nbr2);
}

void my_sub(int nbr1, int nbr2)
{
	return (nbr1 - nbr2);
}

void my_mul(int nbr1, int nbr2)
{
	return (nbr1 * nbr2);
}

void my_div(int nbr1, int nbr2)
{
	return (nbr1 / nbr2);
}

void my_mod(int nbr1, int nbr2)
{
	return (nbr1 % nbr2);
}

void my_usage(int nbr1, int nbr2)
{
	...
}
