/*
** EPITECH PROJECT, 2017
** do-op
** File description:
** my_op, my_check
*/

#include "../include/do-op.h"

int my_check(int nbr1, char operand, int nbr2)
{
	if (nbr1 > 2147483647 || nbr1 < -2147483648)
		return (1);
	if (operand != '+' && operand != '-' && operand != '%' &&
	    operand != '*' && operand != '/')
		return (1);
	if (nbr2 > 2147483647 || nbr2 < -2147483648)
		return (1);
	return (0);
}

int my_op(int nbr1, char operand, int nbr2)
{
	if (operand == '+')
		return (nbr1 + nbr2);
	if (operand == '-')
		return (nbr1 - nbr2);	
       	if (operand == '/')
		return (nbr1 / nbr2);
	if (operand == '*')
		return (nbr1 * nbr2);
	if (operand == '%')
		return (nbr1 % nbr2);
	return (0);
}
