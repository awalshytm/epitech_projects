/*
** EPITECH PROJECT, 2017
** do-op
** File description:
** main and error
*/

#include "../include/my.h"
#include "../include/do-op.h"
#include <unistd.h>

int my_getnbr(char const *str)
{
	int a = 0;
	int nb = 0;
	int neg = 0;
	int b = 0;

	while (str[a] == '\n' || str[a] == ' ' || str[a] == '\t') {
		a++;
	}
	while (str[a] == 45 || str[a] == 43) {
		a++;
	}
	if (str[a - 1] == '-')
		neg = 1;
	while (str[a] >= '0' && str[a] <= '9') {
		nb = nb * 10 + str[a] - 48;
		a++;
	}
	return (neg == 1 ? nb * -1 : nb);
}

int errors(int nbr1, char operand, int nbr2)
{
	if (operand == '/' && nbr2 == 0) {
                        write(1, "Stop : division by zero\n", 24);
			return (84);
	} else if (operand == '%' && nbr2 == 0) {
                        write(1, "Stop : modulo by zero\n", 22);
			return (84);
	}
	return (0);
}

int main(int argc, char **argv)
{
	int nb1 = 0;
	int nb2 = 0;
	char op;

	if (argc == 4) {
		nb1 = my_getnbr(argv[1]);
		nb2 = my_getnbr(argv[3]);
		op = argv[2][0];
		if (errors(nb1, op, nb2) == 84)
			return (84);
		if (my_check(nb1, op, nb2) == 0 && errors(nb1, op, nb2) == 0) {
			my_put_nbr(my_op(nb1, op, nb2));
			my_putchar('\n');
		} else if (my_check(nb1, op, nb2) == 1) {
			write(1, "0\n", 2);
			return (84);
		}
	} else
		return (84);
	return (0);
}
