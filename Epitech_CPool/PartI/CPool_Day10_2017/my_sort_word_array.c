/*
** EPITECH PROJECT, 2017
** my_sort_word_array
** File description:
** sort by ascii via function pointer
*/

int swapping(char**argv, int a, int swap)
{
	char *tmp;

	while (--a > 1) {
		if (my_strcmp(argv[a], argv[a - 1]) < 0) {
			tmp = argv[a];
			argv[a] = argv[a - 1];
			argv[a - 1] = tmp;
			swap++;
		}
	}
	return (swap);
}

int size(char **str)
{
	int i = 0;

	while (str[i] != 0) {
		i++;
	}
	return (i);
}

int my_sort_word_array(char **tab)
{
	int swap = 1;
	int a = 0;

	while (swap != 0) {
		swap = 0;
		a = size(tab);
		swap = swapping(tab, a, swap);
	}
	return (0);
}
