/*
** EPITECH PROJECT, 2017
** my_advaced_sort_word_array
** File description:
** word_array
*/


int swapping(char**argv, int a, int swap, int (*cmp)(char *, char *))
{
	char *tmp;

	while (--a > 1) {
		if (cmp(argv[a], argv[a - 1]) < 0) {
			tmp = argv[a];
			argv[a] = argv[a - 1];
			argv[a - 1] = tmp;
			swap++;
		}
	}
	return (swap);
}

int size(char **str)
{
	int i = 0;

	while (str[i] != 0) {
		i++;
	}
	return (i);
}

int my_advanced_sort_word_array(char **tab, int (*cmp)(char *, char *))
{
	int swap = 1;
	int a = 0;

	while (swap != 0) {
		swap = 0;
		a = size(tab);
		swap = swapping(tab, a, swap, cmp);
	}
	print_params(size(tab), tab);
	return (0);	
}
