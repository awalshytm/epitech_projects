/*
** EPITECH PROJECT, 2017
** header file
** File description:
** prototypes
*/

#ifndef DO_OP_H_
	#define DO_OP_H_
	#define NON "nbr1, operand, nbr2"

int my_check(int nbr1, char operand, int nbr2);
int my_op(int nbr1, char operand, int nbr2);

#endif
