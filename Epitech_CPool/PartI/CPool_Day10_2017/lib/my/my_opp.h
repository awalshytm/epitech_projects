/*
** EPITECH PROJECT, 2017
** adveanced_do-op
** File description:
** my_opp.h
*/

#ifndef MY_OPP_H_
	#define MY_OPP_H_

t_opp gl_opptab[] = {{"+", &my_add}, {"-", &my_sub}, {"/", &my_div}, {"*", &my_mul}, {"%", &my_mod}, {"", &my_usage}};

#endif MY_OPP_H_
