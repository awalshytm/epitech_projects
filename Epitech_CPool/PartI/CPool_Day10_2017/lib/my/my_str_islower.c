/*
** EPITECH PROJECT, 2017
** my_islower
** File description:
** is lower case
*/

int my_str_islower(char const *str)
{
	int i = 0;

	if (str[0] == '\0')
		return (1);
	while (str[i] >= 97 && str[i] <= 122)
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}
