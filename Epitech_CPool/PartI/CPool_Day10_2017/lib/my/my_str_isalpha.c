/*
** EPITECH PROJECT, 2017
** my_isalpha
** File description:
** is only alpha
*/

int my_str_isalpha(char const *str)
{
	int i = 0;

	if (str[0] == '\0')
		return (1);
	while ((str[i] >= 65 && str[i] <= 90) ||
	       (str[i] >= 97 && str[i] <= 122))
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}
