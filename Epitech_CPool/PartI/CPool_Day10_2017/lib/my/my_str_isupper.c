/*
** EPITECH PROJECT, 2017
** my_isupper
** File description:
** is upper case
*/

int my_str_isupper(char const *str)
{
	int i = 0;

	if (str[0] == '\0')
		return (1);
	while (str[i] >= 65 && str[i] <= 90)
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}
