/*
** EPITECH PROJECT, 2017
** my_rev_params
** File description:
** print params in reversed
*/

int main(int argc, char **argv)
{
	int i = argc - 1;

	while (i >= 0) {
		my_putstr(argv[i]);
		my_putchar('\n');
		i--;
	}
	return (0);
}
