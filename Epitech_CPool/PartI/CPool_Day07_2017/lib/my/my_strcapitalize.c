/*
** EPITECH PROJECT, 2017
** my_strcapitalize
** File description:
** capitalize string
*/

char *strlowcase(char *str)
{
	int i = 0;

	while (str[i] != '\0') {
		if (str[i] >= 65 && str[i] <= 90)
			str[i] += 32;
		i++;
	}
	return (str);
}

char *my_strcapitalize(char *str)
{
	int i = 0;
	int j;

	str = strlowcase(str);
	while (str[i] != '\0') {
	       j = 0;
		if (str[i - 1] <= 57 && str[i - 1] >= 48)
			j = 1;
		while ((str[i] >= 65 && str[i] <= 90) ||
		       (str[i] >= 97 && str[i] <= 122)) {
			if ((str[i] >= 97 && str[i] <= 122) && j == 0)
				str[i] -= 32;
			i++;
			j++;
		}
		i++;
	}
	return (str);
}
