/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse string
*/

int my_strlen(char const *str)
{
	int i;

	while (str[i] != '\0') {
		i++;
	}
	return (i);
}

char *my_revstr(char *str)
{
	int i;
	int j = 0;
	char c;

	i = my_strlen(str) - 1;
	while (i != j && j <= i) {
		c = str[i];
		str[i] = str[j];
		str[j] = c;
		i--;
		j++;
	}
	return (str);
}
