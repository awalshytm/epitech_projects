/*
** EPITECH PROJECT, 2017
** my_isnum
** File description:
** check if all num
*/

int my_str_isnum(char const *str)
{
	int i = 0;

	if (str[0] == '\0')
		return (1);
	while (str[i] >= 48 && str[i] <= 57)
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}
