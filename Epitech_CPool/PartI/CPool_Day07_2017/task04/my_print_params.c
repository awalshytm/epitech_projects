/*
** EPITECH PROJECT, 2017
** my_print_params
** File description:
** print parameters
*/

int main(int argc, char **argv)
{
	int i = 0;

	while (i < argc) {
		my_putstr(argv[i]);
		if (i + 1 < argc)
			my_putchar('\n');
		i++;
	}
	return (0);
}
