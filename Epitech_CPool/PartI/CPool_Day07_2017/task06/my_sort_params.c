/*
** EPITECH PROJECT, 2017
** my_sort_params
** File description:
** sort params in ascii order
*/

void print_params(int argc, char **argv)
{
	int i = 0;

	while (i < argc)
	{
		my_putstr(argv[i]);
		my_putchar('\n');
		i++;
	}
}

int swapping(char**argv, int a, int swap)
{
	char *tmp;

	while (--a > 1) {
		if (my_strcmp(argv[a], argv[a - 1]) < 0) {
			tmp = argv[a];
			argv[a] = argv[a - 1];
			argv[a - 1] = tmp;
			swap++;
		}
	}
	return (swap);
}

void my_sort_params(int argc, char **argv)
{
	int swap = 1;
	int a = 0;

	while (swap != 0) {
		swap = 0;
		a = argc;
		swap = swapping(argv, a, swap);
	}
	print_params(argc, argv);
}

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		my_sort_params(argc, argv);
	}
	return (0);
}
