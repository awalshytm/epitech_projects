/*
** EPITECH PROJECT, 2017
** rush1-1
** File description:
** rush1 asignment 1
*/

void print_line(int x, char edge, char inter)
{
	int i = 0;

	if (x > 0)
		my_putchar(edge);
	while (i < (x - 2) && x >= 3) {
		my_putchar(inter);
		i++;
	}
	if (x >= 2)
		my_putchar(edge);
	my_putchar('\n');
}

void rush(int x, int y)
{
	int a = 0;

	if (x <= 0 || y <= 0)
		write(2, "Invalid size", 13);
	if (y != 1)
		write(2, "\n", 1);
	if (y > 0)
		print_line(x, 'o', '-');
	while (a < (y - 2) && y >= 3) {
		print_line(x, '|', ' ');
		a++;
	}
	if (y >= 2)
		print_line(x, 'o', '-');
}
