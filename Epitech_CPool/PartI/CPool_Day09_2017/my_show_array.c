/*
** EPITECH PROJECT, 2017
** my_params_to_array
** File description:
** params to array
*/

#include "my.h"

void my_print_array(char **str)
{
	int i = 0;
	int j;

	while (str[i] != 0) {
		j = 0;
		while (str[i][j] != '\0') {
			my_putchar(str[i][j]);
			j++;
		}
		my_putchar('\n');
		i++;
	}
}

void	my_show_tab(struct s_stock_par const *par)
{
	int i = 0;

	while (par[i].str != 0)
	{
		my_putstr(par[i].copy);
		my_putchar('\n');
		my_put_nbr(par[i].length);
		my_putchar('\n');
		my_print_array(par[i].tab);
		i++;
	}
}
