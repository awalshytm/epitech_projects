/*
** EPITECH PROJECT, 2017
** my_macroABS
** File description:
** macro return absolute value
*/

#ifndef MY_MACROABS_H_
#define MY_MACROABS_H_
# define ABS(value) ((value < 0) ? ((-1) * (value)) : (value))
#endif
