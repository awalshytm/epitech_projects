/*
** EPITECH PROJECT, 2017
** swap_endian_color
** File description:
** endian access to color
*/

union u_color
{
	int i;
	char color[4];
};

int swap_endian_color(int color)
{
	union u_color ucolor;
	char tmp;
	int i = 0;
	int j = 3;

	ucolor.i = color;
	while (i < j) {
		tmp = ucolor.color[i];
		ucolor.color[i] = ucolor.color[j];
		ucolor.color[j] = tmp;
		i++;
		j--;
	}
	return (ucolor.i);
}
