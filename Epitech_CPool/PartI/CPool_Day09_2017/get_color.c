/*
** EPITECH PROJECT, 2017
** get_color
** File description:
** convert RGB colors
*/

int get_color(unsigned char red, unsigned char green, unsigned char blue)
{
	int i = 0;

	i |= red;
	i <<= 8;
	i |= green;
	i <<= 8;
	i |= blue;
	return (i);
}
