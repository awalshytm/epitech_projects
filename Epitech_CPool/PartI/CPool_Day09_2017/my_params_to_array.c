/*
** EPITECH PROJECT, 2017
** my_params_to_tab
** File description:
** parans to tab
*/

struct info_params *my_params_to_array(int ac, char **av)
{
        int i = 0;
        int j;
        struct info_params *src;

        src = (struct info_params *)malloc(sizeof(struct info_params) * (ac + 1));
        if (src == 0)
                return (0);
        while (i < ac) {
                j = 0;
                while (av[i][j] != '\0')
                        j++;
                src[i].length = j;
                src[i].str = av[i];
                src[i].copy = my_strdup(av[i]);
                src[i].word_array = my_str_to_word_array(src[i].str);
                i++;
        }
        src[i].str = 0;
        return (&src[0]);
}

