/*
** EPITECH PROJECT, 2017
** FinalStumper
** File description:
** main + error
*/

#include "../include/final_stumper.h"
#include <stdio.h>

int main()
{
	char buff[BUFF_SIZE + 1];
	int offset = 0;
	int len = 0;

	while ((len = read(0, buff + offset, BUFF_SIZE - offset)) > 0) {
		offset += len;
	}
	buff[offset] = '\0';
	if (len < 0)
		return (ERROR);
	rush3(buff);
	return (OK);
}
