/*
** EPITECH PROJECT, 2017
** FinalStumper
** File description:
** print to screen ft
*/

#include "../include/final_stumper.h"

void print_result(char *rush, int x, int y)
{
	int i = 0;

	while (i < my_strlen(rush)) {
		my_putstr("[rush1-");
		my_putchar(rush[i]);
		my_putstr("] ");
		my_put_nbr(x);
		my_putchar(' ');
		my_put_nbr(y);
		if (i + 1 < my_strlen(rush))
			my_putstr(" || ");
		i++;
	}
	my_putchar('\n');
}
