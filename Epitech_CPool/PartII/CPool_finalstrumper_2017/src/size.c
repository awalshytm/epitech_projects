/*
** EPITECH PROJECT, 2017
** FinalStumper
** File description:
** size x and y
*/

#include "../include/final_stumper.h"

int my_x_size(char *buff)
{
	int size = 0;

	while (buff[size] != '\n')
		size++;
	return (size);
}

int my_y_size(char *buff)
{
	int i = 0;
	int y = 0;

	while (buff[i]) {
		if (buff[i] == '\n')
			y++;
		i++;
	}
	return (y);
}
