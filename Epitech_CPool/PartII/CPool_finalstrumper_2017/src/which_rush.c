/*
** EPITECH PROJECT, 2017
** FinalStumper
** File description:
** which rush
*/

#include "../include/final_stumper.h"

char *three_to_five(char *buff, char *result)
{
	char corner_tr;
	char corner_br;
	int i = 0;

	while (buff[i] != '\n')
		i++;
	corner_tr = buff[i - 1];
	while (buff[i] != '\0')
		i++;
	corner_br = buff[i -2];
	if (corner_tr == 'A')
		result[0] = '3';
	if (corner_tr == 'C' && corner_br == 'C')
		result[0] = '4';
	if (corner_tr == 'C' && corner_br == 'A')
		result[0] = '5';
	result[1] = '\0';
	return (result);
}

char *which_rush_is_that(char *buff, char *result, int x, int y)
{
	int i = 0;

	if (buff[0] == 'o') {
		result[i] = '1';
		i++;
	}
	if (buff[0] == '*' || buff[0] == '/') {
		result[i] = '2';
		i++;
	}
	if (buff[0] == 'A'&& (x > 1 && y > 1))
		return (three_to_five(buff, result));
	if (buff[0] == 'B') {
		while (i < 3) {
			result[i] = '3' + i;
			i++;
		}
	}
	result[i] = '\0';
	return (result);
}
