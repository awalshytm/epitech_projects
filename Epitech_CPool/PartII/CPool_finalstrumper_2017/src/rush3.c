/*
** EPITECH PROJECT, 2017
** FinalStumper
** File description:
** rush3 ft
*/

#include "../include/final_stumper.h"

void rush3(char *buff)
{
	int x = 0;
	int y = 0;
	char rush[4];

	x = my_x_size(buff);
	y = my_y_size(buff);
	which_rush_is_that(buff, rush, x, y);
	print_result(rush, x, y);
}
