#!/bin/bash
echo "we are going to test the program : final Stumper."
sleep 3
echo "Aim test01: Try the program to see if he can compare differents rush1 (x)"
sleep 4
echo "test01"
./bin/rush1-1 1 1 | ./test
./bin/rush1-2 1 1 | ./test
./bin/rush1-3 1 1 | ./test
./bin/rush1-4 1 1 | ./test
./bin/rush1-5 1 1 | ./test
sleep 1
echo "test01 done"
sleep 4
echo "Aim test02: Try the program to see if he can compare differents rush1 with a bigger size (x)"
sleep 4
echo "test02 in 5 sec"
sleep 5
./bin/rush1-1 2 1 | ./test
./bin/rush1-2 2 1 | ./test
./bin/rush1-3 2 1 | ./test
./bin/rush1-4 2 1 | ./test
./bin/rush1-5 2 1 | ./test
sleep 1
echo "test02 done"
sleep 4
echo "Aim test03: Try the program to see if he can compare differents rush1 (y)"
sleep 4
echo "test03 in 5 sec"
sleep 5
./bin/rush1-1 1 2 | ./test
./bin/rush1-2 1 2 | ./test
./bin/rush1-3 1 2 | ./test
./bin/rush1-4 1 2 | ./test
./bin/rush1-5 1 2 | ./test
sleep 1
echo "test03 done"
sleep 4
echo "Aim test04: Try the program to see if he can compare differents rush1 with a bigger size (y)"
sleep 4
echo "test04 in 5 sec"
sleep 5
./bin/rush1-1 1 3 | ./test
./bin/rush1-2 1 3 | ./test
./bin/rush1-3 1 3 | ./test
./bin/rush1-4 1 3 | ./test
./bin/rush1-5 1 3 | ./test
sleep 1
echo "test04 done"
sleep 4
echo "Aim test05: Try the program to see if we can turn with a rush1 able to display something with a paremeter x and y >= 2"
sleep 5
echo "test05 in 5 sec"
sleep 5
./bin/rush1-1 2 2 | ./test
./bin/rush1-2 2 2 | ./test
./bin/rush1-3 2 2 | ./test
./bin/rush1-4 2 2 | ./test
./bin/rush1-5 2 2 | ./test
sleep 1
echo "test05 done"
sleep 5
echo "Aim test06: Try the program to see if we can turn with a rush1 able to display something with a paremeter x and y >= 2"
sleep 5
echo "test06 in 5 sec"
sleep 5
./bin/rush1-1 3 3 | ./test
./bin/rush1-2 3 3 | ./test
./bin/rush1-3 3 3 | ./test
./bin/rush1-4 3 3 | ./test
./bin/rush1-5 3 3 | ./test
sleep 1
echo "test06 done"
sleep 3
echo "done"
sleep 1
echo "all test finished"
