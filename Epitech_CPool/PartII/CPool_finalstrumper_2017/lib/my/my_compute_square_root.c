/*
** EPITECH PROJECT, 2017
** my_compute_sqaure_root
** File description:
** square_root
*/

int my_compute_square_root(int nb)
{
	int a = 1;
	int b = 0;

	if (nb <= 0)
		return (0);
	while (b < nb) {
		b = a * a;
		if (b == nb)
			return (a);
		if (b > nb)
			return (0);
		else
			a++;
	}
	return (0);
}
