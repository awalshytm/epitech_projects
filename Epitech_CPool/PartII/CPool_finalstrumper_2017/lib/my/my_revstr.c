/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse string
*/

int my_strlength1(char const *str)
{
	int i;

	while (str[i] != '\0') {
		i++;
	}
	return (i);
}

char *my_revstr(char *str)
{
	int i = 0;
	int j = 0;
	char c;

	i = my_strlength1(str) - 1;
	while (j < i) {
		c = str[i];
		str[i] = str[j];
		str[j] = c;
		i--;
		j++;
	}
	return (str);
}
