/*
** EPITECH PROJECT, 2017
** InfinAdd
** File description:
** utilitairy functions
*/

#include "../include/my.h"

int biggest(char *str1, char *str2)
{
	int i;
	int j;

	i = my_strlen(str1);
	j = my_strlen(str2);
	if (i > j)
		return (i);
	return (j);
}

char *my_fill(char *str, int bigg)
{
	int i = 0;

	while (str[i])
		i++;
	while (i < bigg) {
		str[i] = '0';
		i++;
	}
	return (str);
}

char *my_strrev(char *str)
{
	int i = my_strlen(str) - 1;
	int j = 0;
	char c;

	while (i > j) {
		c = str[i];
		str[i] = str[j];
		str[j] = c;
		i--;
		j++;
	}
	return (str);
}
