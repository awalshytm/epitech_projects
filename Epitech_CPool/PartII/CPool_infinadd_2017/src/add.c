/*
** EPITECH PROJECT, 2017
** InfinAdd
** File description:
** add function
*/

#include "../include/my.h"

char addition(char nbr1, char nbr2, int *detention)
{
	char result;
	int i = nbr1 - '0';
	int j = nbr2 - '0';
	int res = 0;
	int det = *detention;

	res = i + j;
	if ((res + det) >= 10) {
		*detention = 1;
		return (((res - 10) + det) + '0');
	} else {
		*detention = 0;
		return ((res + det) + '0');
	}
}

char *add(char *result, char **nbr, int bigg)
{
	int i = 0;
	int j = 0;
	int k = 0;
	int detention = 0;
	int *d;
	char *n1;
	char *n2;

	n1 = my_fill(my_strrev(my_strdup(nbr[1])), bigg);
	n2 = my_fill(my_strrev(my_strdup(nbr[2])), bigg);
	d = &detention;
	while (i < bigg && j < bigg) {
		result[k] = addition(n1[i], n2[j], d);
		k++;
		i++;
		j++;
	}
	if (detention == 1)
		result[k] = '1';
	return (my_strrev(result));
}
