/*
** EPITECH PROJECT, 2017
** InfinAdd
** File description:
** main + errors
*/

#include "../include/my.h"

int my_error(char *nbr1, char *nbr2)
{
	int i = 0;

	while (nbr1[i] != '\0') {
		if (nbr1[i] > 57 || (nbr1[i] < 48 && nbr1[i] != '-'))
			return (ERROR);
		i++;
	}
	i = 0;
	while (nbr2[i] != '\0') {
		if (nbr2[i] > 57 || (nbr2[i] < 48 && nbr2[i] != '-'))
			return (ERROR);
		i++;
	}
	return (OK);
}

int main(int argc, char **argv)
{
	char *result;
	int bigg;

	if (argc == 3 && my_error(argv[1], argv[2]) == 0) {
		bigg = biggest(argv[1], argv[2]);
		result = malloc(sizeof(char) * (bigg + 1));
		my_putstr(add(result, argv, bigg));
	}
	else
		return (ERROR);
	return (OK);
}
