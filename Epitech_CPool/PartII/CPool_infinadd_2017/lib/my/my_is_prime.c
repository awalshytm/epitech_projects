/*
** EPITECH PROJECT, 2017
** my_is_prime
** File description:
** is number is pri;e
*/

int my_is_prime(int nb)
{
	int a = 1;
	int b = 2;
	int prime = 1;

	if (nb <= 1)
		return (0);
	if (nb == 2)
		return (1);
	while (a <= nb) {
		if (nb % b == 0)
			prime = 0;
		b++;
		a = b * b;
	}
	if (prime == 1)
		return (1);
	return (0);
}
