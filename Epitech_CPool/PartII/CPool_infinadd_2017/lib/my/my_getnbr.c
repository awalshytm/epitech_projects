/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** atoi
*/

int my_getnbr(char const *str)
{
	int a = 0;
	int nb = 0;
	int neg = 0;
	int b = 0;

	while (str[a] == '\n' || str[a] == ' ' || str[a] == '\t') {
		a++;
	}
	while (str[a] == 45 || str[a] == 43) {
		a++;
	}
	if (str[a - 1] == '-')
		neg = 1;
	while (str[a] >= '0' && str[a] <= '9') {
		nb = nb * 10 + str[a] - 48;
		a++;
	}
	return (neg == 1 ? nb * -1 : nb);
}
