/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** dup ;alloc
*/

#include <stdlib.h>

int my_strlength(char const *str)
{
	int i = 0;

	while (str[i] != '\0')
		i++;
	return (i);
}

char *my_strdup(char const *src)
{
	char *str;
	int i = 0;
	
	if (!(str = (char *)malloc(sizeof(src) * (my_strlength(src) + 1))))
		return (0);
	while (src[i] != '\0') {
		str[i] = src[i];
		i++;
	}
	return (str);
}
