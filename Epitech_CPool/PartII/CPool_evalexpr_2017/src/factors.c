/*
** EPITECH PROJECT, 2017
** EvalExpr
** File description:
** factors ft
*/

#include "header.h"

int factors(char **str)
{
	int nb = my_strtols(str[0], str);

	while (str[0][0] != '\0') {
		switch (str[0][0]) {
		case '*':
			nb *= my_strtols(str[0], str);
			break;
		case '/':
			nb /= my_strtols(str[0], str);
			break;
		case '%':
			nb %= my_strtols(str[0], str);
			break;
		default:
			return (nb);
		}
	}
	return (nb);
}
