/*
** EPITECH PROJECT, 2017
** EvalExpr
** File description:
** eval_expr + summands ft
*/

#include "header.h"

int summands(char **str)
{
	int nb = 0;

	while (str[0][0] != '\0') {
		if (str[0][0] == ')') {
			str[0] = &str[0][1];
			return (nb);
		}
		nb += factors(str);
	}
	return (nb);
}

int eval_expr(char *str)
{
	int nb = 0;

	nb = summands(&str);
	return (nb);
}
