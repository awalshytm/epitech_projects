/*
** EPITECH PROJECT, 2017
** EvalExpr
** File description:
** strtols ft
*/

#include "header.h"

int my_strtols(char *str, char **endptr)
{
	int i = 0;
	int nb = 0;
	int neg = 0;

	while ((str[i] > 57 || str[i] < 48) &&
	       str[i] != '-' && str[i] != '(')
		i++;
	if (str[i] == '(') {
		if (str[i - 1] == '-')
			neg = 1;
		endptr[0] = &str[i + 1];
		nb = summands(endptr);
		return (neg == 1 ? nb * -1 : nb);
	}
	if (str[i] == '-') {
		neg = 1;
		i++;
        }
	while ((str[i] == '-' || str[i] == '+') &&
	       (str[i] > 57 || str[i] < 48))
		i++;
	while (str[i] <= '9' && str[i] >= '0') {
		nb = nb * 10 + str[i] - 48;
		i++;
	}
	endptr[0] = &str[i];
	return (neg == 1 ? nb * -1 : nb);
}
