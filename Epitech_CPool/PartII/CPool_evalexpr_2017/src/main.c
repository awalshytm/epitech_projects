/*
** EPITECH PROJECT, 2017
** EvalExpr
** File description:
** main + error
*/

#include "header.h"

int my_check(char *str)
{
	int i = 0;

	while (str[i]) {
		if ((str[i] < 48 || str[i] > 57) && str[i] != '+' &&
		    str[i] != '-' && str[i] != '/' && str[i] != '*' &&
		    str[i] != '%' && str[i] != ' ' && str[i] != '(' && str[i] != ')') {
			return (ERROR);
		}
		i++;
	}
	return (OK);
}

int main(int argc, char **argv)
{
	if (argc == 2 && my_check(argv[1]) == 0) {
		my_put_nbr(eval_expr(argv[1]));
		my_putchar('\n');
	}
	else
		return (ERROR);
	return (OK);
}
