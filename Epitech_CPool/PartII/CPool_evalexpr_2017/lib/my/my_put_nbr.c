/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** output number
*/

void my_putchar(char c);

int	my_put_nbr(int nb)
{
	long	int	i;

	i = nb;
	if (i < 0) {
		my_putchar('-');
		i = i * -1;
	}
	if (i >= 10)
		my_put_nbr(i / 10);
	my_putchar(i % 10 + 48);
	return 0;
}
