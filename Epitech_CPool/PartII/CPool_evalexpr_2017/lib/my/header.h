/*
** EPITECH PROJECT, 2017
** EvalExpr
** File description:
** header
*/

#ifndef HEADER_H_
# define HEADER_H_

/*functions*/
int my_strtols(char *str, char **endptr);
int factors(char **str);
int summands(char **str);
void my_put_nbr(int nb);
void my_putchar(char c);
int eval_expr(char *str);

/*variables*/
# define ERROR 84
# define OK 0

#endif
