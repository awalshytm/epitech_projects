/*
** EPITECH PROJECT, 2017
** addition
** File description:
** addition module to infin add
*/

#include <stdlib.h>
#include "infinadd.h"
#include "my.h"
#include "tools.h"

char *make_add(number_t *nb1, number_t *nb2, int pos)
{
	char *ans;
	int tmp;
	int ret = 0;
	int i;

	ans = malloc(sizeof(char) * (nb1->len + 4));
	for (int a = 0 ; a < nb1->len + 4 ; a++)
		ans[a] = 0;
	for (i = 0 ; nb1->nb[i] != 0 ; i++) {
		tmp = ret + nb1->nb[i] + nb2->nb[i] - 96;
		ret = 0;
		if (tmp > 9) {
			ret = 1;
			tmp -= 10;
		}
		ans[i] = tmp + '0';
	}
	if (ret == 1) {
		ans[i] = '1';
		i++;
	}
	if (pos == -1)
		ans[i] = '-';
	return (my_revstr(ans));
}

char *add_pos(number_t *nb1, number_t *nb2, int pos)
{
	char *ans;

	if (pos == -1) {
		nb1->nb = &nb1->nb[1];
		nb2->nb = &nb2->nb[1];
	}
	if (nb1->len > nb2->len)
		nb2->nb = make_same_length(nb2->nb, nb1->len, nb2->len);
	else
		nb1->nb = make_same_length(nb1->nb, nb2->len, nb1->len);
	nb1->nb = my_revstr(nb1->nb);
	nb2->nb = my_revstr(nb2->nb);
	ans = make_add(nb1, nb2, pos);
	return (ans);
}
