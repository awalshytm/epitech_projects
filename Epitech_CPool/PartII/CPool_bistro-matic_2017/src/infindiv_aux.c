/*
** EPITECH PROJECT, 2017
** infindiv_aux
** File description:
** auxiliary to infindiv
*/

#include <stdlib.h>
#include "tools.h"
#include "my.h"

char *minus(number_t *num)
{
	char *str = malloc(sizeof(char) * num->len + 2);
	char *tmp = my_strdup(num->nb);

	str = my_revstr(tmp);
	str[num->len] = '-';
	str[num->len + 1] = 0;
	return (my_revstr(str));
}

int check_for_zeros(number_t *num)
{
	int cnt = 0;

	for (int i = 0 ; i < num->len ; i++)
		if (num->nb[i] == '0')
			cnt++;
	if (cnt == num->len)
		return (1);
	return (0);
}

int check_div_zero(number_t *denom)
{
	int i = 0;

	while (denom->nb[i] == '0' && i < denom->len)
		i++;
	return (i == denom->len ? 1 : 0);
}
