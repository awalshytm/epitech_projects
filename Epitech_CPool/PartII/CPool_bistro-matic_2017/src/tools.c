/*
** EPITECH PROJECT, 2017
** tools
** File description:
** tools module for infin add
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "tools.h"

char itc(int digit)
{
	return (digit + '0');
}

int cti(char a)
{
	if (a < '0' || a > '9')
		return (0);
	return (a - '0');
}

char *make_same_length(char *small_nb, int len1, int len2)
{
	char *new_nb = malloc(sizeof(char) * len1);
	int j = 0;

	for (int i = 0 ; i < (len1 - len2) ; i++)
		new_nb[i] = '0';
	for (int i = len1 - len2 ; i < len1 ; i++) {
		new_nb[i] = small_nb[j];
		j++;
	}
	return (new_nb);
}

int my_putstr_error(char *str)
{
	for (int i = 0 ; str[i] != 0 ; i++)
		write(2, &str[i], 1);
	return (0);
}

number_t *attrib_num(char const *str)
{
	number_t *number = malloc(sizeof(*number));
	char *tmp = my_revstr(my_strdup(str));
	int i = my_strlen(str) - 1;
	int pos = 1;

	for ( ; (tmp[i] == '0' || tmp[i] == '-') && i >= 0 ; i--) {
		if (tmp[i] == '-')
			pos *= -1;
		tmp[i] = 0;
	}
	if (i == -1)
		tmp = "0";
	else if (pos == -1)
		tmp[i + 1] = '-';
	number->nb = my_revstr(tmp);
	number->len = my_strlen(tmp);
	return (number);
}
