/*
** EPITECH PROJECT, 2017
** infindiv
** File description:
** makes division of infinit numbers
*/

#include <stdlib.h>
#include "tools.h"
#include "infindiv_aux.h"
#include "infinadd.h"
#include "my.h"

int str_eq_cmp(number_t *num_eq, number_t *denom)
{
	if (num_eq->len > denom->len)
		return (1);
	if (num_eq->len < denom->len)
		return (0);
	return (my_strcmp(num_eq->nb, denom->nb) >= 0 ? 1 : 0);
}

number_t *init_num(number_t *num, number_t *denom)
{
	int i = 0;
	number_t *num_eq;

	if (num->len == denom->len)
		num_eq = attrib_num(num->nb);
	else {
		num_eq = malloc(sizeof(*num_eq));
		i = (my_strncmp(num->nb, denom->nb, denom->len) > 0 ? 0 : 1);
		num_eq->nb = malloc(sizeof(char) * (denom->len + i));
		num_eq->nb = my_strncpy(num_eq->nb, num->nb, denom->len + i);
		num_eq->len = (denom->len  + i);
	}
	return (num_eq);
}

number_t *change_num(number_t *num, number_t *num_eq, int save_len)
{
	int i;
	number_t *new_num = malloc(sizeof(*new_num));

	new_num->nb = malloc(sizeof(char) * (num_eq->len + (num->len - save_len) + 1));
	for (i = 0 ; i < num_eq->len ; i++)
		new_num->nb[i] = num_eq->nb[i];
	for (int j = 0 ; j < (num->len - save_len) ; j++) {
		new_num->nb[i] = num->nb[save_len + j];
		i++;
	}
	new_num->nb[i] = 0;
	new_num->len = my_strlen(new_num->nb);
	return (new_num);
}

char *infin_div_main(number_t *num, number_t *denom, int check)
{
	number_t *num_eq;
	int i = 0;
	int j = 0;
	int save_len;
	char *res = malloc(sizeof(char) * (num->len + 1));

	while (str_eq_cmp(num, denom)) {
		num_eq = init_num(num, denom);
		save_len = num_eq->len;
		for (i = 0 ; str_eq_cmp(num_eq, denom); i++) {
			if (check_for_zeros(num_eq) == 1) {
				num_eq->nb = "0";
				num_eq->len = 1;
				break;
			} else {
				num_eq->nb = infinadd(num_eq->nb, minus(denom));
				num_eq->len = my_strlen(num_eq->nb);
			}
		}
		res[j] = itc(i);
		j++;
		num = change_num(num, num_eq, save_len);
	}
	res[j] = 0;
	return (check == 1 ? res : num_eq->nb);
}

char *infindiv(char const *str1, char const *str2, char c)
{
	number_t *num = attrib_num(str1);
	number_t *denom = attrib_num(str2);

	if (check_div_zero(denom)) {
		my_putstr_error("Division by zero !");
		exit (84);
	}
	if (c == '/') {
		if (num->len < denom->len ||
		    (num->len == denom->len && my_strcmp(num->nb, denom->nb) == -1))
			return ("0");
		return (infin_div_main(num, denom, 1));
	} else if (c == '%') {
		if (num->len < denom->len ||
		    (num->len == denom->len && my_strcmp(num->nb, denom->nb) == -1))
			return (num->nb);
		return (infin_div_main(num, denom, 2));
	}
	return (0);
}
