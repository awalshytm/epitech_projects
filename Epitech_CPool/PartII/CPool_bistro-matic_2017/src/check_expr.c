/*
** EPITECH PROJECT, 2017
** Bistro
** File description:
** check_expr
*/

#include <stdlib.h>
#include "bistro.h"
#include "my.h"

int check_expr_ops(char expr, char *ops)
{
	for (int k = 0 ; ops[k] != 0 ; k++)
		if (expr == ops[k])
			return (1);
	return (0);
}

int check_expr_base(char expr, char *ops, char *base)
{
	for (int j = 0 ; base[j] != 0 ; j++)
		if (expr == base[j])
			return (1);
	return (check_expr_ops(expr, ops));
}

void check_expr(char *expr, char *ops, char *base)
{
	int cnt = 0;

	for (int i = 0 ; expr[i] != '\0' ; i++) {
		cnt = check_expr_base(expr[i], ops, base);
		if (cnt == 0) {
			my_putstr(SYNTAX_ERROR_MSG);
			exit(EXIT_EXPR);
		}
	}
}
