/*
** EPITECH PROJECT, 2017
** eval_expr
** File description:
** eval_expr file
*/

#include "bistro.h"
#include "summands.h"
#include "infin_getnbr_base.h"
#include "convert_to_base.h"

char *replace_ops(char *ops, char *expr)
{
	int j;

	for (int i = 0; expr[i]; ++i) {
		j = 0;
		while (ops[j] != expr[i] && j < 8)
			j++;
		if (j == OP_OPEN_PARENT_IDX)
			expr[i] = '(';
		else if (j == OP_CLOSE_PARENT_IDX)
			expr[i] = ')';
		else if (j == OP_PLUS_IDX)
			expr[i] = '+';
		else if (j == OP_SUB_IDX)
			expr[i] = '-';
		else if (j == OP_MULT_IDX)
			expr[i] = '*';
		else if (j == OP_DIV_IDX)
			expr[i] = '/';
		else if (j == OP_MOD_IDX)
			expr[i] = '%';
	}
	return (expr);
}

char *eval_expr(char *base, char *ops, char *expr)
{
	expr = convert_to_base(expr, base);
	expr = replace_ops(ops, expr);
	return (summands(&expr));
}
