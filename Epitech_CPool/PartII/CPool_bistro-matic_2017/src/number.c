/*
** EPITECH PROJECT, 2017
** number
** File description:
** returns the first number given as a string
*/

#include <stdlib.h>
#include "bistro.h"
#include "infinadd.h"
#include "summands.h"
#include "my.h"

int check_neg(char c)
{
	return ((c == '-') ? -1 : 1);
}

char *create_numstr(int i, int j, char **str, int pos)
{
	int n = 0;
	int len = i - j;
	char *num;

	if (pos == -1) {
		++len;
		num = malloc(sizeof(char) * (len + 1));
		num[0] = '-';
		n++;
	}
	if (pos == 1)
		num = malloc(sizeof(char) * (len + 1));
	num[len] = 0;
	for(int m = 0 ; n < len ; m++) {
		num[n] = str[0][j + m];
		n++;
	}
	*str = &str[0][i];
	return (num);
}

char *number(char **str)
{
	int i = 0;
	int j = 0;
	char *res = NULL;
	int pos = 1;

	while ((str[0][j] > '9' || str[0][j] < '0')
	       && (str[0][j] != 0)) {
		if (str[0][j] == '(') {
			*str = &str[0][j + 1];
			res = summands(str);
			res = create_numstr(my_strlen(res), 0, &res, pos);
			return (res);
		}
		pos *= check_neg(str[0][j]);
		j++;
	}
	i = j;
	while (str[0][i] <= '9' && str[0][i] >= '0')
		i++;
	res = create_numstr(i, j, str, pos);
	return (res);
}
