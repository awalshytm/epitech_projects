/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** adds two strings together
*/

#include <stdlib.h>
#include "addition.h"
#include "substraction.h"
#include "tools.h"
#include "my.h"

int is_pos(char *str)
{
	if (str[0] == '-')
		return (-1);
	return (1);
}

char *infinadd(char const *num1, char const *num2)
{
	number_t *n1;
	number_t *n2 = attrib_num(num2);
	char *ans;

	if (num1 == NULL)
		return (n2->nb);
	n1 = attrib_num(num1);
	if (is_pos(n1->nb) == is_pos(n2->nb)) {
		if (n1->nb[0] == '-')
			ans = add_pos(n1, n2, -1);
		else
			ans = add_pos(n1, n2, 1);
	}
	else
		ans = (n1->nb[0] == '-' ? add_neg(n2, n1) : add_neg(n1, n2));
	free(n1);
	free(n2);
	return (ans);
}
