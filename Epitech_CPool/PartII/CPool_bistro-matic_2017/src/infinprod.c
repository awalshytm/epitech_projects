/*
** EPITECH PROJECT, 2017
** Infinprod
** File description:
** makes product of infinit numbers
*/

#include <stdlib.h>
#include "my.h"
#include "tools.h"

int get_pos(number_t **num1, number_t **num2)
{
	int pos = 1;

	while (num1[0]->nb[0] == '-' || num2[0]->nb[0] == '-') {
		if(num1[0]->nb[0] == '-') {
			pos *= -1;
			num1[0]->nb = &num1[0]->nb[1];
			num1[0]->len--;
		}
		if (num2[0]->nb[0] == '-') {
			pos *= -1;
			num2[0]->nb = &num2[0]->nb[1];
			num2[0]->len--;
		}
	}
	return (pos);
}

char *make_prod(number_t *n1, number_t *n2, int *ret, char *res)
{
	int j = 0;
	int calc = 0;

	for (int i = 0 ; i < n1->len ; i++) {
		ret[0] = 0;
		for (j = 0 ; j < n2->len ; j++) {
			calc = ret[0] + cti(res[i + j]) + cti(n1->nb[i]) * cti(n2->nb[j]);
			ret[0] = calc / 10;
			calc %= 10;
			res[i + j] = itc(calc);
			while (res[j + i] > '9') {
				res[j + i] -= '0';
				ret[0]++;
			}
		}
		if (ret[0] > 0) {
			if (res[i + j] >= '0')
				res[i + j] -= '0';
			res[i + j] += itc(ret[0]);
		}
	}
	return (res);
}

char *infinprod(char const *str1, char const *str2)
{
	number_t *num1 = attrib_num(str1);
	number_t *num2 = attrib_num(str2);
	char *res = malloc(sizeof(char) * (num1->len + num2->len + 1));
	int ret = 0;
	int pos = get_pos(&num1, &num2);

	for (int h = 0 ; h <= num1->len + num2->len ; h++)
		res[h] = 0;
	num1->nb = my_revstr(num1->nb);
	num2->nb = my_revstr(num2->nb);
	res = make_prod(num1, num2, &ret, res);
	if (ret > 0)
		res[num1->len + num2->len - 1] = itc(ret);
	if (pos == -1 && ret != 0)
		res[num1->len + num2->len] = '-';
	else if (pos == -1 && ret == 0)
		res[num1->len + num2->len - 1] = '-';
	return (my_revstr(res));
}
