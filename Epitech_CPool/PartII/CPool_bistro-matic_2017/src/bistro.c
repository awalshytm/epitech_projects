/*
** EPITECH PROJECT, 2017
** main
** File description:
** main function for evalexpr
*/

#include <stdlib.h>
#include <unistd.h>
#include "bistro.h"
#include "eval_expr.h"
#include "check_expr.h"
#include "infin_putnbr_base.h"
#include "my.h"

void check_base(char const *base)
{
	if (my_strlen(base) < 2) {
		my_putstr(SYNTAX_ERROR_MSG);
		exit(EXIT_BASE);
	}
}

char *get_expr(unsigned int size)
{
	char *expr;

	if (size <= 0) {
		my_putstr(SYNTAX_ERROR_MSG);
		exit(EXIT_SIZE_NEG);
	}
	expr = malloc(size + 1);
	if (expr == 0) {
		my_putstr(ERROR_MSG);
		exit(EXIT_MALLOC);
	}
	if (read(0, expr, size) != size) {
		my_putstr(ERROR_MSG);
		exit(EXIT_READ);
	}
	expr[size] = 0;
	return (expr);
}

void check_ops(char const *ops)
{
	if (my_strlen(ops) != 7) {
		my_putstr(SYNTAX_ERROR_MSG);
		exit(EXIT_OPS);
	}
}

void usage_h(char *prog_name)
{
	my_putstr("USAGE\n\t   ");
	my_putstr(prog_name);
	my_putstr(" base operators size_read\n\n");
	my_putstr("DESCRIPTION\n");
	my_putstr("\t   base      all the symbols of the base\n");
	my_putstr("\t   operators the symbols for the parentheses and the 5 operators\n");
	my_putstr("\t   size_read number of characters to be read\n");
}

int main(int ac, char **av)
{
	unsigned int size;
	char *expr;

	if (av[1][0] == '-' && av[1][1] == 'h') {
		usage_h(av[0]);
		return (EXIT_SUCCESS);
	}
	if (ac != 4) {
		my_putstr("Usage: ");
		my_putstr(av[0]);
		my_putstr(" base ops\"+-*/%\" exp_len\n");
		return (EXIT_USAGE);
	}
	check_base(av[1]);
	check_ops(av[2]);
	size = my_getnbr(av[3]);
	expr = get_expr(size);
	check_expr(expr, av[2], av[1]);
	infin_putnbr_base(eval_expr(av[1], av[2], expr), av[1]);
	return (EXIT_SUCCESS);
}
