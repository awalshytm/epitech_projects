/*
** EPITECH PROJECT, 2017
** Bistro
** File description:
** infin power
*/

#include <stdlib.h>
#include "my.h"
#include "infinprod.h"

char *infinpower(char *nb, int p)
{
	char *nb_final = malloc(sizeof(char) * (my_strlen(nb) * 2));

	nb_final = nb;
	if (p == 0)
		return ("1");
	if (p < 0)
		return ("0");
	while (p > 1) {
		nb_final = infinprod(nb_final, nb);
		p--;
	}
	return (nb_final);
}
