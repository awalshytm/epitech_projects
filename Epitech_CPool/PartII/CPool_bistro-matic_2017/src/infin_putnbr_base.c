/*
** EPITECH PROJECT, 2017
** Bistro
** File description:
** infin_put_nbr_base
*/

#include "my.h"
#include "infin_getnbr_base.h"
#include "infindiv.h"

void infin_putnbr_base(char *nb, char *base)
{
	char *divisor = itcs(my_strlen(base));

	if (nb[0] > '0') {
		infin_putnbr_base(infindiv(nb, divisor, '/'), base);
		my_putchar(base[my_getnbr(infindiv(nb, divisor, '%'))]);
	}
}
