/*
** EPITECH PROJECT, 2017
** summands
** File description:
** sums the expression given as argument
*/

#include <stdlib.h>
#include "factors.h"
#include "infinadd.h"

char *summands(char **str)
{
	char *res = NULL;

	while (*str[0] != 0) {
		if (*str[0] == ')') {
			*str = &str[0][1];
			return (res);
		}
		res = infinadd(res, factors(str));
	}
	return (res);
}
