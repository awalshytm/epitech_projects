/*
** EPITECH PROJECT, 2017
** factors
** File description:
** returns the product given as parameter
*/

#include "number.h"
#include "infinprod.h"
#include "infindiv.h"

char *factors(char **str)
{
	char *res = number(str);

	while (*str[0] != 0) {
		switch (*str[0]) {
		case '*':
			*str = &str[0][1];
			res = infinprod(res, number(str));
			break;
		case '/':
			*str = &str[0][1];
			res = infindiv(res, number(str), '/');
			break;
		case '%':
			*str = &str[0][1];
			res = infindiv(res, number(str), '%');
			break;
		case ' ':
			*str = &str[0][1];
			break;
		default:
			return (res);
		}
	}
	return (res);
}
