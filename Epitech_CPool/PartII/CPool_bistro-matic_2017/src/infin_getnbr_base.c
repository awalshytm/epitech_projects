/*
** EPITECH PROJECT, 2017
** infin_getnbr_base
** File description:
** infin getnbr_base
*/

#include <stdio.h>
#include <stdlib.h>
#include "infinadd.h"
#include "infinprod.h"
#include "my.h"
#include "infinpower.h"

char *itcs(int nb)
{
	char *result;
	int i = 0;

	result = malloc(sizeof(char) * 11);
	while(nb > 0) {
		result[i] = ((nb % 10) + 48);
		nb /= 10;
		i++;
	}
	result[i] = '\0';
	return (my_revstr(result));
}

int size_of_number(char **str)
{
	int i = 0;
	int j = 0;
	int k = 0;

	while (str[i] != 0) {
		j = 0;
		while (str[i][j] != '\0') {
			j++;
			k++;
		}
		i++;
	}
	return (k);
}

char *my_add_double_array(char **nb)
{
	int i = 0;
	char *result;

	result = malloc(sizeof(char) * size_of_number(nb));
	result = nb[i];
	while (nb[i + 1] != 0) {
		result = infinadd(result, nb[i + 1]);
		i++;
	}
	return (result);
}

char *infin_getnbr_base(char const *str, char const *base)
{
	char **nb = malloc(sizeof(char *) * my_strlen(base) + 1);
	int i = my_strlen(str) - 1;
	char *size = itcs(my_strlen(base));
	int j = 0;
	int k = 0;
	char *c;

	while (i >= 0) {
		k = 0;
		while (str[i] != base[k])
			k++;
		c = itcs(k);
		nb[j] = infinprod(c, infinpower(size, j));
		j++;
		i--;
	}
	nb[j] = 0;
	return (my_add_double_array(nb));
}
