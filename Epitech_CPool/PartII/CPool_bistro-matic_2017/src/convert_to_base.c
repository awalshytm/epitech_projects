/*
** EPITECH PROJECT, 2017
** Bistromatic
** File description:
** convert number to base
*/

#include <stdlib.h>
#include "my.h"
#include "infin_getnbr_base.h"

int is_in_base(char c, char *base)
{
	int i = 0;

	while (base[i]) {
		if (base[i] == c)
			return (1);
                i++;
	}
	return (0);
}

char *convert_to_base(char *expr, char *base)
{
	char *nb_convert = malloc(sizeof(char) * my_strlen(expr));
	char *nb_converted = malloc(sizeof(char) * my_strlen(expr));
	char *new_expr = malloc(sizeof(char) * (my_strlen(expr) * 2));
	int k;
	int i = 0;
	int j = 0;
	int l;

	while (expr[i]) {
		l = 0;
		while (nb_convert[l]){
			nb_convert[l] = '\0';
			l++;
		}
		while (is_in_base(expr[i], base) == 0) {
			new_expr[j] = expr[i];
			i++;
			j++;
		}
		k = 0;
		while (is_in_base(expr[i], base) == 1) {
			nb_convert[k] = expr[i];
			k++;
			i++;
		}
		nb_converted = infin_getnbr_base(nb_convert, base);
		k = 0;
		while (nb_converted[k]) {
			new_expr[j] = nb_converted[k];
			j++;
			k++;
		}
		while (is_in_base(expr[i], base) == 0 && expr[i] != '\0') {
			new_expr[j] = expr[i];
			i++;
			j++;
		}
	}
	return (new_expr);
}
