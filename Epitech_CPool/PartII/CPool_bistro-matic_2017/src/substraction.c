/*
** EPITECH PROJECT, 2017
** substraction
** File description:
** substraction module for infin add
*/

#include <stdlib.h>
#include "infinadd.h"
#include "my.h"
#include "tools.h"

int check_posneg(number_t *pos_nb, number_t *neg_nb)
{
	if (pos_nb->len == neg_nb->len - 1)
		return my_strcmp(pos_nb->nb, &neg_nb->nb[1]);
	if (pos_nb->len > neg_nb->len - 1)
		return (1);
	return (-1);
}

char *make_sous(number_t *big_nb, number_t *small_nb, int pos)
{
	char *ans;
	int tmp;
	int ret = 0;
	int i;

	ans = malloc(sizeof(char) * (big_nb->len + 2));
	ans[big_nb->len] = 0;
	if (pos == 0) {
		ans[0] = '0';
		return (ans);
	}
	for (i = 0 ; small_nb->nb[i] != 0 ; i++) {
		tmp = big_nb->nb[i] - ret - small_nb->nb[i];
		ret = 0;
		if (tmp < 0) {
			ret = 1;
			tmp += 10;
		}
		ans[i] = tmp + '0';
	}
	while (ans[i - 1] == '0') {
		ans[i - 1] = 0;
		i--;
	}
	if (pos == -1)
		ans[i] = '-';
	return(my_revstr(ans));
}

char *add_neg(number_t *pos_nb, number_t *neg_nb)
{
	char *ans;
	int check = check_posneg(pos_nb, neg_nb);

	neg_nb->len -= 1;
	neg_nb->nb = &neg_nb->nb[1];
	if (pos_nb->len > neg_nb->len)
		neg_nb->nb = make_same_length(neg_nb->nb, pos_nb->len, neg_nb->len);
	else if (pos_nb->len < neg_nb->len)
		pos_nb->nb = make_same_length(pos_nb->nb, neg_nb->len, pos_nb->len);
	pos_nb->nb = my_revstr(pos_nb->nb);
	neg_nb->nb = my_revstr(neg_nb->nb);
	if (check == -1)
		ans = make_sous(neg_nb, pos_nb, check);
	else
		ans = make_sous(pos_nb, neg_nb, check);
	return (ans);
}
