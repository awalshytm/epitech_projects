/*
** EPITECH PROJECT, 2017
** test_infinprod
** File description:
** unit tests for infinprod
*/

#include <criterion/criterion.h>
#include "infinprod.h"

Test(infinprod, simple_v1)
{
	cr_assert_str_eq(infinprod("2", "2"), "4", "Check infinprod ft");
}

Test(infinprod, simple_v2)
{
	cr_assert_str_eq(infinprod("12", "12"), "121", "Check infinprod ft");
}

Test(infinprod, complex_v1)
{
	cr_assert_str_eq(infinprod("1234567890", "0987654321"), "1219326311126352690", "Check complex infinprod");
}

Test(infinprod, complex_v2)
{
	cr_assert_str_eq(infinprod("112233445566778899", "0987654321234567890"), "110847847501073836163001759434953110", "Check complex infinprod");
}

Test(infinprod, neg_v1)
{
	cr_assert_str_eq(infinprod("-1", "543"), "-543", "Check neg infinprod");
}

Test(infinprod, neg_v2)
{
	cr_assert_str_eq(infinprod("-123456787323467", "987654123456789"), "-121932605068750019629140167463", "Check infinprod big numbers");
}

Test(infinprod, neg_v3)
{
	cr_assert_str_eq(infinprod("-345", "-678"), "233910", "Check double neg numbers infinprod");
}

Test(infinprod, null_v1)
{
	cr_assert_str_eq(infinprod("987654321456789", "0"), "0", "Check for null ft");
}
