/*
** EPITECH PROJECT, 2017
** Bistro
** File description:
** test infin add
*/

#include <criterion/criterion.h>
#include "infinadd.h"

Test(infinadd, simple_expr_v1)
{
	cr_assert_str_eq(infinadd("1", "2"), "3", "Check for 1+2");
}

Test(infinadd, simple_expr_v2)
{
	cr_assert_str_eq(infinadd("9", "9"), "18", "Check for 9 + 9");
}

Test(infinadd, simple_expr_v3)
{
	cr_assert_str_eq(infinadd("125", "125"), "250", "Check for 125 + 125");
}

Test(infinadd, complex_expr_v1)
{
	cr_assert_str_eq(infinadd("1234567890", "09876543221234567890"), "9876543222469135780", "Check big numbers");
}

Test(infinadd, complex_expr_v2)
{
	cr_assert_str_eq(infinadd("2147483647", "2147483647"), "4294967294");
}

Test(infinadd, neg_infinadd_v1)
{
	cr_assert_str_eq(infinadd("5", "-6"), "-1", "Check neg ft");
}

Test(infinadd, neg_infinadd_v2)
{
	cr_assert_str_eq(infinadd("-6", "-6"), "-12", "Check double neg add");
}

Test(infinadd, neg_infinadd_v3)
{
	cr_assert_str_eq(infinadd("-987654321", "-1234567890"), "-2222222211", "Check complex neg infinadd");
}

Test(infinadd, eq_0)
{
	cr_assert_str_eq(infinadd("6", "-6"), "0", "Check for 0");
}

Test(infinadd, eq_0_complex)
{
	cr_assert_str_eq(infinadd("987654321", "-987654321"), "0", "Check for 0 complex");
}
