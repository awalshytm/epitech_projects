/*
** EPITECH PROJECT, 2017
** test_replaceops
** File description:
** unit test for replaceops
*/

#include <criterion/criterion.h>
#include "eval_expr.h"

Test(replace_ops, little_expr_v1)
{
	cr_assert_str_eq(replace_ops("abcdef", "1c2"), "1+2", "Check for simple expr lettre");
}

Test(replace_ops, little_expr_v2)
{
	cr_assert_str_eq(replace_ops("{}&$!^£", "3&4"), "3+4", "Check for simple expr symbol");
}

Test(replace_ops, complex_expr_v1)
{
	cr_assert_str_eq(replace_ops("ocpsfdm", "o1f2cp3p4d5s6"), "(1*2)+3+4/5-6", "Check complex expr lettre");
}

Test(replace_ops, complex_expr_v2)
{
	cr_assert_str_eq(replace_ops("{}&$!^£", "{1!2}&3&4^5$6"), "(1*2)+3+4/5-6", "Check for complex symbol expr");
}
