/*
** EPITECH PROJECT, 2017
** test_tools
** File description:
** unit tests for tools
*/

#include <criterion/criterion.h>
#include "tools.h"
#include "infinadd.h"

Test(itc, itc_v1)
{
	cr_assert(itc(6) == '6', "Check itc ft");
	cr_assert(itc(3) == '3', "Check itc ft");
}

Test(cti, cti_v1)
{
	cr_assert(cti('8') == 8, "Check cti ft");
	cr_assert(cti('3') == 3, "Check cti ft");
}

Test(is_pos, is_pos_v1)
{
	cr_assert(is_pos("-2") == -1, "Check for neg is_pos");
}

Test(is_pos, is_pos_v2)
{
	cr_assert(is_pos("2") == 1, "Check for pos numbers is_pos");
}

Test(is_pos, is_pos_v3)
{
	cr_assert(is_pos("-1234567") == -1, "Check for bigg neg nb is_pos");
}

Test(is_pos, is_pos_v4)
{
	cr_assert(is_pos("9854321") == 1, "Check fir bigg nb is_pos");
}
