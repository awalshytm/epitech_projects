#include <criterion/criterion.h>

Test(is_in_base, normal_nbr_v1)
{
        cr_assert(is_in_base('0', "0123456789") == 1);
}

Test(is_in_base, normal_nbr_v2)
{
        cr_assert(is_in_base('3', "0123456789") == 1);
}

Test(is_in_base, normal_nbr_v3)
{
        cr_assert(is_in_base('A', "012345689") == 0);
}

Test(is_in_base, weird_nbr_v1)
{
        cr_assert(is_in_base('%', "@£$&") == 1);
}

Test(is_in_base, base16_v1)
{
        cr_assert(is_in_base('A', "0123456789ABCDEF") == 1);
}

Test(is_in_base, base16_v2)
{
        cr_assert(is_in_base('H', "0123456789ABCDEF") == 0);
}
