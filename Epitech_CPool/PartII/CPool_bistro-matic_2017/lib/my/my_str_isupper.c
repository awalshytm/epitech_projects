/*
** EPITECH PROJECT, 2017
** my_str_isupper
** File description:
** returns 1 if only upper cases
*/

int my_str_isalpha(char const *str);

int my_str_isupper(char *str)
{
	int i = 0;

	if (my_str_isalpha(str) == 0)
		return (0);
	while (str[i] != '\0') {
		if (str[i] < 'A' || str[i] > 'Z')
			return (0);
		i++;
	}
	return (1);
}
