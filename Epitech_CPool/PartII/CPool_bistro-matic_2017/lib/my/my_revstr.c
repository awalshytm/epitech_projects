/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** reverse a char string
*/

char *my_revstr(char *str)
{
	int i = 0;
	int n = 0;
	char tmp;

	while (str[i] != '\0')
		i++;
	i--;
	while (n < i) {
		tmp = str[n];
		str[n] = str[i];
		str[i] = tmp;
		n++;
		i--;
	}
	return (str);
}
