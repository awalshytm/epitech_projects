/*
** EPITECH PROJECT, 2017
** my_strdup
** File description:
** dynamically allocates memory and copies given string
*/

#include <stdlib.h>

void my_putchar(char c);

int my_strlen(char const *src);

char *my_strdup(char const *src)
{
	int i = 0;
	int len = my_strlen(src);
	char *dest = malloc(sizeof(char) * len + 1);

	while (i < len) {
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
