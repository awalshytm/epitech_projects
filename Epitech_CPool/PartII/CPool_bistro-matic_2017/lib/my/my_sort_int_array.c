/*
** EPITECH PROJECT, 2017
** my_sort_int_array
** File description:
** returns a pointer to a sorted intarray
*/

void my_swap(int *a, int *b);

int smallest_index(int *array, int size, int cursor)
{
	int i = cursor + 1;
	int min = cursor;

	while (i < size) {
		if (array[i] < array[min])
			min = i;
		i++;
	}
	return (min);
}

int *my_sort_int_array(int *array, int size)
{
	int cursor = 0;
	int min;

	while (cursor < (size - 1)) {
		min = smallest_index(array, size, cursor);
		my_swap(&array[min], &array[cursor]);
		cursor++;
	}
	return (array);
}
