/*
** EPITECH PROJECT, 2017
** my_getnbr
** File description:
** returns an int given as a string
*/

int my_strlen(char const *str);

int make_num(char const *str, int start, int end)
{
	int output_num = 0;

	while (start < end) {
		output_num += str[start] - '0';
		output_num *= 10;
		start++;
	}
	output_num += str[start] - '0';
	return output_num;
}

int checking_num(char const *str)
{
	int i = 0;
	int pos = 1;

	while (str[i] == '-' || str[i] == '+') {
		if (str[i] == '-')
			pos *= -1;
		i++;
	}
	if (str[i] < '0' || str[i] > '9')
		return 0;
	return pos;
}

int check_overflow(char const *str, int start_num, int end_num)
{
	char val_max[] = "2147483647";
	int len = end_num - start_num + 1;
	int i = 0;

	if (len > 10)
		return 1;
	if (len < 10)
		return 0;
	while (str[i] != 0) {
		if (str[i] > val_max[i])
			return 1;
		i++;
	}
	return 0;
}

int my_getnbr(char const *str)
{
	int i = 0;
	int pos = 1;
	int output;
	int start_num;
	int end_num;

	pos = checking_num(str);
	while (str[i] == '-' || str[i] == '+')
		i++;
	start_num = i;
	while (str[i] <= '9' && str[i] >= '0' && str[i] != '\0')
		i++;
	end_num = i - 1;
	if (check_overflow(str, start_num, end_num) == 1)
		return 0;
	output = make_num (str, start_num, i - 1);
	return (output * pos);
}
