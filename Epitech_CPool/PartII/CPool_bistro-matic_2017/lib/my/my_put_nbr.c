/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** displays an int given as parameter
*/

void my_putchar(char c);

void my_put_digit(int dig)
{
	char display;

	display = dig + '0';
	my_putchar(display);
}

int my_pow(int i)
{
	int cnt = 1;

	while (i != 0) {
		cnt *= 10;
		i--;
	}
	return cnt;
}

int my_abs_val(int i)
{
	if (i < 0)
		return -i;
	else
		return i;
}

int my_put_nbr(int nb)
{
	int cnt = 0;
	int tmp;

	if (nb < 0)
		my_putchar('-');
	tmp = nb;
	while (tmp > 9 || tmp < -9) {
		cnt ++;
		tmp /= 10;
	}
	while (cnt >= 0) {
		tmp = nb / my_pow(cnt);
		tmp = my_abs_val(tmp);
		my_put_digit(tmp);
		nb %= my_pow(cnt);
		cnt --;
	}
	return 0;
}
