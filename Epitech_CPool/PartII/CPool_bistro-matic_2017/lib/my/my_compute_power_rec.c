/*
** EPITECH PROJECT, 2017
** my_compute_power_rec
** File description:
** returns nb to the power of p, recursively
*/

int my_compute_power_rec(int nb, int p)
{
	int res;

	if (p < 0)
		return 0;
	if (p == 0)
		return 1;
	res = nb * my_compute_power_rec(nb, p - 1);
	return (res);
}
