/*
** EPITECH PROJECT, 2017
** my_strcat
** File description:
** concatenates two strings
*/

void my_putchar (char c);

int my_strlen(char const *str);

char *my_strcat(char *dest, char const *src)
{
	int len = my_strlen(dest);
	int i = 0;

	while (src[i] != '\0') {
		dest[len] = src[i];
		len++;
		i++;
	}
	dest[len] = 0;
	return (dest);
}
