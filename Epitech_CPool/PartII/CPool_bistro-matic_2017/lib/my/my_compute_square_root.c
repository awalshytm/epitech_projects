/*
** EPITECH PROJECT, 2017
** my_compute_square_root
** File description:
** returns the square root of nb
*/

int my_compute_square_root(int nb)
{
	int i = 1;

	if (nb <= 0)
		return 0;
	while (i <= nb) {
		if (nb % i == 0 && nb / i == i)
			return i;
		i++;
	}
	return (0);
}
