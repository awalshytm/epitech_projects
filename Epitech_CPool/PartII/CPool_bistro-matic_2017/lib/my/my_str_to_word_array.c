/*
** EPITECH PROJECT, 2017
** my_str_to_word_array
** File description:
** splits a string into words and store them in a tab
*/

#include <stdlib.h>

char *my_strdup(char const *src);

int my_char_is_alphanum(char c)
{
	if (c > 47 && c < 58)
		return (1);
	if (c > 64 && c < 91)
		return (1);
	if (c > 96 && c < 123)
		return (1);
	return (0);
}

int my_count_words(char const *str)
{
	int i = 0;
	int cnt = 0;

	while (str[i] != 0) {
		cnt++;
		while (my_char_is_alphanum(str[i]))
			i++;
		while (!(my_char_is_alphanum(str[i])) && str[i] != 0)
			i++;
	}
	return (cnt);
}

char **my_str_to_word_array(char const *str)
{
	char **tab;
	char *tmp = my_strdup(str);
	int cnt = my_count_words(str);;
	int i = 0;

	tab = malloc(sizeof(char*) * cnt + 1);
	cnt = 0;
	while (tmp[i] != 0) {
		tab[cnt] = &tmp[i];
		cnt++;
		while (my_char_is_alphanum(tmp[i]))
			i++;
		tmp[i] = 0;
		i++;
		while (!(my_char_is_alphanum(tmp[i])) && tmp[i] != 0)
			i++;
	}
	tab[cnt+1] = 0;
	return (tab);
}
