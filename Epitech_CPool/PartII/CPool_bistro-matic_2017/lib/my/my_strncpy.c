/*
** EPITECH PROJECT, 2017
** my_strncpy
** File description:
** copies n chars from a string to another
*/

char *my_strncpy(char *dest, char *src, int n)
{
	int i = 0;

	while (i < n) {
		if (src[i] == '\0')
			dest[i] = '\0';
		else
			dest[i] = src[i];
		i++;
	}
	return (dest);
}
