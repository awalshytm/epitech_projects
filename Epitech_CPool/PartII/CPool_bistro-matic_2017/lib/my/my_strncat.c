/*
** EPITECH PROJECT, 2017
** my_strncat
** File description:
** concatenates n bytes at the end of the dest string
*/

void my_putchar (char c);

int my_strlen(char const *str);

char *my_strncat(char *dest, char const *src, int nb)
{
	int len = my_strlen(dest);
	int i = 0;

	while (src[i] != '\0' && i < nb) {
		dest[len] = src[i];
		len++;
		i++;
	}
	dest[len] = 0;
	return (dest);
}
