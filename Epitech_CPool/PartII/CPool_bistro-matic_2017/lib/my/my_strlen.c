/*
** EPITECH PROJECT, 2017
** my_strlen
** File description:
** returns the number of chars in a char string
*/

int my_strlen(char const *str)
{
	int cnt = 0;

	for (int i = 0 ; str[i] != '\0' ; i++)
		cnt++;
	return (cnt);
}
