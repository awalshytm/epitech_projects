/*
** EPITECH PROJECT, 2017
** my_showstr
** File description:
** prints a string conataining non printable values
*/

void my_putchar(char c);

int my_putnbr_base(int nbr, char const *base);

void my_print_hex(char const *str, int i)
{
	my_putchar('\\');
	if (str[i] < 16)
		my_putchar('0');
	my_putnbr_base(str[i], "0123456789abcdef");
}

int my_showstr(char const *str)
{
	int i = 0;

	while (str[i] != '\0') {
		if (str[i] > 31 && str[i] < 127) {
			my_putchar(str[i]);
		} else
			my_print_hex(str, i);
		i++;
	}
	return (0);
}
