/*
** EPITECH PROJECT, 2017
** infinprod
** File description:
** header to infinprod
*/

#ifndef INFINPROD_H_
#define INFINPROD_H_

#include "tools.h"

int get_pos(number_t **num1, number_t **num2);
char *make_prod(number_t *n1, number_t *n2, int *ret, char *res);
char *infinprod(char const *str1, char const *str2);

#endif
