/*
** EPITECH PROJECT, 2017
** infindiv_aux
** File description:
** header
*/

#ifndef	INFINDIV_AUX_H_
#define	INFINDIV_AUX_H_

#include "tools.h"

char *minus(number_t *num);
int check_for_zeros(number_t *num);
int check_div_zero(number_t *denom);

#endif
