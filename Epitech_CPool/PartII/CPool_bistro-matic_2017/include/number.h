/*
** EPITECH PROJECT, 2017
** number
** File description:
** header
*/

#ifndef NUMBER_H_
#define NUMBER_H_

int check_neg(char c);
char *create_numstr(int i, int j, char **str, int pos);
char *number(char **str);

#endif
