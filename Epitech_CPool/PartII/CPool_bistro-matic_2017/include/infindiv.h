/*
** EPITECH PROJECT, 2017
** infindiv
** File description:
** division or modulo of infinit numbers
*/

#ifndef INFINDIV_H_
#define INFINDIV_H_

#include "tools.h"

int str_eq_cmp(number_t *num_eq, number_t *denom);
number_t *init_num(number_t *num, number_t *denom);
number_t *change_num(number_t *num, number_t *num_eq, int save_len);
char *infin_div_main(number_t *num, number_t *denom, int check);
char *infindiv(char const *str1, char const *str2, char c);

#endif
