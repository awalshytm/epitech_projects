/*
** EPITECH PROJECT, 2017
** tools
** File description:
** tool module for infin calcul
*/

#ifndef TOOLS_H_
#define TOOLS_H_

typedef struct number {
	char *nb;
	int len;
} number_t;

char itc(int digit);
int cti(char a);
char *make_same_length(char *small_nb, int len1, int len2);
int my_putstr_error(char *str);
number_t *attrib_num(char const *str);

#endif
