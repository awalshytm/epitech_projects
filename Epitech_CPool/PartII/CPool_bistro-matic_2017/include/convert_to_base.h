/*
** EPITECH PROJECT, 2017
** convert_base
** File description:
** header
*/

#ifndef CONVERT_BASE_H_
#define CONVERT_BASE_H_

int is_in_base(char c, char *base);
char *convert_to_base(char *expr, char *base);

#endif
