/*
** EPITECH PROJECT, 2017
** check_expr
** File description:
** header
*/

#ifndef CHECK_EXPR_H_
#define CHECK_EXPR_H_

void check_expr_ops(char *expr, char *ops, int i);
void check_expr_base(char *expr, char *ops, char *base, int i);
void check_expr(char *expr, char *ops, char *base);

#endif
