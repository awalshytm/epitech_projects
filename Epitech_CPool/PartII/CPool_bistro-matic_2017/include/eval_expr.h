/*
** EPITECH PROJECT, 2017
** eval_expr
** File description:
** transforms the expression to decimal base and result to given base
*/

#ifndef EVAL_EXPR_H_
#define EVAL_EXPR_H_

char *replace_ops(char *ops, char *str);
char *eval_expr(char *base, char *ops, char *expr);

#endif
