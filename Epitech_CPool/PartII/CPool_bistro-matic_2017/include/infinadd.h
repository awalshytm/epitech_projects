/*
** EPITECH PROJECT, 2017
** infinadd
** File description:
** header of infinadd
*/

#ifndef INFINADD_H_
#define INFINADD_H_

int is_pos(char *str);
char *infinadd(char const *num1, char const *num2);

#endif
