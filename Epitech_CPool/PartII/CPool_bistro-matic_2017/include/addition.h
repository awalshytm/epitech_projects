/*
** EPITECH PROJECT, 2017
** addition
** File description:
** header
*/

#ifndef ADDITION_H_
#define ADDITION_H_

#include "tools.h"

char *make_add(number_t *nb1, number_t *nb2, int pos);
char *add_pos(number_t *nb1, number_t *nb2, int pos);

#endif
