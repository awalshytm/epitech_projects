/*
** EPITECH PROJECT, 2017
** infin_getnbr_base
** File description:
** header
*/

#ifndef INFIN_GETNBR_BASE_H_
#define INFIN_GETNBR_BASE_H_

char *itcs(int nb);
int size_of_number(char **str);
char *my_add_double_array(char **nb);
char *infin_getnbr_base(char const *str, char const *base);

#endif
