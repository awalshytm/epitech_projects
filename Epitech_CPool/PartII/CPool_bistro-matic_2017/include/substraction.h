/*
** EPITECH PROJECT, 2017
** substraction
** File description:
** header
*/

#ifndef SUBSTRACTION_H_
#define SUBSTRACTION_H_

int check_posneg(number_t *pos_nb, number_t *neg_nb);
char *make_sous(number_t *big_nb, number_t *small_nb, int pos);
char *add_neg(number_t *pos_nb, number_t *neg_nb);

#endif
