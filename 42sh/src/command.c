/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** exe
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>
#include "libstr_tool.h"
#include "libprint.h"
#include "macro.h"
#include "shell.h"

char **get_path(char **envp)
{
	for (int i = 0 ; envp[i] != NULL ; i++)
		if (strncmp("PATH=", envp[i], 5) == 0)
			return (str_to_word_array(&envp[i][5], ':'));
	return (NULL);
}

macro_h is_syst_function(char **command, stock_t *get, int red)
{
	char *binary = NULL;
	char *tmp = NULL;
	char **path = get_path(get->cpy_env);

	if (!path)
		return (1);
	for (int i = 0 ; path[i] != NULL ; i++) {
		tmp = my_strconcat(path[i], "/");
		binary = my_strconcat(tmp, command[0]);
		free(tmp);
		if (access(binary, X_OK) == 0) {
			get->return_value = execute(binary,
			command, get->cpy_env, red);
			free(binary);
			destroy_tab((void **) path);
			return (0);
		} else
			free(binary);
	}
	destroy_tab((void **) path);
	return (1);
}

int my_command(char **argv, stock_t *get)
{
	char *my_command[] = {"cd", "env", "setenv", "unsetenv",
			"exit", "alias", NULL};
	int (*ptr[])(char **, stock_t *) = {my_cd, my_env, my_setenv,
					my_unsetenv, my_exit, my_alias};

	for (int i = 0; my_command[i]; i++)
		if (!strcmp(argv[0], my_command[i])) {
			get->return_value = 0;
			ptr[i](argv, get);
			if (i != 4)
				return (0);
			else
				return (EXIT);
		}
	return (1);
}

int execute_commands(char *command, stock_t *get)
{
	char **	tmp = str_to_word_array(command, ' ');
	int my_builtins = my_command(tmp, get);

	if (my_builtins == EXIT || my_builtins == SUCCESS) {
		destroy_tab((void **) tmp);
		return (my_builtins);
	}
	if (is_syst_function(tmp, get, 0) == 0) {
		destroy_tab((void **) tmp);
		return (0);
	}
	if (access(tmp[0], X_OK) == 0) {
		execute(tmp[0], tmp, get->cpy_env, 0);
		destroy_tab((void **) tmp);
		return (0);
	}
	printf("%s: Command not found.\n", tmp[0]);
	get->return_value = 1;
	destroy_tab((void **) tmp);
	return (1);
}

int exe_commands(stock_t *get)
{
	if (put_alias(get) == ERROR)
		return (ERROR);
	for (int i = 0; get->tab_command[i]; i++) {
		if (make_op(&i, get) == 0)
			continue;
		else if (execute_commands(get->tab_command[i], get) == EXIT)
			return (EXIT);
	}
	return (SUCCESS);
}
