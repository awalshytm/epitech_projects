/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** operation
*/

#include <string.h>
#include "struct.h"
#include "libstr_tool.h"
#include "operation.h"

int make_op(int *index, stock_t *get)
{
	char *base[8] = {"||", "|", ">>", ">", "<<", "<", "&&", NULL};
	char **tmp = str_to_word_array(get->tab_command[*index], ' ');
	int (*fct[])(char **, char *, stock_t *) = {
		or, my_pipe, dred_r, red_r, dred_l,red_l, my_and};

	if (!get->tab_link[*index])
		return (1);
	if (strcmp(get->tab_link[*index], ";") == 0)
		return (1);
	for (int i = 0; base[i]; i++) {
		if (strcmp(base[i], get->tab_link[*index]) == 0) {
			fct[i](tmp, get->tab_command[*index + 1], get);
			index[0]++;
			break;
		}
	}
	return (0);
}
