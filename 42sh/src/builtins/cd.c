/*
** EPITECH PROJECT, 2018
** cd.c
** File description:
** 42sh
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "struct.h"
#include "macro.h"
#include "shell.h"

static char *cd_bttf(char **envp)
{
	char *old = get_envvar_val("OLDPWD", envp);

	if (!old)
		return (NULL);
	set_old(envp);
	return (old);
}

static void cd_home(char **envp, int *exit)
{
	char *home = get_envvar_val("HOME", envp);

	if (!home)
		return;
	if (chdir(home) != 0) {
		perror("42sh: cd");
		*exit = 1;
		return;
	}
	*exit = 0;
}

static char *create_cd_path(char *command)
{
	char *cwd = NULL;
	char *path = NULL;
	int i = 0;

	if (!(cwd = getcwd(path, 0)))
		return (NULL);
	if (!(path = malloc(strlen(cwd) + strlen(command) + 2)))
		return (NULL);
	for (i = 0; cwd[i]; i++)
		path[i] = cwd[i];
	path[i++] = '/';
	for (int j = 0; command[j]; j++)
		path[i++] = command[j];
	path[i] = '\0';
	free(cwd);
	return (path);
}

void perror_cd(char *name, char *arg)
{
	if (arg)
		printf("cd: Too many arguments.\n");
	else if (errno == EACCES)
		printf("%s: Permission denied.\n", name);
	else if (errno == ELOOP)
		printf("%s: Too many levels of symbolic links.\n", name);
	else if (errno == ENOENT)
		printf("%s: No such file or directory.\n", name);
	else if (errno == ENOTDIR)
		printf("%s: Not a directory.\n", name);
	else if (errno == EBADF)
		printf("%s: Bad file descriptor.\n", name);
}

int my_cd(char **commands, stock_t *get)
{
	char *path = NULL;

	if (!commands[1]) {
		cd_home(get->cpy_env, &(get->return_value));
		return (get->return_value);
	}
	if (commands[1][0] == '-')
		path = cd_bttf(get->cpy_env);
	else if (commands[1][0] == '/')
		path = strdup(commands[1]);
	else
		path = create_cd_path(commands[1]);
	set_old(get->cpy_env);
	if (chdir(path) != 0 || commands[2] != NULL) {
		perror_cd(commands[1], commands[2]);
		get->return_value = 1;
	} else
		get->return_value = 0;
	free(path);
	return (get->return_value);
}
