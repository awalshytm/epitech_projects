/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** cd_tools
*/


#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "macro.h"
#include "shell.h"

void set_old(char **envp)
{
	char *cwd = NULL;
	char *new_old = NULL;
	char old[] = "OLDPWD=";
	int index = get_envvar_index("OLDPWD", envp);

	if (!(cwd = getcwd(cwd, 0)))
		return;
	if (!(new_old = malloc(8 + strlen(cwd))))
		return;
	for (int i = 0; old[i]; i++)
		new_old[i] = old[i];
	for (int j = 0; cwd[j]; j++)
		new_old[j + 7] = cwd[j];
	new_old[7 + strlen(cwd)] = '\0';
	free(cwd);
	if (envp[index])
		free(envp[index]);
	envp[index] = NULL;
	envp[index] = new_old;
}
