/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** env_manipulation
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"

int my_env(char **av, stock_t *get)
{
	(void) av;
	for (int i = 0 ; get->cpy_env[i] ; i++)
		printf("%s\n", get->cpy_env[i]);
	return (get->return_value);
}

char **dup_env(char const *const *env)
{
	char **new = NULL;
	int size = 0;

	while (env[size])
		size++;
	new = malloc(sizeof(char *) * (size + 1));
	if (!new)
		return (NULL);
	for (int i = 0 ; env[i] ; i++) {
		new[i] = strdup(env[i]);
		if (!new[i])
			return (NULL);
	}
	new[size] = NULL;
	return (new);
}
