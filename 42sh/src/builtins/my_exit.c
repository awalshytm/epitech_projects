/*
** EPITECH PROJECT, 2018
** my_exit
** File description:
** 42sh
*/

#include <stdio.h>
#include "libmath.h"
#include "macro.h"
#include "struct.h"
#include "libstr_tool.h"

int my_tablen(char **tab)
{
	int len = 0;

	if (tab)
		while (tab[len])
			len++;
	return (len);
}

int my_exit(char **av, stock_t *get)
{
	int tab_len = my_tablen(av);

	if (tab_len != 1 && (tab_len != 2 || check_int(av[1], 1))) {
		printf("exit: Expression Syntax.\n");
		return (1);
	}
	printf("exit\n");
	if (tab_len == 1)
		get->return_value = 0;
	else
		get->return_value = my_getnbr(av[1]);
	return (EXIT);
}
