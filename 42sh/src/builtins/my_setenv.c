/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** setenv
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "macro.h"
#include "libstr_tool.h"
#include "shell.h"

int lenght_tab(char **tab)
{
	int nbr = 0;

	while (tab[nbr])
		nbr++;
	return (nbr);
}

macro_h av_gestion(char **av, stock_t *get)
{
	int ac = lenght_tab(av);

	if (ac == 1) {
		my_env(av, get);
		return (ERROR);
	}
	if (ac >= 4) {
		printf("setenv: Too many arguments.\n");
		return (ERROR);
	}
	for (int i = 0 ; av[1][i] ; i++)
		if ((av[1][i] < 'A' || av[1][i] > 'Z')
		&& (av[1][i] < 'a' || av[1][i] > 'z')
		&& (av[1][i] < '0' || av[1][i] > '9')) {
			printf("setenv: Variable name must ");
			printf("contain alphanumeric characters.\n");
			return (ERROR);
		}
	return (SUCCESS);
}

int my_setenv(char **av, stock_t *get)
{
	char **new = NULL;
	char *add = NULL;
	int lines = lenght_tab(get->cpy_env);

	if (av_gestion(av, get) == ERROR)
		return (get->return_value);
	add = concate(av[1], av[2], '=');
	new = malloc(sizeof(char *) * (lines + 2));
	if (new == NULL)
		return (get->return_value);
	for (int i = 0 ; i < lines ; i++) {
		new[i] = strdup(get->cpy_env[i]);
		if (!new[i])
			return (get->return_value);
	}
	new[lines] = strdup(add);
	new[lines + 1] = NULL;
	free(add);
	destroy_tab((void **) get->cpy_env);
	get->cpy_env = new;
	return (get->return_value);
}
