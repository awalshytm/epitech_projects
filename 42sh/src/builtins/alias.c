/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** alias
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "shell.h"
#include "struct.h"

alias_t *put_in_list(char *name, char *cmd, alias_t *old, char **env)
{
	alias_t *new = malloc(sizeof(alias_t));

	if (!new)
		return (NULL);
	new->name = clean_line(name, env);
	new->cmd = clean_line(cmd, env);
	new->next = old;
	return (new);
}

int display_alias(alias_t *list)
{
	for (alias_t *tmp = list; tmp; tmp = tmp->next)
		printf("%s\t%s --color=auto\n", tmp->name, tmp->cmd);
	return (0);
}

int my_alias(char **av, stock_t *get)
{
	char *cmd = NULL;
	char *name = NULL;
	int size = 0;

	while (av[size])
		size++;
	if (size == 1)
		return (display_alias(get->alias));
	name = strdup(av[1]);
	if (!name)
		return (1);
	for (int i = 2; av[i]; i++) {
		cmd = concate(cmd, av[i], ' ');
		if (!cmd)
			return (1);
	}
	get->alias = put_in_list(name, cmd, get->alias, get->cpy_env);
	if (!get->alias)
		return (1);
	return (0);
}

char *if_an_alias(char *cmd, alias_t *list)
{
	for (alias_t *tmp = list; tmp; tmp = tmp->next)
		if (strcmp(cmd, tmp->name) == 0)
			return (tmp->cmd);
	return (NULL);
}

int put_alias(stock_t *get)
{
	char *tmp = NULL;

	for (int i = 0; get->tab_command[i]; i++) {
		tmp = if_an_alias(get->tab_command[i], get->alias);
		if (tmp) {
			free(get->tab_command[i]);
			get->tab_command[i] = strdup(tmp);
		}
		if (!get->tab_command[i])
			return (ERROR);
	}
	return (SUCCESS);
}
