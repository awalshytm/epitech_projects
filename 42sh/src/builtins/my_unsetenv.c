/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** unsetenv
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "macro.h"
#include "shell.h"

macro_h unset_av(char **av)
{
	int ac = lenght_tab(av);

	if (ac == 1) {
		printf("unsetenv: Too few arguments.\n");
		return (ERROR);
	}
	return (SUCCESS);
}

macro_h if_in_path(char **env, char *buff)
{
	char *add = strdup(buff);
	int size = strlen(add);

	if(add[size] != '=')
		add = concate(add, NULL, '=');
	for (int i = 0 ; env[i] ; i++) {
		if (strncmp(add, env[i], size) == 0) {
			free(add);
			return (SUCCESS);
		}
	}
	free(add);
	return (ERROR);
}

macro_h had_to_remove(char *var, char **av)
{
	char *buff = NULL;
	int size = 0;

	for (int i = 1 ; av[i] ; i++) {
		buff = strdup(av[i]);
		size = strlen(buff);
		if(buff[size] != '=')
			buff = concate(buff, NULL, '=');
		if (strncmp(var, buff, size) == 0) {
			free(buff);
			return (SUCCESS);
		}
		free(buff);
	}
	return (ERROR);
}

int my_unsetenv(char **av, stock_t *get)
{
	char **new = NULL;
	int size = 0;
	int new_index = 0;

	if (unset_av(av) == ERROR)
		return (get->return_value);
	for ( ; get->cpy_env[size] ; size++);
	for (int i = 1 ; av[i] ; i++)
		if (if_in_path(get->cpy_env, av[i]) == SUCCESS)
			size--;
	new = malloc(sizeof(char *) * (size + 1));
	if (!new)
		return (get->return_value);
	for (int i = 0 ; get->cpy_env[i] ; i++) {
		if (had_to_remove(get->cpy_env[i], av) != SUCCESS)
			new[new_index++] = strdup(get->cpy_env[i]);
		if (new[new_index] == NULL)
			return (84);
	}
	new[size] = 0;
	destroy_tab((void **) get->cpy_env);
	get->cpy_env = new;
	return (get->return_value);
}
