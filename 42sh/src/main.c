/*
** EPITECH PROJECT, 2018
** projet
** File description:
** main.c
*/

#include <stdlib.h>
#include <unistd.h>
#include "macro.h"
#include "shell.h"

int init_struct(stock_t *get, char **env)
{
	get->tab_command = NULL;
	get->tab_link = NULL;
	get->cpy_env = dup_env(env);
	if (!get->cpy_env)
		return (ERROR);
	get->return_value = 0;
	get->alias = NULL;
	return (SUCCESS);
}

void destroy_alias(alias_t *alias)
{
	for (alias_t *tmp = alias; tmp; tmp = tmp->next) {
		free(tmp->name);
		free(tmp->cmd);
		free(tmp);
	}
}

int main(int ac UNUSED, char **av UNUSED, char **env)
{
	stock_t get;
	int shell_value = 0;

	if (init_struct(&get, env) == ERROR)
		return (ERROR);
	if (!get.cpy_env)
		return (ERROR);
	while (shell_value != EXIT && my_parser(&get)) {
		shell_value = exe_commands(&get);
		destroy_tab((void **) get.tab_command);
		destroy_tab((void **) get.tab_link);
	}
	destroy_tab((void **) get.cpy_env);
	destroy_alias(get.alias);
	return (get.return_value);
}
