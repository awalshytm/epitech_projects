/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** pipe
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "libstr_tool.h"
#include "shell.h"

static int check_both(char **cmd, char **tmp , stock_t *get)
{
	if (my_command(cmd, get) == 0 || is_syst_function(cmd, get, 0) == 0
	|| access(cmd[0], X_OK) == 0)
		return (0);
	if (my_command(cmd, get) != 0 && is_syst_function(cmd, get, 0) != 0
	&& access(cmd[0], X_OK) != 0) {
		printf("%s: Command not found.\n", cmd[0]);
		if (my_command(tmp, get) == 0 ||
		is_syst_function(tmp, get, 0) == 0 ||
		access(tmp[0], X_OK) == 0)
			return (0);
	}
	if (my_command(tmp, get) != 0 && is_syst_function(tmp, get, 0) != 0
	&& access(tmp[0], X_OK) != 0) {
		printf("%s: Command not found.\n", tmp[0]);
		return (1);
	}
	return (0);
}

int or(char **cmd, char *next, stock_t *get)
{
	char **tmp = str_to_word_array(next, ' ');

	if (tmp == NULL)
		exit(84);
	if (cmd[0][0] == 0 || next[0] == 0) {
		printf("Invalid null command.\n");
		get->return_value = 1;
		return (0);
	}
	if (check_both(cmd, tmp , get) == 1)
		get->return_value = 1;
	return (0);
}
