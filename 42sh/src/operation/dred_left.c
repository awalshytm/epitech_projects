/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** pipe
*/

#include <stdio.h>
#include <stdlib.h>
#include "shell.h"
#include "libstr_tool.h"

static char *remove_new_line(char *buffer)
{
	int len = 0;

	if (!buffer)
		return (NULL);
	while (buffer[len] && buffer[len] != '\n')
		len++;
	buffer[len] = '\0';
	return (buffer);
}

char *concat_2(char *str1, char *str2)
{
	char *str = my_strconcat(str1, " ");
	char *tmp = my_strconcat(str, str2);

	free(str);
	return (tmp);
}

char *concat_tab(char **cmd, char *c UNUSED)
{
	char *str = my_strcpy(cmd[0]);
	char *cpy = str;

	for (int i = 1; cmd[i]; i++) {
		if (!str)
			return (NULL);
		concat_2(str, cpy);
		free(cpy);
		cpy = str;
	}
	return (str);
}

int dred_l(char **cmd, char *next, stock_t *get UNUSED)
{
	char *buffer = NULL;
	size_t size = 0;
	char *command = concat_tab(cmd, " ");
	int value = 0;

	printf("> ");
	getline(&buffer, &size, stdin);
	buffer = remove_new_line(buffer);
	while (my_strcmp(next, buffer, EVEN) != EVEN && buffer) {
		printf("> ");
		getline(&buffer, &size, stdin);
		buffer = remove_new_line(buffer);
	}
	if (buffer)
		free(buffer);
	value = execute_commands(command, get);
	free(command);
	return (value);
}
