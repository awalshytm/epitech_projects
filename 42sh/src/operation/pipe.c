/*
** EPITECH PROJECT, 2018
** pipe
** File description:
** pipe
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <wait.h>
#include <stdio.h>
#include "struct.h"
#include "shell.h"

void first_pipe(char *command, int *pipefd, stock_t *get)
{
	dup2(pipefd[1], 1);
	close(pipefd[0]);
	close(pipefd[1]);
	execute_commands(command, get);
}

void intermediate_pipe(char *command, int **pipefd, int i, stock_t *get)
{
	pid_t pid;
	int exit;

	if ((pid = fork()) == -1)
		return;
	if (pid == 0) {
		dup2(pipefd[i - 1][0], 0);
		dup2(pipefd[i][1], 1);
		close(pipefd[i - 1][1]);
		close(pipefd[i][0]);
		close(pipefd[i - 1][0]);
		close(pipefd[i][1]);
		execute_commands(command, get);
	}
	close(pipefd[i - 1][1]);
	close(pipefd[i - 1][0]);
//	wait(&exit);
}

void final_pipe(char *command, int **pipefd, int i, stock_t *get)
{
	int exit;
	pid_t pid;

	if ((pid = fork()) == -1)
		return;
	if (pid == 0) {
		dup2(pipefd[i - 1][0], 0);
		close(pipefd[i - 1][1]);
		close(pipefd[i - 1][0]);
		execute_commands(command, get);
	}
	close(pipefd[i - 1][1]);
	close(pipefd[i - 1][0]);
//	wait(&exit);
	destroy_tab((void **)pipefd);
	pipefd = NULL;
}

int **pipe_all(int **pipefd, int nb_pipe)
{
	if (!(pipefd = malloc(sizeof(int *) * nb_pipe)))
		return (NULL);
	for (int i = 0; i < nb_pipe; i++) {
		if (!(pipefd[i] = malloc(sizeof(int) * 2)))		
			return (NULL);
		if (pipe(pipefd[i]) != 0)
			return (NULL);
	}
	return (pipefd);
}

int get_nb_pipe(char **links)
{
	int nb = 0;

	for (int i = 0; links[i]; i++)
		if (strcmp(links[i], "|") == 0)
			nb++;
	return (nb);
}

void my_pipe(char **cmd UNUSED, char *next UNUSED, stock_t *get)
{
	static int **pipefd = NULL;
	pid_t pid;
	int exit;
	static int i = 0;

	if (i == 0)
		pipefd = pipe_all(pipefd, get_nb_pipe(get->tab_link));
	if (pipefd == NULL)
		return;
	if (i == 0)
		if ((pid = fork()) == -1)
			return;
	if (pid == 0 && i == 0) {
		first_pipe(get->tab_command[0], pipefd[0], get);
		i++;
	}
	i == 0 ? /*wait(&exit)*/ i++ : pid == 0;
	if (i > 0 && i < get_nb_pipe(get->tab_link) - 1) {
		intermediate_pipe(get->tab_command[i], pipefd, i, get);
	}
	final_pipe(get->tab_command[i], pipefd, i, get);
}
