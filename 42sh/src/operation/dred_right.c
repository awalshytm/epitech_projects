/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** pipe
*/

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "shell.h"

int dred_r(char **cmd, char *file, stock_t *get)
{
	int fd = 0;

	fd = open(file, O_WRONLY);
	if (fd == -1)
		fd = open(file, O_CREAT | O_WRONLY, 00664);
	if (fd == -1)
		return (1);
	lseek(fd, 0, SEEK_END);
	if (my_command(cmd, get) == 0)
		return (0);
	if (access(cmd[0], X_OK) == 0) {
		execute(cmd[0], cmd, get->cpy_env, fd);
		return (0);
	}
	if (is_syst_function(cmd, get, fd) == SUCCESS)
		return (0);
	printf("%s: Command not found.\n", cmd[0]);
	close(fd);
	return (1);
}
