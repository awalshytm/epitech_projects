/*
** EPITECH PROJECT, 2018
** destroy.c
** File description:
** 42sh
*/

#include <stdlib.h>

void destroy_tab(void **tab)
{
	if (!tab)
		return;
	for (int i = 0; tab[i]; i++)
		free(tab[i]);
	free(tab);
}
