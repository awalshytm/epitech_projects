/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** get_envvar_info
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "macro.h"
#include "shell.h"

char *get_envvar_val(char *var, char **envp)
{
	char *i_val = NULL;
	int size = strlen(var);
	char *val = NULL;

	for (int i = 0; envp[i]; i++)
		if (strncmp(var, envp[i], size) == 0) {
			i_val = strdup(envp[i]);
			break;
		}
	if (!(val = malloc(strlen(i_val) - (size))))
		return (NULL);
	for (int i = size + 1; i_val[i]; i++)
		val[i - (size + 1)] = i_val[i];
	val[strlen(i_val) - (size + 1)] = '\0';
	free(i_val);
	return (val);
}

int get_envvar_index(char *var, char **envp)
{
	for (int i = 0; envp[i]; i++)
		if (strncmp(var, envp[i], strlen(var)) == 0)
			return (i);
	return (0);
}
