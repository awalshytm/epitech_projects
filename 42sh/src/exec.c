/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** exe
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <errno.h>
#include <signal.h>
#include "libstr_tool.h"
#include "libprint.h"
#include "macro.h"
#include "shell.h"

static void check_process_son(pid_t pid, int wstatus, int *return_value)
{
	if (waitpid(pid, &wstatus, 0) == -1)
		exit(ERROR);
	kill(pid, SIGINT);
	if (WIFEXITED(wstatus) == 1) {
		if (WEXITSTATUS(wstatus) == 2)
			*return_value = 1;
	} else if (WIFSIGNALED(wstatus) == 1) {
		*return_value = WTERMSIG(wstatus) + 128;
		if (*return_value == 139)
			write (1, "Segmentation fault", 18);
		if (*return_value == 136)
			write(1, "Floating exception", 18);
		if (WCOREDUMP(wstatus) == 128)
			write (1, " (core dumped)", 14);
		my_printf("\n");
	}
}

void execformat(char *prog)
{
	if (!prog)
		return;
	my_putstr(prog);
	my_putstr(": Exec format error. Wrong Architecture.\n");
}

int execute(char *command, char **argv, char **env, int red)
{
	pid_t pid = fork();
	int wstatus = 0;
	int return_value = 0;

	if (pid == -1)
		exit(ERROR);
	if (pid == 0) {
		if (red != 0 && dup2(red, 1) == -1)
			exit(ERROR);
		if (execve(command, argv, env) == -1 && command[0] != '/') {
			errno == ENOEXEC ? execformat(command) : perror("42sh");
			exit(ERROR);
		}
	} else
		check_process_son(pid, wstatus, &return_value);
	return (return_value);
}
