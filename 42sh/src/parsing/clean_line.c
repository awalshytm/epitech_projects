/*
** EPITECH PROJECT, 2018
** parsing
** File description:
** clean_line
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "shell.h"
#include "libstr_tool.h"
#include "macro.h"

static char *remove_space(char *in)
{
	char *out = malloc(sizeof(char) * (my_strlen(in) + 1));
	unsigned int i = 0;

	if (!out)
		exit(ERROR);
	while (in[0] && (in[0] == ' ' || in[0] == '\t' || in[0] == '\n'))
		in = &in[1];
	while (in[0] && in[0] != '\n') {
		if (in[0] != ' ' && in[0] != '\t')
			out[i] = in[0];
		else if ((in[0] == ' ' || in[0] == '\t') && in[1] &&
			(in[1] != ' ' && in[1] != '\t' && in[1] != '\n'))
			out[i] = ' ';
		else
			i--;
		i++;
		in = &in[1];
	}
	out[i] = '\0';
	return (out);
}

char *replace_tild(char *s, char **envp)
{
	char *home = get_envvar_val("HOME", envp);
	char *new = malloc(strlen(home) + strlen(s));
	int i = 0;
	int j = 0;

	while (s[i] != '~' && s[i])
		new[j++] = s[i++];
	i++;
	for (int k = 0; home[k]; k++)
		new[j++] = home[k];
	while (s[i])
		new[j++] = s[i++];
	new[j] = '\0';
	free(s);
	return (new);
}

int search_tild(char *s)
{
	for (int i = 0; s[i]; i++)
		if (s[i] == '~' && s[i - 1] == ' ')
			return (1);
	return (0);
}

char *clean_line(char *input, char **envp)
{
	char *output = NULL;

	if (!input)
		return (NULL);
	output = remove_space(input);
	free(input);
	if (search_tild(output) == 1)
		output = replace_tild(output, envp);
	return (output);
}
