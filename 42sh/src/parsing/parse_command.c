/*
** EPITECH PROJECT, 2018
** parsing
** File description:
** 42sh
*/

#include <stdlib.h>
#include <unistd.h>
#include "shell.h"
#include "libstr_tool.h"
#include "macro.h"

static const char *word_link[] = {"&&", "||", "|", ";", "<<",
"<", ">>", ">", NULL};

int str_is_links(const char *str, int *index)
{
	for (unsigned int i = 0; word_link[i]; i++)
		if (my_strcmp(word_link[i], str, UNEVEN) == EVEN) {
			*index = i;
			return (1);
		}
	return (0);
}

int getlen_links(const char *command)
{
	char *cpy = (char *) command;
	unsigned int len = 0;
	int index = 0;

	while (cpy[0]) {
		if (str_is_links(cpy, &index)) {
			len++;
			cpy = &cpy[my_strlen(word_link[index]) - 1];
		}
		cpy = &cpy[1];
	}
	return (len);
}

char *get_next_link(char **command)
{
	char *link = NULL;
	int index = 0;

	while ((*command)[0]) {
		if (str_is_links(*command, &index)) {
			*command = &(*command)[my_strlen(link) + 1];
			return (my_strcpy(word_link[index]));
		}
		(*command) = &(*command)[1];
	}
	return (link);
}

unsigned int getlen_next_word(const char *command, int *index_word)
{
	unsigned int len = 0;

	while (command[len]) {
		if (str_is_links(&command[len], index_word))
			return (len);
		len++;
	}
	return (len);
}

char *get_next_word(char **command)
{
	int index_word = 0;
	unsigned int len = getlen_next_word((const char *) (*command),
	&index_word);
	char *word = malloc(sizeof(char) * (len + 1));

	if (!word)
		exit(ERROR);
	for (unsigned int i = 0; i != len; i++)
		word[i] = (*command)[i];
	word[len] = '\0';
	*command = &(*command)[len + my_strlen(word_link[index_word])];
	return (word);
}
