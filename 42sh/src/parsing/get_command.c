/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** get
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "struct.h"
#include "shell.h"
#include "macro.h"
#include "libprint.h"

char *get_command(char **envp)
{
	char *line = NULL;
	size_t size = 0;

	my_printf("$> ");
	if (getline(&line, &size, stdin) == -1)
		return (NULL);
	return (clean_line(line, envp));
}

int get_tab_parsed(char ***tab, unsigned int len, const char *command,
int index)
{
	char *cpy = (char *) command;
	char *(*ptr[])(char **) = {get_next_word, get_next_link};

	(*tab) = malloc(sizeof(char *) * (len + 1));
	if (!(*tab))
		exit(ERROR);
	for (unsigned int i = 0; i != len; i++)
		(*tab)[i] = ptr[index](&cpy);
	(*tab)[len] = NULL;
	return (0);
}

int my_parser(stock_t *get)
{
	char *command = get_command(get->cpy_env);
	unsigned int len_links = 0;

	if (!command)
		return (0);
	len_links = getlen_links(command);
	get_tab_parsed(&(get->tab_command), len_links + 1, command, 0);
	for (unsigned int i = 0 ; get->tab_command[i] ; i++)
		get->tab_command[i] = clean_line(get->tab_command[i],
		get->cpy_env);
	get_tab_parsed(&(get->tab_link), len_links, command, 1);
	free(command);
	return (1);
}
