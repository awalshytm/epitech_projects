/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** include
*/

#include "struct.h"
#include "macro.h"

#ifndef SHELL_H_
#define SHELL_H_

int execute_commands(char *, stock_t *);

//alias
alias_t *put_in_list(char *, char *, alias_t *, char **);
int display_alias(alias_t *);
int my_alias(char **, stock_t *);
char *if_an_alias(char *, alias_t *);
int put_alias(stock_t *);

//make_op
int make_op(int *, stock_t *);

//my_exit.c
int my_exit(char **, stock_t *);

//my_alias.c
int my_alias(char **, stock_t *);

//tools.c
int get_envvar_index(char*, char **);
char *get_envvar_val(char *, char **);

//cd.c
int my_cd(char **, stock_t *);
void set_old(char **);

//command.c
char **get_path(char **);
int execute(char *, char **, char **, int);
macro_h is_syst_function(char **, stock_t *, int);
int exe_commands(stock_t *);
int my_command(char **, stock_t *);

//env.c
char *concate(char *, char *, char);
int my_env(char **, stock_t *);
char **dup_env(char **);

//main
void destroy_tab(void **);
void display_tab(char **);

//my_setenv.c
int lenght_tab(char **);
macro_h av_gestion(char **, stock_t *);
int my_setenv(char **, stock_t *);

//my_unsetenv.c
macro_h unset_av(char **);
macro_h if_in_path(char **, char *);
macro_h had_to_remove(char *, char **);
int my_unsetenv(char **, stock_t *);

//clean_line.c
char *clean_line(char *, char **);

//get_command.c
char *get_command(char **);
int get_tab_parsed(char ***, unsigned int, const char *, int);
int my_parser(stock_t *);

//parse_command.c
int str_is_links(const char *, int *);
int getlen_links(const char *);
char *get_next_link(char **);
unsigned int getlen_next_word(const char *, int *);
char *get_next_word(char **);

#endif
