/*
** EPITECH PROJECT, 2018
** projet
** File description:
** libprint
*/

#ifndef MY_PRINT_H_
#define My_PRINT_H_

void my_putchar(char);
void my_put_nbr(int);
void my_putstr(char *);
void my_printf(char *, ...);

#endif
