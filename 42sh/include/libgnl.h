/*
** EPITECH PROJECT, 2018
** projet
** File description:
** libgnl
*/

#ifndef GNL_H_
#define GNL_H_

#define READ_SIZE 10

char *get_next_line(const int);

#endif
