/*
** EPITECH PROJECT, 2017
** libstr_tool.h
** File description:
** contain lib function prototypes
*/

#ifndef STR_TOOL_H_
#define STR_TOOL_H_

#define EVEN 100
#define UNEVEN 101

int my_getnbr(char const *);
char *my_revstr(char *);
int my_strlen(char const *);
int my_atoi(char const *);
int my_strcmp(char const *, char const *, int);
char *my_strconcat(char const *, char const *);
char *my_strcpy(char const *);
int my_strlen(char const *);
int get_nbr_char(char *, char);
char **str_to_word_array(char *, char);

#endif
