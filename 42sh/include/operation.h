/*
** EPITECH PROJECT, 2018
** 42sh
** File description:
** include
*/

#pragma once

#include "struct.h"

int my_pipe(char **, char *, stock_t *);
int red_r(char **, char *, stock_t *);
int red_l(char **, char *, stock_t *);
int dred_r(char **, char *, stock_t *);
int dred_l(char **, char *, stock_t *);
int my_and(char **, char *, stock_t *);
int or(char **, char *, stock_t *);
