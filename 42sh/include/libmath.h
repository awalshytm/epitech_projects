/*
** EPITECH PROJECT, 2018
** projet
** File description:
** libmath
*/

#ifndef LIBMATH_H_
#define LIBMATH_H_

int check_int(char *, int);
int get_powten(int);
int my_pow(int, int);
char *nbr_to_str(int);

#endif
