/*
** EPITECH PROJECT, 2018
** projet
** File description:
** macro
*/

#ifndef MACRO_H_
#define MACRO_H_

#define UNUSED __attribute__((__unused__))

typedef enum {
	SUCCESS = 0,	
	ERROR = 84,
	GOOD_INPUT = 1,
	EXIT = -6000
} macro_h;

#endif
