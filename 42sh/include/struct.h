/*
** EPITECH PROJECT, 2018
** project
** File description:
** struct
*/

#ifndef STRUCT_H_
#define STRUCT_H_

typedef struct alias {
	char *name;
	char *cmd;
	struct alias *next;
} alias_t;

typedef struct stock {
	char **tab_command;
	char **tab_link;
	char **cpy_env;
	alias_t *alias;
	int return_value;
} stock_t;

#endif
