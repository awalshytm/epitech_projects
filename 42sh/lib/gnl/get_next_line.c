/*
** EPITECH PROJECT, 2018
** gnl
** File description:
** oui
*/

#include <stdlib.h>
#include <unistd.h>

char *re_malloc(char *buff, char add, int size)
{
	char *str = malloc(size + 1);

	for (int i = 0; i != size ; i++)
		str[i] = buff[i];
	free(buff);
	str[size] = add;
	return (str);
}

char *get_next_line(int fd)
{
	char *line = NULL;
	char readed;
	int size = 0;

	if (read(fd, &readed, 1) <= 0)
		return (NULL);
	if (readed == '\n')
		return ("\n");
	do {
		line = re_malloc(line, readed, size);
		size++;
		if (read(fd, &readed, 1) <= 0)
			break;
	} while (readed != '\0');
	line = re_malloc(line, 0, size);
	return (line);
}
