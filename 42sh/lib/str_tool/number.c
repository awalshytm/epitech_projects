/*
** EPITECH PROJECT, 2017
** strtool
** File description:
** number
*/

int my_atoi(char const *str)
{
	int nbr = 0;

	for (unsigned int i = 0; str[i]; i++)
		nbr = nbr * 10 + str[i] - 48;
	return (nbr);
}

int my_getnbr(char const *str)
{
	int sign = 1;
	int result = 0;
	int i = 0;

	if (str[i] == '-') {
		sign = -1;
		i++;
	}
	while (str[i] && str[i] >= '0' && str[i] <= '9') {
		result = result * 10 + str[i] - 48;
		i = i + 1;
	}
	return (result * sign);
}
