/*
** EPITECH PROJECT, 2018
** lib
** File description:
** concate
*/

#include <stdlib.h>
#include "libstr_tool.h"

char *concate(char *first, char *second, char c)
{
	int ier = my_strlen(first);
	int snd = my_strlen(second);
	char *result = malloc(sizeof(char) * (ier + snd + 2));
	int i = 0;

	if (result == NULL)
		return (NULL);
	for ( ; i != ier ; i++)
		result[i] = first[i];
	result[i] = c;
	i++;
	for (int y = 0; y != snd; y++) {
		result[i] = second[y];
		i++;
	}
	result[i] = 0;
	return (result);
}
