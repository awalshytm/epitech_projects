/*
** EPITECH PROJECT, 2018
** lib
** File description:
** strlen
*/

#include <stdlib.h>

int my_strlen(char const *str)
{
	int h = 0;

	if (str == NULL)
		return (0);
	while (str[h] != '\0')
		h = h + 1;
	return (h);
}
