/*
** EPITECH PROJECT, 2018
** strtool
** File description:
** str to word array
*/

#include <stdlib.h>
#include <unistd.h>

int get_nbr_char(char *str, char c)
{
	int nbr = 0;

	for (int i = 0; str[i]; i++)
		if (str[i] == c)
			nbr++;
	return (nbr);
}

static int len_next_word(char *str, char c)
{
	int len = 0;

	while (str[len] != c && str[len])
		len++;
	return (len);
}

static char *get_next_word(char **str, char c)
{
	char *word = malloc(sizeof(char) * (len_next_word(str[0], c) + 1));
	int i = 0;

	if (!word)
		return (NULL);
	for (i = 0; str[0][i] && str[0][i] != c; i++)
		word[i] = str[0][i];
	word[i] = '\0';
	*str = &str[0][++i];
	return (word);
}

char **str_to_word_array(char *str, char c)
{
	int len = get_nbr_char(str, c) + 1;
	char **array = malloc(sizeof(char *) * (len + 1));
	int index = 0;

	if (!array)
		return (NULL);
	while (index != len) {
		array[index] = get_next_word(&str, c);
		if (!array[index])
			return (NULL);
		index++;
	}
	array[index] = NULL;
	return (array);
}
