/*
** EPITECH PROJECT, 2017
** strtool
** File description:
** tool
*/

#include <stdlib.h>
#include "libstr_tool.h"

int my_strlen(char const *str)
{
	unsigned int i = 0;

	if (str)
		while (str[i])
			i++;
	return (i);
}

char *my_revstr(char *str)
{
	char save;
	int i = 0;
	int lenght = my_strlen(str) - 1;

	while (i < lenght) {
		save = str[i];
		str[i] = str[lenght];
		str[lenght] = save;
		i++;
		lenght--;
	}
	return (str);
}

int my_strcmp(char const *str, char const *cmp, int equal)
{
	if (equal == EVEN && my_strlen(str) != my_strlen(cmp))
		return (UNEVEN);
	for (unsigned int i = 0; str[i] && cmp[i]; i++)
		if (str[i] != cmp[i])
			return (UNEVEN);
	return (EVEN);
}

char *my_strconcat(char const *start, char const *end)
{
	char *str = malloc(sizeof(char) *
	(my_strlen(start) + my_strlen(end) + 1));
	unsigned int i;

	for (i = 0; start[i]; i++)
		str[i] = start[i];
	for (unsigned int j = 0; end[j]; j++) {
		str[i] = end[j];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char *my_strcpy(char const *str)
{
	char *cpy = malloc(sizeof(char) * my_strlen(str) + 1);

	for (unsigned int i = 0; str[i]; i++)
		cpy[i] = str[i];
	cpy[my_strlen(str)] = '\0';
	return (cpy);
}
