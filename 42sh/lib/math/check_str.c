/*
** EPITECH PROJECT, 2018
** libmath
** File description:
** checkstr
*/

#include <stdlib.h>
#include <unistd.h>

int check_int(char *str, int sign)
{
	int i = 0;

	if (str[i] == '-' && sign == 1)
		i++;
	for (i = i; str[i] != '\0'; i++)
		if (str[i] > '9' || str[i] < '0')
			return (1);
	return (0);
}

int get_powten(int nb)
{
	int i;

	for (i = 0; nb != 0; i++)
		nb /= 10;
	return (i);
}

int my_pow(int nb, int pow)
{
	int res = 1;

	for (int i = 0; i != pow; i++)
		res *= nb;
	return (res);
}

char *nbr_to_str(int nb)
{
	int pow = get_powten(nb);
	char *str = malloc(sizeof(char) * (pow + 1) + 1);
	int i = 0;

	if (str == NULL)
		return (NULL);
	if (nb < 0) {
		str[i] = '-';
		i++;
		nb *= -1;
	}
	while (pow != 0) {
		str[i] = nb / my_pow(10, pow - 1) + 48;
		nb %= my_pow(10, pow - 1);
		pow--;
		i++;
	}
	str[i] = '\0';
	return (str);
}
