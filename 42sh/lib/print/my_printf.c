/*
** EPITECH PROJECT, 2018
** my_printf
** File description:
** display function
*/

#include <stdarg.h>
#include <unistd.h>
#include "libstr_tool.h"

void my_putchar(void *c)
{
	write(1, &c, 1);
}

void my_putstr(void *ptr)
{
	char *str = (char *) ptr;

	for (unsigned int i = 0; str[i]; i++)
		write(1, &str[i], 1);
}

void my_put_nbr(void *ptr)
{
	int *ptr_int = (int *) &ptr;
	int nb = *ptr_int;
	int mod;
	char c;
	void **tmp;

	if (nb < 0) {
		write(1, "-", 1);
		nb *= -1;
	}
	if (nb >= 10) {
		mod = nb % 10;
		nb = (nb - mod) / 10;
		tmp = (void **) &nb;
		my_put_nbr(*tmp);
		c = 48 + mod;
	} else
		c = 48 + nb % 10;
	write(1, &c, 1);
}

static void get_flag(char **str, va_list ag)
{
	char tab[] = "dsc";
	void (*ptr_funct[])(void *) = {my_put_nbr, my_putstr, my_putchar};

	if (str[0][0] == '%') {
		write(1, "%", 1);
		return;
	}
	for (unsigned int i = 0; tab[i] != '\0'; i++)
		if (tab[i] == str[0][0])
			ptr_funct[i](va_arg(ag, void *));
}

void my_printf(char *str, ...)
{
	va_list ag;

	va_start(ag, str);
	while (str[0] != '\0') {
		if (str[0] == '%') {
			str = &str[1];
			get_flag(&str, ag);
		} else
			write(1, &str[0], 1);
		str = &str[1];
	}
	va_end(ag);
}
