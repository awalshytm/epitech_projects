/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** simple list
*/

#include <stdlib.h>
#include <stdio.h>
#include "simple_list.h"

unsigned int list_get_size(list_t list)
{
    list_t tmp = list;
    int i = 0;

    while (tmp != NULL) {
        tmp = tmp->next;
        i++;
    }
    return (i);
}

bool_t list_is_empty(list_t list)
{
    if (list == NULL)
        return (TRUE);
    return (FALSE);
}

void list_dump(list_t list)
{
    list_t tmp = list;

    while (tmp != NULL) {
        printf("%f\n", tmp->value);
        tmp = tmp->next;
    }
}