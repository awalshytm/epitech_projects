#include <criterion/criterion.h>
#include "simple_list.h"

Test(list, add_back)
{
    list_t list = NULL;

    list_add_elem_at_back(&list, 1.3);
    cr_assert_neq(list, NULL, "list null");
    cr_assert_eq(list->value, 1.3, "node value false");
    list_add_elem_at_back(&list, 1.2);
    list_add_elem_at_back(&list, 1.4);
    list_add_elem_at_back(&list, 1.5);
    list_add_elem_at_back(&list, 1.6);
    list_add_elem_at_back(&list, 1.7);
    list_add_elem_at_back(&list, 1.8);
    list_add_elem_at_back(&list, 1.9);
    cr_assert_eq(list_get_size(list), 8, "size not good, missing elements");
}

Test(list, add_front)
{
    list_t list = NULL;

    list_add_elem_at_front(&list, 1.3);
    cr_assert_neq(list, NULL, "list null");
    cr_assert_eq(list->value, 1.3, "node value false");
    list_add_elem_at_front(&list, 1.2);
    list_add_elem_at_front(&list, 1.4);
    list_add_elem_at_front(&list, 1.5);
    list_add_elem_at_front(&list, 1.6);
    list_add_elem_at_front(&list, 1.7);
    list_add_elem_at_front(&list, 1.8);
    list_add_elem_at_front(&list, 1.9);
    cr_assert_eq(list_get_size(list), 8, "missing elements");
}

Test(list, add_pos)
{
    list_t list = NULL;

    list_add_elem_at_position(&list, 1.3, 1);
    cr_assert_neq(list, NULL, "list null");
    cr_assert_eq(list->value, 1.3, "node value false");
    list_add_elem_at_position(&list, 1.2, 0);
    list_add_elem_at_position(&list, 1.4, 1);
    list_add_elem_at_position(&list, 1.5, 1);
    list_add_elem_at_position(&list, 1.6, 2);
    list_add_elem_at_position(&list, 1.7, 4);
    list_add_elem_at_position(&list, 1.8, 3);
    cr_assert_eq(list_get_size(list), 7, "missing element");
}

Test(add, mix)
{
    list_t list = NULL;

    list_add_elem_at_back(&list, 1.3);
    list_dump(list);
    list_add_elem_at_front(&list, 88.6);
    list_dump(list);
    list_add_elem_at_position(&list, 42.5, 1);
    list_dump(list);
    list_add_elem_at_position(&list, 5.3, 0);
    list_dump(list);
    list_add_elem_at_back(&list, 1.2);
    list_dump(list);
}
