/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** simple list
*/

#include <stdio.h>
#include <stdlib.h>
#include "simple_list.h"

double list_get_elem_at_front(list_t list)
{
    return (list->value);
}

double list_get_elem_at_back(list_t list)
{
	if (list == NULL)
		return (0);
	else if (list->next == NULL)
		return (list->value);
	else
		return (list_get_elem_at_back(list->next));
}

double list_get_elem_at_position(list_t list, unsigned int position)
{
    list_t tmp = list;

    if (list == NULL || position > list_get_size(list))
        return (-1);
    for (unsigned int i = 0; i < position && tmp->next != NULL; i++)
        tmp = tmp->next;
    return (tmp->value);
}

node_t *list_get_first_node_with_value(list_t list, double value)
{
    list_t tmp = list;

    while (tmp != NULL) {
        if (tmp->value == value)
            return (tmp);
        tmp = tmp->next;
    }
    return (NULL);
}
