/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** simple list
*/

#include <stdio.h>
#include <stdlib.h>
#include "simple_list.h"

bool_t list_add_elem_at_front(list_t *front_ptr, double elem)
{
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    e->next = NULL;
    if ((*front_ptr) == NULL) {
        (*front_ptr) = e;
        return (TRUE);
    }
    e->next = (*front_ptr);
    (*front_ptr) = e;
    return (TRUE);
}

bool_t list_add_elem_at_back(list_t *front_ptr, double elem)
{
    list_t tmp = (*front_ptr);
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    e->next = NULL;
    if ((*front_ptr) == NULL) {
        (*front_ptr) = e;
        return (TRUE);
    }
    while (tmp->next != NULL)
        tmp = tmp->next;
    tmp->next = e;
    return (TRUE);
}

bool_t list_add_elem_at_position(list_t *front_ptr, double elem,
unsigned int position)
{
    list_t tmp = (*front_ptr);
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    e->next = NULL;
    if ((*front_ptr) == NULL) {
        (*front_ptr) = e;
        return (TRUE);
    }
    if (position == 0)
        return (list_add_elem_at_front(front_ptr, elem));
    if (position > list_get_size(*front_ptr))
        return (FALSE);
    for (unsigned int i = 1; i < position && tmp != NULL; i++)
        tmp = tmp->next;
    e->next = tmp->next;
    tmp->next = e;
    return (TRUE);
}
