/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** btree
*/

#include <stdlib.h>
#include "simple_btree.h"

unsigned int btree_get_size(tree_t tree)
{
    int size = 1;

    if (tree->right != NULL)
        size += btree_get_size(tree->right);
    if (tree->left != NULL)
        size += btree_get_size(tree->left);
    return (size);
}

unsigned int btree_get_depth(tree_t tree)
{
    int depth_right = 0;
    int depth_left = 0;

    if (tree->right != NULL)
        depth_right = btree_get_depth(tree->right);
    if (tree->left != NULL)
        depth_left = btree_get_depth(tree->left);
    if (depth_left < depth_right)
        return (depth_right + 1);
    return (depth_left + 1);
}

double btree_get_max_value(tree_t tree)
{
    double max = tree->value;
    double res = 0;

    if (tree->right != NULL)
        res = btree_get_min_value(tree->right);
    if (res > max)
        max = res;
    if (tree->left != NULL)
        res = btree_get_min_value(tree->left);
    if (res > max)
        max = res;
    return (max);
}

double btree_get_min_value(tree_t tree)
{
    double min = tree->value;
    double res = -1;

    if (tree->right != NULL)
        res = btree_get_min_value(tree->right);
    if (res != -1 && res < min)
        min = res;
    if (tree->left != NULL)
        res = btree_get_min_value(tree->left);
    if (res != -1 && res < min)
        min = res;
    return (min);
}