/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02a
*/

#include <stdlib.h>
#include "simple_btree.h"

bool_t btree_is_empty(tree_t tree)
{
    if (tree == NULL)
        return TRUE;
    return FALSE;
}


bool_t btree_delete(tree_t *root_ptr)
{
    bool_t res;

    if ((*root_ptr) == NULL)
        return FALSE;
    if ((*root_ptr)->right != NULL)
        res = btree_delete(&(*root_ptr)->right);
    if ((*root_ptr)->left != NULL && res == TRUE)
        res = btree_delete(&(*root_ptr)->left);
    if (res == TRUE) {
        free((*root_ptr));
        return TRUE;
    }
    return FALSE;
}

bool_t btree_create_node(tree_t *root_ptr, double value)
{
    tree_t node = malloc(sizeof(tree_t));

    if (!node)
        return FALSE;
    node->right = NULL;
    node->left = NULL;
    node->value = value;
    (*root_ptr) = node;
    return TRUE;
}