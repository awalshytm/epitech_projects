/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** generic list
*/

#include <stdlib.h>
#include "generic_list.h"

unsigned int list_get_size(list_t list)
{
    list_t tmp = list;
    int i = 0;

    while (tmp != NULL) {
        tmp = tmp->next;
        i++;
    }
    return (i);
}

void *list_get_elem_at_front(list_t list)
{
    return (list->value);
}

void *list_get_elem_at_back(list_t list)
{
	if (list == NULL)
		return (0);
	else if (list->next == NULL)
		return (list->value);
	else
		return (list_get_elem_at_back(list->next));
}

void *list_get_elem_at_postition(list_t list, unsigned int position)
{
    list_t tmp = list;

    for (unsigned int i = 0; i < position && tmp->next != NULL; i++)
        tmp = tmp->next;
    return (tmp->value);
}

node_t *list_get_first_node_with_value(list_t list, void *value, value_comparator_t val_comp)
{
    list_t tmp = list;

    while (tmp != NULL)
        if ((val_comp)(tmp->value, value) == 0)
            return (tmp);
    return (NULL);
}