/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** generic list
*/

#include <stdlib.h>
#include "generic_list.h"

bool_t list_is_empty(list_t list)
{
    if (list == NULL)
        return TRUE;
    return FALSE;
}

void list_dump(list_t list, value_displayer_t val_disp)
{
    list_t tmp = list;

    while (tmp != NULL) {
        (val_disp)(tmp->value);
        tmp = tmp->next;
    }
}

void list_clear(list_t *front_ptr)
{
    list_t tmp = (*front_ptr);
    list_t tmp2;

    if (tmp != NULL)
        tmp2 = tmp->next;
    while (tmp2 != NULL) {
        free(tmp);
        tmp = tmp2;
        tmp2 = tmp2->next;
    }
}