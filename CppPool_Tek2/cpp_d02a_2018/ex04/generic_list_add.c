/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** generic list
*/

#include <stdlib.h>
#include "generic_list.h"

bool_t list_add_elem_at_front(list_t *front_ptr, void *elem)
{
    list_t tmp = (*front_ptr);
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    e->next = tmp;
    (*front_ptr) = e;
    return (TRUE);
}

bool_t list_add_elem_at_back(list_t *front_ptr, void *elem)
{
    list_t tmp = (*front_ptr);
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    if ((*front_ptr) == NULL) {
        (*front_ptr) = e;
        return (TRUE);
    }
    while (tmp->next != NULL)
        tmp = tmp->next;
    tmp->next = e;
    return (TRUE);
}

bool_t list_add_elem_at_position(list_t *front_ptr, void *elem, unsigned int position)
{
    list_t tmp = (*front_ptr);
    list_t e;

    if (!(e = malloc(sizeof(list_t))))
        return (FALSE);
    e->value = elem;
    for (unsigned int i = 0; i < position && tmp != NULL; i++)
        tmp = tmp->next;
    tmp->next = e;
    return (TRUE);
}