/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** generic list
*/

#include <stdlib.h>
#include "generic_list.h"

bool_t list_del_elem_at_front(list_t *front_ptr)
{
    list_t tmp = (*front_ptr);

    if (tmp == NULL)
        return FALSE;
    front_ptr = &(*front_ptr)->next;
    free(tmp);
    return TRUE;
}

bool_t list_del_elem_at_back(list_t *front_ptr)
{
    list_t tmp = (*front_ptr);
    list_t tmp2;

    if (tmp == NULL)
        return FALSE;
    if (tmp->next == NULL) {
        front_ptr = NULL;
        free(tmp);
        return TRUE;
    }
    tmp2 = tmp->next;
    while (tmp2->next != NULL) {
        tmp2 = tmp2->next;
        tmp = tmp->next;
    }
    tmp->next = NULL;
    free(tmp2);
    return TRUE;
}

bool_t list_del_elem_at_position(list_t *front_ptr, unsigned int position)
{
    list_t tmp = (*front_ptr);
    list_t tmp2;

    if (tmp == NULL)
        return FALSE;
    tmp2 = tmp->next;
    for (unsigned int i = 1; i < position && tmp != NULL; i++) {
        tmp = tmp->next;
        tmp2 = tmp2->next;
    }
    tmp->next = tmp2->next;
    free(tmp2);
    return TRUE;
}