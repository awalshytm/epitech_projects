/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** map
*/

#include <stdlib.h>
#include <stdio.h>
#include "generic_list.h"
#include "map.h"

unsigned int map_get_size(map_t map)
{
    return (list_get_size(map));
}

bool_t map_is_empty(map_t map)
{
    return (list_is_empty(map));
}

key_comparator_t key_cmp_container(bool_t store, key_comparator_t new_key_cmp)
{
    static key_comparator_t key_cmp;

    if (store == TRUE)
        key_cmp = new_key_cmp;
    return(key_cmp);
}

int pair_comparator(void *first, void *second)
{
    pair_t firstp = *(pair_t *)first;
    pair_t secondp = *(pair_t *)second;
    void *key1 = firstp.key;
    void *key2 = secondp.key;

    return (key2 - key1);
}

bool_t map_add_elem(map_t *map_ptr, void *key, void *value,
key_comparator_t key_cmp)
{
    pair_t *pair = malloc(sizeof(pair_t));
    map_t tmp = (*map_ptr);
    void *val;

    if (pair == NULL || key == NULL || value == NULL || map_ptr == NULL)
        return (FALSE);
    pair->key = key;
    pair->value = value;
    while (tmp != NULL) {
        if ((val = map_get_elem(*map_ptr, key, key_cmp)) != NULL) {
            ((pair_t *)tmp->value)->value = value;
            return (TRUE);
        }
        tmp = tmp->next;
    }
    list_add_elem_at_back(map_ptr, pair);
    return (TRUE);
}

bool_t map_del_elem(map_t *map_ptr, void*key, key_comparator_t key_cmp)
{
    map_t tmp = (*map_ptr);
    pair_t pair = {key, NULL};
    int i = 0;

    if (map_ptr == NULL)
        return (FALSE);
    while (tmp != NULL) {
        if (key_cmp(tmp->value, &pair) == 0) {
            list_del_elem_at_position(map_ptr, i);
            return (TRUE);
        }
        tmp = tmp->next;
        i++;
    }
    return (FALSE);
}

void *map_get_elem(map_t map, void *key, key_comparator_t key_cmp)
{
    map_t tmp = map;
    pair_t pair = {key, NULL};
    
    if (map == NULL || key == NULL)
        return (FALSE);
    while (tmp != NULL) {
        if (key_cmp(tmp->value, &pair) == 0)
            return (((pair_t *)tmp->value)->value);
        tmp = tmp->next;
    }
    return (NULL);
}