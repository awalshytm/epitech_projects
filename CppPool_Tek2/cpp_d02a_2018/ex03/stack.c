/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** stack
*/

#include <stdlib.h>
#include "generic_list.h"
#include "stack.h"

unsigned int stack_get_size(stack_t stack)
{
    return (list_get_size(stack));
}

bool_t stack_is_empty(stack_t stack)
{
    return (list_is_empty(stack));
}

bool_t stack_push(stack_t *stack, void *elem)
{
    return (list_add_elem_at_front(stack, elem));
}

bool_t stack_pop(stack_t *stack)
{
    return (list_del_elem_at_front(stack));
}

void *stack_top(stack_t stack)
{
    return (list_get_elem_at_front(stack));
}