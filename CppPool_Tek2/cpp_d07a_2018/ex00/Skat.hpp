/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** skat
*/

#ifndef _SKAT_H_
#define _SKAT_H_

#include <string>

class Skat {
        public:
                Skat(const std::string &name = "Bob", int stimPaks = 5);
                ~Skat();
                unsigned int &stimPaks();
                const std::string &name() const;
                void shareStimPaks(int number, unsigned int &stock);
                void addStimPaks(unsigned int number);
                void useStimPaks();
                void status() const;
        private:
                std::string _name;
                unsigned int _number;
};

#endif
