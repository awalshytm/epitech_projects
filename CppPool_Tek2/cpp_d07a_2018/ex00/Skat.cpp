/*
** EPITECH PROJECT, 2019
** cppol
** File description:
** skat
*/

#include <iostream>
#include <string>
#include "Skat.hpp"

Skat::Skat(const std::string &name, int stimPaks)
	:_name(name), _number(stimPaks)
{}

Skat::~Skat()
{
}

unsigned int &Skat::stimPaks()
{
	return (_number);
}

const std::string &Skat::name() const
{
	return (_name);
}

void Skat::shareStimPaks(int number, unsigned int &stock)
{
	if (number > _number) {
		std::cout << "Don't be gready" << std::endl;
		return;
	}
        stock += number;
        _number -= number;
	std::cout << "Keep the change." << std::endl;
}

void Skat::addStimPaks(unsigned int number)
{
	if (number == 0) {
		std::cout << "Hey boya, did you forget something?"
		<< std::endl;
		return;
	}
	_number += number;
}

void Skat::useStimPaks()
{
	if (_number - 1 <= 0) {
		std::cout << "Mediiiiiic" << std::endl;
		return;
	}
	std::cout << "Time to kick some ass and chew bubble gum" << std::endl;
	--_number;
}

void Skat::status() const
{
	std::cout << "Soldier " << _name << " reporting " << _number <<
	" stimpaks remaining sir!" << std::endl;
}
