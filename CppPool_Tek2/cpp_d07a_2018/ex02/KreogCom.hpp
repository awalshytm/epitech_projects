/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _KREOG_COM_H_
#define _KREOG_COM_H_

class KreogCom {
	private:
		int _x;
		int _y;
		int m_serial;
		KreogCom *_next;
	public:
		KreogCom(int x, int y, int serial);
		~KreogCom();
		void addCom(int x, int y, int serial);
		void removeCom();
		KreogCom *getCom();
		void ping();
		void locateSquad();
};

#endif