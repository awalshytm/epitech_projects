/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** ex02
*/

#include <string>
#include <iostream>
#include "KreogCom.hpp"

KreogCom::KreogCom(int x, int y, int serial)
        :_x(x), _y(y), m_serial(serial), _next(nullptr)
{
        std::cout << "KreogCom " << m_serial << " initialized" << std::endl;
}

KreogCom::~KreogCom()
{
        std::cout << "KreogCom " << m_serial << " shutting down" << std::endl;
}

KreogCom *KreogCom::getCom()
{
        return _next;
}

void KreogCom::ping()
{
        std::cout << "KreogCom " << m_serial << " currently at "
        << _x << " " << _y << std::endl;
}

void KreogCom::addCom(int x, int y, int serial)
{
        KreogCom *e = new KreogCom(x, y, serial);

        if (!_next) {
                _next = e;
                return;
        }
        e->_next = _next;
        _next = e;
}

void KreogCom::removeCom()
{
        KreogCom *tmp;

        if (!_next)
                return;
        if (_next->_next) {
                tmp = _next;
                _next = _next->_next;
                delete tmp;
        }
        delete _next;
        _next = nullptr;
}

void KreogCom::locateSquad()
{
        KreogCom *tmp = _next;

        while (tmp != nullptr) {
                tmp->ping();
                tmp = tmp->getCom();
        }
        ping();
}