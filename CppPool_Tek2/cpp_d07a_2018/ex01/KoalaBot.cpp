/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** koala bot
*/

#include <iostream>
#include <string>
#include "Parts.hpp"
#include "KoalaBot.hpp"

KoalaBot::KoalaBot(std::string serial = std::string("Bob-01"))
	:_serial(serial), _arms(nullptr), _legs(nullptr), _head(nullptr)
{}

KoalaBot::KoalaBot()
	:_serial("Bob-01"), _arms(new Arms()), _legs(new Legs()), 
	_head(new Head())
{}

void KoalaBot::setParts(Arms *arms)
{
	if (!arms)
		return;
	_arms = arms;
}

void KoalaBot::setParts(Legs *legs)
{
	if (!legs)
		return;
	_legs = legs;
}

void KoalaBot::setParts(Head *head)
{
	if (!head)
		return;
	_head = head;
}

void KoalaBot::swapParts(Arms *arms)
{
	if (!arms)
		return;
	_arms = arms;
}

void KoalaBot::swapParts(Legs *legs)
{
	if (!legs)
		return;
	_legs = legs;
}

void KoalaBot::swapParts(Head *head)
{
	if (!head)
		return;
	_head = head;
}

void KoalaBot::informations()
{
	std::cout << "[KoalaBot] " << _serial << std::endl;
	_arms->informations();
	_legs->informations();
	_head->informations();
}

bool KoalaBot::status()
{
	if (_arms->isFunctionnal() == false || _legs->isFunctionnal() == false
	|| _head->isFunctionnal() == false)
		return (false);
	return (true);
}