/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _PARTS_H_
#define _PARTS_H_

class Arms {
	private:
		std::string _serial;
		bool _functionnal;
	public:
		Arms(std::string _serial, bool _functionnal);
		Arms();
		std::string serial();
		bool isFunctionnal();
		void informations();
};

class Legs {
	private:
		std::string _serial;
		bool _functionnal;
	public:
		Legs(std::string _serial, bool _functionnal);
		Legs();
		std::string serial();
		bool isFunctionnal();
		void informations();
};

class Head {
	private:
		std::string _serial;
		bool _functionnal;
	public:
		Head(std::string _serial, bool _functionnal);
		Head();
		std::string serial();
		bool isFunctionnal();
		void informations();
};

#endif