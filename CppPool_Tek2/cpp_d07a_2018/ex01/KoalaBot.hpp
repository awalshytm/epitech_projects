/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _KOALA_BOT_H_
#define _KOALA_BOT_H_

class KoalaBot {
        private:
                std::string _serial;
                Arms *_arms;
                Legs *_legs;
                Head *_head;
        public:
                KoalaBot(std::string serial);
                KoalaBot();
                void setParts(Arms *arms);
                void setParts(Legs *legs);
                void setParts(Head *head);
                void swapParts(Arms *arms);
                void swapParts(Legs *legs);
                void swapParts(Head *head);
                void informations();
                bool status();
};

#endif