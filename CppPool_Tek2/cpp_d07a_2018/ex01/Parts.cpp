/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** parts
*/

#include <string>
#include <iostream>
#include "Parts.hpp"

//ARMS
Arms::Arms(std::string serial = std::string("A-01"), bool functionnal = true)
        :_serial(serial), _functionnal(functionnal)
{}

Arms::Arms()
        :_serial("A-01"), _functionnal(true)
{}

std::string Arms::serial()
{
        return (_serial);
}

bool Arms::isFunctionnal()
{
        return (_functionnal);
}

void Arms::informations()
{
        std::cout << "\t[Parts] Arms " << _serial << " status: " <<
        (_functionnal ? "OK" : "KO") << std::endl;
}

//LEGS
Legs::Legs(std::string serial = std::string("L-01"), bool functionnal = true)
        :_serial(serial), _functionnal(functionnal)
{}

Legs::Legs()
        :_serial("L-01"), _functionnal(true)
{}

std::string Legs::serial()
{
        return (_serial);
}

bool Legs::isFunctionnal()
{
        return (_functionnal);
}

void Legs::informations()
{
        std::cout << "\t[Parts] Legs " << _serial << " status: " <<
        (_functionnal ? "OK" : "KO") << std::endl;
}

//HEAD
Head::Head(std::string serial = std::string("H-01"), bool functionnal = true)
        :_serial(serial), _functionnal(functionnal)
{}

Head::Head()
        :_serial("H-01"), _functionnal(true)
{}

std::string Head::serial()
{
        return (_serial);
}

bool Head::isFunctionnal()
{
        return (_functionnal);
}

void Head::informations()
{
        std::cout << "\t[Parts] Head " << _serial << " status: " <<
        (_functionnal ? "OK" : "KO") << std::endl;
}