/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Woody.hpp"

Woody::Woody(std::string const &name, std::string ascii)
        :Toy(Toy::WOODY, name, ascii)
{}

Woody::~Woody()
{}

void Woody::speak(std::string statement)
{
        std::cout << "WOODY: " <<
        _name << " \"" << statement << "\"" << std::endl;
}