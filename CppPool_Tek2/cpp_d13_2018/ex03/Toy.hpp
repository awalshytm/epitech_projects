/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _TOY_H_
#define _TOY_H_

#include "Picture.hpp"

class Toy {
        public:
                enum ToyType { ALIEN, BASIC_TOY, WOODY, BUZZ };
                Toy(ToyType type = Toy::BASIC_TOY,
                std::string name = "name",
                std::string filename = "");
                Toy(Toy const &other);
                ~Toy();
                ToyType getType() const;
                std::string getName() const;
                std::string getFilename() const;
                void setName(std::string name);
                void setAscii(const std::string &filename);
                std::string getAscii() const;
                virtual void speak(std::string statement);
                Toy &operator=(Toy &rt);
        protected:
                ToyType _type;
                std::string _name;
                std::string _filename;
                Picture _picture;
};

#endif