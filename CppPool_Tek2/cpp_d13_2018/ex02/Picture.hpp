/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#ifndef _PICTURE_H_
#define _PICTURE_H_

#include <iostream>
#include <fstream>
#include <string>

class Picture {
        private:
                std::string data;
                bool _error;
        public:
                Picture(const std::string &file);
                ~Picture();
                Picture(Picture const &other);
                bool getPictureFromFile(const std::string &file);
                std::string getAscii() const;
                bool hasError() const;
                Picture &operator=(Picture &p);
};

#endif