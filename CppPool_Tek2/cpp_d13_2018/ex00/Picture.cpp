/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Picture.hpp"

Picture::Picture(const std::string &file)
        :data(""), _error(false)
{
        if (file.empty())
                return;
        if (getPictureFromFile(file) == false) {
                _error = true;
        }
}

std::string Picture::getAscii() const { return data; }

bool Picture::hasError() const { return _error; }

bool Picture::getPictureFromFile(const std::string &path)
{
        std::ifstream file(path);
	std::string line;
        std::string res;

	if (file.is_open())
	{
		while (getline(file, line)) {
			res.append(line);
                        res.append("\n");
		}
		file.close();
                data = res;
	} else {
		data = std::string("ERROR");
                return (false);
	}
        return (true);
}