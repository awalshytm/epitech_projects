/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _TOY_H_
#define _TOY_H_

#include "Picture.hpp"

class Toy {
        public:
                enum ToyType { ALIEN, BASIC_TOY };
                Toy(ToyType type = Toy::BASIC_TOY,
                std::string name = "name",
                std::string filename = "");
                ToyType getType() const;
                std::string getName() const;
                void setName(std::string name);
                void setAscii(const std::string &filename);
                std::string getAscii() const;
        private:
                ToyType _type;
                std::string _name;
                std::string _filename;
                Picture _picture;
};

#endif