/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Toy.hpp"

Toy::Toy(ToyType type, std::string name, std::string filename)
        :_type(type), _name(name), _filename(filename),
        _picture(Picture(filename))
{}

std::string Toy::getName() const { return _name; }

Toy::ToyType Toy::getType() const { return _type; }

std::string Toy::getAscii() const { return _picture.getAscii(); }

void Toy::setName(std::string name)
{
        _name = name;
}

void Toy::setAscii(std::string const &filename)
{
        _picture.getPictureFromFile(filename);
}