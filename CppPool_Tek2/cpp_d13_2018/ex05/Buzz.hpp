/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef BUZZ_HPP_
	#define BUZZ_HPP_

#include "Toy.hpp"

class Buzz : public Toy
{
	public:
		Buzz(std::string const &name, std::string ascii = "buzz.txt");
		~Buzz();
		void speak(std::string statement) override;
		bool speak_es(std::string statement) override;
};

#endif /* !BUZZ_HPP_ */
