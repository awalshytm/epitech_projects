/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Toy.hpp"

Toy::Toy(ToyType type, std::string name, std::string filename)
        :_type(type), _name(name), _filename(filename),
        _picture(Picture(filename))
{}

Toy::Toy(Toy const &other)
        :_type(other.getType()), _name(other.getName()),
        _filename(other.getFilename()), _picture(Picture(_filename))
{}

Toy::~Toy()
{}

std::string Toy::getName() const { return _name; }

std::string Toy::getFilename() const { return _filename; }

Toy::ToyType Toy::getType() const { return _type; }

std::string Toy::getAscii() const
{
        if (_picture.hasError())
                return ("");
        return _picture.getAscii();
}

void Toy::setName(std::string name)
{
        _name = name;
}

void Toy::setData(std::string s)
{
        _picture.setData(s);
}

bool Toy::setAscii(std::string const &filename)
{
        return (_picture.getPictureFromFile(filename));
}

void Toy::speak(std::string statement)
{
        std::cout << _name << " \"" << statement << "\"" << std::endl;
}

bool Toy::speak_es(std::string statement)
{
        (void)statement;
        return (false);
}

Toy &Toy::operator=(Toy &t)
{
        this->_filename = t.getFilename();
        this->_name = t.getName();
        this->_type = t.getType();
        return (*this);
}

std::ostream &operator<<(std::ostream &s, Toy &t)
{
        s << t.getName() << std::endl << t.getAscii() << std::endl;
        return (s);
}

Toy &Toy::operator<<(std::string s)
{
        this->setData(s);
        return (*this);
}

Toy::Error Toy::getLastError()
{
        return (Error());
}

Toy::Error::Error()
        :_type(Error::UNKNOWN), _what(nullptr), _where(nullptr)
{
        std::cout << "Error class created" << std::endl;
}

void Toy::Error::setType(ErrorType type)
{
        _type = type;
}

void Toy::Error::setWhat(std::string what)
{
        _what = what;
}

void Toy::Error::setWhere(std::string where)
{
        _where = where;
}

std::string Toy::Error::what() const { return _what; }

std::string Toy::Error::where() const { return _where; }

Toy::Error::ErrorType Toy::Error::type() const { return _type; }