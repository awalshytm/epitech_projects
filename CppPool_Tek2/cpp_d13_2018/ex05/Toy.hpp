/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _TOY_H_
#define _TOY_H_

#include <iostream>
#include <memory>
#include "Picture.hpp"

class Toy {
        public:

                class Error {
                        public:
                                enum ErrorType { UNKNOWN, PICTURE, SPEAK };
                                Error();
                                std::string what() const;
                                std::string where() const;
                                ErrorType type() const;
                                void setType(ErrorType type);
                                void setWhere(std::string where);
                                void setWhat(std::string what);
                        private:
                                ErrorType _type;
                                std::string _what;
                                std::string _where;
                };

                enum ToyType { ALIEN, BASIC_TOY, WOODY, BUZZ };
                Toy(ToyType type = Toy::BASIC_TOY,
                std::string name = "name",
                std::string filename = "");
                Toy(Toy const &other);
                ~Toy();
                Error getLastError();
                ToyType getType() const;
                std::string getName() const;
                std::string getFilename() const;
                void setName(std::string name);
                bool setAscii(const std::string &filename);
                void setData(std::string s);
                std::string getAscii() const;
                virtual void speak(std::string statement);
                virtual bool speak_es(std::string statement);
                Toy &operator=(Toy &rt);
                Toy &operator<<(std::string s);

        protected:
                ToyType _type;
                std::string _name;
                std::string _filename;
                Picture _picture;
};

std::ostream &operator<<(std::ostream &s, Toy &t);

#endif