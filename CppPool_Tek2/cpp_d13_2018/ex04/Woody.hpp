/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef WOODY_HPP_
	#define WOODY_HPP_

#include "Toy.hpp"

class Woody : public Toy
{
	public:
		Woody(std::string const &name,
                std::string ascii = "woody.txt");
		~Woody();
		void speak(std::string statment) override;
};

#endif /* !WOODY_HPP_ */
