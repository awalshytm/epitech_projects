/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Toy.hpp"

Toy::Toy(ToyType type, std::string name, std::string filename)
        :_type(type), _name(name), _filename(filename),
        _picture(Picture(filename))
{}

Toy::Toy(Toy const &other)
        :_type(other.getType()), _name(other.getName()),
        _filename(other.getFilename()), _picture(Picture(_filename))
{}

Toy::~Toy()
{}

std::string Toy::getName() const { return _name; }

std::string Toy::getFilename() const { return _filename; }

Toy::ToyType Toy::getType() const { return _type; }

std::string Toy::getAscii() const { return _picture.getAscii(); }

void Toy::setName(std::string name)
{
        _name = name;
}

void Toy::setData(std::string s)
{
        _picture.setData(s);
}

bool Toy::setAscii(std::string const &filename)
{
        return (_picture.getPictureFromFile(filename));
}

void Toy::speak(std::string statement)
{
        std::cout << _name << " \"" << statement << "\"" << std::endl;
}

Toy &Toy::operator=(Toy &t)
{
        this->_filename = t.getFilename();
        this->_name = t.getName();
        this->_type = t.getType();
        return (*this);
}

std::ostream &operator<<(std::ostream &s, Toy &t)
{
        s << t.getName() << std::endl << t.getAscii() << std::endl;
        return (s);
}

Toy &Toy::operator<<(std::string s)
{
        this->setData(s);
        return (*this);
}