/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <ncurses.h>
#include "SystemMonitor.hpp"
#include "HostAndUserNameModule.hpp"
#include "DateTimeModule.hpp"
#include "KernelInfoModule.hpp"
#include "RAMModule.hpp"
#include "CPUModule.hpp"
#include "NetworkModule.hpp"
#include "BoxDisplay.hpp"

void initModules(SystemMonitor &sysMon)
{
	sysMon.addModule(new DateTimeModule());
	sysMon.addModule(new HostAndUserNameModule());
	sysMon.addModule(new KernelInfoModule());
	sysMon.addModule(new RAMModule());
	sysMon.addModule(new CPUModule());
}

int main(int ac, char **av)
{
	SystemMonitor sysMon;
	char c = 0;

	initModules(sysMon);
	while (true) {
		c = getch();
		if (c == 'q')
			break;
		sysMon.setOp(false);
		sysMon.ModDeactivation(c);
		sysMon.ModActivation(c);
		sysMon.display();
		sleep(1);
	}
}
