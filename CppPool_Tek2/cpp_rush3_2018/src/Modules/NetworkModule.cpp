/*
** EPITECH PROJECT, 2018
** NetworkModule.cpp
** File description:
** NetworkModule.cpp
*/

#include "NetworkModule.hpp"

//Ctor Dtor
NetworkModule::NetworkModule(std::string const &name,
std::string const &title)
        :BaseModule(name, title)
{}

NetworkModule::~NetworkModule()
{}

void NetworkModule::display() const
{
	BoxDisplay box(35, 10);


	box.drawBoarders(10, 24, _title);


	std::ifstream file("/proc/net/dev");
	std::string line;
    	char buf[200], ifname[20];
    	unsigned long int r_bytes, t_bytes, r_packets, t_packets;
    	int j = 0;

	if (!file.is_open())
		return;
    	for (int i = 0; i < 2; i++) {
        	getline(file, line);
    	}
    	while (getline(file, line)) {
        	scanw(buf, "%[^:]: %lu %lu %*lu %*lu %*lu %*lu %*lu %*lu %lu %lu",
               	ifname, &r_bytes, &r_packets, &t_bytes, &t_packets);
		mvprintw(28, 12,"wlp58s0\tBytes\t\tPackets\n\t    received\t%lu\t%lu\n\t    transmitted\t%lu\t%lu\n",
		std::string(ifname, 20), r_bytes, r_packets, t_bytes, t_packets);
		j++;
		if (j == 1)
			break;
		printw("\n");
    	}
	file.close();
}
void NetworkModule::refreshData()
{}
