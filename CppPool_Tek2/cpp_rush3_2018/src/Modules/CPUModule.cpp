/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** cpu module
*/

#include "CPUModule.hpp"

CPUModule::CPUModule(std::string const &name, std::string const &title)
	:BaseModule(name, title)
{}

CPUModule::~CPUModule()
{}

void CPUModule::getTemps()
{
	std::ifstream _cpuName("/sys/class/thermal/thermal_zone1/type");
	std::ifstream _cpuTemp("/sys/class/thermal/thermal_zone1/temp",
	std::ios::in);
	std::string temp;

	if (!_cpuTemp.is_open())
		throw Exceptions("failed to open");
	if (!_cpuName.is_open())
		throw Exceptions("failed to open");
	std::getline(_cpuTemp, temp);
	std::getline(_cpuName, _name);
	_temp = atoi(temp.c_str()) / 1000;
	_cpuTemp.close();
	_cpuName.close();
}

std::vector<size_t> CPUModule::get_cpu_times()
{
	std::ifstream _procStat("/proc/stat", std::ios::in);
	_procStat.ignore(5, ' ');
	std::vector<size_t> times;

	if (!_procStat.is_open())
		throw Exceptions("/proc/stat failed to open");
	for (size_t stime; _procStat >> stime; times.push_back(stime));
	return times;
}

bool CPUModule::get_cpu_times(size_t &idle_time, size_t &total_time) {
	const std::vector<size_t> cpu_times = get_cpu_times();

	if (cpu_times.size() < 4)
		return false;
	idle_time = cpu_times[3];
	total_time = std::accumulate(cpu_times.begin(), cpu_times.end(), 0);
	return true;
}

std::string CPUModule::getCPUName()
{
	char CPUBrandString[0x40];
	unsigned int CPUInfo[4] = {0,0,0,0};
	__cpuid(0x80000000, CPUInfo[0], CPUInfo[1], CPUInfo[2], CPUInfo[3]);
	unsigned int nExIds = CPUInfo[0];

	memset(CPUBrandString, 0, sizeof(CPUBrandString));
	for (unsigned int i = 0x80000000; i <= nExIds; ++i) {
		__cpuid(i, CPUInfo[0], CPUInfo[1], CPUInfo[2], CPUInfo[3]);

		if (i == 0x80000002)
		memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
		else if (i == 0x80000003)
		memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
		else if (i == 0x80000004)
		memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
	}
	return (CPUBrandString);
}

void CPUModule::refreshData()
{
	float idle_time_delta;
	float total_time_delta;

	try {
		get_cpu_times(_idle_time, _total_time);
		idle_time_delta = _idle_time - _previous_idle_time;
		total_time_delta = _total_time - _previous_total_time;
		_utilization = 100.0 * (1.0 - idle_time_delta /
		total_time_delta);
		_previous_total_time = _total_time;
		_previous_idle_time = _idle_time;
		getTemps();
	}
	catch (Exceptions &e){
		throw e;
	}
}

void CPUModule::display() const
{
	BoxDisplay box(35, 10);

	box.drawBoarders(60, 8, _title);
	mvprintw(12, 62, "Global Usage: %.2f%\n", _utilization);
	mvprintw(14, 62, "CPU Name: %s\n", _name.c_str());
	mvprintw(16, 62, "CPU Temp: %.2f °C\n", _temp);
}
