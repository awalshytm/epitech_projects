/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** Kernel info
*/

#include "KernelInfoModule.hpp"

//Ctor Dtor
KernelInfoModule::KernelInfoModule(std::string const &name,
std::string const &title)
	:BaseModule(name, title), _kernel(), _os(), _arch()
{}

KernelInfoModule::~KernelInfoModule()
{}

//getters
std::string KernelInfoModule::getKernel() const { return _kernel; }

std::string KernelInfoModule::getOS() const { return _os; }

std::string KernelInfoModule::getArch() const { return _arch; }

//bast ft override
void KernelInfoModule::refreshData()
{
	struct utsname u;

	if (uname(&u) == -1)
		throw Exceptions("uname failed");
	_os.assign(u.version);
	_os.erase(_os.find(" "));
	_arch.assign(u.machine);
	_kernel.assign(u.sysname);
	_kernel += " ";
	_kernel.append(u.release);
}

void KernelInfoModule::display() const
{
	BoxDisplay box(35, 10);

	box.drawBoarders(10, 8, _title);
	mvprintw(12, 12, "OS: %s", _os.c_str());
	mvprintw(14, 12, "Kernel: %s", _kernel.c_str());
	mvprintw(16, 12, "Architecture: %s", _arch.c_str());
}
