/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "HostAndUserNameModule.hpp"

//Ctor Dtor
HostAndUserNameModule::HostAndUserNameModule(std::string const &name, std::string const &title)
	:BaseModule(name, title), _username(nullptr), _hostname(nullptr)
{
	refreshData();
}

HostAndUserNameModule::~HostAndUserNameModule()
{
	if (_username)
		delete _username;
	if (_hostname)
		delete _hostname;
}

//getters
std::string HostAndUserNameModule::getUserName() const { return *_username; }

std::string HostAndUserNameModule::getHostName() const { return *_hostname; }

//base ft override
void HostAndUserNameModule::refreshData()
{
	struct utsname u;

	if (uname(&u) == -1)
		throw Exceptions("uname failed");
	if (_hostname)
		delete _hostname;
	_hostname = new std::string(u.nodename);
	if (_username)
		delete _username;
	_username = new std::string(getlogin());
}

void HostAndUserNameModule::display() const
{
	if (!_username || !_hostname)
		throw Exceptions("username or hostname emtpy");
	mvprintw(2, 5, "%s@%s", _username->c_str(),
	_hostname->c_str());
}