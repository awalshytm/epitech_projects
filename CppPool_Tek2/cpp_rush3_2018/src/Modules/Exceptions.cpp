/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Exceptions.hpp"

Exceptions::Exceptions(std::string const &msg)
	:_msg(msg)
{}

const char *Exceptions::what() const noexcept { return _msg.c_str(); }