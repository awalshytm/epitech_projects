/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** datetime module
*/

#include "DateTimeModule.hpp"

DateTimeModule::DateTimeModule(std::string const &name,
std::string const &title)
	:BaseModule(name, title), _dateTime()
{}

// DateTimeModule::~DateTimeModule()
// {}

void DateTimeModule::display() const
{
	int x = getmaxx(stdscr);

	mvprintw(2, x - (5 + _dateTime.size()), "%s", _dateTime.c_str());
}

void DateTimeModule::refreshData()
{
    time_t t;

    time(&t);
    _dateTime = ctime(&t);
    _dateTime.pop_back();
}