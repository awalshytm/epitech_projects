/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** base module
*/

#include "BaseModule.hpp"

BaseModule::BaseModule(std::string const &name, std::string const &title)
	:_name(name), _title(title)
{}

std::string BaseModule::getName() const { return _name; }

std::string BaseModule::getTitle() const { return _title; }