/*
** EPITECH PROJECT, 2019
** cpp poll
** File description:
** ram module
*/

#include "RAMModule.hpp"

RAMModule::RAMModule(std::string const &name, std::string const &title)
	:BaseModule(name, title), _total_ram(0), _used(0)
{}

RAMModule::~RAMModule()
{}

//getters
int RAMModule::getTotalRam() const { return _total_ram; }

unsigned long RAMModule::getCachedMem()
{
	std::ifstream file("/proc/meminfo", std::ios::in);
	std::string line;
	std::string target = "";
	unsigned long cached;

	if (!file.is_open())
		throw Exceptions("file failed to open");
	for (int i = 0; i != 5; i++) {
		std::getline(file, line);
	}
	for (char c : line) {
		if (std::isdigit(c)) target += c;
	}
	cached = atoi(target.c_str());
	file.close();
	return (cached);
}

void RAMModule::refreshData()
{
	struct sysinfo si;

	if (sysinfo(&si) == -1)
		throw Exceptions("sysinfo failed");
	_total_ram = si.totalram;
	_used = si.totalram - (si.freeram + si.bufferram + getCachedMem());
	_total_ram /= 1048576;
	_used /= 1048576;
}

void RAMModule::display() const
{
	BoxDisplay box(35, 10);

	if (box.getSizeWX() > 145) {
		box.drawBoarders(110, 8, _title);
		mvprintw(13, 114, "RAM Usage: %lu/%lu Mb\n", _used, _total_ram);
	}
	else {
		box.drawBoarders(10, 20, _title);
		mvprintw(26, 14, "%lu/%lu Mb\n", _used, _total_ram);
	}
}
