/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "BoxDisplay.hpp"

BoxDisplay::BoxDisplay(int sizex, int sizey)
	:BaseDisplay(sizex, sizey)
{}

void BoxDisplay::display()
{}

void BoxDisplay::drawBoarders(int startx, int starty, std::string const &title,
 int sizex, int sizey)
{
	if (sizex == 0)
		sizex = _sizex;
	if (sizey == 0)
		sizey = _sizey;
	mvprintw(starty, startx, "+");
	mvprintw(starty + sizey, startx, "+");
	mvprintw(starty + 2, startx, "+");
	mvprintw(starty + 2, startx + sizex, "+");
	mvprintw(starty + 1, (startx + (sizex / 2)) - (title.size() / 2),
	title.c_str());
	mvprintw(starty, startx + sizex, "+");
	mvprintw(starty + sizey, startx + sizex, "+");
}