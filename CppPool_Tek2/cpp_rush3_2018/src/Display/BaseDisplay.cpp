/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "BaseDisplay.hpp"

BaseDisplay::BaseDisplay(int sizex, int sizey, int wx, int wy)
	:_sizex(sizex), _sizey(sizey), _wx(wx), _wy(wy)
{
	if (wx == 0)
		_wx = getmaxx(stdscr);
	if (wy == 0)
		_wy = getmaxy(stdscr);
}

BaseDisplay::~BaseDisplay()
{}

int BaseDisplay::getSizeX() const { return _sizex; }
int BaseDisplay::getSizeY() const { return _sizey; }
int BaseDisplay::getSizeWX() const { return _wx; }
int BaseDisplay::getSizeWY() const { return _wy; }