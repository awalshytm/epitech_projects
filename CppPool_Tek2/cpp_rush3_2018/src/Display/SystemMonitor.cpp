/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "SystemMonitor.hpp"

SystemMonitor::SystemMonitor()
	:_cpu(true), _ram(true), _kernel(true), _op(false)
{
	initscr();
	getmaxyx(stdscr, _sizey, _sizex);
	nodelay(stdscr, TRUE);
	noecho();
	curs_set(0);
}

SystemMonitor::~SystemMonitor()
{
	for (int i = 0; i < (int)_mods.size(); i++)
		delete _mods[i];
	curs_set(1);
	endwin();
}

void SystemMonitor::setOp(bool set)
{
	_op = set;
}

bool SystemMonitor::addModule(BaseModule *mod)
{
	_mods.push_back(mod);
	return (true);
}

void SystemMonitor::ModDeactivation(char c)
{
	std::string module;

	if (c == 'r')
		module.assign("RamModule");
	if (c == 'c')
		module.assign("CPU");
	if (c == 'k')
		module.assign("KernelInfo");
	for (int i = 0; i < (int)_mods.size(); i++)
		if (_mods[i]->getName() == module) {
			_mods.erase(_mods.begin() + i);
			setOp(true);
		}
}

void SystemMonitor::ModActivation(char c)
{
	if (_op == true)
		return;
	if (c == 'r')
		_mods.push_back(new RAMModule());
	if (c == 'c')
		_mods.push_back(new CPUModule());
	if (c == 'k')
		_mods.push_back(new KernelInfoModule());
}

void SystemMonitor::displayModules()
{
	try{
		for (int i = 0; i < (int)_mods.size(); i++) {
			_mods[i]->refreshData();
			_mods[i]->display();
		}
	}
	catch (Exceptions &e) {
		printw("%s\n", e.what());
		throw e;
	}
}

void SystemMonitor::drawBoarders(int i, int j, std::string const &title,
int a, int b)
{
	(void)title;
	(void)i;
	(void)j;
	(void)a;
	(void)b;
}

void SystemMonitor::display()
{
	std::string title("Rush3");

	clear();
	getmaxyx(stdscr, _sizey, _sizex);
	if (_sizex < 95 || _sizey < 20) {
		title.assign("Window too small...");
		mvprintw(4, (COLS / 2) - (title.size() / 2), "%s",
		title.c_str());
		return;
	}
	mvprintw(2, (COLS / 2) - (title.size() / 2), "%s",
	title.c_str());
	displayModules();
	mvprintw(_sizey - 2, 0, "c: CPU | r: RAM | k: KERNEL | q: QUIT");
	refresh();
}