/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** host user name test
*/

#include <criterion/criterion.h>
#include <sys/utsname.h>
#include "HostAndUserNameModule.hpp"

Test(HostUserName, hostname)
{
        HostAndUserNameModule uh;
        struct utsname u;

        uname(&u);
        cr_assert_str_eq(u.nodename, uh.getHostName().c_str(),
        "Hostname different");
}

Test(HostUserName, username)
{
        HostAndUserNameModule uh;
        char *username = NULL;

        username = getlogin();
        cr_assert_str_eq(username, uh.getUserName().c_str(),
        "Username different");
}

Test(HostUserName, moduleDetails)
{
        HostAndUserNameModule uh;

        cr_assert_eq(uh.getName(), "HostandUser",
        "Module Name differs");
        cr_assert_eq(uh.getTitle(), "Username and Hostname",
        "Module title differs");
}