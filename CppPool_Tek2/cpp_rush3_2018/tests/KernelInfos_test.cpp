/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <criterion/criterion.h>
#include "KernelInfoModule.hpp"

Test(KernelModule, arch)
{
        KernelInfoModule k;
        std::string version;
        struct utsname u;

        uname(&u);
        version.assign(version);
        version.erase(version.find(" "));
        cr_assert_str_eq(u.machine, k.getArch().c_str(), "Architecture");
}

Test(KernelModule, os)
{
        KernelInfoModule k;
        std::string version;
        struct utsname u;

        uname(&u);
        version.assign(version);
        version.erase(version.find(" "));
        cr_assert_eq(version, k.getOS(), "OS");
}