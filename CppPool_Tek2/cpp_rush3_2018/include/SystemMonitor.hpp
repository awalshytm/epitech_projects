/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef SYSTEMMONITOR_HPP_
	#define SYSTEMMONITOR_HPP_

#include <ncurses.h>
#include <vector>
#include "IMonitorDisplay.hpp"
#include "BaseModule.hpp"
#include "RAMModule.hpp"
#include "CPUModule.hpp"
#include "KernelInfoModule.hpp"

class SystemMonitor : public IMonitorDisplay
{
	private:
		int _sizex;
		int _sizey;
		std::vector<BaseModule *> _mods;
		bool _cpu;
		bool _ram;
		bool _kernel;
		bool _op;
	public:
		SystemMonitor();
		~SystemMonitor();
		void display() override;
		void drawBoarders(int startx, int starty,
		std::string const &title,
		int sizex = 0, int sizey = 0) override;
		void setOp(bool set);
		void displayModules();
		bool addModule(BaseModule *mod);
		void ModActivation(char c);
		void ModDeactivation(char c);
};

#endif /* !SYSTEMMONITOR_HPP_ */
