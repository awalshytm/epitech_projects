/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** cpu header
*/

#ifndef CPUMODULE_HPP_
	#define CPUMODULE_HPP_

#include <cpuid.h>
#include <cstring>
#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <fstream>
#include <iostream>
#include <numeric>
#include <unistd.h>
#include <vector>
#include "BaseModule.hpp"
#include "BoxDisplay.hpp"
#include "Exceptions.hpp"

class CPUModule : public BaseModule
{
	private:
		size_t _previous_idle_time;
		size_t _previous_total_time;
		size_t _total_time;
		size_t _idle_time;
		float _utilization;
		float _temp;
		std::string _name;
	public:
		CPUModule(std::string const &name = "CPU",
		std::string const &title = "CPU usage and temperature");
		~CPUModule();
		void display() const override;
		void refreshData() override;
		void getTemps();
		std::vector<size_t> get_cpu_times();
		bool get_cpu_times(size_t &idle_time, size_t &total_time);
		std::string getCPUName();
};

#endif /* !CPUMODULE_HPP_ */
