/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef BOXDISPLAY_HPP_
	#define BOXDISPLAY_HPP_

#include "BaseDisplay.hpp"

class BoxDisplay : public BaseDisplay
{
	public:
		BoxDisplay(int sizex, int sizey);
		void display() override;
		void drawBoarders(int startx, int starty,
		std::string const &title = "BOX",
		int sizex = 0, int sizey = 0) override;
};

#endif /* !BOXDISPLAY_HPP_ */
