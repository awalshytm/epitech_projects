/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** ram header
*/

#ifndef RAMMODULE_HPP_
	#define RAMMODULE_HPP_

#include <sys/sysinfo.h>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include "BaseModule.hpp"
#include "BoxDisplay.hpp"
#include "Exceptions.hpp"

class RAMModule : public BaseModule
{
	private:
		unsigned long _total_ram;
		unsigned long _used;
		std::string parse(std::string const &line);
	public:
		//Ctor Dtor
		RAMModule(std::string const &name = "RamModule",
		std::string const &title = "RAM Amount");
		~RAMModule();
		//getters
		int getTotalRam() const;
		int getPercentage() const;
		//base ft override
		void refreshData() override;
		void display() const override;
		unsigned long getCachedMem();
};

#endif /* !RAMMODULE_HPP_ */
