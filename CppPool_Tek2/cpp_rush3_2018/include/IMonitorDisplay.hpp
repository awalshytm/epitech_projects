/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef IMONITORDISPLAY_HPP_
	#define IMONITORDISPLAY_HPP_

#include "IMonitorModule.hpp"

class IMonitorDisplay {
	public:
		virtual ~IMonitorDisplay() {}
		virtual void display() = 0;
		virtual void drawBoarders(int startx, int starty,
		std::string const &title, int sizex, int sizey) = 0;
};

#endif /* !IMONITORDISPLAY_HPP_ */