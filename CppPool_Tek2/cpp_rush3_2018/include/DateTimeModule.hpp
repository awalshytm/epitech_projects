/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef DATETIMEMODULE_HPP_
	#define DATETIMEMODULE_HPP_

#include "BaseModule.hpp"
#include <time.h>

class DateTimeModule : public BaseModule
{
	private:
		std::string _dateTime;
	public:
		DateTimeModule(std::string const &name = "DateTime",
		std::string const &title = "Date and Time");
		void display() const override;
		void refreshData();
};

#endif /* !DATETIMEMODULE_HPP_ */