/*
** EPITECH PROJECT, 2018
** NetworkModule.hpp
** File description:
** NetworkModule.hpp
*/

#ifndef NETWORKMODULE_HPP_
	#define NETWORKMODULE_HPP_

#include <stdio.h>
#include <fstream>
#include "BaseModule.hpp"
#include "BoxDisplay.hpp"

class NetworkModule : public BaseModule
{
	private:
		unsigned long int _r_bytes;
		unsigned long int _t_bytes;
		unsigned long int _r_packets;
		unsigned long int _t_packets;
	public:
                //Ctor Dtor
		NetworkModule(std::string const &name = "Network",
                std::string const &title = "Network");
		~NetworkModule();
		void display() const;
		void refreshData();
};

#endif /* !NETWORKMODULE_HPP_ */
