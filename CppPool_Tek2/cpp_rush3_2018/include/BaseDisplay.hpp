/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef BASEDISPLAY_HPP_
	#define BASEDISPLAY_HPP_

#include <ncurses.h>
#include "IMonitorDisplay.hpp"

class BaseDisplay : public IMonitorDisplay
{
	protected:
		int _sizex;
		int _sizey;
		int _wx;
		int _wy;
	public:
		BaseDisplay(int sizez, int sizey, int wx = 0, int wy = 0);
		~BaseDisplay();
		int getSizeX() const;
		int getSizeY() const;
		int getSizeWX() const;
		int getSizeWY() const;
		virtual void display() = 0;
		virtual void drawBoarders(int startx, int starty,
		std::string const &title, int sizex, int sizey) = 0;
};

#endif /* !BASEDISPLAY_HPP_ */
