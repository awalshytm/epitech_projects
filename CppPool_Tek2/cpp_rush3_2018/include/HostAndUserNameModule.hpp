/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef HOSTANDUSERNAMEMODULE_HPP_
	#define HOSTANDUSERNAMEMODULE_HPP_

#include <algorithm>
#include <unistd.h>
#include <fstream>
#include <sys/utsname.h>
#include "BaseModule.hpp"
#include "Exceptions.hpp"

class HostAndUserNameModule : public BaseModule
{
        private:
                std::string *_username;
                std::string *_hostname;
	public:
                //Ctor Dtor
		HostAndUserNameModule(std::string const &name = "HostandUser",
                std::string const &title = "Username and Hostname");
                ~HostAndUserNameModule();
                //getters
                std::string getUserName() const;
                std::string getHostName() const;
                //base ft override
                void refreshData() override;
                void display() const override;

};

#endif /* !HOSTANDUSERNAMEMODULE_HPP_ */
