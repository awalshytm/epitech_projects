/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef EXCEPTIONS_HPP_
	#define EXCEPTIONS_HPP_

#include <exception>
#include <string>

class Exceptions : public std::exception
{
	private:
		std::string _msg;
	public:
		Exceptions(std::string const &msg);
		const char *what() const noexcept override;
};

#endif /* !EXCEPTIONS_HPP_ */
