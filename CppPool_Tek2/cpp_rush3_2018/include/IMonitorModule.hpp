/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef IMONITORMODULE_HPP_
	#define IMONITORMODULE_HPP_

#include <string>
#include <iostream>

class IMonitorModule {
	public:
		virtual ~IMonitorModule() {};
		virtual void display() const = 0;
		virtual void refreshData() = 0;
		virtual std::string getName() const = 0;
		virtual std::string getTitle() const = 0;
};

#endif /* !IMONITORMODULE_HPP_ */
