/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** base module header
*/

#ifndef BASEMODULE_HPP_
	#define BASEMODULE_HPP_

#include <ncurses.h>
#include "IMonitorModule.hpp"

class BaseModule : public IMonitorModule
{
	protected:
		std::string _name;
		std::string _title;
	public:
		BaseModule(std::string const &name, std::string const &title);
		virtual ~BaseModule() {};
		std::string getName() const final;
		std::string getTitle() const final;
		virtual void display() const = 0;
		virtual void refreshData() = 0;
};

#endif /* !BASEMODULE_HPP_ */