/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** kernel info header
*/

#ifndef KERNELINFOMODULE_HPP_
	#define KERNELINFOMODULE_HPP_

#include <sys/utsname.h>
#include <string>
#include <exception>
#include <sstream>
#include "BaseModule.hpp"
#include "BoxDisplay.hpp"
#include "Exceptions.hpp"

class KernelInfoModule : public BaseModule
{
	private:
		std::string _kernel;
		std::string _os;
		std::string _arch;
	public:
		//Ctor Dtor
		KernelInfoModule(std::string const &name = "KernelInfo",
		std::string const &title = "Kernel Informations");
		~KernelInfoModule();
		//getters
		std::string getOS() const;
		std::string getKernel() const;
		std::string getArch() const;
		//ft base overrride
		void display() const override;
		void refreshData() override;
};

#endif /* !KERNELINFOMODULE_HPP_ */