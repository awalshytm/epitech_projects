/*
** EPITECH PROJECT, 2018
** cpp_rush1
** File description:
** Exercice 02
*/

#include <stdio.h>
#include <string.h>
#include "new.h"
#include "point.h"

typedef struct
{
    Class   base;
    int     x, y;
}   PointClass;

static const char *Point_str(PointClass *this)
{
    char str[200];

    sprintf(str, "<Point (%d, %d)>", this->x, this->y);
    return (strdup(str));
}

static Object *Point_addition(const PointClass *a, const PointClass *b)
{
    int x;
    int y;

    if (!a || !b)
        return (NULL);
    x = a->x + b->x;
    y = a->y + b->y;
    return (new(Point, x, y));
}

static Object *Point_subtraction(const PointClass *a, const PointClass *b)
{
    int x;
    int y;

    if (!a || !b)
        return (NULL);
    x = a->x - b->x;
    y = a->y - b->y;
    return (new(Point, x, y));
}

static void Point_ctor(PointClass *this, va_list *args)
{
    Class *tmp = (Class *)this;

    if (args) {
        this->x = va_arg(*args, int);
        this->y = va_arg(*args, int);
    }
    tmp->__str__ = (to_string_t)&Point_str;
    tmp->__add__ = (binary_operator_t)&Point_addition;
    tmp->__sub__ = (binary_operator_t)&Point_subtraction;
}

static void Point_dtor(PointClass *this)
{
    if (!this)
        return;
}

static PointClass _description = {
    {   /* Class struct */
        .__size__ = sizeof(PointClass),
        .__name__ = "Point",
        .__ctor__ = (ctor_t)&Point_ctor,
        .__dtor__ = (dtor_t)&Point_dtor,
        .__str__ = (to_string_t)&Point_str,
        .__add__ = (binary_operator_t)&Point_addition,
        .__sub__ = (binary_operator_t)&Point_subtraction,
        .__mul__ = NULL,
        .__div__ = NULL,
        .__eq__ = NULL,
        .__gt__ = NULL,
        .__lt__ = NULL
    },
    .x = 0,
    .y = 0
};

Class   *Point = (Class *)&_description;
