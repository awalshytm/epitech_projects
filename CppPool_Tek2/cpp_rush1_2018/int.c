/*
** EPITECH PROJECT, 2018
** cpp_rush1
** File description:
** Exercice 04
*/

#include <stdio.h>
#include <string.h>
#include "new.h"
#include "int.h"

typedef struct
{
    Class   base;
    int     i;
}   IntClass;

static const char *Int_str(IntClass *this)
{
    char str[200];

    sprintf(str, "%d", this->i);
    return (strdup(str));
}

static Object *Int_addition(const IntClass *a, const IntClass *b)
{
    int res = 0;

    if (!a || !b)
        return (NULL);
    res = a->i + b->i;
    return (new(Int, res));
}

static Object *Int_subtraction(const IntClass *a, const IntClass *b)
{
    int res;

    if (!a || !b)
        return (NULL);
    res = a->i - b->i;
    return (new(Int, res));
}

static Object *Int_division(const IntClass *a, const IntClass *b)
{
    int res;

    if (!a || !b)
        return (NULL);
    res = a->i / b->i;
    return (new(Int, res));
}

static Object *Int_multiplication(const IntClass *a, const IntClass *b)
{
    int res;

    if (!a || !b)
        return (NULL);
    res = a->i * b->i;
    return (new(Int, res));
}

static bool Int_eq(const IntClass *a, const IntClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i == b->i)
        return (true);
    return (false);
}

static bool Int_lt(const IntClass *a, const IntClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i < b->i)
        return (true);
    return (false);
}

static bool Int_gt(const IntClass *a, const IntClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i > b->i)
        return (true);
    return (false);
}

static void Int_ctor(IntClass *this, va_list *args)
{
    Class *tmp = (Class *)this;

    if (args) {
        this->i = va_arg(*args, int);
    }
    tmp->__str__ = (to_string_t)&Int_str;
    tmp->__add__ = (binary_operator_t)&Int_addition;
    tmp->__sub__ = (binary_operator_t)&Int_subtraction;
}

static void Int_dtor(IntClass *this)
{
    if (!this)
        return;
}

static IntClass _description = {
    {   /* Class struct */
        .__size__ = sizeof(IntClass),
        .__name__ = "Int",
        .__ctor__ = (ctor_t)&Int_ctor,
        .__dtor__ = (dtor_t)&Int_dtor,
        .__str__ = (to_string_t)&Int_str,
        .__add__ = (binary_operator_t)&Int_addition,
        .__sub__ = (binary_operator_t)&Int_subtraction,
        .__mul__ = (binary_operator_t)&Int_multiplication,
        .__div__ = (binary_operator_t)&Int_division,
        .__eq__ = (binary_comparator_t)&Int_eq,
        .__gt__ = (binary_comparator_t)&Int_gt,
        .__lt__ = (binary_comparator_t)&Int_lt
    },
    .i = 0,
};

Class   *Int = (Class *)&_description;