/*
** EPITECH PROJECT, 2018
** cpp_rush1
** File description:
** Exercice 04
*/

#include <stdio.h>
#include <string.h>
#include "new.h"
#include "float.h"

typedef struct
{
    Class   base;
    float     i;
}   FloatClass;

static const char *Float_str(FloatClass *this)
{
    char str[200];

    sprintf(str, "%d", this->i);
    return (strdup(str));
}

static Object *Float_addition(const FloatClass *a, const FloatClass *b)
{
    float res = 0;

    if (!a || !b)
        return (NULL);
    res = a->i + b->i;
    return (new(Float, res));
}

static Object *Float_subtraction(const FloatClass *a, const FloatClass *b)
{
    float res;

    if (!a || !b)
        return (NULL);
    res = a->i - b->i;
    return (new(Float, res));
}

static Object *Float_division(const FloatClass *a, const FloatClass *b)
{
    float res;

    if (!a || !b)
        return (NULL);
    res = a->i / b->i;
    return (new(Float, res));
}

static Object *Float_multiplication(const FloatClass *a, const FloatClass *b)
{
    float res;

    if (!a || !b)
        return (NULL);
    res = a->i * b->i;
    return (new(Float, res));
}

static bool Float_eq(const FloatClass *a, const FloatClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i == b->i)
        return (true);
    return (false);
}

static bool Float_lt(const FloatClass *a, const FloatClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i < b->i)
        return (true);
    return (false);
}

static bool Float_gt(const FloatClass *a, const FloatClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i > b->i)
        return (true);
    return (false);
}

static void Float_ctor(FloatClass *this, va_list *args)
{
    Class *tmp = (Class *)this;

    if (args) {
        this->i = va_arg(*args, float);
    }
    tmp->__str__ = (to_string_t)&Float_str;
    tmp->__add__ = (binary_operator_t)&Float_addition;
    tmp->__sub__ = (binary_operator_t)&Float_subtraction;
}

static void Float_dtor(FloatClass *this)
{
    if (!this)
        return;
}

static FloatClass _description = {
    {   /* Class struct */
        .__size__ = sizeof(FloatClass),
        .__name__ = "Float",
        .__ctor__ = (ctor_t)&Float_ctor,
        .__dtor__ = (dtor_t)&Float_dtor,
        .__str__ = (to_string_t)&Float_str,
        .__add__ = (binary_operator_t)&Float_addition,
        .__sub__ = (binary_operator_t)&Float_subtraction,
        .__mul__ = (binary_operator_t)&Float_multiplication,
        .__div__ = (binary_operator_t)&Float_division,
        .__eq__ = (binary_comparator_t)&Float_eq,
        .__gt__ = (binary_comparator_t)&Float_gt,
        .__lt__ = (binary_comparator_t)&Float_lt
    },
    .i = 0,
};

Class   *Float = (Class *)&_description;