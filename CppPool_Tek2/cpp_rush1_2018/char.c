/*
** EPITECH PROJECT, 2018
** cpp_rush1
** File description:
** Exercice 04
*/

#include <stdio.h>
#include <string.h>
#include "new.h"
#include "char.h"

typedef struct
{
    Class   base;
    char     i;
}   CharClass;

static const char *Char_str(CharClass *this)
{
    char str[200];

    sprintf(str, "%d", this->i);
    return (strdup(str));
}

static Object *Char_addition(const CharClass *a, const CharClass *b)
{
    char res = 0;

    if (!a || !b)
        return (NULL);
    res = a->i + b->i;
    return (new(Char, res));
}

static Object *Char_subtraction(const CharClass *a, const CharClass *b)
{
    char res;

    if (!a || !b)
        return (NULL);
    res = a->i - b->i;
    return (new(Char, res));
}

static Object *Char_division(const CharClass *a, const CharClass *b)
{
    char res;

    if (!a || !b)
        return (NULL);
    res = a->i / b->i;
    return (new(Char, res));
}

static Object *Char_multiplication(const CharClass *a, const CharClass *b)
{
    char res;

    if (!a || !b)
        return (NULL);
    res = a->i * b->i;
    return (new(Char, res));
}

static bool Char_eq(const CharClass *a, const CharClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i == b->i)
        return (true);
    return (false);
}

static bool Char_lt(const CharClass *a, const CharClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i < b->i)
        return (true);
    return (false);
}

static bool Char_gt(const CharClass *a, const CharClass *b)
{
    if (!a || !b)
        return (NULL);
    if (a->i > b->i)
        return (true);
    return (false);
}

static void Char_ctor(CharClass *this, va_list *args)
{
    Class *tmp = (Class *)this;

    if (args) {
        this->i = va_arg(*args,  char);
    }
    tmp->__str__ = (to_string_t)&Char_str;
    tmp->__add__ = (binary_operator_t)&Char_addition;
    tmp->__sub__ = (binary_operator_t)&Char_subtraction;
}

static void Char_dtor(CharClass *this)
{
    if (!this)
        return;
}

static CharClass _description = {
    {   /* Class struct */
        .__size__ = sizeof(CharClass),
        .__name__ = "Char",
        .__ctor__ = (ctor_t)&Char_ctor,
        .__dtor__ = (dtor_t)&Char_dtor,
        .__str__ = (to_string_t)&Char_str,
        .__add__ = (binary_operator_t)&Char_addition,
        .__sub__ = (binary_operator_t)&Char_subtraction,
        .__mul__ = (binary_operator_t)&Char_multiplication,
        .__div__ = (binary_operator_t)&Char_division,
        .__eq__ = (binary_comparator_t)&Char_eq,
        .__gt__ = (binary_comparator_t)&Char_gt,
        .__lt__ = (binary_comparator_t)&Char_lt
    },
    .i = 0,
};

Class   *Char = (Class *)&_description;