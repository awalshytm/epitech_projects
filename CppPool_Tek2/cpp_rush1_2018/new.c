/*
** EPITECH PROJECT, 2019
** rush1
** File description:
** new
*/

#include "new.h"
#include <string.h>

Object *new(Class *class, ...)
{
    va_list arg;
    Object *object = NULL;

    if (!class)
        return (NULL);
    if (!(object = malloc(class->__size__)))
        raise("Out of memory");
    memcpy(object, class, class->__size__);
    va_start(arg, class);
    if (class->__ctor__)
        class->__ctor__(object, &arg);
    ((Class *)object)->__dtor__ = class->__dtor__;
    va_end(arg);
    return (object);
}

void delete(Object *ptr)
{
    if (ptr && ((Class *)ptr)->__dtor__)
        (*(Class *)ptr).__dtor__(ptr);
    free(ptr);
}

Object *va_new(Class *class, va_list *ap)
{
    Object *object = NULL;

    if (!class)
        return (NULL);
    if (!(object = malloc(class->__size__)))
        raise("Out of memory");
    memcpy(object, class, class->__size__);
    if (class->__ctor__)
        class->__ctor__(object, ap);
    ((Class *)object)->__dtor__ = class->__dtor__;    
    return (object);
}