
#include "new.h"
#include "player.h"
#include "vertex.h"
#include "point.h"

int         main(void)
{
    Object  *player = new(Player);
    Object  *point = new(Point, 42, -42);
    Object  *vertex = new(Vertex, 0, 1, 2);
    Object  *p1 = new(Point, 12, 13);
    Object  *p2 = new(Point, 2, 2);
    Object  *v1 = new(Vertex, 1, 2, 3);
    Object  *v2 = new(Vertex, 4, 5, 6);
//    Object  *i = new(Int, 3);
//    Object  *f = new(Float, 4.5);
//    Object  *c = new(Char, 'a');

    printf("point = %s\n", str(point));
    printf("vertex = %s\n", str(vertex));
    printf("%s + %s = %s\n", str(p1), str(p2), str(addition(p1, p2)));
    printf("%s - %s = %s\n", str(p1), str(p2), str(subtraction(p1, p2)));
    printf("%s + %s = %s\n", str(v1), str(v2), str(addition(v1, v2)));
    printf("%s - %s = %s\n", str(v1), str(v2), str(subtraction(v1, v2)));

    delete(player);
    delete(point);
    delete(vertex);
//    delete(i);
//    delete(f);
//    delete(c);
    return (0);
}
