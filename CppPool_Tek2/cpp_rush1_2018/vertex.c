/*
** EPITECH PROJECT, 2019
** rush
** File description:
** vertex
*/

#include "new.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "vertex.h"
#include "object.h"

typedef struct
{
    Class base;
    int x;
    int y;
    int z;
} VertexClass;

static const char *Vertex_str(VertexClass *this)
{
    char str[200];

    sprintf(str, "<Vertex (%d, %d, %d)>", this->x, this->y, this->z);
    return (strdup(str));
}

static Object *Vertex_addition(const VertexClass *a, const VertexClass *b)
{
    int x;
    int y;
    int z;

    if (!a || !b)
        return (NULL);
    x = a->x + b->x;
    y = a->y + b->y;
    z = a->z + b->z;
    return (new(Vertex, x, y, z));
}

static Object *Vertex_subtraction(const VertexClass *a, const VertexClass *b)
{
    int x;
    int y;
    int z;

    if (!a || !b)
        return (NULL);
    x = a->x - b->x;
    y = a->y - b->y;
    z = a->z - b->z;
    return (new(Vertex, x, y, z));
}

static void Vertex_ctor(VertexClass *this, va_list *args)
{
    Class *tmp = (Class *)this;

    if (args)
    {
        this->x = va_arg(*args, int);
        this->y = va_arg(*args, int);
        this->z = va_arg(*args, int);
    }
    tmp->__str__ = (to_string_t) &Vertex_str;
    tmp->__add__ = (binary_operator_t)&Vertex_addition;
    tmp->__sub__ = (binary_operator_t)&Vertex_subtraction;
}

static void Vertex_dtor(VertexClass *this)
{
    if (!this)
        return;
}

static VertexClass _description = {
    {
        .__size__ = sizeof(VertexClass),
        .__name__ = "Vertex",
        .__ctor__ = (ctor_t)&Vertex_ctor,
        .__dtor__ = (dtor_t)&Vertex_dtor,
        .__str__ = (to_string_t)&Vertex_str,
        .__add__ = (binary_operator_t)&Vertex_addition,
        .__sub__ = (binary_operator_t)&Vertex_subtraction,
        .__mul__ = NULL,
        .__div__ = NULL,
        .__eq__ = NULL,
        .__gt__ = NULL,
        .__lt__ = NULL
    },
    .x = 0,
    .y = 0,
    .z = 0
};

Class *Vertex = (Class *)&_description;
