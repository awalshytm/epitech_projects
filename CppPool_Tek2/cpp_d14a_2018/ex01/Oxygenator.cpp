
#include <iostream>
#include "Errors.hpp"
#include "Oxygenator.hpp"

Oxygenator::Oxygenator()
    : _quantity(0)
{}

void
Oxygenator::generateOxygen()
{
    std::cout << "\tgenerate() -> " << _quantity + 10 << std::endl;
    _quantity += 10;
}

void
Oxygenator::useOxygen(int quantity)
{
    std::cout << "\tuse(" << quantity << ") -> left " << _quantity - quantity << std::endl;
    // if (_quantity - quantity <= 5)
    //     throw MissionCriticalError("Not enough oxygen to continue the mission.",
    //     "Oxygenator");
    _quantity -= quantity;
    if (_quantity <= 5)
        throw MissionCriticalError("Not enough oxygen to live.", "Oxygenator");
}
