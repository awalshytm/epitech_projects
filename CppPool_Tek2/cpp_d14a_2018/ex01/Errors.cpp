/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "BaseComponent.hpp"
#include "Errors.hpp"

BaseComponent::~BaseComponent() {}

//Nasa Error
NasaError::NasaError(std::string const &message, std::string const &component)
        :_message(message), _component(component)
{}

std::string const &NasaError::getComponent() const { return _component; }

const char *NasaError::what() const noexcept { return _message.c_str(); }

MissionCriticalError::MissionCriticalError(std::string const &message,
std::string const &component)
        :NasaError(message, component)
{}

LifeCriticalError::LifeCriticalError(std::string const &message, 
std::string const &component)
        :NasaError(message, component)
{}

UserError::UserError(std::string const &message, std::string const &component)
        :NasaError(message, component)
{}

CommunicationError::CommunicationError(std::string const &message)
        :NasaError(message, "CommunicationDevice")
{}