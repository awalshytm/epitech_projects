/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Errors.hpp"

//Nasa Error
NasaError::NasaError(std::string const &message, std::string const &component)
noexcept :_message(message), _component(component)
{}

std::string const &NasaError::getComponent() const { return _component; }

const char *NasaError::what() const throw() { return _message.c_str(); }

MissionCriticalError::MissionCriticalError(std::string const &message,
std::string const &component)
	:NasaError(message, component)
{}

LifeCriticalError::LifeCriticalError(std::string const &message, 
std::string const &component)
	:NasaError(message, component)
{}

UserError::UserError(std::string const &message, std::string const &component)
	:NasaError(message, component)
{}

CommunicationError::CommunicationError(std::string const &message)
	:NasaError(message, "CommunicationDevice")
{}