/*
** EPITECH PROJECT, 2018
** cpp pool
** File description:
** menger
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "menger.h"
#include "drawing.h"

static uint32_t get_color(int const level)
{
    int div = level;
    int ret = 0x00;
    int tmp = 0;

    
    for (int i = 0; i < 3; i++) {
        tmp = (0x00FF / div);
        ret <<= 8;
        ret |= tmp;
    }
    printf("depth: %d => (255 / %d) => 0x%.08X\n", level, div, ret);
    return (ret);
}

static sponge_t *create_sponge(int size, int level, int x, int y)
{
    sponge_t *sponge = malloc(sizeof(sponge));

    sponge->size = size;
    sponge->level = level;
    sponge->x = x;
    sponge->y = y;
    return (sponge);
}

static void call_recursive_bis(sponge_t *sponge, int i, unsigned int **img)
{
    for (int j = 0; j < 3; j++) {
        if (i != 1 || j != 1) {
            menger(sponge->size / 3, sponge->level - 1,
            i * (sponge->size / 3) + sponge->x,
            j * (sponge->size / 3) + sponge->y,
            img);
        }
    }
}

static void call_recursive(int size, int level, int x, int y, unsigned int **img)
{
    sponge_t *sponge = create_sponge(size, level, x, y);

    for (int i = 0; i < 3; i++) {
        call_recursive_bis(sponge, i, img);
    }
    free(sponge);
}

void menger(int size, int level, int x, int y, unsigned int **img)
{
    point_t p;

    if (level > 0) {
        p.x = (size / 3 + x) * 10;
        p.y = (size / 3 + y) * 10;
        draw_square(img, &p, (size / 3) * 10, get_color(level));
    }
    if (level > 1)
        call_recursive(size, level, x, y, img);
}
