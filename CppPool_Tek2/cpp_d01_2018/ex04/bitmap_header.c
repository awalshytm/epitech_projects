/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** bitmap header
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include "bitmap.h"
#include "drawing.h"

void make_bmp_header(bmp_header_t *header, size_t size)
{
    unsigned int i = 1;
    char *c = (char *)&i;

    header->magic = (*c) ? 0x4d42 : 0x424d;
    header->size = ((size * size) * sizeof(uint32_t)) +
    sizeof(bmp_header_t) + sizeof(bmp_info_header_t);
    header->_app1 = 0;
    header->_app2 = 0;
    header->offset = 54;
}

void make_bmp_info_header(bmp_info_header_t *header, size_t size)
{
    header->size = 40;
    header->width = size;
    header->height = size;
    header->planes = 1;
    header->bpp = 32;
    header->compression = 0;
    header->raw_data_size = (size * size);
    header->h_resolution = 0;
    header->v_resolution = 0;
    header->palette_size = 0;
    header->important_colors = 0;
}

void initialize_image(size_t size, unsigned int *buffer,
unsigned int **img)
{
    memset (buffer, 0, size * size * sizeof (*buffer));
    for (size_t i = 0; i < size ; ++i)
        img [i] = buffer + i * size;
}

void create_bitmap_from_buffer(size_t size, unsigned int *buffer,
char const *path)
{
    int fd = open (path, O_CREAT | O_TRUNC | O_WRONLY , 0644);

    write_bmp_headers(fd, size);
    write (fd, buffer, size * size * sizeof (*buffer));
    close (fd);
}
