/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** menger
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include "bitmap.h"
#include "drawing.h"

void draw_square(uint32_t **img, point_t *origin, size_t size, uint32_t color)
{
    for (uint32_t i = origin->y; i < origin->y + size; i++) {
        for (uint32_t j = origin->x; j < origin->x + size; j++) {
            img[i][j] = color;
        }
    }
}

void write_bmp_headers(int fd, size_t size)
{
    bmp_header_t header;
    bmp_info_header_t info;

    make_bmp_header(&header, size);
    write(fd, &header, sizeof(header));
    make_bmp_info_header(&info, size);
    write(fd, &info, sizeof(info));
}
