/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** drawing header
*/

#ifndef _DRAWING_H_
#define _DRAWING_H_

typedef struct point {
    uint32_t x;
    uint32_t y;
} point_t;

void draw_square(uint32_t **img, point_t *origin, size_t size, uint32_t color);
void write_bmp_headers(int fd, size_t size);

#endif
