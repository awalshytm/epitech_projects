/*
** EPITECH PROJECT, 2018
** cpp pool
** File description:
** menger
*/

#ifndef _MENGER_H_
#define _MENGER_H_

typedef struct {
    int size;
    int level;
    int x;
    int y;
} sponge_t;

void menger(int size, int level, int x, int y, unsigned int **img);

#endif
