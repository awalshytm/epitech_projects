/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** pyramid
*/

int pyramid_path(int size, const int **map)
{
    int sum = 0;
    int x = 0;

    for (int i = 0; i < size; i++) {
        sum += map[i][x];
        if (i + 1 < size && (map[i + 1][x + 1] < map[i + 1][x]))
        {
            x++;
        }
    }
    return (sum);
}
