/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** log
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include "log.h"

int set_to_stderr(void)
{
    stream = stderr;
    return (EXIT_SUCCESS);
}

int set_to_stdout(void)
{
    stream = stderr;
    return (EXIT_SUCCESS);
}

static char *get_log_level_string(enum log_level log)
{
    switch (log) {
    case WARNING:
        return "WARNING";
    case ERROR:
        return "ERROR";
    case NOTICE:
        return "NOTICE";
    case INFO:
        return "INFO";
    case DEBUG:
        return "DEBUG";
    default:
        return NULL;
    }
}

void log_msg(enum log_level loglevel, const char *fmt, ...)
{
    va_list args;
    time_t now = time(NULL);
    char *c = NULL;
    
    va_start(args, fmt);
    c = get_log_level_string(loglevel);
    if (c != NULL && loglevel <= level)
        fprintf(stream, "%.24s [%s]: %s", ctime(&now), c, fmt);
    va_end(args);
    return;
}
