/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** log
*/

#ifndef _LOG_H_
#define _LOG_H_

enum log_level {
    ERROR,
    WARNING,
    NOTICE,
    INFO,
    DEBUG
};

extern enum log_level level;
extern FILE *stream;

enum log_level get_log_level(void);
enum log_level set_log_level(enum log_level);
int set_log_file(const char *);
int close_log_file(void);

int log_to_stdout(void);
int log_to_stderr(void);
void log_msg(enum log_level, const char *fmt, ...);

#endif
