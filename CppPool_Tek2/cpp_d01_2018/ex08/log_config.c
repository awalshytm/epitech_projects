/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** log
*/

#include <stdio.h>
#include <stdlib.h>
#include "log.h"

FILE *stream = NULL;
enum log_level level = 0;

enum log_level get_log_level(void)
{
    return level;
}

enum log_level set_log_level(enum log_level new)
{
    level = new;
    return level;
}

int set_log_file(const char *name)
{
    FILE *tmp = fopen(name, "a");

    if (tmp == NULL)
        return (EXIT_FAILURE);
    stream = tmp;
    return (EXIT_SUCCESS);
}

int close_log_file(void)
{
    int res = fclose(stream);

    if (res != 0)
        return (EXIT_FAILURE);
    return (EXIT_SUCCESS);
}
