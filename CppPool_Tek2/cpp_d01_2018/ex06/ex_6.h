/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header 
*/

#ifndef _EX_6_H_
#define _EX_6_H_

union bar_u {
    int foo;
    int bar;
};

struct foo_s {
	int foo;
	union bar_u bar;
};

typedef union foo_u {
    long bar;
    struct foo_s foo;
} foo_t;

#endif
