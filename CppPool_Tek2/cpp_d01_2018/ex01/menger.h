/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** ex01
*/

#ifndef _HEADER_H_

#define _HEADER_H_

typedef struct {
    int size;
    int level;
    int x;
    int y;
} sponge_t;

void menger(int size, int level, int x, int y);

#endif
