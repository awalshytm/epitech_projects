/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** menger.c
*/

#include <stdlib.h>
#include <stdio.h>
#include "menger.h"

sponge_t *create_sponge(int size, int level, int x, int y)
{
    sponge_t *sponge = malloc(sizeof(sponge));

    sponge->size = size;
    sponge->level = level;
    sponge->x = x;
    sponge->y = y;
    return (sponge);
}

void call_recursive_bis(sponge_t *sponge, int i)
{
    for (int j = 0; j < 3; j++) {
        if (i != 1 || j != 1) {
            menger(sponge->size / 3, sponge->level - 1,
            i * (sponge->size / 3) + sponge->x,
            j * (sponge->size / 3) + sponge->y);
        }
    }
}

void call_recursive(int size, int level, int x, int y)
{
    sponge_t *sponge = create_sponge(size, level, x, y);

    for (int i = 0; i < 3; i++) {
        call_recursive_bis(sponge, i);
    }
    free(sponge);
}

void menger(int size, int level, int x, int y)
{
    if (level > 0)
        printf("%.3d %.3d %.3d\n", size / 3, (size / 3) + x, (size / 3) + y);
    if (level > 1)
        call_recursive(size, level, x, y);
}
