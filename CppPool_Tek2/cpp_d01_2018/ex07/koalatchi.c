/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** koalatchi
*/

#include <stdint.h>
#include <stdio.h>

void display_message(const char *type, const char *domain, const char *message,
const char *content)
{
    if (content != NULL)
        printf("%s %s %s %s\n", type, domain, message, content);
    else
        printf("%s %s %s\n", type, domain, message);
}

int check_education(char type, char domain, char message, char const *content)
{
    if (domain != 4)
        return (0);
    else if (type == 1 && message == 1) {
        display_message("Notification", "Education", "TeachCoding", content);
        return (1);
    } else if (type == 1 && message == 2) {
        display_message("Notification", "Education", "BeAwesome", content);
        return (1);
    } else if (type == 2 && message == 1) {
        display_message("Request", "Education", "FeelStupid", content);
        return (1);
    } else if (type == 4 && message == 1) {
        display_message("Error", "Education", "BrainDamage", content);
        return (1);
    }
    return (0);
}

int check_nutrition(char type, char domain, char message,
char const *content)
{
    if (domain != 1)
        return (0);
    else if (type == 1 && message == 1) {
        display_message("Notification", "Nutrition", "Eat", content);
        return (1);
    } else if (type == 1 && message == 2) {
        display_message("Notification", "Nutrition", "Defecate", content);
        return (1);
    } else if (type == 2 && message == 1) {
        display_message("Request", "Nutrition", "Hungry", content);
        return (1);
    } else if (type == 2 && message == 2) {
        display_message("Request", "Nutrition", "Thirsty", content);
        return (1);
    } else if (type == 4 && message == 1) {
        display_message("Error", "Nutrition", "Indigestion", content);
        return (1);
    } else if (type == 4 && message == 2) {
        display_message("Error", "Nutrition", "Starving", content);
        return (1);
    }
    return (0);
}

int check_entertainment(char type, char domain, char message,
char const *content)
{
    if (domain != 2)
        return (0);
    else if (type == 1 && message == 1) {
        display_message("Notification", "Entertainment", "Ball", content);
        return (1);
    } else if (type == 1 && message == 2) {
        display_message("Notification", "Entertainment", "Bite", content);
        return (1);
    } else if (type == 2 && message == 1) {
        display_message("Request", "Entertainment", "NeedAffection", content);
        return (1);
    } else if (type == 2 && message == 2) {
        display_message("Request", "Entertainment", "WannaPlay", content);
        return (1);
    } else if (type == 2 && message == 3) {
        display_message("Request", "Entertainment", "Hug", content);
        return (1);
    } else if (type == 4 && message == 1) {
        display_message("Error", "Entertainment", "Bored", content);
        return (1);
    }
    return (0);
}

int prettyprint_message (uint32_t header, const char *content)
{
    char type = header >> 24;
    char domain = header >> 16;
    char message = (header & 0xFFFFFFFF);

    if (check_nutrition(type, domain, message, content) == 1)
        return (1);
    else if (check_entertainment(type, domain, message, content) == 1)
        return (1);
    else if (check_education(type, domain, message, content) == 1)
        return (1);
    printf("Invalid message.\n");
    return (0);
}
