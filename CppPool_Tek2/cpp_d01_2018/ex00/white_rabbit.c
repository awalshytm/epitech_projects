/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** white rabbit
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int follow_the_white_rabbit(void)
{
    int sum = 0;
    int res = 0;

    while (sum != 421 && sum != 591) {
        res = (int)random() % 37 + 1;
        sum += res;
        if (res == 37 || (sum > 591) || res == 1)
            break;
        else if (res == 4 || res == 5 || res == 6 || res == 17 || res == 28 ||
        (res >= 18 && res <= 21))
            write(1, "left\n", 5);
        else if (res == 10 || res == 15 || res == 23)
            write(1, "forward\n", 8);
        else if (res == 26 || (res % 8 == 0))
            write(1, "backward\n", 9);
        else if (res == 13 || res >= 34)
            write(1, "right\n", 6);
    }
    write(1, "RABBIT!!!\n", 10);
    return (sum);
}
