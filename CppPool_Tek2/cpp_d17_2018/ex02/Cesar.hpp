/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef CESAR_HPP_
	#define CESAR_HPP_

#include <iostream>
#include "IEncryptionMethod.hpp"

class Cesar : public IEncryptionMethod
{
	public:
		Cesar();
		virtual ~Cesar();

                void encryptChar(char c) override;
                void decryptChar(char c) override;
                void reset() override;
	private:
                int _shifting;
};

#endif /* !CESAR_HPP_ */