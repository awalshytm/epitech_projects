/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "OneTime.hpp"

OneTime::OneTime(std::string const &key)
        :_key(key), _index(0)
{}

OneTime::~OneTime()
{}

void OneTime::reset()
{
        _index = 0;
}

void OneTime::encryptChar(char c)
{
        int decal = 0;
        int kdecal = 0;

        if (_index >= (int)_key.length())
                reset();
        if (isalpha(c)) {
                decal = c > 96 ? 97 : 65;
                kdecal = _key[_index] > 96 ? 97 : 65;
                c = (c - decal + (_key[_index] - kdecal)) % 26 + decal;
        }
        std::cout << c;
        _index = (_index + 1) % _key.size();
}

void OneTime::decryptChar(char c)
{
        int decal = 0;
        int kdecal = 0;

        if (_index >= (int)_key.length())
                reset();
        if (isalpha(c)) {
                decal = c > 96 ? 122 : 90;
                kdecal = _key[_index] > 96 ? 97 : 65;
                c = (c - decal - (_key[_index] - kdecal)) % 26 + decal;
        }
        std::cout << c;
        _index = (_index + 1) % _key.size();
}