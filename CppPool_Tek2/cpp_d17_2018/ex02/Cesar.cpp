/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Cesar.hpp"

Cesar::Cesar()
	:_shifting(3)
{}

Cesar::~Cesar()
{}

void Cesar::reset() { _shifting = 3; }

void Cesar::encryptChar(char c)
{
	if (c >= 'A' && c <= 'Z')
		c = 'A' + ((c - 'A' + _shifting) % 26);
	else if (c >= 'a' && c <= 'z')
		c = 'a' + ((c - 'a' + _shifting) % 26);
	std::cout << c;
	_shifting++;
}

void Cesar::decryptChar(char c)
{
    if (c >= 'a' && c <= 'z') {
        c = (c - 'a' - _shifting) % 26;
        if (c < 0)
            c += 26;
        c += 'a';
    } else if (c >= 'A' && c <= 'Z') {
        c = (c - 'A' - _shifting) % 26;
        if (c < 0)
            c += 26;
        c += 'A';
    }
    std::cout << c;
    _shifting++;
}