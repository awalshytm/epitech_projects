/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef ONETIME_HPP_
	#define ONETIME_HPP_

#include <string>
#include <iostream>
#include "IEncryptionMethod.hpp"

class OneTime : public IEncryptionMethod
{
	private:
                std::string _key;
                int _index;
	public:
		OneTime(std::string const &key);
		virtual ~OneTime();
                void encryptChar(char c) override;
                void decryptChar(char c) override;
                void reset() override;
};

#endif /* !ONETIME_HPP_ */
