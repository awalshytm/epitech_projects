/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef FIND_HPP_
	#define FIND_HPP_

#include <algorithm>

template<class T>
class T::iterator do_find(T &tab, int to_find)
{
        return std::find(tab.begin(), tab.end(), to_find);
}

#endif /* !FIND_HPP_ */