/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** character
*/

#include <string>
#include <iostream>
#include "Character.hpp"

Character::Character(const std::string &name, int level)
        :_name(name), _level(level), _strength(5), _power(100),
        _pv(100), _stamina(5), _agility(5), _intelligence(5), _spirit(5),
        _race(new std::string("Koala"))
{
        Range = Character::CLOSE;
        std::cout << _name << " Created" << std::endl;
}

const std::string &Character::getName() const
{
        return (_name);
}

int Character::getLvl() const { return _level; }

int Character::getPv() const { return _pv; }

int Character::getPower() const { return _power; }

void Character::TakeDamage(int damage)
{
        if (this->_pv <= damage) {
                this->_pv = 0;
                std::cout << this->_name << " out of combat" << std::endl;
                return;
        }
        this->_pv -= damage;
        std::cout << this->_name << " takes " << damage << " damage"
        << std::endl;
}

void Character::Heal()
{
        _pv += 50;
        std::cout << _name << " takes a potion" << std::endl;
}

void Character::RestorePower()
{
        _power = 100;
        std::cout << _name << " eats" << std::endl;
}

int Character::CloseAttack()
{
        if (_power < 10)
                return (0);
        _power -= 10;
        std::cout << _name << " strikes with a wooden stick" << std::endl;
        return (10 + _strength);
}

int Character::RangeAttack()
{
        if (_power < 10)
                return (0);
        _power -= 10;
        std::cout << _name << " tosses a stone" << std::endl;
        return (5 + _strength);
}
