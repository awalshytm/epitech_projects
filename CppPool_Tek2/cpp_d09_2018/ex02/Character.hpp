/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** character
*/

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <iostream>

class Character {
        protected:
                std::string const _name;
                int _level;
                int _strength;
                int _power;
                int _pv;
                int _stamina;
                int _agility;
                int _intelligence;
                int _spirit;
                std::string *_race;
        public:
                enum AttackRange { CLOSE, RANGE };
                AttackRange Range;
                Character(const std::string &name, int level);
                const std::string &getName() const;
                int getLvl() const;
                int getPv() const;
                int getPower() const;
                virtual void TakeDamage(int damage);
                void Heal();
                void RestorePower();
                virtual int CloseAttack();
                virtual int RangeAttack();
};

#endif
