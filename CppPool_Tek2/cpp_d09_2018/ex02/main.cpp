
#include "Character.hpp"
#include "Warrior.hpp"

int main()
{
    Character c("poney", 42);
    Warrior w("Thor", 42);

    std::cout << "take close attack warrior = 32 | " << c.getName() << " " << w.getName() << std::endl;
    c.Range = Character::RANGE;
    w.Range = Warrior::RANGE;
    c.TakeDamage(w.CloseAttack());
    return (0);
}
