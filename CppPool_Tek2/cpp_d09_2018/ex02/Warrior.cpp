/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** warrior
*/

#include "Character.hpp"
#include "Warrior.hpp"

Warrior::Warrior(std::string const &name, int level, std::string weapon)
        :Character(name, level),  _weaponName(weapon)
{
        _strength = 12;
        _stamina = 12;
        _agility = 7;
        _intelligence = 6;
	if (_race)
		delete _race;
        _race = new std::string("Dwarf");
        std::cout << "I'm " << _name <<
        " KKKKKKKKKKRRRRRRRRRRRRRREEEEEEEEOOOOOOORRRRGGGGGGG" << std::endl;
}

int Warrior::CloseAttack()
{
        std::string hammer("hammer");

        if (_power < 30)
                return (0);
        _power -= 30;
        std::cout << _name << " strikes with his " << _weaponName << std::endl;
        return (20 + _strength);
}

int Warrior::RangeAttack()
{
        if (_power < 10)
                return (0);
        _power -= 10;
        std::cout << _name << " intercepts";
        return (0);
}
