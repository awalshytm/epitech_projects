/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _WARRIOR_H_
#define _WARRIOR_H_

#include <string>
#include <iostream>
#include "Character.hpp"

class Warrior : public Character
{
        private:
                std::string _weaponName;
        public:
                Warrior(std::string const &name, int level,
                std::string weapon);
                int CloseAttack() override;
                int RangeAttack() override;
};

#endif
