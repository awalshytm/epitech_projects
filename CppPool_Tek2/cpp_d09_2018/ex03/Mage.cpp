/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** mage
*/

#include <string>
#include <iostream>
#include "Mage.hpp"

Mage::Mage(std::string const &name, int level)
        :Character(name, level)
{
        std::cout << _name << " teleported" << std::endl;
}

int Mage::CloseAttack()
{
        if (_power < 10)
                return (0);
        _power -= 10;
        std::cout << _name << " blinks" << std::endl;
        return (RangeAttack());
}

int Mage::RangeAttack()
{
        if (_power < 25)
                return (0);
        _power -= 25;
        std::cout << _name << " langhes a fire ball" << std::endl;
        return (20 + _spirit);
}

void Mage::RestorePower()
{
        _power += (50 + _intelligence);
        std::cout << _name << " takes a mana potion" << std::endl;
}

void Mage::Heal()
{
        _pv += 50;
        std::cout << _name << " takes a potion" << std::endl;
}