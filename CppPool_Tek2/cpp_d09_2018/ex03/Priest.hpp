/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _PRIEST_H_
#define _PRIEST_H_

#include <string>
#include <iostream>
#include "Mage.hpp"

class Priest : public Mage
{
        public:
                Priest(std::string const &name, int level);
                void Heal() override;
};

#endif