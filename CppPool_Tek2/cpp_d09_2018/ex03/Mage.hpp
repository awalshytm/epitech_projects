/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _MAGE_H_
#define _MAGE_H_

#include <string>
#include <iostream>
#include "Character.hpp"

class Mage : public Character
{
        public:
                Mage(std::string const &name, int level);
                int CloseAttack() override;
                int RangeAttack() override;
                void RestorePower() override;
                virtual void Heal();
};

#endif