/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** priest
*/

#include <iostream>
#include <string>
#include "Priest.hpp"

Priest::Priest(std::string const &name, int level)
        :Mage(name, level)
{
        std::cout << _name << " enters in the order" << std::endl;
}

void Priest::Heal()
{
        if (_power < 10)
                return;
        _power -= 10;
        _pv += 70;
        std::cout << _name << " casts a little heal spell" << std::endl;
}