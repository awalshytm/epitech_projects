/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** ex00
*/

#include <stdlib.h>
#include "ex00.h"

static void koala_initializer(koala_t *this, char *_name, char _is_a_legend)
{
    this->m_parent = *new_cthulhu();
    this->m_parent.m_name = strdup(_name);
    this->m_is_a_legend = _is_a_legend;
    if (_is_a_legend == 0)
        this->m_parent.m_power = 0;
    printf("Building %s\n", _name);
}

static void cthulhu_initializer(cthulhu_t *this)
{
    this->m_name = strdup("Cthulhu");
    this->m_power = 42;
    printf("----\nBuilding %s\n", this->m_name);
}

cthulhu_t *new_cthulhu()
{
    cthulhu_t *new = malloc(sizeof(cthulhu_t));

    if (!new)
        return (NULL);
    cthulhu_initializer(new);
    return (new);
}

void print_power(cthulhu_t *this)
{
    printf("Power => %d\n", this->m_power);
}

void attack(cthulhu_t *this)
{
    if (this->m_power < 42) {
        printf("%s can't attack, he doesn't have enough power\n",
        this->m_name);
        return;
    }
    this->m_power -= 42;
    printf("%s attacks and destroys the city\n", this->m_name);
}

void sleeping(cthulhu_t *this)
{
    printf("%s sleeps\n", this->m_name);
    this->m_power += 42000;
}

koala_t *new_koala(char *name, char is_a_legend)
{
    koala_t *e = malloc(sizeof(koala_t));

    if (!e)
            return (NULL);
    koala_initializer(e, name, is_a_legend);
    return (e);
}

void eat(koala_t *this)
{
    printf("%s eats\n", this->m_parent.m_name);
    this->m_parent.m_power += 42;
}