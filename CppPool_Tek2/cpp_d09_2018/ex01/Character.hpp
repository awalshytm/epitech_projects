/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** character
*/

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <iostream>

class Character {
        private:
                std::string const _name;
                int _level;
                int _strength;
                int _power;
                int _pv;
                int _stamina;
                int _agility;
        public:
                enum AttackRange { CLOSE, RANGE };
                AttackRange Range;
                Character(const std::string &name, int level);
                const std::string &getName() const;
                int getLvl() const;
                int getPv() const;
                int getPower() const;
                void TakeDamage(int damage);
                void Heal();
                void RestorePower();
                int CloseAttack();
                int RangeAttack();
};

#endif
