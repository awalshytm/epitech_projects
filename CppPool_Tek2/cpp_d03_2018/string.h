/*
** EPITECH PROJECT, 2019
** string
** File description:
** header
*/

#ifndef _STRING_H_
#define _STRING_H_

typedef struct string_s {
    char *str;
    void (*string_init)(struct string_s *str, const char *s);
    void (*string_destroy)(struct string_s *str);
    void (*assign_s)(struct string_s *this,
    const struct string_s *str);
    void (*assign_c)(struct string_s *this, const char *s);
    void (*append_s)(struct string_s *this,
    const struct string_s *str);
    void (*append_c)(struct string_s *this, const char *s);
    char (*at)(const struct string_s *this, size_t pos);
    void (*clear)(struct string_s *this);
    int (*size)(const struct string_s *this);
    int (*compare_s)(const struct string_s *this,
    const struct string_s *str);
    int (*compare_c)(const struct string_s *this,
    const char *s);
    size_t (*copy)(const struct string_s *this, char *s,
    size_t n, size_t pos);
    char *(*c_str)(struct string_s *this);
    int (*empty)(const struct string_s *this);
    int (*find_s)(const struct string_s *this,
    const struct string_s *str, size_t pos);
    int (*find_c)(const struct string_s *this,
    const char *str, size_t pos);
    void (*insert_s)(struct string_s *this, size_t pos,
    const struct string_s *str);
    void (*insert_c)(struct string_s *this, size_t pos,
    const char *str);
    int (*to_int)(const struct string_s *this);
    struct string_s **(*split_s)(const struct string_s *this,
    char separator);
    char **(*split_c)(const struct string_s *this, char separator);
    void (*aff)(const struct string_s *this);
    void (*join_s)(struct string_s *this, char delim,
    const struct string_s ** tab);
    void (*join_c)(struct string_s *this, char delim, const char ** tab);
    struct string_s *(*substr)(struct string_s *this,
    int offset, int length);
} string_t;

/* init */
void string_init(string_t *str, const char *s);
void string_destroy(string_t *str);

/* assign */
void (*get_assign_s(void))(string_t *, const string_t *);
void (*get_assign_c(void))(string_t *, const char *);

/* append */
void (*get_append_s(void))(string_t *, const string_t *);
void (*get_append_c(void))(string_t *, const char *);

/* tools */
char (*get_at(void))(const string_t *, size_t pos);
void (*get_clear(void))(string_t *);
int (*get_size(void))(const string_t *);
char *(*get_c_str(void))(string_t *);
size_t copy (const string_t *this, char *s, size_t n, size_t pos);
int empty(const string_t *this);
int to_int(const string_t *this);
void aff(const string_t *this);
string_t *substr(string_t *this, int offset, int length);

/* compare */
int (*get_compare_s(void))(const string_t *this, const string_t *str);
int (*get_compare_c(void))(const string_t *this, const char *s);

/* find */
int find_s(const string_t *this, const string_t *str, size_t pos);
int find_c(const string_t *this, const char *str, size_t pos);

/* insert */
void insert_s(string_t *this, size_t pos, const string_t *str);
void insert_c(string_t *this, size_t pos, const char *str);

/* split */
string_t **split_s(const string_t *this, char separator);
char **split_c(const string_t *this, char separator);

/* join */
void join_s(string_t *this, char delim, const string_t ** tab);
void join_c(string_t *this, char delim, const char ** tab);

/* split_whitespaces */
char *get_line(char **str, char spliter);
int nbr_words(char *str, char spliter);
char **split_whitespaces(char *str, char spliter);

#endif
