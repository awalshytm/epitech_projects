/*
** EPITECH PROJECT, 2019
** string
** File description:
** conpare
*/

#include <string.h>
#include "string.h"

static int compare_s(const string_t *this, const string_t *str)
{
    if (!this || !str)
        return (-1);
    return (strcmp(this->str, str->str));
}

static int compare_c(const string_t *this, const char *s)
{
    if (!this)
        return (-1);
    return (strcmp(this->str, s));
}

int (*get_compare_s(void))(const string_t *, const string_t *)
{
    return (&compare_s);
}

int (*get_compare_c(void))(const string_t *, const char *)
{
    return (&compare_c);
}
