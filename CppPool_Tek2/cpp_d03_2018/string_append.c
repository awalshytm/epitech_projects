/*
** EPITECH PROJECT, 2019
** string
** File description:
** append
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "string.h"

static void append_s(string_t *this, const string_t *ap)
{
    char *s = NULL;
    int size = 0;;

    if (!this || !ap)
        return;
    size = strlen(this->str) + strlen(ap->str) + 1;
    if (!(s = malloc(size)))
        return;
    s = strcpy(s, this->str);
    s = strcat(s, ap->str);
    s[size] = '\0';
    this->str = s;
}

static void append_c(string_t *this, const char *ap)
{
    char *s = NULL;
    int size = 0;

    if (!this || !ap)
        return;
    size = strlen(this->str) + strlen(ap) + 1;
    if (!(s = malloc(size)))
        return;
    s = strcpy(s, this->str);
    s = strcat(s, ap);
    s[size] = '\0';
    this->str = s;
}

void (*get_append_s(void))(string_t *, const string_t *)
{
    return (&append_s);
}

void (*get_append_c(void))(string_t *, const char *)
{
    return (&append_c);
}
