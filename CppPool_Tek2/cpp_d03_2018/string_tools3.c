/*
** EPITECH PROJECT, 2019
** string
** File description:
** cpoy
*/

#include <string.h>
#include "string.h"

size_t copy(const string_t *this, char *s, size_t n, size_t pos)
{
    size_t i = 0;

    if (!this)
        return (0);
    for (i = 0; i < n && s[i] && this->str[pos + i]; i++) {
        s[i] = this->str[pos + i];
    }
    return (i);
}

int empty(const string_t *this)
{
    if (this->str == NULL || strlen(this->str) == 0)
        return (1);
    return (0);
}
