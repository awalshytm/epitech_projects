/*
** EPITECH PROJECT, 2019
** string
** File description:
** find insert
*/

#include <string.h>
#include <stdlib.h>
#include "string.h"

int find_s(const string_t *this, const string_t *str, size_t pos)
{
    int i = 0;
    int j = 0;

    if (!this || pos >= strlen(this->str) ||
    (strlen(str->str) + pos > strlen(this->str)))
        return (-1);
    while (this->str[i] != '\0') {
            while (str->str[j] == this->str[i + j] && str->str[j]) {
                    j++;
            }
            if (str->str[j] == '\0')
                    return (i);
            i++;
    }
    return (-1);
}

int find_c(const string_t *this, const char *str, size_t pos)
{
    int i = 0;
    int j = 0;

    if (!this || pos >= strlen(this->str) ||
    (strlen(str) + pos > strlen(this->str)))
        return (-1);
    while (this->str[i] != '\0') {
            while (str[j] == this->str[i + j] && str[j]) {
                    j++;
            }
            if (str[j] == '\0')
                    return (i);
            i++;
    }
    return (-1);
}

void insert_c(string_t *this, size_t pos, const char *str)
{
    char *new = malloc(strlen(this->str) + strlen(str) + 1);
    int i = 0;
    int j = 0;

    if (!this)
        return;
    for (j = 0; this->str[j] && j < (int)pos; j++)
        new[i++] = this->str[j];
    for (int k = 0; str[k]; k++)
        new[i++] = str[k];
    for ( ; this->str[j]; j++)
        new[i++] = this->str[j];
    new[i] = '\0';
    free(this->str);
    this->str = new;
}

void insert_s(string_t *this, size_t pos, const string_t *str)
{
    char *new = malloc(strlen(this->str) + strlen(str->str) + 1);
    int i = 0;
    int j = 0;

    if (!this)
        return;
    for (j = 0; this->str[j] && j < (int)pos; j++)
        new[i++] = this->str[j];
    for (int k = 0; str->str[k]; k++)
        new[i++] = str->str[k];
    for ( ; this->str[j]; j++)
        new[i++] = this->str[j];
    new[i] = '\0';
    free(this->str);
    this->str = new;
}
