/*
** EPITECH PROJECT, 2019
** string
** File description:
** tools
*/

#include <string.h>
#include <stdlib.h>
#include "string.h"

static char at(const string_t *this, size_t pos)
{
    if (!this)
        return (-1);
    if (pos >= strlen(this->str))
        return (-1);
    return (this->str[pos]);
}

static void clear(string_t *this)
{
    if (!this)
        return;
    free(this->str);
    this->str = strdup("");
}

char (*get_at(void))(const string_t *, size_t)
{
    return (&at);
}

void (*get_clear(void))(string_t *)
{
    return (&clear);
}
