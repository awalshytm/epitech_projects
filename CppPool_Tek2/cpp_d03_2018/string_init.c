/*
** EPITECH PROJECT, 2019
** string
** File description:
** string
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "string.h"

void string_init(string_t *this, const char *s)
{
    if (!this)
        return;
    this->str = strdup(s);
    this->string_init = &string_init;
    this->string_destroy = &string_destroy;
    this->assign_c = get_assign_c();
    this->assign_s = get_assign_s();
    this->append_c = get_append_c();
    this->append_s = get_append_s();
    this->at = get_at();
    this->clear = get_clear();
    this->size = get_size();
    this->compare_s = get_compare_s();
    this->compare_c = get_compare_c();
    this->copy = &copy;
    this->c_str = get_c_str();
    this->empty = &empty;
    this->find_s = &find_s;
    this->find_c = &find_c;
    this->insert_s = &insert_s;
    this->insert_c = &insert_c;
    this->to_int = &to_int;
    this->split_s = &split_s;
    this->split_c = &split_c;
    this->aff = &aff;
    this->join_s = &join_s;
    this->join_c = &join_c;
    this->substr = &substr;
}

void string_destroy(string_t *this)
{
    if (!this)
        return;
    free(this->str);
}
