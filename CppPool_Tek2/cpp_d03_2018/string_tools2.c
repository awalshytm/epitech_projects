/*
** EPITECH PROJECT, 2019
** string
** File description:
** tools2
*/

#include <string.h>
#include "string.h"

static int size(const string_t *this)
{
    if (!this || this->str == NULL)
        return (-1);
    return (strlen(this->str));
}

static char *c_str(string_t *this)
{
    if (!this)
        return (NULL);
    return (this->str);
}

int (*get_size(void))(const string_t *)
{
    return (&size);
}

char *(*get_c_str(void))(string_t *)
{
    return (&c_str);
}
