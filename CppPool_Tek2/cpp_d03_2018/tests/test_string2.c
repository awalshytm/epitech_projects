/*
** EPITECH PROJECT, 2019
** string 
** File description:
** test
*/

#include <criterion/criterion.h>
#include "../string.h"

Test(string2, size)
{
    string_t s;
    int size;

    string_init(&s, "Bonjour");
    size = s.size(&s);
    cr_assert_eq(size, 7, "size");
}

Test(string2, c_str)
{
    string_t s;
    char *str = "Walsh";

    string_init(&s, str);
    cr_assert_str_eq(s.str, str, "c_str");
}

Test(string2, compare)
{
    string_t s;
    string_t test;

    string_init(&s, "Bonjour");
    string_init(&test, "Bonjour");
    cr_assert_eq(s.compare_s(&s, &test), 0, "compare_s");
    cr_assert_eq(s.compare_c(&s, "Bonjour"), 0, "compare_c");
}
