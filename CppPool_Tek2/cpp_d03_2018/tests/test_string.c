/*
** EPITECH PROJECT, 2019
** string
** File description:
** test
*/

#include <criterion/criterion.h>
#include <stdlib.h>
#include "../string.h"

Test(string, init)
{
    string_t s;
    string_t s1;

    string_init(&s, "Bonjour");
    cr_assert_str_eq(s.str, "Bonjour", "init bonjour");
    string_init(&s1, "Hello World");
    cr_assert_str_eq(s1.str, "Hello World", "init hello world");
    string_destroy(&s);
    string_destroy(&s1);
}

Test(string, append)
{
    string_t s;
    string_t as;
    char *str = " Walsh";

    string_init(&s, "Bonjour");
    string_init(&as, " Arthur");
    s.append_s(&s, &as);
    cr_assert_str_eq(s.str, "Bonjour Arthur", "append s");
    s.append_c(&s, str);
    cr_assert_str_eq(s.str, "Bonjour Arthur Walsh", "append c");
}

Test(string, assign)
{
    string_t s;
    string_t as;
    char *str = "Walsh";

    string_init(&s, "Bonjour");
    string_init(&as, "Arthur");
    s.assign_s(&s, &as);
    cr_assert_str_eq(s.str, "Arthur", "assign s");
    s.assign_c(&s, str);
    cr_assert_str_eq(s.str, "Walsh", "assign c");
}

Test(string, at)
{
    string_t s;
    char c;

    string_init(&s, "Bonjour");
    c = s.at(&s, 4);
    cr_assert_eq(c, 'o', "at");
    c = s.at(&s, 10);
    cr_assert_eq(c, -1, "at");
    c = s.at(&s, -5);
    cr_assert_eq(c, -1, "at");
    c = s.at(&s, 0);
    cr_assert_eq(c, 'B', "at");
    c = s.at(&s, 7);
    cr_assert_eq(c, -1, "at");
}

Test(string, clear)
{
    string_t s;

    string_init(&s, "Bonjour");
    cr_assert_str_eq(s.str, "Bonjour");
    s.clear(&s);
    cr_assert_str_eq(s.str, "", "clear");
}
