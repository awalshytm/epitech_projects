/*
** EPITECH PROJECT, 2019
** string
** File description:
** test
*/

#include <criterion/criterion.h>
#include "../string.h"

Test(string, split)
{
    string_t s;
    char *bonjour = "Bonjour";
    char *arthur = "Arthur";
    string_t **split_s;
    char **split_c;

    string_init(&s, "Bonjour,Arthur");
    split_s = s.split_s(&s, ',');
    cr_assert_str_eq(split_s[0]->str, bonjour, "split_s bonjour");
    cr_assert_str_eq(split_s[1]->str, arthur, "split_s arthur");
    split_c = s.split_c(&s, ',');
    cr_assert_str_eq(split_c[0], bonjour, "split_c bonjour");
    cr_assert_str_eq(split_c[1], arthur, "split_c arthur");
}

Test(string, join_s)
{
    string_t *str = malloc(sizeof(string_t) * 3);
    string_t s;

    string_init(&s, "Bonjour");
    string_init(&str[0], "Bonjour");
    string_init(&str[1], "Arthur");
    string_init(&str[2], "Walsh");
    s.join_s(&s, ' ', &str);
    cr_assert_str_eq(s.str, "Bonjour Arthur Walsh", "join_s");
}

Test(string, join_c)
{
    char **str = malloc(sizeof(char *) * 4);
    string_t s;
    char *solution = "bonjour arthur walsh";

    string_init(&s, "Jour");
    str[0] = strdup("bonjour");
    str[1] = strdup("arthur");
    str[2] = strdup("walsh");
    str[3] = NULL;
    s.join_c(&s, ' ', str);
    cr_assert_str_eq(s.str, solution, "[%s]", s.str);
}
