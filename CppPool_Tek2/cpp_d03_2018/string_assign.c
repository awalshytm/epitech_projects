/*
** EPITECH PROJECT, 2019
** string
** File description:
** assign
*/

#include <string.h>
#include <stdlib.h>
#include "string.h"

static void assign_s(string_t *this, const string_t *str)
{
    if (!this)
        return;
    if (this->str)
        free(this->str);
    this->str = strdup(str->str);
}

static void assign_c(string_t *this, const char *s)
{
    if (!this)
        return;
    if (this->str)
        free(this->str);
    this->str = strdup(s);
}

void (*get_assign_s(void))(string_t *, const string_t *)
{
    return (&assign_s);
}

void (*get_assign_c(void))(string_t *, const char *)
{
    return (&assign_c);
}
