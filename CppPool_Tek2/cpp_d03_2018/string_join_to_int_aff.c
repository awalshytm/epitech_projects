/*
** EPITECH PROJECT, 2019
** string
** File description:
** join
*/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "string.h"

void join_s(string_t *this, char delim, const string_t **str)
{
    if (!this || !str)
        return;
    if (this->empty(this) == 0)
        this->clear(this);
    this->assign_s(this, str[0]);
    for (int i = 1; str[i]; i++) {
        this->append_c(this, &delim);
        this->append_s(this, str[i]);
   }
}

void join_c(string_t *this, char delim, const char **str)
{
    if (!this || !str)
        return;
    if (this->empty(this) == 0)
        this->clear(this);
    this->assign_c(this, str[0]);
    for (int i = 1; str[i]; i++) {
        this->append_c(this, &delim);
        this->append_c(this, str[i]);
    }
}

int to_int(const string_t *this)
{
    if (!this)
        return (-1);
    return (atoi(this->str));
}

void aff(const string_t *this)
{
    if (!this)
        return;
    write(1, this->str, strlen(this->str));
}

string_t *substr(string_t *this, int offset, int length)
{
    char *s = NULL;
    string_t *str = malloc(sizeof(string_t));
    int i = 0;
    int size;

    if (!this)
        return (NULL);
    size = strlen(this->str) - offset;
    size = size < length ? size : length;
    if (!(s = malloc(size)))
        return (NULL);
    for (i = offset; this->str[i] && i - offset < length; i++) {
        s[i - offset] = this->str[i];
    }
    s[i] = '\0';
    string_init(str, s);
    return (str);
}
