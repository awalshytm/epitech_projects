/*
** EPITECH PROJECT, 2019
** string
** File description:
** split
*/

#include <string.h>
#include <stdlib.h>
#include "string.h"

string_t **split_s(const string_t *this, char separator)
{
    string_t **s = NULL;
    int i = 0;
    int words;
    char *str = strdup(this->str);

    if (!(s = malloc(sizeof(string_t *) * (nbr_words(this->str, separator)))))
          return (NULL);
    words = nbr_words(this->str, separator);
    while (i < words) {
        if (!(s[i] = malloc(sizeof(string_t))))
            return (NULL);
        string_init(s[i], get_line(&str, separator));
        i++;
    }
    return (s);
}

char **split_c(const string_t *this, char separator)
{
    if (!this)
        return (NULL);
    return (split_whitespaces(this->str, separator));
}
