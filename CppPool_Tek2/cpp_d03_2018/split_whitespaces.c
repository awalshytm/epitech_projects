/*
** EPITECH PROJECT, 2018
** minishell1
** File description:
** split_whitespaces
*/

#include <stdlib.h>

int nbr_words(char *str, char spliter)
{
    int i = 0;
    int word = 0;

    while (str[i]) {
        if (str[i] == spliter || str[i] == '\n')
            word++;
        i++;
    }
    return (word + 1);
}

char *get_line(char **str, char spliter)
{
    int i = 0;
    char *s = NULL;

    while (str[0][i] != spliter && str[0][i] != '\n' && str[0][i]) {
        i++;
    }
    if (!(s = malloc(sizeof(char) * (i + 1))))
        return (NULL);
    i = 0;
    while (str[0][i] != spliter && str[0][i] != '\n' && str[0][i]) {
        s[i] = str[0][i];
        i++;
    }
    s[i] = '\0';
    str[0] = &str[0][i + 1];
    return (s);
}

char **split_whitespaces(char *str, char spliter)
{
    char **s = malloc(sizeof(char *) * (nbr_words(str, spliter) + 1));
    int i = 0;
    int words = nbr_words(str, spliter);

    if (!s)
        return (NULL);
    while (i < words) {
        s[i] = get_line(&str, spliter);
        i++;
    }
    s[i] = 0;
    return (s);
}
