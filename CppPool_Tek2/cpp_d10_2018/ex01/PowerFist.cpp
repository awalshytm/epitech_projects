/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "PowerFist.hpp"

PowerFist::PowerFist(std::string const &name)
        :AWeapon(name, 8, 50)
{}

void PowerFist::attack() const
{
        std::cout << "* pschhh... SBAM! *" << std::endl;
}