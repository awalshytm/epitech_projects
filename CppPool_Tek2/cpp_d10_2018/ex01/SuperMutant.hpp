/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _SUPER_MUTANT_H_
#define _SUPER_MUTANT_H_

#include <string>
#include <iostream>
#include "AEnemy.hpp"
#include "ISpaceMarine.hpp"

class SuperMutant : public AEnemy
{
        public:
        SuperMutant(std::string const &type = "Super Mutant");
        ~SuperMutant();
        void takeDamage(int damage) override;
};

#endif