/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#ifndef _AENEMY_H_
#define _AENEMY_H_

#include <string>
#include <iostream>

class AEnemy {
        protected:
        std::string const _type;
        int _hp;
        public:
        AEnemy(int hp, const std::string &type);
        virtual ~AEnemy();
        virtual void takeDamage(int damage);
        std::string const &getType() const;
        int getHP() const;
};

#endif