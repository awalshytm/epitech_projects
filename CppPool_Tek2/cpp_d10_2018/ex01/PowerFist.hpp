/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _POWER_FIST_H_
#define _POWER_FIST_H_

#include <string>
#include <iostream>
#include "AWeapon.hpp"

class PowerFist : public AWeapon
{
        public:
        PowerFist(std::string const &name = "Power Fist");
        void attack() const override;
};

#endif