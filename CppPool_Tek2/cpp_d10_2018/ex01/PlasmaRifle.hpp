/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _PLASMA_RIFLE_H_
#define _PLASMA_RIFLE_H_

#include <string>
#include <iostream>
#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon
{
        public:
        PlasmaRifle(std::string const &name = "Plasma Rifle");
        void attack() const override;
};

#endif