/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** cpp pool
*/

#ifndef _RAD_SCORP_H_
#define _RAD_SCORP_H_

#include <string>
#include <iostream>
#include "AEnemy.hpp"

class RadScorpion : public AEnemy
{
        public:
        RadScorpion(std::string const &type = "RadScorpion");
        ~RadScorpion();
};

#endif