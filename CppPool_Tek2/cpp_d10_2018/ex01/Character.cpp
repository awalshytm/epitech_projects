/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <string>
#include <iostream>
#include "Character.hpp"

Character::Character(const std::string &name)
        :_name(name), _ap(40), _weapon(nullptr)
{}

Character::~Character()
{
        if (_weapon)
                delete _weapon;
}

void Character::recoverAP()
{
        _ap += 10;
        if (_ap > 40)
                _ap = 40;
}

void Character::equip(AWeapon *weapon)
{
        if (!weapon)
                return;
        _weapon = weapon;
}

void Character::attack(AEnemy *enemy)
{
        if (!_weapon || _ap < _weapon->getAPCost())
                return;
        std::cout << _name << " attacks " << enemy->getType() <<
        " with a " << _weapon->getName() << std::endl;
        _ap -= _weapon->getAPCost();
        enemy->takeDamage(_weapon->getDamage());
        _weapon->attack();
        if (enemy->getHP() <= 0)
                delete enemy;
}

std::string const &Character::getName() const { return _name; }

int Character::getAP() const { return _ap; }

bool Character::isWeaponSet() const
{ 
        return (_weapon != nullptr ? true : false);
}

std::string const &Character::getWeaponName() const
{
        return (_weapon->getName());
}

std::ostream &operator<<(std::ostream &s, Character &c)
{
        if (c.isWeaponSet() == false)
                s << c.getName() << " has " << c.getAP() <<
                " AP and is unarmed" << std::endl;
        else
                s << c.getName() << " has " << c.getAP() <<
                " AP and wields a " << c.getWeaponName() << std::endl;
        return (s);
}