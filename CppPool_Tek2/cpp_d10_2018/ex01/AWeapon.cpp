/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <string>
#include <iostream>
#include "AWeapon.hpp"

AWeapon::AWeapon(std::string const &name, int apcost, int damage)
        :_name(name), _damage(damage), _apcost(apcost)
{}

AWeapon::~AWeapon() {}

std::string const &AWeapon::getName() const { return _name; }

int AWeapon::getAPCost() const { return _apcost; }

int AWeapon::getDamage() const { return _damage; }