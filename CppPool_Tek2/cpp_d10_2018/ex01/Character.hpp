/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <iostream>
#include "AWeapon.hpp"
#include "AEnemy.hpp"

class Character {
        private:
        std::string const _name;
        int _ap;
        AWeapon *_weapon;
        public:
        Character(const std::string &name);
        ~Character();
        void recoverAP();
        void equip(AWeapon *weapon);
        void attack(AEnemy *enemy);
        std::string const &getName() const;
        int getAP() const;
        bool isWeaponSet() const;
        std::string const &getWeaponName() const;
};

std::ostream &operator<<(std::ostream &s, Character &c);

#endif