/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <string>
#include <iostream>
#include "AEnemy.hpp"

AEnemy::AEnemy(int hp, const std::string &type)
        :_type(type), _hp(hp)
{}

AEnemy::~AEnemy() {}

void AEnemy::takeDamage(int damage)
{
        if (damage < 0)
                return;
        _hp -= damage;
}

std::string const &AEnemy::getType() const { return _type; }

int AEnemy::getHP() const { return _hp; }