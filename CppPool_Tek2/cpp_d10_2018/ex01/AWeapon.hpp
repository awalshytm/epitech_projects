/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _WEAPON_H_
#define _WEAPON_H_

#include <string>
#include <iostream>

class AWeapon {
        protected:
        std::string const _name;
        int _damage;
        int const _apcost;
        public:
        AWeapon(std::string const &name, int apcost, int damage);
        virtual ~AWeapon();
        std::string const &getName() const;
        int getAPCost() const;
        int getDamage() const;
        virtual void attack() const = 0;
};

#endif