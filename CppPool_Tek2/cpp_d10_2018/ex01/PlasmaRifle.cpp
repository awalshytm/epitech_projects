/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <string>
#include <iostream>
#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle(std::string const &name)
        :AWeapon(name, 5, 21)
{}

void PlasmaRifle::attack() const
{
        std::cout << "* piouuu piouuu piouuu *" << std::endl;
}