/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Cure.hpp"

Cure::Cure()
        :AMateria("cure")
{}

Cure::~Cure()
{}

void Cure::use(ICharacter &target)
{
        xp_ += 10;
        std::cout << "* heals " << target.getName() << "'s wounds *"
        << std::endl;
}