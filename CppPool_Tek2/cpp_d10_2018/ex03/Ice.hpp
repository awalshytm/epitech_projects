/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _ICE_H_
#define _ICE_H_

#include "AMateria.hpp"

class Ice : public AMateria
{
        public:
        Ice();
        ~Ice();
        void use(ICharacter &target) override;
};

#endif