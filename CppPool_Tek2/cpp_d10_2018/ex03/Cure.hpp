/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _CURE_H_
#define _CURE_H_

#include "AMateria.hpp"

class Cure : public AMateria
{
        public:
        Cure();
        ~Cure();
        void use(ICharacter &target) override;
};

#endif