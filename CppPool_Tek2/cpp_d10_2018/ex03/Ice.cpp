/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Ice.hpp"

Ice::Ice()
        :AMateria("ice")
{}

void Ice::use(ICharacter &target)
{
        xp_ += 10;
        std::cout << "* shoots an ice bolt at " << target.getName() <<
        " *" << std::endl;
}