/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "AMateria.hpp"

AMateria::AMateria(const std::string &type)
        :_type(type), xp_(0)
{}

AMateria::~AMateria()
{}

const std::string &AMateria::getType() const { return _type; }

unsigned int AMateria::getXP() const { return xp_; }

void AMateria::use(ICharacter &target)
{
        (void)target;
        xp_ += 10;
}