/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <iostream>

class ICharacter
{
        public:
        virtual ~ICharacter() {}
        virtual const std::string &getName() const = 0;
        virtual void equip(AMateria *m) = 0;
        virtual void unequip(int idx) = 0;
        virtual void use(int idx, ICharacter &target) = 0;
};

#endif