/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef _A_MATERIA_H_
#define _A_MATERIA_H_

#include <string>
#include <iostream>
#include "Character.hpp"

class AMateria {
        protected:
        std::string const _type;
        unsigned int xp_;
        public:
        AMateria(const std::string &type);
        virtual ~AMateria();
        const std::string &getType() const;
        unsigned int getXP() const;
        virtual AMateria *clone() const = 0;
        virtual void use(ICharacter &target);
};

#endif