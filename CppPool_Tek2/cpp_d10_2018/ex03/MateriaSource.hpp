/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef _MATERIA_SOURCE_H_
#define _MATERIA_SOURCE_H_

#include <string>
#include <iostream>

class IMateriaSource
{
        public:
        virtual ~IMateriaSource() {}
        virtual void learnMateria(AMateria *materia) = 0;
        virtual AMateria *createMateria(const std::string &type) = 0;
};

#endif