/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _ASSAULT_TERMINATOR_H_
#define _ASSAULT_TERMINATOR_H_

#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine
{
        public:
        AssaultTerminator();
        ~AssaultTerminator();
        ISpaceMarine *clone() const;
        void battleCry() const;
        void rangedAttack() const;
        void meleeAttack() const;
};

#endif