/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Squad.hpp"

Squad::Squad()
{}

Squad::~Squad()
{
        for (auto i = _tab.begin(); i != _tab.end(); i++)
              delete *i;
}

int Squad::getCount() const
{
        return (_tab.size());
}

ISpaceMarine *Squad::getUnit(int i)
{
        return (_tab[i]);
}

int Squad::push(ISpaceMarine *e)
{
        if (!e)
                return _tab.size();
        for (auto i = _tab.begin(); i != _tab.end(); i++)
                if(*i == e)
                        return (_tab.size());
        _tab.push_back(e);
        return (_tab.size());
}