/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _I_SQUAD_H_
#define _I_SQUAD_H_

#include <string>
#include <iostream>
#include "ISpaceMarine.hpp"

class ISquad
{
        public:
        virtual ~ISquad() {}
        virtual int getCount() const = 0;
        virtual ISpaceMarine *getUnit(int) = 0;
        virtual int push(ISpaceMarine *) = 0;
};

#endif