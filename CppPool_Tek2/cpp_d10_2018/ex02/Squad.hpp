/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _SQUAD_H_
#define _SQUAD_H_

#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "ISquad.hpp"

class Squad : public ISquad
{
        private:
        std::vector<ISpaceMarine *> _tab;
       
        public:
        Squad();
        ~Squad();
        int getCount() const;
        ISpaceMarine *getUnit(int i);
        int push(ISpaceMarine *);
};



#endif