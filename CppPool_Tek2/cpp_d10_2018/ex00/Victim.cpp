/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <string>
#include <iostream>
#include "Victim.hpp"

Victim::Victim(std::string const &name)
        :_name(name)
{
        std::cout << "Some random victim called " << _name <<
        " just popped!" << std::endl;
}

Victim::~Victim()
{
        std::cout << "Victim " << _name << " just died for no apparent reason!"
        << std::endl;
}

std::string Victim::getName() const { return _name; }

void Victim::getPolymorphed() const
{
        std::cout << _name << " has been turned into a cute little sheep!"
        << std::endl;
}

std::ostream &operator<<(std::ostream &s, Victim &v)
{
        s << "I'm " << v.getName() << " and i like otters!" << std::endl;
        return (s);
}