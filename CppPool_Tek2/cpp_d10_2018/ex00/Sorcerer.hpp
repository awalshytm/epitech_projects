/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _SORCERER_H_
#define _SORCERER_H_

#include <string>
#include <iostream>
#include "Victim.hpp"

class Sorcerer {
        private:
        std::string const _name;
        std::string const _title;
        public:
        Sorcerer(std::string const &name, std::string const &title);
        ~Sorcerer();
        std::string getName() const;
        std::string getTitle() const;
        void polymorph(const Victim &victim) const;
};

std::ostream &operator<<(std::ostream &s, Sorcerer &e);

#endif