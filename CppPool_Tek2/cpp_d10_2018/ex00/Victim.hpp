/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _VICTIM_H_
#define _VICTIM_H_

#include <string>
#include <iostream>

class Victim {
        protected:
        std::string const _name;
        public:
        Victim(std::string const &name);
        ~Victim();
        std::string getName() const;
        virtual void getPolymorphed() const;
};

std::ostream &operator<<(std::ostream &s, Victim &v);

#endif