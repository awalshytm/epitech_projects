/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef _PEON_H_
#define _PEON_H_

#include <string>
#include <iostream>
#include "Victim.hpp"

class Peon : public Victim
{
        public:
        Peon(std::string const &name);
        ~Peon();
        void getPolymorphed() const override;
};

#endif