/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** sorcerer
*/

#include <string>
#include <iostream>
#include "Sorcerer.hpp"

Sorcerer::Sorcerer(std::string const &name, std::string const &title)
	:_name(name), _title(title)
{
	std::cout << _name << ", " << _title << ", is born!" << std::endl;
}

Sorcerer::~Sorcerer()
{
	std::cout << _name << ", " << _title << ", is dead. " << 
	"Consequences will never be the same!" << std::endl;
}

std::string Sorcerer::getName() const { return _name; }

std::string Sorcerer::getTitle() const { return _title; }

void Sorcerer::polymorph(const Victim &victim) const
{
	victim.getPolymorphed();
}

std::ostream &operator<<(std::ostream &s, Sorcerer &e)
{
        s << "I am " << e.getName() << ", " << e.getTitle()
        << ", and I like ponies!" << std::endl;
	return (s);
}
