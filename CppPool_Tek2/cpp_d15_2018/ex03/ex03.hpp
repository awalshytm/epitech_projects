/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef EX03_HPP_
	#define EX03_HPP_

#include <string>
#include <iostream>

template<typename T>
void print(T const &to_print)
{
        std::cout << to_print << std::endl;
}

template<typename T>
void foreach(T const *tab, void (*func)(const T& elem), unsigned int size)
{
        for (unsigned int i = 0; i < size; i++)
                (func)(tab[i]);
}

#endif /* !EX03_HPP_ */