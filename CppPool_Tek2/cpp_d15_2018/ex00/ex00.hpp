/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** heder
*/

#ifndef EX00_HPP_
	#define EX00_HPP_

template<typename T>
T add(T a, T b)
{
        return a + b;
}

template<typename T>
T min(T a, T b)
{
        if (a < b)
                return (a);
        return (b);
}

template<typename T>
T max(T a, T b)
{
        if (a > b)
                return (a);
        return (b);
}

template<typename T>
void swap(T &a, T &b)
{
        T c = a;

        a = b;
        b = c;
}

#endif /* !EX00_HPP_ */
