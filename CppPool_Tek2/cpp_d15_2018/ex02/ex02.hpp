/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef EX02_HPP_
	#define EX02_HPP_

#include <string>
#include <iostream>

template<typename T>
T min(T a, T b)
{
        std::cout << "template min" << std::endl;
        if (a == b)
                return (a);
        if (a < b)
                return (a);
        return (b);
}

template<typename T>
T templateMin(T tab[], int size)
{
        T smallest = tab[0];

        for (int i = 0; i < size - 1; i++)
                smallest = min<T>(tab[i], tab[i + 1]);
        return (smallest);
}

int min(int a, int b)
{
        std::cout << "non-template min" << std::endl;
        if (a == b)
                return (a);
        if (a < b)
                return (a);
        return (b);
}


int nonTemplateMin(int tab[], int size)
{
        int smallest = 0;

        for (int i = 0; i < size - 1; i++)
                smallest = min(tab[i], tab[i + 1]);
        return (smallest);
}

#endif /* !EX02_HPP_ */
