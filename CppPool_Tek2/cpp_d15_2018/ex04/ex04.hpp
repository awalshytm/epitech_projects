/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef EX04_HPP_
	#define EX04_HPP_

#include <iostream>
#include <string>

template<typename T>
bool equal(T const &a, T const &b)
{
        if (a == b)
                return (true);
        return (false);
}

template<typename T>
class Tester
{
        public:
                Tester();
                bool equal(T const &a, T const &b);
};

#endif /* !EX04_HPP_ */
