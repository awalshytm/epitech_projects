/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "ex04.hpp"

template<typename T>
Tester<T>::Tester()
{}

template bool equal<int>(int const &a, int const &b);
template bool equal<float>(float const &a, float const &b);
template bool equal<double>(double const &a, double const &b);
template bool equal<std::string>(std::string const &a, std::string const &b);

template<typename T>
bool Tester<T>::equal(T const &a, T const &b)
{
        if (a == b)
                return (true);
        return (false);
}

template class Tester<int>;
template class Tester<float>;
template class Tester<double>;
template class Tester<std::string>;