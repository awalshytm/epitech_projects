#include <iostream>
#include <string>

class toto
{
public :
    toto () = default ;
    toto & operator =( const toto &) = delete ;
    toto ( const toto &) = delete ;
    bool operator ==( const toto &) const { return true ; }
    bool operator >( const toto &) const { return false ; }
    bool operator <( const toto &) const { return false ; }
};
