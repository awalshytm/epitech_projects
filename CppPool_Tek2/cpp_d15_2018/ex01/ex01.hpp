/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef EX01_HPP_
	#define EX01_HPP_

#include <cstring>

template<typename T>
int compare(T const &a, T const &b)
{
        if (a == b)
                return (0);
        if (a < b)
                return (-1);
        return (1);
}

#endif /* !EX01_HPP_ */
