/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef PARSER_HPP_
	#define PARSER_HPP_

#include <stack>
#include <string>
#include <iostream>
#include <algorithm>

template<typename T>
void reverse_stack(std::stack<T> &s)
{
        std::stack<T> tmp;

        while (!s.empty()) {
                tmp.push(s.top());
                s.pop();
        }
        s = tmp;
}

class Parser {
	public:
		Parser();
		~Parser();
                int result() const;
                void reset();
                void feed(const std::string &exp);
	private:
                int _res;
                std::stack<char> _operators;
                std::stack<std::string> _operands;
                bool isOperator(char c) const;
                int compute(std::stack<int> &s, std::string const &op);
                void fillStacks(std::string &s);
                void calculate();
                int operation(std::stack<int> &s, std::string &o);
};

#endif /* !PARSER_HPP_ */
