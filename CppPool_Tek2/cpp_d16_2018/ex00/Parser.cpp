/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Parser.hpp"

Parser::Parser()
        :_res(0), _operators(), _operands()
{}

Parser::~Parser()
{
        reset();
}

int Parser::result() const { return _res; }

void Parser::reset()
{
        while (!_operands.empty())
                _operands.pop();
        while (!_operators.empty())
                _operators.pop();
        _res = 0;
}

bool Parser::isOperator(char c) const
{
        if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
        || c == '(')
                return true;
        return false;
}

void Parser::fillStacks(std::string &s)
{
        for (unsigned int i = 0; i < s.length(); i++)
        {
                if (isOperator(s[i]))
                        _operators.push(s[i]);
                else if (s[i] == ')') {
                        while (!_operators.empty()) {
                                char c[2];
                                c[0] = _operators.top();
                                _operators.pop();
                                c[1] = 0;
                                if (c[0] == '(')
                                        break;
                                _operands.push(c);
                        }
                } else {
                        const char* c = s.c_str();
                        int tmp = atoi(&c[i]);
                        _operands.push(std::to_string(tmp));
                        for ( ; i < s.length() && !isOperator(c[i + 1])
                        && c[i + 1] != ')'; i++);
                }
        }
        while (!_operators.empty()) {
                char c[2];
                c[0] = _operators.top();
                c[1] = 0;
                if (c[0] != '(') {
                        _operands.push(c);
                }
                _operators.pop();
        }
}

int Parser::operation(std::stack<int> &s, std::string &o)
{
        int a = s.top();
        s.pop();
        int b = s.top();
        s.pop();

        if (o[0] == '+')
                return (a + b);
        if (o[0] == '-')
                return (a - b);
        if (o[0] == '*')
                return (a * b);
        if (o[0] == '/')
                return (a / b);
        if (o[0] == '%')
                return (a % b);
        return (0);
}

void Parser::calculate()
{
        std::stack<int> number;

        while (!_operands.empty()) {
                std::string tmp = _operands.top();
                _operands.pop();
                if (isOperator(tmp[0]))
                        number.push(operation(number, tmp));
                else
                        number.push(atoi(tmp.c_str()));
        }
        _res += number.top();
}

void Parser::feed(std::string const &expression)
{
        std::string s = *new std::string(expression);
        s.erase(std::remove(s.begin(), s.end(), ' '), s.end());

        fillStacks(s);
        reverse_stack(_operands);
        calculate();
}