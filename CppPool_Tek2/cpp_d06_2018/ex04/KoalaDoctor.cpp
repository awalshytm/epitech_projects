/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** doctor
*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include "SickKoala.hpp"
#include "KoalaNurse.hpp"
#include "KoalaDoctor.hpp"

KoalaDoctor::KoalaDoctor(std::string new_name)
	:_name(new_name), _working(false)
{
	std::cout << "Dr." << _name << ": I'm Dr." <<
        _name << "! How do you kreog ?" << std::endl;
}

void KoalaDoctor::timeCheck(void)
{
	_working = !_working;
	if (_working == true)
		std::cout << "Dr." << _name <<
                ": Time to get to work!" << std::endl;
	if (_working == false)
		std::cout << "Dr." << _name <<
                ": Time to go home to my eucalyptus forest!"
                << std::endl;
}

void KoalaDoctor::diagnose(SickKoala *patient)
{
	std::string path = patient->getName() + ".report";
	std::ofstream file(path);
	int i = 0;
	const char *drugs[5] = {"mars", "Buronzad", "Viagra", "Extasy",
	"Eucalyptus leaf"};

	if (!file.is_open())
		return;
	std::cout << "Dr." << _name << ": So what’s goerking you Mr." <<
        patient->getName() << "?" << std::endl;
	patient->poke();
	i = std::rand() % 5;
	file << drugs[i];
}
