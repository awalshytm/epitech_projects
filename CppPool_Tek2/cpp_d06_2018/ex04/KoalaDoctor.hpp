/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** Doctor header
*/

#ifndef _KOALA_DOC_H_
#define _KOALA_DOC_H_

class KoalaDoctor {
	private:
		std::string _name;
		bool _working;
	public:
		KoalaDoctor(std::string);
		void diagnose(SickKoala *patient);
		void timeCheck(void);
};

#endif
