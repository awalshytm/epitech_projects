/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** nurse header
*/

#ifndef _KOALA_NURSE_H_
#define _KOALA_NURSE_H_

#include "SickKoala.hpp"

class KoalaNurse {
	private:
		int _id;
		bool _working;
	public:
		KoalaNurse(int id);
		~KoalaNurse();
		void giveDrug(std::string, SickKoala *patient);
		std::string readReport(std::string file);
		void timeCheck();

};

#endif
