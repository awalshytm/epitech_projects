/*
** EPITECH PROJECT, 2019
** nurse list header
** File description:
** cpp pool
*/

#ifndef _NURSE_LIST_H_
#define _NURSE_LIST_H_

class KoalaNurseList {
	private:
		KoalaNurse *nurse;
		KoalaNurseList *next;
	public:
		KoalaNurseList(KoalaNurse *node);
		bool isEnd(void);
		void append(KoalaNurseList *node);
		KoalaNurse *getFromId(int id);
		KoalaNurseList *remove(KoalaNurseList *node);
		KoalaNurseList *removeFromId(int id);
		void dump(void);
};

#endif
