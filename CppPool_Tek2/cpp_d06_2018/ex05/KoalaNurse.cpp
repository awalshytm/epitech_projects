/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** nurse
*/

#include <iostream>
#include <fstream>
#include "KoalaNurse.hpp"

KoalaNurse::KoalaNurse(int new_id)
	:_id(new_id), _working(false)
{}

KoalaNurse::~KoalaNurse()
{
        std::cout << "Nurse " << _id << ": Finally some rest!" << std::endl;
}

void KoalaNurse::giveDrug(std::string drug, SickKoala *patient)
{
	patient->takeDrug(drug);
}

std::string KoalaNurse::readReport(std::string path)
{
	std::fstream file(path);
	std::string drug;
	std::string patientName(path);
	std::string delim(".report");

	if (!file.is_open())
		return (drug);
	getline(file, drug);
	if (drug.empty())
		return (drug);
	patientName.erase(patientName.find(delim), delim.length());
	std::cout << "Nurse " << _id << ": Kroeg! Mr." << patientName <<
	" needs a " << drug << "!" << std::endl;
	return (drug);
}

void KoalaNurse::timeCheck()
{
	_working = !_working;
	if (_working == true)
		std::cout << "Nurse " << _id <<
                ": Time to get to work!" << std::endl;
	if (_working == false)
		std::cout << "Nurse " << _id <<
                ": Time to go home to my eucalyptus forest!" << std::endl;
}

int KoalaNurse::getID()
{
	return (_id);
}
