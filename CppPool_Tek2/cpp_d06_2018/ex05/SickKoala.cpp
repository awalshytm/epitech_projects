/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** sick koala
*/

#include <string>
#include <iostream>
#include <algorithm>
#include <cstring>
#include "SickKoala.hpp"

SickKoala::SickKoala(std::string name)
	:_name(name)
{}

SickKoala::~SickKoala()
{
	std::cout << "Mr." << _name << ": Kreooogg!! I'm cuuuured!"
	<< std::endl;
}

void SickKoala::poke()
{
	std::cout << "Mr." << _name << ": Gooeeeeerrk!!" << std::endl;
}

bool SickKoala::takeDrug(std::string drug)
{
	if (strcmp(drug.c_str(), "Buronzand") == 0) {
		std::cout << "Mr." << _name << ": And you'll " << 
		"sleep right away!" << std::endl;
		return (true);
	}
	std::transform(drug.begin(), drug.end(), drug.begin(), ::tolower);
	if (strcmp(drug.c_str(), "mars") == 0) {
		std::cout << "Mr." << _name << ": Mars, and it kreogs!"
		<< std::endl;
		return (true);	
	}
	std::cout << "Mr." << _name << ": Goerkreog!" << std::endl;
	return (false);
}

void SickKoala::overDrive(std::string str)
{
	std::string delimiter = "Kreog";
	size_t pos = -1;

	while ((pos = str.find(delimiter)) != std::string::npos) {
		str.erase(pos, delimiter.length());
		str.insert(pos, "1337");
	}
	std::cout << "Mr." << _name << ": " << str << std::endl;
}

std::string SickKoala::getName()
{
	return (_name);
}
