/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** nurse list
*/

#include <cstring>
#include <iostream>
#include <string>
#include "KoalaNurse.hpp"
#include "KoalaNurseList.hpp"

KoalaNurseList::KoalaNurseList(KoalaNurse *node)
	:nurse(node), next(nullptr)
{}

bool KoalaNurseList::isEnd(void)
{
	if (next == nullptr)
		return (true);
	return (false);
}

void KoalaNurseList::append(KoalaNurseList *node)
{
	KoalaNurseList *last = this;

	while (!last->isEnd())
		last = last->next;
	if (this->getFromId(node->nurse->getID()) == nullptr)
		last->next = node;
}

void KoalaNurseList::dump(void)
{
	KoalaNurseList *tmp = this;

	std::cout << "Nurses: ";
	while (!tmp->isEnd()) {
		std::cout << tmp->nurse->getID() << ", ";
		tmp = tmp->next;
	}
	std::cout << tmp->nurse->getID() << ".\n";
}

KoalaNurseList *KoalaNurseList::remove(KoalaNurseList *node)
{
	return (removeFromId(node->nurse->getID()));
}

KoalaNurseList *KoalaNurseList::removeFromId(int id)
{
	KoalaNurseList *tmp = this;

	if (this->nurse->getID() == id)
		return (this->next);
	while (!tmp->isEnd()) {
		if (tmp->next->nurse->getID() == id) {
			tmp->next = tmp->next->next;
			return (this);
		}
		tmp = tmp->next;
	}
	return (this);
}

KoalaNurse *KoalaNurseList::getFromId(int id)
{
	KoalaNurseList *tmp = this;

	while (!tmp->isEnd()) {
		if (tmp->nurse->getID() == id)
			return (tmp->nurse);
		tmp = tmp->next;
	}
	return (nullptr);
}
