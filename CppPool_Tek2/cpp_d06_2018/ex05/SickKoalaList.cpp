/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** koala list
*/

#include <cstring>
#include <iostream>
#include <string>
#include "SickKoala.hpp"
#include "SickKoalaList.hpp"

SickKoalaList::SickKoalaList(SickKoala *node)
	:patient(node), next(nullptr)
{}

bool SickKoalaList::isEnd(void)
{
	if (next == nullptr)
		return (true);
	return (false);
}

void SickKoalaList::append(SickKoalaList *node)
{
	SickKoalaList *last = this;

	while(!last->isEnd())
		last = last->next;
	if (this->getFromName(node->patient->getName()) == nullptr)
		last->next = node;
}

void SickKoalaList::dump(void)
{
	SickKoalaList *tmp = this;

	std::cout << "Patients: ";
	while (!tmp->isEnd()) {
		std::cout << tmp->patient->getName() << ", ";
		tmp = tmp->next;
	}
	std::cout << tmp->patient->getName() << ".\n";
}

SickKoalaList *SickKoalaList::remove(SickKoalaList *node)
{
	return(removeFromName(node->patient->getName()));
}

SickKoalaList *SickKoalaList::removeFromName(std::string name)
{
	SickKoalaList *tmp = this;

	if (strcmp(this->patient->getName().c_str(), name.c_str()) == 0)
		return (this->next);
	while (!tmp->isEnd()) {
		if (strcmp(tmp->next->patient->getName().c_str(), name.c_str()) == 0) {
			tmp->next = tmp->next->next;
			return (this);
		}
		tmp = tmp->next;
	}
	return (this);
}

SickKoalaList *SickKoalaList::getContent(void)
{
	return (this);
}

SickKoalaList *SickKoalaList::getNext(void)
{
	return (this->next);
}

SickKoala *SickKoalaList::getFromName(std::string name)
{
	SickKoalaList *tmp = this;

	while (!tmp->isEnd()) {
		if (strcmp(tmp->patient->getName().c_str(), name.c_str()) == 0)
			return (tmp->patient);
		tmp = tmp->next;
	}
	return (nullptr);
}
