/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** sick koala
*/

#ifndef _SICK_KOALA_H_
#define _SICK_KOALA_H_

class SickKoala {
	private:
	std::string _name;
	public:
	SickKoala(std::string name);
	~SickKoala();
	void poke();
	bool takeDrug(std::string drug);
	void overDrive(std::string str);
};

#endif