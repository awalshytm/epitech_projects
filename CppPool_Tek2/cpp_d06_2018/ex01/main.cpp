/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** main
*/

#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include "temp.hpp"

float get_nb(std::string *str)
{
	std::string delimiter = " ";
	std::string nb;
	size_t pos = 0;
	float res = 0;

	pos = str->find(delimiter);
	nb = str->substr(0, pos);
	res = atof(nb.c_str());
	str->erase(0, pos + 1);
	return (res);
}

int main(int ac, char **av)
{
	std::string input;
	float nb;

	std::cout << std::setprecision(3) << std::fixed;
	std::getline(std::cin, input);
	nb = get_nb(&input);
	if (std::strcmp(input.c_str(), "Celsius") == 0)
		convert_to_far(nb);
	else if (std::strcmp(input.c_str(), "Fahrenheit") == 0)
		convert_to_cel(nb);
	return (0);
}