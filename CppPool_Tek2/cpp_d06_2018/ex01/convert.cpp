/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** convert temp
*/

#include <iostream>
#include <iomanip>
#include "temp.hpp"

void convert_to_cel(const float far)
{
        float cel = 0;

        cel = (far - 32) * 5 / 9;
        std::cout << std::setw(16) << cel << std::setw(16) << "Celsius"
        << std::endl;
}

void convert_to_far(const float cel)
{
        float far = 0;

        far = cel * 9 / 5 + 32;
        std::cout << std::setw(16) << far << std::setw(16) << "Fahrenheit"
        << std::endl;
}