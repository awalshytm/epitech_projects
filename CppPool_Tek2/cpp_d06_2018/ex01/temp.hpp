/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** convert header
*/

#ifndef _CONVERT_H_
#define _CONVERT_H_

void convert_to_far(const float temp);
void convert_to_cel(const float temp);

#endif