/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** cat
*/

#include <fstream>
#include <iostream>
#include <string>

void cat_file(const char *prog, const char*path)
{
	std::ifstream file(path);
	std::string line;

	if (file.is_open())
	{
		while (getline(file, line)) {
			std::cout << line << std::endl;
		}
		file.close();
	} else {
		std::cerr << prog << ": " << path <<
		": No such file or directory" << std::endl;
	}
}

int main(int ac, char **av)
{
	std::string prog_name;

	if (ac < 2) {
		prog_name.assign(av[0]);
		prog_name.erase(0, 2);
		std::cerr << prog_name << ": Usage: " << av[0]
		<< " file [...]" << std::endl;
	}
        prog_name.assign(av[0]);
        prog_name.erase(0, 2);
	for (int i = 1; i < ac; i++)
            cat_file(prog_name.c_str(), av[i]);
	return (0);
}
