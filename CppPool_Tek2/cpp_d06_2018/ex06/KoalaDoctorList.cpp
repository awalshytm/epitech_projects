/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** doc list
*/

#include <string>
#include <cstring>
#include <iostream>
#include "SickKoala.hpp"
#include "KoalaDoctor.hpp"
#include "KoalaDoctorList.hpp"

KoalaDoctorList::KoalaDoctorList(KoalaDoctor *node)
	:doc(node), next(nullptr)
{}

bool KoalaDoctorList::isEnd(void)
{
	if (next == nullptr)
		return (true);
	return (false);
}

void KoalaDoctorList::append(KoalaDoctorList *node)
{
	KoalaDoctorList *last = this;

	while (!last->isEnd())
		last = last->next;
	if (this->getFromName(node->doc->getName()) == nullptr)
		last->next = node;
}

void KoalaDoctorList::dump(void)
{
	KoalaDoctorList *tmp = this;

	std::cout << "Doctors : ";
	while (!tmp->isEnd()) {
		std::cout << tmp->doc->getName() << ", ";
		tmp = tmp->next;
	}
	std::cout << tmp->doc->getName() << ".\n";
}

KoalaDoctorList *KoalaDoctorList::remove(KoalaDoctorList *node)
{
	return (removeFromName(node->doc->getName()));
}

KoalaDoctorList *KoalaDoctorList::removeFromName(std::string name)
{
	KoalaDoctorList *tmp = this;

	if (strcmp(this->doc->getName().c_str(), name.c_str()) == 0)
		return (this->next);
	while (!tmp->isEnd()) {
		if (strcmp(this->next->doc->getName().c_str(), name.c_str()) == 0) {
			tmp->next = tmp->next->next;
			return (this);
		}
		tmp = tmp->next;
	}
	return (this);
}

KoalaDoctor *KoalaDoctorList::getFromName(std::string name)
{
	KoalaDoctorList *tmp = this;

	while (!tmp->isEnd()) {
		if (strcmp(tmp->doc->getName().c_str(), name.c_str()) == 0)
			return (tmp->doc);
		tmp = tmp->next;
	}
	return (nullptr);
}

KoalaDoctor *KoalaDoctorList::getCurrent()
{
	return (this->doc);
}

KoalaDoctorList *KoalaDoctorList::getNext()
{
	return (this->next);
}

void KoalaDoctorList::clear()
{
	this->next = nullptr;
}