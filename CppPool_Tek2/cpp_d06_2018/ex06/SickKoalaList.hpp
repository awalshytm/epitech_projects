/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** koala list header
*/

#ifndef _SICK_KOALA_LIST_H_
#define _SICK_KOALA_LIST_H_

class SickKoalaList {
	private: 
	SickKoala *patient;
	SickKoalaList *next;
	public:
	SickKoalaList(SickKoala *node);
	bool isEnd(void);
	void append(SickKoalaList *node);
	SickKoala *getFromName(std::string name);
	SickKoalaList *remove(SickKoalaList *node);
	SickKoalaList *removeFromName(std::string name);
	SickKoalaList *getContent(void);
	SickKoalaList *getNext();
	void dump(void);
	SickKoala *getCurrent();
	void clear();
};

#endif