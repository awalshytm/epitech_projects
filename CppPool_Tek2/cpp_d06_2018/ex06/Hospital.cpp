#include <iostream>
#include "SickKoala.hpp"
#include "KoalaNurse.hpp"
#include "KoalaDoctor.hpp"
#include "SickKoalaList.hpp"
#include "KoalaNurseList.hpp"
#include "KoalaDoctorList.hpp"
#include "Hospital.hpp"

Hospital::Hospital(void)
    :docs(nullptr), patients(nullptr), nurses(nullptr)
{}

void Hospital::addDoctor(KoalaDoctorList *node)
{
    if (docs == nullptr) {
        docs = node;
        return;
    }
    docs->append(node);
}

void Hospital::addNurse(KoalaNurseList *node)
{
    if (nurses == nullptr) {
        nurses = node;
        return;
    }
    nurses->append(node);
}

void Hospital::addSick(SickKoalaList *node)
{
    if (patients == nullptr) {
        patients = node;
        return;
    }
    patients->append(node);
}

void Hospital::wakeUpDocs(void)
{
    KoalaDoctorList *tmp = docs;

    while (!tmp->isEnd()) {
        std::cout << "[HOSPITAL] Doctor " << tmp->getCurrent()->getName() << " just arrived!\n";
        tmp->getCurrent()->timeCheck();
        tmp = tmp->getNext();
    }
    std::cout << "[HOSPITAL] Doctor " << tmp->getCurrent()->getName() << " just arrived!\n";
    tmp->getCurrent()->timeCheck();
}

void Hospital::wakeUpNurses()
{
    KoalaNurseList *tmp = nurses;

    while (!tmp->isEnd()) {
        std::cout << "[HOSPITAL] Nurse " << tmp->getCurrent()->getID() << " just arrived!\n";
        tmp->getCurrent()->timeCheck();
        tmp = tmp->getNext();
    }
    std::cout << "[HOSPITAL] Nurse " << tmp->getCurrent()->getID() << " just arrived!\n";
    tmp->getCurrent()->timeCheck();
}

void Hospital::displayArrivedPatients(void)
{
    SickKoalaList *tmp = patients;

    while (!tmp->isEnd()) {
        std::cout << "[HOSPITAL] Patient " << tmp->getCurrent()->getName() << " just arrived!\n";
        tmp = tmp->getNext();
    }
    std::cout << "[HOSPITAL] Patient " << tmp->getCurrent()->getName() << " just arrived!\n";
}

void Hospital::summary(void)
{
    std::cout << "[HOSPITAL] Work starting with:\n";
    docs->dump();
    nurses->dump();
    patients->dump();
    std::cout << "\n";
}

void Hospital::clearLists(void)
{
    KoalaNurseList *tmp = nurses;
    KoalaDoctorList *tmpDoc = docs;

    while (!tmp->isEnd()) {
        tmp->getCurrent()->timeCheck();
        tmp = tmp->getNext();
    }
    tmp->getCurrent()->timeCheck();
    while (!tmpDoc->isEnd()) {
        tmpDoc->getCurrent()->timeCheck();
        tmpDoc = tmpDoc->getNext();
    }
    tmpDoc->getCurrent()->timeCheck();
    patients->clear();
    nurses->clear();
    docs->clear();
}

void Hospital::main_run(void)
{
    SickKoalaList *tmpPatient = patients;
    KoalaDoctorList *tmpDoc = docs;
    KoalaNurseList *tmpNurse = nurses;
    std::string drug;

    while (tmpPatient != nullptr) {
        tmpDoc->getCurrent()->diagnose(tmpPatient->getCurrent());
        drug = tmpNurse->getCurrent()->readReport(tmpPatient->getCurrent()->getName() + ".report");
        std::cout << "Nurse " << tmpNurse->getCurrent()->getID() << ": Kreog! Mr." << tmpPatient->getCurrent()->getName() << " needs a " << drug << "!\n";
        tmpNurse->getCurrent()->giveDrug(drug, tmpPatient->getCurrent());
        tmpDoc = tmpDoc->getNext();
        if (tmpDoc == nullptr)
            tmpDoc = docs;
        tmpNurse = tmpNurse->getNext();
        if (tmpNurse == nullptr)
            tmpNurse = nurses;
        tmpPatient = tmpPatient->getNext();
    }
}

void Hospital::run(void)
{
    wakeUpDocs();
    displayArrivedPatients();
    wakeUpNurses();
    summary();
    main_run();
    clearLists();
}