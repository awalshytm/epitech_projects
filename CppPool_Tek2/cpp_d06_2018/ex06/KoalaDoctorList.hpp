/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** doc list header
*/

#ifndef _DOC_LIST_H_
#define _DOC_LIST_H_

class KoalaDoctorList {
	private:
	KoalaDoctor *doc;
	KoalaDoctorList *next;
	public:
	KoalaDoctorList(KoalaDoctor *node);
	bool isEnd(void);
	void append(KoalaDoctorList *node);
	KoalaDoctor *getFromName(std::string name);
	KoalaDoctorList *remove(KoalaDoctorList *node);
	KoalaDoctorList *removeFromName(std::string name);
	void dump(void);
	KoalaDoctor *getCurrent();
	KoalaDoctorList *getNext();
	void clear();
};

#endif