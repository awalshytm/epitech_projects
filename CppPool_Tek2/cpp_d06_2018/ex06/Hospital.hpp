/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hospital header
*/

#ifndef _HOSPITAL_H_
#define _HOSPITAL_H_

class Hospital {
    private:
        KoalaDoctorList *docs;
        SickKoalaList *patients;
        KoalaNurseList *nurses;
        void wakeUpDocs(void);
        void wakeUpNurses(void);
        void summary(void);
        void displayArrivedPatients(void);
        void clearLists(void);
        void main_run(void);
    public:
        Hospital();
        void addDoctor(KoalaDoctorList *node);
        void addNurse(KoalaNurseList *node);
        void addSick(SickKoalaList *node);
        void run(void);
};

#endif
