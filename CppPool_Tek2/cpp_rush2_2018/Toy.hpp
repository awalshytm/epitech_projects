/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _TOY_H_
#define _TOY_H_

#include <string>
#include "Object.hpp"

class Toy : public Object
{
        private:
        bool _taken;

        public:
        Toy(std::string const &title = "toy",
        ObjectClass oc = ObjectClass::DEF);
        ~Toy();
        virtual void isTaken();
};

#endif