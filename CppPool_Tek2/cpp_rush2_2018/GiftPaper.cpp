/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "GiftPaper.hpp"

GiftPaper::GiftPaper()
        :Wrap()
{}

GiftPaper::~GiftPaper()
{}

bool GiftPaper::wrapMeThat(Object *object)
{
        if (hasObject()) {
                std::cerr << "The Gift Paper already wraps an object..."
                << std::endl;
                return (false);
        }
        _object = object;
        return (true);
}

Object *GiftPaper::openMe()
{
        if (!_isOpen)
                _isOpen = true;
        return (new Object(_object));
}