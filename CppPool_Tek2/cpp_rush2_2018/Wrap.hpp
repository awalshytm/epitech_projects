/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _WRAP_H_
#define _WRAP_H_

#include "Object.hpp"

class Wrap : public Object {
	public:
		Wrap(std::string const &title = "wrap",
		ObjectClass oc = ObjectClass::DEF);
		~Wrap();
};

#endif