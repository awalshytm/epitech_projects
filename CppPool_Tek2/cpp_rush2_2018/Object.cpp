/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Object.hpp"

Object::Object(std::string const &title, ObjectType ot, ObjectClass oc)
        :_title(title), _objectType(ot), _objectClass(oc), _isOpen(true),
        _object(nullptr)
{}

Object::Object(Object const *obj)
        :_title(obj->getTitle()), _objectType(obj->getObjectType()),
        _objectClass(obj->getObjectClass())
{}

Object::~Object()
{
        if (_object)
                delete _object;
}

//getters
std::string Object::getTitle() const { return _title; }

ObjectClass Object::getObjectClass() const { return _objectClass; }

ObjectType Object::getObjectType() const { return _objectType; }

Object *Object::openMe()
{
        Object *tmp = nullptr;

        _isOpen = true;
        if (!_object)
                return (nullptr);
        tmp = new Object(_object);
        delete _object;
        return (tmp);
}

bool Object::isOpen() const { return _isOpen; }

void Object::closeMe()
{
        _isOpen = false;
}

bool Object::wrapMeThat(Object *o)
{
        if (_object) {
                std::cerr << "There is already something..." << std::endl;
                return (false);
        }
        if (!_isOpen) {
                std::cerr << "Is closed..." << std::endl;
                return (false);
        }
        _object = o;
        return (true);
}

void Object::isTaken()
{
        std::cout << "Is taken !" << std::endl;
}

bool Object::hasObject() const { return (_object == nullptr ? false : true); }

//operator
std::ostream &operator<<(std::ostream &s, Object *o)
{
        s << "Object : " << o->getTitle();
        return (s);
}