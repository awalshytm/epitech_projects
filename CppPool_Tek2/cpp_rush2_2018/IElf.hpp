/*
** EPITECH PROJECT, 2019
** cpp_rush2
** File description:
** Elf Interface
*/

#ifndef ELF_HPP_
	#define ELF_HPP_

class PapaXmasElf {
	public:
		PapaXmasElf();
		~PapaXmasElf();
	private:
		Elf *cp;
};

class IElf {
	public:
		IElf();
		~IElf();
		ITable *createtable() = 0;
		IConvoyerBelt *createConveyorBelt() = 0;
		virtal bool Wrap::Object::wrapMeThat(Object *object) = 0;
		std::string PapaXmasTable::In();
		std::string PapaXmasTable::Out();

	private:
};

#endif /* !ELF_HPP_ */
