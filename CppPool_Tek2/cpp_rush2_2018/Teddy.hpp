/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _TEDDY_H_
#define _TEDDY_H_

#include <iostream>
#include "Toy.hpp"

class Teddy : public Toy
{
	public:
		Teddy(std::string const &title = "teddy");
		void isTaken() override;
};

#endif