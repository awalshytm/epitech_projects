/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _LITTLE_PONY_H_
#define _LITTLE_PONY_H_

#include <iostream>
#include "Toy.hpp"

class LittlePony : public Toy
{
    public:
        LittlePony(std::string const &title = "happy pony");
        void isTaken() override;
};

#endif