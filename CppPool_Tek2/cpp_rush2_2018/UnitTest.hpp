/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** unit test header
*/

#ifndef _UNIT_TEST_H_
#define _UNIT_TEST_H_

Object **MyUnitTests();
Object *MyUnitTests(Object **obj);

#endif