/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _GIFT_PAPER_H
#define _GIFT_PAPER_H_

#include <iostream>
#include "Wrap.hpp"

class GiftPaper : public Wrap
{
        public:
                GiftPaper();
                ~GiftPaper();
                bool wrapMeThat(Object *object) override;
                Object *openMe() override;
};

#endif