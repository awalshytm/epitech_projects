
#include "GiftPaper.hpp"
#include "Box.hpp"
#include "LittlePony.hpp"
#include "Teddy.hpp"
#include "UnitTest.hpp"

std::string cbts(bool a)
{
        if (a)
                return ("true");
        return ("false");
}

int main()
{
        Teddy *teddy = new Teddy("cuddle");
        LittlePony *lp = new LittlePony("happy pony");
        Box *box = new Box();
        GiftPaper *gift = new GiftPaper();

        std::cout << "Teddy class : " << teddy << std::endl;
        std::cout << "Little class : " << lp << std::endl;

        teddy->isTaken();

        if (box->wrapMeThat(teddy)) {
                std::cout << "Oups forgot to open box..." << std::endl;
                box->openMe();
                if (!box->isOpen())
                        abort();
                std::cout << "Opened!" << std::endl;
                box->wrapMeThat(teddy);
        }
        std::cout << "Box opened ? " << cbts(box->isOpen()) << std::endl;
        std::cout << "Wrap the box" << std::endl;

        box->isTaken();

        gift->wrapMeThat(box);
        std::cout << "Gift opened ? " << cbts(gift->isOpen()) << std::endl;
        std::cout << "Wrapped successfully" << std::endl;

        std::cout << "Open gift:" << std::endl;
        Box *tmp = (Box *)gift->openMe();
        std::cout << tmp->getTitle() << std::endl;
        Object *tmp2 = tmp->openMe();
        std::cout << tmp2->getTitle() << std::endl;
        std::cout << "unbox successfull" << std::endl;
        return 0;
}
