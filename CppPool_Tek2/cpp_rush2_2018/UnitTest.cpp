/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** unit test
*/

#include <vector>
#include "Object.hpp"
#include "Teddy.hpp"
#include "LittlePony.hpp"
#include "Box.hpp"
#include "GiftPaper.hpp"

Object **MyUnitTests()
{
        Object **res = new Object*[2];

        res[0] = (Object *)(new Teddy("cuddle"));
        res[1] = (Object *)(new LittlePony("happy pony"));

        return (res);
}

Object *MyUnitTests(Object **obj)
{
        Teddy *teddy = nullptr;
        Box *box = nullptr;
        GiftPaper *giftpaper = nullptr;

        if (!obj)
                return (nullptr);
        if (obj[0] && obj[0]->getObjectClass() == ObjectClass::TEDDY)
                teddy = (Teddy *)obj[0];
        if (obj[1] && obj[1]->getObjectClass() == ObjectClass::BOX)
                box = (Box *)obj[1];
        if (obj[2] && obj[2]->getObjectClass() == ObjectClass::GIFT_PAPER)
                giftpaper = (GiftPaper *)obj[2];
        
        box->wrapMeThat(teddy);
        giftpaper->wrapMeThat(box);
        return (giftpaper);
}