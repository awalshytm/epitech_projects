/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _BOX_H_
#define _BOX_H_

#include <iostream>
#include "Wrap.hpp"

class Box : public Wrap
{
        public:
        Box();
        bool wrapMeThat(Object *object) override;
        Object *openMe() override;
};

#endif