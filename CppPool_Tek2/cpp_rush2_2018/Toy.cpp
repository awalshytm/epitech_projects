/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Toy.hpp"

Toy::Toy(std::string const &title, ObjectClass oc)
        :Object(title, ObjectType::TOY, oc), _taken(false)
{}

Toy::~Toy()
{}

void Toy::isTaken()
{
        std::cout << "Toy taken!" << std::endl;
}