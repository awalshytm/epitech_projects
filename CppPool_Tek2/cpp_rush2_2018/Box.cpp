/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Box.hpp"

Box::Box()
        :Wrap()
{}

bool Box::wrapMeThat(Object *object)
{
        if (hasObject()) {
                std::cerr << "The Box already contains an object..."
                << std::endl;
                return (false);
        }
        if (!_isOpen) {
                std::cerr << "The Box is closed, you can't put things in it\n"
                << std::endl;
                return (false);
        }
        _object = object;
        this->closeMe();
        return (true);
}

Object *Box::openMe()
{
        if (!_isOpen)
                _isOpen = true;
        return (new Object(_object));
}