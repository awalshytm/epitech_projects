/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <iostream>
#include <string>

class Toy;
class Box;
class GiftPaper;
class Wrap;
class LittlePony;

enum ObjectType
{
        DEFAULT,
        TOY,
        WRAP
};

enum ObjectClass
{
        DEF,
        LITTLE_PONEY,
        TEDDY,
        BOX,
        GIFT_PAPER
};

class Object {
        protected:
                std::string const _title;
                ObjectType _objectType;
                ObjectClass _objectClass;
                bool _isOpen;
                Object *_object;
                
        public:
                //Ctor Dtor
                Object(std::string const &title = "object",
                ObjectType ot = ObjectType::DEFAULT,
                ObjectClass oc = ObjectClass::DEF);
                Object(Object const *obj);
                virtual ~Object();
                //getters
                std::string getTitle() const;
                ObjectClass getObjectClass() const;
                ObjectType getObjectType() const;
                virtual Object *openMe();
                virtual bool isOpen() const;
                virtual void closeMe();
                virtual bool wrapMeThat(Object *o);
                virtual void isTaken();
                virtual bool hasObject() const;
};

std::ostream &operator<<(std::ostream &s, Object *o);

#endif