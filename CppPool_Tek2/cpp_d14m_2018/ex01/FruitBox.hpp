/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef FRUITBOX_HPP_
	#define FRUITBOX_HPP_

#include <list>
#include <algorithm>
#include "Fruit.hpp"
#include "FruitNode.hpp"

class FruitBox {
	public:
		FruitBox(int size);
		int nbFruits() const;
		bool putFruit(Fruit *f);
		Fruit *pickFruit();
		FruitNode *head();
	private:
		unsigned int _size;
		std::list<FruitNode *> _fruits;
};

#endif /* !FRUITBOX_HPP_ */