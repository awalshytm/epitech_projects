/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include <iostream>
#include "FruitBox.hpp"

FruitBox::FruitBox(int size)
        :_size(size), _fruits()
{
        _fruits.clear();
}

int FruitBox::nbFruits() const { return _fruits.size(); }

bool FruitBox::putFruit(Fruit *f)
{
        FruitNode *tmp = new FruitNode(f);
        
        if ((_fruits.size()) + 1 > _size)
                return (false);
        if (_fruits.size() == 0) {
                _fruits.push_back(tmp);
                return (true);
        }
        if (std::find(_fruits.begin(), _fruits.end(), tmp) != _fruits.end())
                return (false);
        
        _fruits.push_back(tmp);
        return (true);
}

FruitNode *FruitBox::head()
{
        if (_fruits.size())
                return (nullptr);
        return _fruits.front();
}

Fruit *FruitBox::pickFruit()
{
        FruitNode *tmp = _fruits.front();

        _fruits.pop_front();
        return (tmp->getFruit());
}
