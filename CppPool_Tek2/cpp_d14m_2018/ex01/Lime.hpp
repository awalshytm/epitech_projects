/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef LIME_HPP_
	#define LIME_HPP_

#include "Lemon.hpp"

class Lime : public Lemon
{
	public:
		Lime();
};

#endif /* !LIME_HPP_ */
