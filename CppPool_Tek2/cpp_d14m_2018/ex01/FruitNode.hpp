/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef FRUITNODE_HPP_
	#define FRUITNODE_HPP_

#include "Fruit.hpp"

class FruitNode {
	public:
		FruitNode(Fruit *f);
		~FruitNode();
                FruitNode *getNext() const;
                Fruit *getFruit() const;
	private:
                Fruit *_fruit;
                FruitNode *_next;
};

bool operator==(FruitNode &lf, FruitNode &rf);

#endif /* !FRUITNODE_HPP_ */
