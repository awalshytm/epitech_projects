/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef BANANA_HPP_
	#define BANANA_HPP_

#include "Fruit.hpp"

class Banana : public Fruit
{
	public:
		Banana();
};

#endif /* !BANANA_HPP_ */
