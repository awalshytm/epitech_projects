/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "Fruit.hpp"

Fruit::Fruit(int vitamins, std::string const &name)
        :_vitamins(vitamins), _name(name)
{}

Fruit::~Fruit()
{}

std::string Fruit::getName() const { return _name; }

int Fruit::getVitamins() const { return _vitamins; }