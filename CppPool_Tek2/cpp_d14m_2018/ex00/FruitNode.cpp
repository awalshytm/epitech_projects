/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** file
*/

#include "FruitNode.hpp"

FruitNode::FruitNode(Fruit *f)
        :_fruit(f), _next(nullptr)
{}

FruitNode::~FruitNode()
{
        if (_next)
                delete _next;
        if (_fruit)
                delete _fruit;
}

FruitNode *FruitNode::getNext() const { return _next; }

Fruit *FruitNode::getFruit() const { return _fruit; }

bool operator==(FruitNode &lf, FruitNode &rf)
{
        return (lf.getFruit()->getName() == rf.getFruit()->getName() &&
        lf.getFruit()->getVitamins() == rf.getFruit()->getVitamins());
}