/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef FRUIT_HPP_
	#define FRUIT_HPP_

#include <string>

class Fruit {
	public:
		Fruit(int vitamins, std::string const &name);
		~Fruit();
                std::string getName() const;
                int getVitamins() const;
	protected:
                int _vitamins;
                std::string _name;
};

#endif /* !FRUIT_HPP_ */