/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef LEMON_HPP_
	#define LEMON_HPP_

#include "Fruit.hpp"

class Lemon : public Fruit
{
	public:
		Lemon();
};

#endif /* !LEMON_HPP_ */
