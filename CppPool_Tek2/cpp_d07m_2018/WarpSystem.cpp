/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07m
*/

#include <iostream>
#include <string>
#include "WarpSystem.hpp"

WarpSystem::QuantumReactor::QuantumReactor()
    :_stability(true)
{}

bool WarpSystem::QuantumReactor::isStable()
{
    return (_stability);
}

void WarpSystem::QuantumReactor::setStability(bool stability)
{
    _stability = stability;
}

WarpSystem::Core::Core(WarpSystem::QuantumReactor *coreReactor)
    :_coreReactor(coreReactor)
{}

WarpSystem::QuantumReactor *WarpSystem::Core::checkReactor()
{
    return (_coreReactor);
}
