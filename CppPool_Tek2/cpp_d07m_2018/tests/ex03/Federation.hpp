/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07
*/

#ifndef _FEDERATION_H_
#define _FEDERATION_H_

#include <string>
#include <iostream>
#include "WarpSystem.hpp"
#include "Destination.hpp"
#include "Borg.hpp"

namespace Borg {
    class Ship;
};

namespace Federation {
	namespace Starfleet {
		class Ensign {
			private:
				std::string _name;
			public:
				Ensign(std::string name);
		};

		class Captain {
			private:
				std::string _name;
				int _age;
			public:
				Captain(std::string name);
				std::string getName();
				int getAge();
				void setAge(int age);
		};

		class Ship {
			private:
				int _width;
				int _length;
				std::string _name;
				short _maxWarp;
				WarpSystem::Core *_core;
				Captain *_captain;
				Destination _home;
				Destination _location;
				int _shield;
				int _photonTorpedo;
			public:
				Ship(int length, int width,
				const std::string name, short _maxWarp,
                                int torpedo = 0);
				Ship();
				void setupCore(WarpSystem::Core *core);
				void checkCore(void);
				void promote(Captain *captain);
				bool move(int warp, Destination d);
        	        	bool move(int warp);
                		bool move(Destination d);
                		bool move();
				int getShield();
				void setShield(int shield);
				int getTorpedo();
				void setTorpedo(int torpedo);
				void fire(Borg::Ship *target);
				void fire(int torpedo, Borg::Ship *target);
		};
	};

	class Ship {
		private:
			int _width;
			int _length;
			std::string _name;
			short _maxWarp;
			WarpSystem::Core *_core;
			Destination _home;
			Destination _location;
			int _shield;
			int _photonTorpedo;
		public:
			Ship(int length, int width, std::string name);
			void setupCore(WarpSystem::Core *core);
			void checkCore(void);
			bool move(int warp, Destination d);
		        bool move(int warp);
           		bool move(Destination d);
			bool move();
			WarpSystem::Core *getCore();
	};
 };

#endif
