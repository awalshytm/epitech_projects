/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07
*/

#include <string>
#include <iostream>
#include "Destination.hpp"
#include "WarpSystem.hpp"
#include "Borg.hpp"
#include "Federation.hpp"

//STARTFLEET SHIP
Federation::Starfleet::Ship::Ship(int length, int width, std::string name,
short maxWarp, int torpedo)
	:_width(width), _length(length), _name(name), _maxWarp(maxWarp),
	_core(nullptr), _home(EARTH), _location(_home), _shield(100),
    _photonTorpedo(torpedo)
{
	std::cout << "The ship USS " << _name << " has been finished."
	<< std::endl << "It is " <<
	_length << " m in length and " << _width << " m in width." << std::endl
	<< "It can go to Warp " << _maxWarp << "!" << std::endl;
    if (_photonTorpedo > 0)
        std::cout << "Weapons are set: " << _photonTorpedo <<
        " torpedoes ready." << std::endl;
}

Federation::Starfleet::Ship::Ship()
	:_width(123), _length(289), _name("Enterprise"), _maxWarp(6),
	_core(nullptr), _home(EARTH), _location(_home), _shield(100),
    _photonTorpedo(20)
{
	std::cout << "The ship USS " << _name << " has been finished."
	<< std::endl << "It is " <<
	_length << " m in length and " << _width << " m in width." << std::endl
	<< "It can go to Warp " << _maxWarp << "!" << std::endl;
}

void Federation::Starfleet::Ship::setupCore(WarpSystem::Core *core)
{
	_core = core;
	std::cout << "USS " << _name << ": the core is set." << std::endl;
}

void Federation::Starfleet::Ship::checkCore()
{
	bool stabilityB = _core->checkReactor()->isStable();
	std::string stabilityS;

	if (stabilityB)
		stabilityS.assign("stable");
	else
		stabilityS.assign("unstable");
	std::cout << "USS " << _name << ": The core is " << stabilityS <<
	" at the time." << std::endl;
}

void Federation::Starfleet::Ship::promote(Federation::Starfleet::Captain *cap)
{
	_captain = cap;
	std::cout << _captain->getName() << ": I'm glad to be the captain of"
	<< " the USS " << _name << "." << std::endl;
}

bool Federation::Starfleet::Ship::move(int warp, Destination d)
{
    if (warp > _maxWarp || d == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Federation::Starfleet::Ship::move(int warp)
{
    if (warp > _maxWarp || _home == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

bool Federation::Starfleet::Ship::move(Destination d)
{
    if (d == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Federation::Starfleet::Ship::move()
{
    if (_home == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

int Federation::Starfleet::Ship::getShield()
{
    return (_shield);
}

void Federation::Starfleet::Ship::setShield(int shield)
{
    _shield = shield;
}

int Federation::Starfleet::Ship::getTorpedo()
{
    return (_photonTorpedo);
}

void Federation::Starfleet::Ship::setTorpedo(int torpedo)
{
    _photonTorpedo = torpedo;
}

void Federation::Starfleet::Ship::fire(Borg::Ship *target)
{
    _photonTorpedo -= 1;
    target->setShield(target->getShield() - 50);
    std::cout << _name << ": Firing on target. " << _photonTorpedo
    << " torpedoes remaining.\n";
    if (_photonTorpedo == 0)
        std::cout << _name << ": No more torpedo to fire, " <<
        _captain->getName() << "!\n";
}

void Federation::Starfleet::Ship::fire(int torpedoNb, Borg::Ship *target)
{
    if (torpedoNb > _photonTorpedo) {
        std::cout << _name << ": No enough torpedoes to fire, " <<
        _captain->getName() << "!\n";
        return;
    }
    _photonTorpedo -= torpedoNb;
    target->setShield(target->getShield() - (50 * torpedoNb));
    std::cout << _name << ": Firing on target. " << _photonTorpedo
    << " torpedoes remaining.\n";
}

//SHIP
Federation::Ship::Ship(int length, int width, std::string name)
	:_width(width), _length(length), _name(name), _maxWarp(1),
	_core(nullptr), _home(VULCAN), _location(_home)
{
	std::cout << "The independant ship " << name <<
	" just finished its construction." << std::endl <<
	"It is " << length <<
	" m in length and " << width << " m in width." << std::endl;
}

void Federation::Ship::setupCore(WarpSystem::Core *core)
{
	_core = core;
	std::cout << _name << ": the core is set." << std::endl;
}

void Federation::Ship::checkCore()
{
	bool stabilityB = _core->checkReactor()->isStable();
	std::string stabilityS;

	if (stabilityB)
		stabilityS.assign("stable");
	else
		stabilityS.assign("unstable");
	std::cout <<_name << ": The core is " << stabilityS << " at the time."
	<< std::endl;
}

bool Federation::Ship::move(int warp, Destination d)
{
    if (warp > _maxWarp || d == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Federation::Ship::move(int warp)
{
    if (warp > _maxWarp || _home == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

bool Federation::Ship::move(Destination d)
{
    if (d == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Federation::Ship::move()
{
    if (_home == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

WarpSystem::Core *Federation::Ship::getCore()
{
    return (_core);
}

//CAPTAIN
Federation::Starfleet::Captain::Captain(std::string name)
    :_name(name), _age(0)
{}

int Federation::Starfleet::Captain::getAge()
{
    return (_age);
}

std::string Federation::Starfleet::Captain::getName()
{
    return (_name);
}

void Federation::Starfleet::Captain::setAge(int age)
{
    _age = age;
}

//ENSIGN
Federation::Starfleet::Ensign::Ensign(std::string name)
	:_name(name)
{
	std::cout << "Ensign " << _name << ", awaiting for orders."
	<< std::endl;
}
