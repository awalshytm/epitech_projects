/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** borg queen
*/

#include <string>
#include <iostream>
#include "Destination.hpp"
#include "Borg.hpp"
#include "Federation.hpp"
#include "BorgQueen.hpp"

Borg::BorgQueen::BorgQueen()
    :movePtr(nullptr), firePtr(nullptr), destroyPtr(nullptr)
{}

void Borg::BorgQueen::fire(Borg::Ship *ship, Federation::Starfleet::Ship *target)
{
    firePtr = &ship->fire;
    (*firePtr)(target);
}

void Borg::BorgQueen::destroy(Borg::Ship *ship, Federation::Ship *target)
{
    destroyPtr = &ship->fire;
    (*destroyPtr)(target);
}

bool Borg::BorgQueen::move(Borg::Ship *ship, Destination dest)
{
    movePtr = &ship->move;
    return ((*movePtr)(dest));
}
