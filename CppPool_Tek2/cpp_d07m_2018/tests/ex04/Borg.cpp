/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07m
*/

#include <string>
#include <iostream>
#include "Destination.hpp"
#include "WarpSystem.hpp"
#include "Borg.hpp"
#include "Federation.hpp"

Borg::Ship::Ship(int weaponF)
    :_side(300), _maxWarp(9), _home(UNICOMPLEX), _location(_home),
    _shield(100), _weaponFrequency(weaponF),  _repair(3)
{
    std::cout << "We are the Borgs. Lower your shields and " << 
    "surrender yourselves unconditionally.\nYour biological characteristics" << 
    " and technologies will be assimilated.\nResistance is futile.\n";
}

Borg::Ship::Ship(int weaponF, short repair)
    :_side(300), _maxWarp(9), _home(UNICOMPLEX), _location(_home),
    _shield(100), _weaponFrequency(weaponF),  _repair(repair)
{
    std::cout << "We are the Borgs. Lower your shields and " << 
    "surrender yourselves unconditionally.\nYour biological characteristics" << 
    " and technologies will be assimilated.\nResistance is futile.\n";
}

void Borg::Ship::setupCore(WarpSystem::Core *core)
{
    _core = core;
}

void Borg::Ship::checkCore()
{
    bool stability = _core->checkReactor()->isStable();

    if (stability)
        std::cout << "Everything is in order.\n";
    else
        std::cout << "Critical failure imminent.\n";
}

bool Borg::Ship::move(int warp, Destination d)
{
    if (warp > _maxWarp || d == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Borg::Ship::move(int warp)
{
    if (warp > _maxWarp || _home == _location ||
    _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

bool Borg::Ship::move(Destination d)
{
    if (d == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = d;
    return (true);
}

bool Borg::Ship::move()
{
    if (_home == _location || _core->checkReactor()->isStable() == false)
        return (false);
    _location = _home;
    return (true);
}

int Borg::Ship::getShield()
{
    return (_shield);
}

void Borg::Ship::setShield(int shield)
{
    _shield = shield;
}

short Borg::Ship::getRepair()
{
    return (_repair);
}

void Borg::Ship::setRepair(short repair)
{
    _repair = repair;
}

int Borg::Ship::getWeaponFrequency()
{
    return (_weaponFrequency);
}

void Borg::Ship::setWeaponFrequency(int frequency)
{
    _weaponFrequency = frequency;
}

void Borg::Ship::fire(Federation::Starfleet::Ship *target)
{
    target->setShield(target->getShield() - _weaponFrequency);
    std::cout << "Firing on target with " << _weaponFrequency <<
    "GW frequency.\n";
}

void Borg::Ship::fire(Federation::Ship *target)
{
    target->getCore()->checkReactor()->setStability(false);
    std::cout << "Firing on target with " << _weaponFrequency <<
    "GW frequency.\n";
}

void Borg::Ship::repair()
{
    if (_repair > 0) {
        _shield = 100;
        _repair -= 1;
        std::cout << "Begin shield re-initialisation... Done." <<
        " Awaiting further instructions.\n";
        return;
    }
    std::cout << "Energy cells depleted, shield weakening.\n";
}