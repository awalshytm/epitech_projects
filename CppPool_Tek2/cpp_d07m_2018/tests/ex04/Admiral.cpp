/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** admiral
*/

#include <string>
#include <iostream>
#include "Destination.hpp"
#include "Borg.hpp"
#include "Federation.hpp"
#include "Admiral.hpp"

Federation::Starfleet::Admiral::Admiral(std::string name)
	:_name(name), movePtr(nullptr), firePtr(nullptr)
{
	std::cout << "Admiral " << _name << " ready for action.\n";
}

void Federation::Starfleet::Admiral::fire(Federation::Starfleet::Ship *ship, Borg::Ship *target)
{
	std::cout << "On order from Admiral " << _name << ":\n";
	firePtr = &ship->fire;
	(*firePtr)(target);
}

bool Federation::Starfleet::Admiral::move(Federation::Starfleet::Ship *ship, Destination dest)
{
	movePtr = &ship->move;
	return ((*movePtr)(dest));
}
