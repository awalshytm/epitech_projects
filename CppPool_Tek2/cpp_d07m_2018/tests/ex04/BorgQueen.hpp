/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _BORGQUEEN_H_
#define _BORGQUEEN_H_

namespace Borg {
    class BorgQueen {
        private:
        public:
            BorgQueen();
            bool (*movePtr)(Destination dest);
            void (*firePtr)(Federation::Starfleet::Ship *target);
            void (*destroyPtr)(Federation::Ship *target);
            bool move(Borg::Ship *ship, Destination dest);
            void fire(Borg::Ship *ship, Federation::Starfleet::Ship *target);
            void destroy(Borg::Ship *ship, Federation::Ship *target);
    }
};

#endif
