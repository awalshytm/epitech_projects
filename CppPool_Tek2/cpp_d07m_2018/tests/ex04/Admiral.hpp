/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** hedaer
*/

#ifndef _ADMIRAL_H_
#define _ADMIRAL_H_


namespace Federation
{	
	namespace Starfleet {
		class Admiral {
			private:
				std::string _name;
				bool (*movePtr)(Destination);
				void (*firePtr)(Borg::Ship *);
			public:
				Admiral(std::string name);
				void fire(Federation::Starfleet::Ship *ship,
				Borg::Ship *target);
				bool move(Federation::Starfleet::Ship *ship,
				Destination d);
		};
	};
};


#endif