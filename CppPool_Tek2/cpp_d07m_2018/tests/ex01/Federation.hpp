/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07
*/

#ifndef _FEDERATION_H_
#define _FEDERATION_H_

namespace Federation {
	namespace Starfleet {
		class Ensign {
			private:
				std::string _name;
			public:
				Ensign(std::string name);
		};

		class Captain {
			private:
				std::string _name;
				int _age;
			public:
				Captain(std::string name);
				std::string getName();
				int getAge();
				void setAge(int age);
		};

		class Ship {
			private:
				int _width;
				int _length;
				std::string _name;
				short _maxWarp;
				WarpSystem::Core *_core;
				Captain *_captain;
			public:
				Ship(int length, int width, std::string name,
				short _maxWarp);
				void setupCore(WarpSystem::Core *core);
				void checkCore(void);
				void promote(Captain *captain);
		};
	};

	class Ship {
		private:
			int _width;
			int _length;
			std::string _name;
			short _maxWarp;
			WarpSystem::Core *_core;
		public:
			Ship(int length, int width, std::string name);
			void setupCore(WarpSystem::Core *core);
			void checkCore(void);
	};
 };

#endif