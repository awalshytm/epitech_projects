/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _BORG_H_
#define _BORG_H_

namespace Borg {
	class Ship {
		private:
			int _side;
			short _maxWrap;
			WarpSystem::Core *_core;
		public:
			Ship();
			void setupCore(WarpSystem::Core *core);
			void checkCore(void);
	};
};

#endif