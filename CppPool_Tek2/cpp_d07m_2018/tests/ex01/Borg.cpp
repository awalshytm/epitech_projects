/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07m
*/

#include <string>
#include <iostream>
#include "WarpSystem.hpp"
#include "Borg.hpp"

Borg::Ship::Ship()
    :_side(300), _maxWrap(9)
{
    std::cout << "We are the Borgs. Lower your shields and " << 
    "surrender yourselves unconditionally." << std::endl << 
    "Your biological characteristics and technologies will be" << 
    " assimilated." << std::endl << " Resistance is futile." << std::endl;
}

void Borg::Ship::setupCore(WarpSystem::Core *core)
{
    _core = core;
}

void Borg::Ship::checkCore()
{
    bool stability = _core->checkReactor()->isStable();

    if (stability)
        std::cout << "Everything is in order." << std::endl;
    else
        std::cout << "Critical failure imminent." << std::endl;
}
