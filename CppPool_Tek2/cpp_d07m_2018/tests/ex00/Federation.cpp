/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d07
*/

#include <string>
#include <iostream>
#include "WarpSystem.hpp"
#include "Federation.hpp"

Federation::Starfleet::Ship::Ship(int length, int width, std::string name,
short maxWarp)
	:_width(width), _length(length), _name(name), _maxWarp(maxWarp),
	_core(nullptr)
{
	std::cout << "The ship USS " << name << " has been finished."
	<< std::endl << "It is " <<
	length << " m in length and " << width << " m in width." << std::endl
	<< "It can go to Warp " << _maxWarp << "!" << std::endl;
}

void Federation::Starfleet::Ship::setupCore(WarpSystem::Core *core)
{
	_core = core;
	std::cout << "USS " << _name << ": the core is set." << std::endl;
}

void Federation::Starfleet::Ship::checkCore()
{
	bool stabilityB = _core->checkReactor()->isStable();
	std::string stabilityS;

	if (stabilityB)
		stabilityS.assign("stable");
	else
		stabilityS.assign("unstable");
	std::cout << "USS " << _name << ": The core is " << stabilityS <<
	" at the time." << std::endl;
}

Federation::Ship::Ship(int length, int width, std::string name)
	:_width(width), _length(length), _name(name), _maxWarp(1),
	_core(nullptr)
{
	std::cout << "The independant ship " << name <<
	" just finished its construction." << std::endl <<
	"It is " << length <<
	" m in length and " << width << " m in width." << std::endl;
}

void Federation::Ship::setupCore(WarpSystem::Core *core)
{
	_core = core;
	std::cout << _name << ": the core is set." << std::endl;
}

void Federation::Ship::checkCore()
{
	bool stabilityB = _core->checkReactor()->isStable();
	std::string stabilityS;

	if (stabilityB)
		stabilityS.assign("stable");
	else
		stabilityS.assign("unstable");
	std::cout <<_name << ": The core is " << stabilityS << " at the time."
	<< std::endl;
}
