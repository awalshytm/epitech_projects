/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _WARPSYSTEM_H_
#define _WARPSYSTEM_H_

namespace WarpSystem {
	class QuantumReactor {
		private: 
			bool _stability;
		public:
			QuantumReactor();
			bool isStable();
			void setStability(bool stability);
	};

	class Core {
		private:
			QuantumReactor *_coreReactor;
		public: 
			Core(QuantumReactor *coreReactor);
			QuantumReactor *checkReactor();
	};
};

#endif
