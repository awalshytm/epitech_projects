/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** header
*/

#ifndef _DROID_MEM_H_
#define _DROID_MEM_H_

#include <iostream>
#include <cstdlib>

class DroidMemory {
        private:
                size_t Exp;
                size_t FingerPrint;
        public:
                DroidMemory();
                size_t getExp() const { return Exp; }
                size_t getFingerPrint() const { return FingerPrint; }
                void setExp(size_t experience) { Exp = experience; }
                void setFingerPrint(size_t fp) { FingerPrint = fp; }
};

DroidMemory &operator<<(DroidMemory &ld, DroidMemory &rd);
DroidMemory &operator>>(DroidMemory &ld, DroidMemory &rd);
DroidMemory &operator+=(DroidMemory &ld, DroidMemory &rd);
DroidMemory &operator+=(DroidMemory &ld, size_t rd);
DroidMemory &operator+(DroidMemory &ld, DroidMemory &rd);

bool operator==(DroidMemory &ld, DroidMemory &rd);
bool operator!=(DroidMemory &ld, DroidMemory &rd);

std::ostream &operator<<(std::ostream &s, DroidMemory &d);

#endif