/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** droid
*/

#ifndef _DROID_H_
#define _DROID_H_

#include "DroidMemory.hpp"

class Droid {
        private:
                std::string ID;
                size_t Energy;
                const size_t Attack;
                const size_t Toughness;
                std::string *Status;
                DroidMemory *BattleData;
        public:
                Droid(std::string const &serial);
                Droid(Droid const &droid);
                ~Droid();
                std::string const &getId() const { return ID; }
                size_t getEnergy() const { return Energy; }
                size_t getAttak() const { return Attack; }
                size_t getToughness() const { return Toughness; }
                std::string *getStatus() const { return Status; }
                void setId(std::string id) { ID = id; }
                void setEnergy(size_t const energy);
                void setStatus(std::string const &status);
                void setStatus(std::string *status);
                bool operator==(Droid &d);
                bool operator!=(Droid &d);
                Droid &operator=(Droid &d);
                bool operator()(std::string *task, size_t expReq);
};

Droid &operator<<(Droid &d, Droid &rd);
Droid &operator<<(Droid &d, size_t &energy);
std::ostream &operator<<(std::ostream &s, const Droid &d);

#endif