/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** droid
*/

#ifndef _DROID_H_
#define _DROID_H_

#include <string>
#include <iostream>

class Droid {
        private:
                std::string ID;
                size_t Energy;
                size_t const Attack;
                size_t const Toughness;
                std::string *Status;
        
        public:
                Droid(std::string const &serial = "");
                Droid(Droid const &droid);
                ~Droid();
                std::string const &getId() const;
                size_t getEnergy() const;
                size_t getAttack() const;
                size_t getToughness() const;
                std::string const*getStatus() const;
                void setId(std::string id);
                void setEnergy(size_t const energy);
                void setStatus(std::string const &status);
                void setStatus(std::string const *status);
                Droid &operator=(Droid &d);
};

bool operator==(Droid &ld, Droid &rd);
bool operator!=(Droid &ld, Droid &rd);

Droid &operator<<(Droid &d, Droid &rd);
Droid &operator<<(Droid &d, size_t &energy);
std::ostream &operator<<(std::ostream &s, Droid const &d);

#endif
