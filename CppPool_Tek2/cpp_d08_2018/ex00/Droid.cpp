/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** droid
*/

#include <string>
#include <iostream>
#include "Droid.hpp"

//CTOR DTOR
Droid::Droid(std::string const &serial)
	:ID(serial), Energy(50), Attack(25), Toughness(15),
	Status(new std::string("Standing by"))
{
	std::cout << "Droid '" << ID << "' Activated" << std::endl;
}

Droid::Droid(Droid const &d)
	:ID(d.ID), Energy(d.Energy), Attack(d.Attack), Toughness(d.Toughness),
	Status(d.Status)
{
	std::cout << "Droid '" << ID << "' Activated, Memory Dumped"
	<< std::endl; 
}

Droid::~Droid()
{
	if (Status)
		delete Status;
	std::cout << "Droid '" << ID << "' Destroyed" << std::endl;
}

//GETTER SETTER
size_t Droid::getEnergy() const { return Energy; }

size_t Droid::getAttack() const { return Attack; }

size_t Droid::getToughness() const { return Toughness; }

std::string const *Droid::getStatus() const { return Status; }

std::string const &Droid::getId() const
{
	return (ID);
}

void Droid::setId(std::string id)
{
	ID = id;
}

void Droid::setEnergy(size_t energy)
{
	Energy = energy;
	if (Energy > 100)
		Energy = 100;
}

void Droid::setStatus(std::string const &status)
{
	if (Status)
		delete Status;
	Status = new std::string(status);
}

void Droid::setStatus(std::string const *status)
{
	if (Status)
		delete Status;
	Status = new std::string(*status);
}

//OPERATORS
std::ostream &operator<<(std::ostream &s, const Droid &d)
{
	s << "Droid '" << d.getId() << "', " << *d.getStatus()
	<< ", " << d.getEnergy();
	return s;
}

Droid &Droid::operator=(Droid &d)
{
	if (this == &d)
		return (*this);
	this->setEnergy(d.getEnergy());
	this->setId(d.getId());
	this->setStatus(d.getStatus());
	return (*this);
}

Droid &operator<<(Droid &d, size_t &energy)
{
	if (d.getEnergy() + energy > 100) {
		energy -= 50;
		d.setEnergy(50);
		return (d);
	}
	d.setEnergy(d.getEnergy() + energy);
	energy = 0;
	return (d);
}

Droid &operator<<(Droid &d, Droid &rd)
{
	if (d.getEnergy() + rd.getEnergy() > 50) {
		rd.setEnergy(rd.getEnergy() - 50 - d.getEnergy());
		d.setEnergy(50);
	}
	d.setEnergy(d.getEnergy() + rd.getEnergy());
	rd.setEnergy(0);
	return (d);
}

bool operator==(Droid &ld, Droid &rd)
{
	return (
		ld.getAttack() == rd.getAttack() &&
		ld.getToughness() == rd.getToughness() &&
		ld.getId() == rd.getId() &&
		ld.getStatus() == rd.getStatus()
	);
}

bool operator!=(Droid &ld, Droid &rd)
{
	return !(
		ld.getAttack() == rd.getAttack() &&
		ld.getToughness() == rd.getToughness() &&
		ld.getId() == rd.getId() &&
		ld.getStatus() == rd.getStatus()
	);
}