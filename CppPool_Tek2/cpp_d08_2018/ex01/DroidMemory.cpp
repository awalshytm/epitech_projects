/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** doird
*/

#include <iostream>
#include <cstdlib>
#include "DroidMemory.hpp"

DroidMemory::DroidMemory()
        :Exp(0), FingerPrint(random())
{}

std::ostream &operator<<(std::ostream &s, DroidMemory &d)
{
        s << "DroidMemory '" << d.getFingerPrint() << "', " << d.getExp();
        return (s);
}

DroidMemory &operator<<(DroidMemory &ld, DroidMemory &rd)
{
        ld.setExp(ld.getExp() + rd.getExp());
        ld.setFingerPrint(ld.getFingerPrint() xor rd.getFingerPrint());
        return (ld);
}

DroidMemory &operator>>(DroidMemory &ld, DroidMemory &rd)
{
        rd.setExp(rd.getExp() + ld.getExp());
        rd.setFingerPrint(rd.getFingerPrint() xor ld.getFingerPrint());
        return (rd);
}

DroidMemory &operator+=(DroidMemory &ld, DroidMemory &rd)
{
        ld.setExp(ld.getExp() + rd.getExp());
        ld.setFingerPrint(ld.getFingerPrint() xor rd.getFingerPrint());
        return (ld);
}

DroidMemory &operator+=(DroidMemory &ld, size_t rd)
{
        ld.setExp(ld.getExp() + rd);
        ld.setFingerPrint(ld.getFingerPrint() xor rd);
        return (ld);
}

DroidMemory &operator+(DroidMemory &ld, DroidMemory &rd)
{
        DroidMemory *d = new DroidMemory();

        d->setExp(ld.getExp() + rd.getExp());
        d->setFingerPrint(ld.getFingerPrint() + rd.getFingerPrint());
        return (*d);
}