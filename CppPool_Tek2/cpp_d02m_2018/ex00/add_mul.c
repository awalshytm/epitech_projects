/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

void add_mul_4param(int first, int second, int *sum, int *product)
{
    *sum = first + second;
    *product = first * second;
}

void add_mul_2param(int *sum, int *product)
{
    int res_s;
    int res_p;

    res_s = *sum + *product;
    res_p = *sum * *product;
    *sum = res_s;
    *product = res_p;
}
