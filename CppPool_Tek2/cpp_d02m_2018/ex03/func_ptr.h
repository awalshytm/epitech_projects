/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

#ifndef _FUNC_PTR_H_
#define _FNUC_PTR_H_

#include "func_ptr_enum.h"

void print_normal(const char *str);
void print_reverse(const char *str);
void print_upper(const char *str);
void print_42(const char *str);

void do_action(action_t action, const char *str);

#endif
