/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

#include <string.h>
#include <unistd.h>
#include "func_ptr.h"

void print_normal(const char *str)
{
    if (str == NULL)
        return;
    for (int i = 0; str[i]; i++)
        write(1, &str[i], 1);
    write(1, "\n", 1);
}

void print_reverse(const char *str)
{
    int i = 0;

    if (str == NULL)
        return;
    i = strlen(str);
    for (i -= 1; i >= 0; i--)
        write(1, &str[i], 1);
    write(1, "\n", 1);
}

void print_upper(const char *str)
{
    char c;

    if (str == NULL)
        return;
    for (int i = 0; str[i]; i++) {
        if (str[i] < 123 && str[i] > 96) {
            c = str[i] - 32;
            write(1, &c, 1);
        }
        else
            write(1, &str[i], 1);
    }
    write(1, "\n", 1);
}

void print_42(const char *str)
{
    if (str == NULL)
        return;
    write(1, "42\n", 3);
}

void do_action(action_t action, const char *str)
{
    void (*fptr[4])(const char *str) = {&print_normal, &print_reverse,
    &print_upper, &print_42};

    for (enum action_e i = PRINT_NORMAL; i <= PRINT_42; i++) {
        if (action == i)
            fptr[i](str);
    }
}
