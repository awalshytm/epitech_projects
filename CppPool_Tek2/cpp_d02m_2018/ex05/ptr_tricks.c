/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

#include "ptr_tricks.h"

int get_array_nb_elem(int *ptr1, int *ptr2)
{
    int a = ptr2 - ptr1;
    
    return (a < 0 ? -1 * a : a);
}

whatever_t *get_struct_ptr(int *member_ptr)
{
    whatever_t initial;
    whatever_t *res;

    res = (void *)member_ptr - ((void *)&initial.member - (void *)&initial);
    return (res);
}
