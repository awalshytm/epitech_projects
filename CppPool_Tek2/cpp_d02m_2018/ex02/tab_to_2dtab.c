/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

#include <stdlib.h>

static int *get_line(int *tab, int *j, int width)
{
    int *res = malloc(sizeof(int) * (width + 1));

    for (int i = 0; i < width; i++) {
        res[i] = tab[*j];
        *j += 1;
    }
    return (res);
}

void tab_to_2dtab(int *tab, int length, int width, int ***res)
{
    int j = 0;

    res[0] = malloc(sizeof(int *) * length);
    for (int i = 0; i < length; i++)
        res[0][i] = get_line(tab, &j, width);
}
