/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** mem_ptr
*/

#include <string.h>
#include <stdlib.h>
#include "mem_ptr.h"

void add_str(char *str1, char *str2, char **res)
{
    int i = 0;
    char *str = malloc(strlen(str1) + strlen(str2) + 1);

    for (i = 0; str1[i]; i++)
        str[i] = str1[i];
    for (int j = 0; str2[j]; j++)
        str[i++] = str2[j];
    str[i] = '\0';
    res[0] = str;
}

void add_str_struct(str_op_t *str_op)
{
    int i = 0;
    char *str = malloc(strlen(str_op->str1) + strlen(str_op->str2) + 1);

    for (i = 0; str_op->str1[i]; i++)
        str[i] = str_op->str1[i];
    for (int j = 0; str_op->str2[j]; j++)
        str[i++] = str_op->str2[j];
    str[i] = '\0';
    str_op->res = str;
}
