/*
** EPITECH PROJECT, 2019
** d02m
** File description:
** d02m
*/

#include <stdio.h>
#include <unistd.h>
#include "castmania.h"

void display_div(instruction_t *i)
{
    if (((division_t *)i->operation)->div_type == INTEGER)
        printf("%d\n",
        ((integer_op_t *) ((division_t *)i->operation)->div_op)->res);
    else if (((division_t *)i->operation)->div_type == DECIMALE)
        printf("%f\n",
        ((decimale_op_t *) ((division_t *)i->operation)->div_op)->res);
}

void exec_operation(instruction_type_t instruction_type, void *data)
{
    instruction_t *ins = (instruction_t *)data;

    if (instruction_type == ADD_OPERATION) {
        exec_add(ins->operation);
        ins->output_type == VERBOSE ? printf("%d\n",
        ((addition_t *)ins->operation)->add_op.res) : write(1, "", 0);
    } else if (instruction_type == DIV_OPERATION) {
        exec_div(ins->operation);
        if (ins->output_type == VERBOSE)
            display_div(ins);
    }
}

void exec_instruction(instruction_type_t instruction_type, void *data)
{
    switch(instruction_type){
    case (PRINT_INT):
        printf("%d\n", (int)*((int *)data));
        break;
    case (PRINT_FLOAT):
        printf("%f\n", (float)*((float *)data));
        break;
    default:
        exec_operation(instruction_type, data);
        break;
    }
}
