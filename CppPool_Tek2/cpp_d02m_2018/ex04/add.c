/*
** EPITECH PROJECT, 2019
** cpp pool
** File description:
** d02m
*/

#include <stdio.h>
#include <stdlib.h>
#include "castmania.h"

int normal_add(int a, int b)
{
    return (a + b);
}

int absolute_add(int a, int b)
{
    int res = 0;

    res = abs(a) + abs(b);
    return (res);
}

void exec_add(addition_t *operation)
{
    if (operation->add_type == NORMAL) {
        operation->add_op.res = normal_add(operation->add_op.a, operation->add_op.b);
    } else if (operation->add_type == ABSOLUTE){
        operation->add_op.res = absolute_add(operation->add_op.a, operation->add_op.b);
    }
}
